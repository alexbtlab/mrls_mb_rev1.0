1. Добавить на bd QSPI
2. Открываем Synthesis Device
3. Tool -> Edit Device Properties 
4. Вкладка Configuration -> Configuration Rate 66 MHz
5. Вкладка Configuration Mode -> SPIx4
6. Сохраняем constrains.xdc
7. Жмем Generate BitStream
8. Создать в SDK программу SPECBootLoader
9. В blconfig.h устанавливаем адрес #define FLASH_IMAGE_BASEADDR  0x003D0900
10. Коментируем #define VERBOSE для быстрой работы загрузчика
11. В настройках BSP SPECBootLoader необходимо установить значение "Device family" = 5 (для Spansion)
12. В vivado Tool -> Associate ELF file -> выбрать SPECBootLoader.elf  
13. Жмем Generate BitStream
14. File -> Export -> ExportHardware (set include BitStream)
15. В SDK Жмем Programm FPGA
16. Вместо bootLoop устанавливаем SPECBootLoader.elf 
17. Программируем FPGA -> получаем файл downLoad.bit
18. В SDK:
$ cd C:/project/mrls_mb_rev2.1/mrls_mb_rev2.1.sdk/mrls/Debug
$ exec mb-objcopy -O srec mrls.elf mrls_srec.srec
19. Копируем из C:\project\mrls_mb_rev2.1\mrls_mb_rev2.1.sdk\mrls\Debug файл mrls_srec.srec 
             в  C:\project\mrls_mb_rev2.1\mrls_mb_rev2.1.sdk\design_1_wrapper_hw_platform_0, затем в Vivado
20. В Vivado:
$ cd C:/project/mrls_mb_rev2.1/mrls_mb_rev2.1.sdk/design_1_wrapper_hw_platform_0
$ write_cfgmem -force -format MCS -size 16 -interface SPIx4 -loadbit " up 0 download.bit" -loaddata " up 0x003D0900 mrls_srec.srec " boot.mcs
21. Получаем файл .msc