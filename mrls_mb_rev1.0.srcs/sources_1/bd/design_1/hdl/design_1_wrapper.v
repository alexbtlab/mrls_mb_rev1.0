//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
//Date        : Mon Mar 22 13:35:54 2021
//Host        : zl-04 running 64-bit major release  (build 9200)
//Command     : generate_target design_1_wrapper.bd
//Design      : design_1_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_wrapper
   (DDR3_addr,
    DDR3_ba,
    DDR3_cas_n,
    DDR3_ck_n,
    DDR3_ck_p,
    DDR3_cke,
    DDR3_cs_n,
    DDR3_dm,
    DDR3_dq,
    DDR3_dqs_n,
    DDR3_dqs_p,
    DDR3_odt,
    DDR3_ras_n,
    DDR3_reset_n,
    DDR3_we_n,
    LED,
    PAMP_EN_FPGA,
    SD_pin10_io,
    SD_pin1_io,
    SD_pin2_io,
    SD_pin3_io,
    SD_pin4_io,
    SD_pin7_io,
    SD_pin8_io,
    SD_pin9_io,
    UART_rxd,
    UART_txd,
    mdio_mdc,
    mdio_mdio_io,
    rmii_crs_dv,
    rmii_rx_er,
    rmii_rxd,
    rmii_tx_en,
    rmii_txd,
    spi_rtl_io0_io,
    spi_rtl_io1_io,
    spi_rtl_io2_io,
    spi_rtl_io3_io,
    spi_rtl_ss_io,
    sys_clk);
  output [13:0]DDR3_addr;
  output [2:0]DDR3_ba;
  output DDR3_cas_n;
  output [0:0]DDR3_ck_n;
  output [0:0]DDR3_ck_p;
  output [0:0]DDR3_cke;
  output [0:0]DDR3_cs_n;
  output [1:0]DDR3_dm;
  inout [15:0]DDR3_dq;
  inout [1:0]DDR3_dqs_n;
  inout [1:0]DDR3_dqs_p;
  output [0:0]DDR3_odt;
  output DDR3_ras_n;
  output DDR3_reset_n;
  output DDR3_we_n;
  output LED;
  output PAMP_EN_FPGA;
  inout SD_pin10_io;
  inout SD_pin1_io;
  inout SD_pin2_io;
  inout SD_pin3_io;
  inout SD_pin4_io;
  inout SD_pin7_io;
  inout SD_pin8_io;
  inout SD_pin9_io;
  input UART_rxd;
  output UART_txd;
  output mdio_mdc;
  inout mdio_mdio_io;
  input rmii_crs_dv;
  input rmii_rx_er;
  input [1:0]rmii_rxd;
  output rmii_tx_en;
  output [1:0]rmii_txd;
  inout spi_rtl_io0_io;
  inout spi_rtl_io1_io;
  inout spi_rtl_io2_io;
  inout spi_rtl_io3_io;
  inout [0:0]spi_rtl_ss_io;
  input sys_clk;

  wire [13:0]DDR3_addr;
  wire [2:0]DDR3_ba;
  wire DDR3_cas_n;
  wire [0:0]DDR3_ck_n;
  wire [0:0]DDR3_ck_p;
  wire [0:0]DDR3_cke;
  wire [0:0]DDR3_cs_n;
  wire [1:0]DDR3_dm;
  wire [15:0]DDR3_dq;
  wire [1:0]DDR3_dqs_n;
  wire [1:0]DDR3_dqs_p;
  wire [0:0]DDR3_odt;
  wire DDR3_ras_n;
  wire DDR3_reset_n;
  wire DDR3_we_n;
  wire LED;
  wire PAMP_EN_FPGA;
  wire SD_pin10_i;
  wire SD_pin10_io;
  wire SD_pin10_o;
  wire SD_pin10_t;
  wire SD_pin1_i;
  wire SD_pin1_io;
  wire SD_pin1_o;
  wire SD_pin1_t;
  wire SD_pin2_i;
  wire SD_pin2_io;
  wire SD_pin2_o;
  wire SD_pin2_t;
  wire SD_pin3_i;
  wire SD_pin3_io;
  wire SD_pin3_o;
  wire SD_pin3_t;
  wire SD_pin4_i;
  wire SD_pin4_io;
  wire SD_pin4_o;
  wire SD_pin4_t;
  wire SD_pin7_i;
  wire SD_pin7_io;
  wire SD_pin7_o;
  wire SD_pin7_t;
  wire SD_pin8_i;
  wire SD_pin8_io;
  wire SD_pin8_o;
  wire SD_pin8_t;
  wire SD_pin9_i;
  wire SD_pin9_io;
  wire SD_pin9_o;
  wire SD_pin9_t;
  wire UART_rxd;
  wire UART_txd;
  wire mdio_mdc;
  wire mdio_mdio_i;
  wire mdio_mdio_io;
  wire mdio_mdio_o;
  wire mdio_mdio_t;
  wire rmii_crs_dv;
  wire rmii_rx_er;
  wire [1:0]rmii_rxd;
  wire rmii_tx_en;
  wire [1:0]rmii_txd;
  wire spi_rtl_io0_i;
  wire spi_rtl_io0_io;
  wire spi_rtl_io0_o;
  wire spi_rtl_io0_t;
  wire spi_rtl_io1_i;
  wire spi_rtl_io1_io;
  wire spi_rtl_io1_o;
  wire spi_rtl_io1_t;
  wire spi_rtl_io2_i;
  wire spi_rtl_io2_io;
  wire spi_rtl_io2_o;
  wire spi_rtl_io2_t;
  wire spi_rtl_io3_i;
  wire spi_rtl_io3_io;
  wire spi_rtl_io3_o;
  wire spi_rtl_io3_t;
  wire [0:0]spi_rtl_ss_i_0;
  wire [0:0]spi_rtl_ss_io_0;
  wire [0:0]spi_rtl_ss_o_0;
  wire spi_rtl_ss_t;
  wire sys_clk;

  IOBUF SD_pin10_iobuf
       (.I(SD_pin10_o),
        .IO(SD_pin10_io),
        .O(SD_pin10_i),
        .T(SD_pin10_t));
  IOBUF SD_pin1_iobuf
       (.I(SD_pin1_o),
        .IO(SD_pin1_io),
        .O(SD_pin1_i),
        .T(SD_pin1_t));
  IOBUF SD_pin2_iobuf
       (.I(SD_pin2_o),
        .IO(SD_pin2_io),
        .O(SD_pin2_i),
        .T(SD_pin2_t));
  IOBUF SD_pin3_iobuf
       (.I(SD_pin3_o),
        .IO(SD_pin3_io),
        .O(SD_pin3_i),
        .T(SD_pin3_t));
  IOBUF SD_pin4_iobuf
       (.I(SD_pin4_o),
        .IO(SD_pin4_io),
        .O(SD_pin4_i),
        .T(SD_pin4_t));
  IOBUF SD_pin7_iobuf
       (.I(SD_pin7_o),
        .IO(SD_pin7_io),
        .O(SD_pin7_i),
        .T(SD_pin7_t));
  IOBUF SD_pin8_iobuf
       (.I(SD_pin8_o),
        .IO(SD_pin8_io),
        .O(SD_pin8_i),
        .T(SD_pin8_t));
  IOBUF SD_pin9_iobuf
       (.I(SD_pin9_o),
        .IO(SD_pin9_io),
        .O(SD_pin9_i),
        .T(SD_pin9_t));
  design_1 design_1_i
       (.DDR3_addr(DDR3_addr),
        .DDR3_ba(DDR3_ba),
        .DDR3_cas_n(DDR3_cas_n),
        .DDR3_ck_n(DDR3_ck_n),
        .DDR3_ck_p(DDR3_ck_p),
        .DDR3_cke(DDR3_cke),
        .DDR3_cs_n(DDR3_cs_n),
        .DDR3_dm(DDR3_dm),
        .DDR3_dq(DDR3_dq),
        .DDR3_dqs_n(DDR3_dqs_n),
        .DDR3_dqs_p(DDR3_dqs_p),
        .DDR3_odt(DDR3_odt),
        .DDR3_ras_n(DDR3_ras_n),
        .DDR3_reset_n(DDR3_reset_n),
        .DDR3_we_n(DDR3_we_n),
        .LED(LED),
        .PAMP_EN_FPGA(PAMP_EN_FPGA),
        .SD_pin10_i(SD_pin10_i),
        .SD_pin10_o(SD_pin10_o),
        .SD_pin10_t(SD_pin10_t),
        .SD_pin1_i(SD_pin1_i),
        .SD_pin1_o(SD_pin1_o),
        .SD_pin1_t(SD_pin1_t),
        .SD_pin2_i(SD_pin2_i),
        .SD_pin2_o(SD_pin2_o),
        .SD_pin2_t(SD_pin2_t),
        .SD_pin3_i(SD_pin3_i),
        .SD_pin3_o(SD_pin3_o),
        .SD_pin3_t(SD_pin3_t),
        .SD_pin4_i(SD_pin4_i),
        .SD_pin4_o(SD_pin4_o),
        .SD_pin4_t(SD_pin4_t),
        .SD_pin7_i(SD_pin7_i),
        .SD_pin7_o(SD_pin7_o),
        .SD_pin7_t(SD_pin7_t),
        .SD_pin8_i(SD_pin8_i),
        .SD_pin8_o(SD_pin8_o),
        .SD_pin8_t(SD_pin8_t),
        .SD_pin9_i(SD_pin9_i),
        .SD_pin9_o(SD_pin9_o),
        .SD_pin9_t(SD_pin9_t),
        .UART_rxd(UART_rxd),
        .UART_txd(UART_txd),
        .mdio_mdc(mdio_mdc),
        .mdio_mdio_i(mdio_mdio_i),
        .mdio_mdio_o(mdio_mdio_o),
        .mdio_mdio_t(mdio_mdio_t),
        .rmii_crs_dv(rmii_crs_dv),
        .rmii_rx_er(rmii_rx_er),
        .rmii_rxd(rmii_rxd),
        .rmii_tx_en(rmii_tx_en),
        .rmii_txd(rmii_txd),
        .spi_rtl_io0_i(spi_rtl_io0_i),
        .spi_rtl_io0_o(spi_rtl_io0_o),
        .spi_rtl_io0_t(spi_rtl_io0_t),
        .spi_rtl_io1_i(spi_rtl_io1_i),
        .spi_rtl_io1_o(spi_rtl_io1_o),
        .spi_rtl_io1_t(spi_rtl_io1_t),
        .spi_rtl_io2_i(spi_rtl_io2_i),
        .spi_rtl_io2_o(spi_rtl_io2_o),
        .spi_rtl_io2_t(spi_rtl_io2_t),
        .spi_rtl_io3_i(spi_rtl_io3_i),
        .spi_rtl_io3_o(spi_rtl_io3_o),
        .spi_rtl_io3_t(spi_rtl_io3_t),
        .spi_rtl_ss_i(spi_rtl_ss_i_0),
        .spi_rtl_ss_o(spi_rtl_ss_o_0),
        .spi_rtl_ss_t(spi_rtl_ss_t),
        .sys_clk(sys_clk));
  IOBUF mdio_mdio_iobuf
       (.I(mdio_mdio_o),
        .IO(mdio_mdio_io),
        .O(mdio_mdio_i),
        .T(mdio_mdio_t));
  IOBUF spi_rtl_io0_iobuf
       (.I(spi_rtl_io0_o),
        .IO(spi_rtl_io0_io),
        .O(spi_rtl_io0_i),
        .T(spi_rtl_io0_t));
  IOBUF spi_rtl_io1_iobuf
       (.I(spi_rtl_io1_o),
        .IO(spi_rtl_io1_io),
        .O(spi_rtl_io1_i),
        .T(spi_rtl_io1_t));
  IOBUF spi_rtl_io2_iobuf
       (.I(spi_rtl_io2_o),
        .IO(spi_rtl_io2_io),
        .O(spi_rtl_io2_i),
        .T(spi_rtl_io2_t));
  IOBUF spi_rtl_io3_iobuf
       (.I(spi_rtl_io3_o),
        .IO(spi_rtl_io3_io),
        .O(spi_rtl_io3_i),
        .T(spi_rtl_io3_t));
  IOBUF spi_rtl_ss_iobuf_0
       (.I(spi_rtl_ss_o_0),
        .IO(spi_rtl_ss_io[0]),
        .O(spi_rtl_ss_i_0),
        .T(spi_rtl_ss_t));
endmodule
