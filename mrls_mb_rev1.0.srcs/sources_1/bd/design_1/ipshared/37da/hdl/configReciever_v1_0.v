
`timescale 1 ns / 1 ps

module configReciever_v1_0 #
	(
		parameter integer C_S00_AXI_DATA_WIDTH	= 32,
		parameter integer C_S00_AXI_ADDR_WIDTH	= 6
	)
	(
		output reg         PAMP_EN_FPGA,             
		output reg [ 5:0 ] ADC_MODE_PDn,
		output wire        PLL_ROW_EN_FPGA,           
		output reg [ 1:0 ] ETH_REF_CLK_EN_and_RST,    
		output reg [ 2:0 ] IF1_CH1_AMP123,           
		output reg [ 2:0 ] IF2_CH1_AMP123,           
		output reg [ 3:0 ] SW_IN_IF1_IF2,               
		output reg [ 3:0 ] SW_OUT_CH1_IF1_IF2,        
		output reg [ 3:0 ] SW12_IF12_CH1,             
		// Ports of Axi Slave Bus Interface S00_AXI
		input wire  s00_axi_aclk,
		input wire  s00_axi_aresetn,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_awaddr,
		input wire [2 : 0] s00_axi_awprot,
		input wire  s00_axi_awvalid,
		output wire  s00_axi_awready,
		input wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_wdata,
		input wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb,
		input wire  s00_axi_wvalid,
		output wire  s00_axi_wready,
		output wire [1 : 0] s00_axi_bresp,
		output wire  s00_axi_bvalid,
		input wire  s00_axi_bready,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_araddr,
		input wire [2 : 0] s00_axi_arprot,
		input wire  s00_axi_arvalid,
		output wire  s00_axi_arready,
		output wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_rdata,
		output wire [1 : 0] s00_axi_rresp,
		output wire  s00_axi_rvalid,
		input wire  s00_axi_rready
	);
	     wire [32-1:0]	slv_reg0;
         wire [32-1:0]	slv_reg1;
         wire [32-1:0]	slv_reg2;
         wire [32-1:0]	slv_reg3;
         wire [32-1:0]	slv_reg4;
         wire [32-1:0]	slv_reg5;
         wire [32-1:0]	slv_reg6;
         wire [32-1:0]	slv_reg7;
        
         wire [32-1:0]	ip2mb_reg0;
         wire [32-1:0]	ip2mb_reg1;
         wire [32-1:0]	ip2mb_reg2;
         wire [32-1:0]	ip2mb_reg3;
         wire [32-1:0]	ip2mb_reg4;
         wire [32-1:0]	ip2mb_reg5;
         wire [32-1:0]	ip2mb_reg6;
         wire [32-1:0]	ip2mb_reg7;
         
// Instantiation of Axi Bus Interface S00_AXI
	configReciever_v1_0_S00_AXI # ( 
		.C_S_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
	) configReciever_v1_0_S00_AXI_inst (
		.S_AXI_ACLK(s00_axi_aclk),
		.S_AXI_ARESETN(s00_axi_aresetn),
		.S_AXI_AWADDR(s00_axi_awaddr),
		.S_AXI_AWPROT(s00_axi_awprot),
		.S_AXI_AWVALID(s00_axi_awvalid),
		.S_AXI_AWREADY(s00_axi_awready),
		.S_AXI_WDATA(s00_axi_wdata),
		.S_AXI_WSTRB(s00_axi_wstrb),
		.S_AXI_WVALID(s00_axi_wvalid),
		.S_AXI_WREADY(s00_axi_wready),
		.S_AXI_BRESP(s00_axi_bresp),
		.S_AXI_BVALID(s00_axi_bvalid),
		.S_AXI_BREADY(s00_axi_bready),
		.S_AXI_ARADDR(s00_axi_araddr),
		.S_AXI_ARPROT(s00_axi_arprot),
		.S_AXI_ARVALID(s00_axi_arvalid),
		.S_AXI_ARREADY(s00_axi_arready),
		.S_AXI_RDATA(s00_axi_rdata),
		.S_AXI_RRESP(s00_axi_rresp),
		.S_AXI_RVALID(s00_axi_rvalid),
		.S_AXI_RREADY(s00_axi_rready),
		
	    .slv_reg0(slv_reg0),
		.slv_reg1(slv_reg1),
		.slv_reg2(slv_reg2),
		.slv_reg3(slv_reg3),
		.slv_reg4(slv_reg4),
		.slv_reg5(slv_reg5),
		.slv_reg6(slv_reg6),
		.slv_reg7(slv_reg7),
		.ip2mb_reg0(ip2mb_reg0),
		.ip2mb_reg1(ip2mb_reg1),
		.ip2mb_reg2(ip2mb_reg2),
		.ip2mb_reg3(ip2mb_reg3),
		.ip2mb_reg4(ip2mb_reg4),  
	    .ip2mb_reg5(ip2mb_reg5),
		.ip2mb_reg6(ip2mb_reg6),
		.ip2mb_reg7(ip2mb_reg7) 
	);
    assign PLL_ROW_EN_FPGA = 1'b1;

    always @(posedge s00_axi_aclk) begin    // ���� ������� �� ����������� � ������ ��������
        if(s00_axi_aresetn) begin
            /*---------------------------------------------------------------------------------------*/        
            if(slv_reg2[0] == 1'b1)     ADC_MODE_PDn[0] <= 1'b1;    // ADC_EN   IF1_CH1
            else                        ADC_MODE_PDn[0] <= 1'b0;    // ADC_EN   IF1_CH1
            if(slv_reg2[1] == 1'b1)     ADC_MODE_PDn[1] <= 1'b1;    // ADC_EN   IF1_CH2
            else                        ADC_MODE_PDn[1] <= 1'b0;    // ADC_EN   IF1_CH2
            if(slv_reg2[2] == 1'b1)     ADC_MODE_PDn[2] <= 1'b1;    // ADC_EN   IF2_CH1
            else                        ADC_MODE_PDn[2] <= 1'b0;    // ADC_EN   IF2_CH1
            if(slv_reg2[3] == 1'b1)     ADC_MODE_PDn[3] <= 1'b1;    // ADC_EN   IF2_CH2
            else                        ADC_MODE_PDn[3] <= 1'b0;    // ADC_EN   IF2_CH2
            if(slv_reg2[4] == 1'b1)     ADC_MODE_PDn[4] <= 1'b1;    // TWO_LANES
            else                        ADC_MODE_PDn[4] <= 1'b0;    // TWO_LANES
            if(slv_reg2[5] == 1'b1)     ADC_MODE_PDn[5] <= 1'b1;    // TEST_PAT
            else                        ADC_MODE_PDn[5] <= 1'b0;    // TEST_PAT
            /*---------------------------------------------------------------------------------------*/
            if(slv_reg3[0] == 1'b1)     PAMP_EN_FPGA <= 1;                  // ���� ���� ������ �� �� ��������� ��������� ��������� 
            else                        PAMP_EN_FPGA <= 0; 
            /*---------------------------------------------------------------------------------------*/
            if(slv_reg3[1] == 1'b1)     ETH_REF_CLK_EN_and_RST <= 2'b11;    // ���� ���� ������ �� �� ��������� ������ LAN8720 
            else                        ETH_REF_CLK_EN_and_RST <= 2'b00;
            /*---------------------------------------------------------------------------------------*/
            if(slv_reg4[1:0] == 2'b00) begin     // ��������� ��������� ���������
            /*[2:0] L13 k16 L14*/       IF1_CH1_AMP123      <= 3'b000;
            /*[2:0] L13 k16 L14*/       IF2_CH1_AMP123      <= 3'b000;
            /*[3:0] F15 F16 C15 F18*/   SW_IN_IF1_IF2       <= 4'b0100;
            /*[3:0] AB2 AB1 K13 k14*/   SW_OUT_CH1_IF1_IF2  <= 4'b1100; 
            /*[3:0] J16 L15 T5 R6*/     SW12_IF12_CH1       <= 4'b1111;
            end
            if(slv_reg4[1:0] == 2'b01) begin    // 1-� ��������� ��������� �������
                IF1_CH1_AMP123      <= 3'b001;
                IF2_CH1_AMP123      <= 3'b001;
                SW_IN_IF1_IF2       <= 4'b1110;  // b1101
                SW_OUT_CH1_IF1_IF2  <= 4'b1011; 
                SW12_IF12_CH1       <= 4'b0101;
            end
            if(slv_reg4[1:0] == 2'b10) begin    // 2 ��������� ��������� ��������
                IF1_CH1_AMP123      <= 3'b011;
                IF2_CH1_AMP123      <= 3'b011;
                SW_IN_IF1_IF2       <= 4'b1110;
                SW_OUT_CH1_IF1_IF2  <= 4'b0110;
                SW12_IF12_CH1       <= 4'b1010;
            end
            if(slv_reg4[1:0] == 2'b11) begin    // 3 ��������� ��������� ��������
                IF1_CH1_AMP123      <= 3'b111;
                IF2_CH1_AMP123      <= 3'b111;
                SW_IN_IF1_IF2       <= 4'b1110;
                SW_OUT_CH1_IF1_IF2  <= 4'b0001;
                SW12_IF12_CH1       <= 4'b0000;
            end
            /*------------------------------------------------------------------------------*/
        end
    end
endmodule
