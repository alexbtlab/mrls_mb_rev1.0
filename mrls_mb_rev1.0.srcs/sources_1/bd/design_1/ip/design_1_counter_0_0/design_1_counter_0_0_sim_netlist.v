// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Fri Mar 19 16:34:09 2021
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               c:/project/mrls_mb_rev1.0/mrls_mb_rev1.0.srcs/sources_1/bd/design_1/ip/design_1_counter_0_0/design_1_counter_0_0_sim_netlist.v
// Design      : design_1_counter_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_counter_0_0,counter,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "counter,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module design_1_counter_0_0
   (clk,
    reset,
    FrameSize,
    ready,
    count,
    div_clk);
  input clk;
  input reset;
  input [15:0]FrameSize;
  output ready;
  output [15:0]count;
  output div_clk;

  wire [15:0]FrameSize;
  wire clk;
  wire [15:0]count;
  wire reset;

  assign div_clk = count[13];
  assign ready = clk;
  design_1_counter_0_0_counter inst
       (.FrameSize(FrameSize),
        .Q(count),
        .clk(clk),
        .reset(reset));
endmodule

(* ORIG_REF_NAME = "counter" *) 
module design_1_counter_0_0_counter
   (Q,
    FrameSize,
    reset,
    clk);
  output [15:0]Q;
  input [15:0]FrameSize;
  input reset;
  input clk;

  wire [15:0]FrameSize;
  wire [15:0]Q;
  wire clk;
  wire [15:1]cnt0;
  wire cnt0_carry__0_i_1_n_0;
  wire cnt0_carry__0_i_2_n_0;
  wire cnt0_carry__0_i_3_n_0;
  wire cnt0_carry__0_i_4_n_0;
  wire cnt0_carry__0_n_0;
  wire cnt0_carry__0_n_1;
  wire cnt0_carry__0_n_2;
  wire cnt0_carry__0_n_3;
  wire cnt0_carry__1_i_1_n_0;
  wire cnt0_carry__1_i_2_n_0;
  wire cnt0_carry__1_i_3_n_0;
  wire cnt0_carry__1_i_4_n_0;
  wire cnt0_carry__1_n_0;
  wire cnt0_carry__1_n_1;
  wire cnt0_carry__1_n_2;
  wire cnt0_carry__1_n_3;
  wire cnt0_carry__2_i_1_n_0;
  wire cnt0_carry__2_i_2_n_0;
  wire cnt0_carry__2_i_3_n_0;
  wire cnt0_carry__2_n_2;
  wire cnt0_carry__2_n_3;
  wire cnt0_carry_i_1_n_0;
  wire cnt0_carry_i_2_n_0;
  wire cnt0_carry_i_3_n_0;
  wire cnt0_carry_i_4_n_0;
  wire cnt0_carry_n_0;
  wire cnt0_carry_n_1;
  wire cnt0_carry_n_2;
  wire cnt0_carry_n_3;
  wire cnt1;
  wire cnt1_carry__0_i_1_n_0;
  wire cnt1_carry__0_i_2_n_0;
  wire cnt1_carry__0_n_3;
  wire cnt1_carry_i_1_n_0;
  wire cnt1_carry_i_2_n_0;
  wire cnt1_carry_i_3_n_0;
  wire cnt1_carry_i_4_n_0;
  wire cnt1_carry_n_0;
  wire cnt1_carry_n_1;
  wire cnt1_carry_n_2;
  wire cnt1_carry_n_3;
  wire \cnt[0]_i_1_n_0 ;
  wire \cnt[0]_i_2_n_0 ;
  wire \cnt[0]_i_3_n_0 ;
  wire \cnt[0]_i_4_n_0 ;
  wire \cnt[10]_i_1_n_0 ;
  wire \cnt[11]_i_1_n_0 ;
  wire \cnt[12]_i_1_n_0 ;
  wire \cnt[13]_i_1_n_0 ;
  wire \cnt[14]_i_1_n_0 ;
  wire \cnt[15]_i_1_n_0 ;
  wire \cnt[15]_i_2_n_0 ;
  wire \cnt[15]_i_3_n_0 ;
  wire \cnt[15]_i_4_n_0 ;
  wire \cnt[15]_i_6_n_0 ;
  wire \cnt[1]_i_1_n_0 ;
  wire \cnt[2]_i_1_n_0 ;
  wire \cnt[3]_i_1_n_0 ;
  wire \cnt[4]_i_1_n_0 ;
  wire \cnt[5]_i_1_n_0 ;
  wire \cnt[6]_i_1_n_0 ;
  wire \cnt[7]_i_1_n_0 ;
  wire \cnt[8]_i_1_n_0 ;
  wire \cnt[9]_i_1_n_0 ;
  wire \cnt_reg[12]_i_2_n_0 ;
  wire \cnt_reg[12]_i_2_n_1 ;
  wire \cnt_reg[12]_i_2_n_2 ;
  wire \cnt_reg[12]_i_2_n_3 ;
  wire \cnt_reg[15]_i_5_n_2 ;
  wire \cnt_reg[15]_i_5_n_3 ;
  wire \cnt_reg[4]_i_2_n_0 ;
  wire \cnt_reg[4]_i_2_n_1 ;
  wire \cnt_reg[4]_i_2_n_2 ;
  wire \cnt_reg[4]_i_2_n_3 ;
  wire \cnt_reg[8]_i_2_n_0 ;
  wire \cnt_reg[8]_i_2_n_1 ;
  wire \cnt_reg[8]_i_2_n_2 ;
  wire \cnt_reg[8]_i_2_n_3 ;
  wire [15:1]data0;
  wire direct;
  wire p_0_in;
  wire reset;
  wire [3:2]NLW_cnt0_carry__2_CO_UNCONNECTED;
  wire [3:3]NLW_cnt0_carry__2_O_UNCONNECTED;
  wire [3:0]NLW_cnt1_carry_O_UNCONNECTED;
  wire [3:2]NLW_cnt1_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_cnt1_carry__0_O_UNCONNECTED;
  wire [3:2]\NLW_cnt_reg[15]_i_5_CO_UNCONNECTED ;
  wire [3:3]\NLW_cnt_reg[15]_i_5_O_UNCONNECTED ;

  CARRY4 cnt0_carry
       (.CI(1'b0),
        .CO({cnt0_carry_n_0,cnt0_carry_n_1,cnt0_carry_n_2,cnt0_carry_n_3}),
        .CYINIT(Q[0]),
        .DI(Q[4:1]),
        .O(data0[4:1]),
        .S({cnt0_carry_i_1_n_0,cnt0_carry_i_2_n_0,cnt0_carry_i_3_n_0,cnt0_carry_i_4_n_0}));
  CARRY4 cnt0_carry__0
       (.CI(cnt0_carry_n_0),
        .CO({cnt0_carry__0_n_0,cnt0_carry__0_n_1,cnt0_carry__0_n_2,cnt0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(Q[8:5]),
        .O(data0[8:5]),
        .S({cnt0_carry__0_i_1_n_0,cnt0_carry__0_i_2_n_0,cnt0_carry__0_i_3_n_0,cnt0_carry__0_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry__0_i_1
       (.I0(Q[8]),
        .O(cnt0_carry__0_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry__0_i_2
       (.I0(Q[7]),
        .O(cnt0_carry__0_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry__0_i_3
       (.I0(Q[6]),
        .O(cnt0_carry__0_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry__0_i_4
       (.I0(Q[5]),
        .O(cnt0_carry__0_i_4_n_0));
  CARRY4 cnt0_carry__1
       (.CI(cnt0_carry__0_n_0),
        .CO({cnt0_carry__1_n_0,cnt0_carry__1_n_1,cnt0_carry__1_n_2,cnt0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(Q[12:9]),
        .O(data0[12:9]),
        .S({cnt0_carry__1_i_1_n_0,cnt0_carry__1_i_2_n_0,cnt0_carry__1_i_3_n_0,cnt0_carry__1_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry__1_i_1
       (.I0(Q[12]),
        .O(cnt0_carry__1_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry__1_i_2
       (.I0(Q[11]),
        .O(cnt0_carry__1_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry__1_i_3
       (.I0(Q[10]),
        .O(cnt0_carry__1_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry__1_i_4
       (.I0(Q[9]),
        .O(cnt0_carry__1_i_4_n_0));
  CARRY4 cnt0_carry__2
       (.CI(cnt0_carry__1_n_0),
        .CO({NLW_cnt0_carry__2_CO_UNCONNECTED[3:2],cnt0_carry__2_n_2,cnt0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,Q[14:13]}),
        .O({NLW_cnt0_carry__2_O_UNCONNECTED[3],data0[15:13]}),
        .S({1'b0,cnt0_carry__2_i_1_n_0,cnt0_carry__2_i_2_n_0,cnt0_carry__2_i_3_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry__2_i_1
       (.I0(Q[15]),
        .O(cnt0_carry__2_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry__2_i_2
       (.I0(Q[14]),
        .O(cnt0_carry__2_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry__2_i_3
       (.I0(Q[13]),
        .O(cnt0_carry__2_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry_i_1
       (.I0(Q[4]),
        .O(cnt0_carry_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry_i_2
       (.I0(Q[3]),
        .O(cnt0_carry_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry_i_3
       (.I0(Q[2]),
        .O(cnt0_carry_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry_i_4
       (.I0(Q[1]),
        .O(cnt0_carry_i_4_n_0));
  CARRY4 cnt1_carry
       (.CI(1'b0),
        .CO({cnt1_carry_n_0,cnt1_carry_n_1,cnt1_carry_n_2,cnt1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_cnt1_carry_O_UNCONNECTED[3:0]),
        .S({cnt1_carry_i_1_n_0,cnt1_carry_i_2_n_0,cnt1_carry_i_3_n_0,cnt1_carry_i_4_n_0}));
  CARRY4 cnt1_carry__0
       (.CI(cnt1_carry_n_0),
        .CO({NLW_cnt1_carry__0_CO_UNCONNECTED[3:2],cnt1,cnt1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_cnt1_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,cnt1_carry__0_i_1_n_0,cnt1_carry__0_i_2_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    cnt1_carry__0_i_1
       (.I0(FrameSize[15]),
        .I1(Q[15]),
        .O(cnt1_carry__0_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    cnt1_carry__0_i_2
       (.I0(FrameSize[14]),
        .I1(Q[14]),
        .I2(FrameSize[13]),
        .I3(Q[13]),
        .I4(Q[12]),
        .I5(FrameSize[12]),
        .O(cnt1_carry__0_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    cnt1_carry_i_1
       (.I0(FrameSize[11]),
        .I1(Q[11]),
        .I2(FrameSize[10]),
        .I3(Q[10]),
        .I4(Q[9]),
        .I5(FrameSize[9]),
        .O(cnt1_carry_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    cnt1_carry_i_2
       (.I0(FrameSize[8]),
        .I1(Q[8]),
        .I2(FrameSize[7]),
        .I3(Q[7]),
        .I4(Q[6]),
        .I5(FrameSize[6]),
        .O(cnt1_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    cnt1_carry_i_3
       (.I0(FrameSize[5]),
        .I1(Q[5]),
        .I2(FrameSize[4]),
        .I3(Q[4]),
        .I4(Q[3]),
        .I5(FrameSize[3]),
        .O(cnt1_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h8400008421000021)) 
    cnt1_carry_i_4
       (.I0(FrameSize[2]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(FrameSize[0]),
        .I5(FrameSize[1]),
        .O(cnt1_carry_i_4_n_0));
  LUT6 #(
    .INIT(64'h8800CCCCDDF0CCCC)) 
    \cnt[0]_i_1 
       (.I0(cnt1),
        .I1(FrameSize[0]),
        .I2(\cnt[0]_i_2_n_0 ),
        .I3(direct),
        .I4(reset),
        .I5(Q[0]),
        .O(\cnt[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \cnt[0]_i_2 
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(Q[15]),
        .I3(\cnt[0]_i_3_n_0 ),
        .I4(\cnt[0]_i_4_n_0 ),
        .O(\cnt[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \cnt[0]_i_3 
       (.I0(Q[9]),
        .I1(Q[10]),
        .I2(Q[11]),
        .I3(Q[12]),
        .I4(Q[13]),
        .I5(Q[14]),
        .O(\cnt[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \cnt[0]_i_4 
       (.I0(Q[3]),
        .I1(Q[4]),
        .I2(Q[5]),
        .I3(Q[6]),
        .I4(Q[7]),
        .I5(Q[8]),
        .O(\cnt[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \cnt[10]_i_1 
       (.I0(\cnt[15]_i_3_n_0 ),
        .I1(FrameSize[10]),
        .I2(\cnt[15]_i_4_n_0 ),
        .I3(cnt0[10]),
        .I4(data0[10]),
        .I5(\cnt[15]_i_6_n_0 ),
        .O(\cnt[10]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \cnt[11]_i_1 
       (.I0(\cnt[15]_i_3_n_0 ),
        .I1(FrameSize[11]),
        .I2(\cnt[15]_i_4_n_0 ),
        .I3(cnt0[11]),
        .I4(data0[11]),
        .I5(\cnt[15]_i_6_n_0 ),
        .O(\cnt[11]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \cnt[12]_i_1 
       (.I0(\cnt[15]_i_3_n_0 ),
        .I1(FrameSize[12]),
        .I2(\cnt[15]_i_4_n_0 ),
        .I3(cnt0[12]),
        .I4(data0[12]),
        .I5(\cnt[15]_i_6_n_0 ),
        .O(\cnt[12]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \cnt[13]_i_1 
       (.I0(\cnt[15]_i_3_n_0 ),
        .I1(FrameSize[13]),
        .I2(\cnt[15]_i_4_n_0 ),
        .I3(cnt0[13]),
        .I4(data0[13]),
        .I5(\cnt[15]_i_6_n_0 ),
        .O(\cnt[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \cnt[14]_i_1 
       (.I0(\cnt[15]_i_3_n_0 ),
        .I1(FrameSize[14]),
        .I2(\cnt[15]_i_4_n_0 ),
        .I3(cnt0[14]),
        .I4(data0[14]),
        .I5(\cnt[15]_i_6_n_0 ),
        .O(\cnt[14]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cnt[15]_i_1 
       (.I0(direct),
        .I1(reset),
        .O(\cnt[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \cnt[15]_i_2 
       (.I0(\cnt[15]_i_3_n_0 ),
        .I1(FrameSize[15]),
        .I2(\cnt[15]_i_4_n_0 ),
        .I3(cnt0[15]),
        .I4(data0[15]),
        .I5(\cnt[15]_i_6_n_0 ),
        .O(\cnt[15]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'h8F)) 
    \cnt[15]_i_3 
       (.I0(direct),
        .I1(cnt1),
        .I2(reset),
        .O(\cnt[15]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \cnt[15]_i_4 
       (.I0(cnt1),
        .I1(direct),
        .I2(reset),
        .O(\cnt[15]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0E00)) 
    \cnt[15]_i_6 
       (.I0(Q[0]),
        .I1(\cnt[0]_i_2_n_0 ),
        .I2(direct),
        .I3(reset),
        .O(\cnt[15]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \cnt[1]_i_1 
       (.I0(\cnt[15]_i_3_n_0 ),
        .I1(FrameSize[1]),
        .I2(\cnt[15]_i_4_n_0 ),
        .I3(cnt0[1]),
        .I4(data0[1]),
        .I5(\cnt[15]_i_6_n_0 ),
        .O(\cnt[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \cnt[2]_i_1 
       (.I0(\cnt[15]_i_3_n_0 ),
        .I1(FrameSize[2]),
        .I2(\cnt[15]_i_4_n_0 ),
        .I3(cnt0[2]),
        .I4(data0[2]),
        .I5(\cnt[15]_i_6_n_0 ),
        .O(\cnt[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \cnt[3]_i_1 
       (.I0(\cnt[15]_i_3_n_0 ),
        .I1(FrameSize[3]),
        .I2(\cnt[15]_i_4_n_0 ),
        .I3(cnt0[3]),
        .I4(data0[3]),
        .I5(\cnt[15]_i_6_n_0 ),
        .O(\cnt[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \cnt[4]_i_1 
       (.I0(\cnt[15]_i_3_n_0 ),
        .I1(FrameSize[4]),
        .I2(\cnt[15]_i_4_n_0 ),
        .I3(cnt0[4]),
        .I4(data0[4]),
        .I5(\cnt[15]_i_6_n_0 ),
        .O(\cnt[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \cnt[5]_i_1 
       (.I0(\cnt[15]_i_3_n_0 ),
        .I1(FrameSize[5]),
        .I2(\cnt[15]_i_4_n_0 ),
        .I3(cnt0[5]),
        .I4(data0[5]),
        .I5(\cnt[15]_i_6_n_0 ),
        .O(\cnt[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \cnt[6]_i_1 
       (.I0(\cnt[15]_i_3_n_0 ),
        .I1(FrameSize[6]),
        .I2(\cnt[15]_i_4_n_0 ),
        .I3(cnt0[6]),
        .I4(data0[6]),
        .I5(\cnt[15]_i_6_n_0 ),
        .O(\cnt[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \cnt[7]_i_1 
       (.I0(\cnt[15]_i_3_n_0 ),
        .I1(FrameSize[7]),
        .I2(\cnt[15]_i_4_n_0 ),
        .I3(cnt0[7]),
        .I4(data0[7]),
        .I5(\cnt[15]_i_6_n_0 ),
        .O(\cnt[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \cnt[8]_i_1 
       (.I0(\cnt[15]_i_3_n_0 ),
        .I1(FrameSize[8]),
        .I2(\cnt[15]_i_4_n_0 ),
        .I3(cnt0[8]),
        .I4(data0[8]),
        .I5(\cnt[15]_i_6_n_0 ),
        .O(\cnt[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \cnt[9]_i_1 
       (.I0(\cnt[15]_i_3_n_0 ),
        .I1(FrameSize[9]),
        .I2(\cnt[15]_i_4_n_0 ),
        .I3(cnt0[9]),
        .I4(data0[9]),
        .I5(\cnt[15]_i_6_n_0 ),
        .O(\cnt[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[0]_i_1_n_0 ),
        .Q(Q[0]),
        .R(\cnt[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[10]_i_1_n_0 ),
        .Q(Q[10]),
        .R(\cnt[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[11]_i_1_n_0 ),
        .Q(Q[11]),
        .R(\cnt[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[12]_i_1_n_0 ),
        .Q(Q[12]),
        .R(\cnt[15]_i_1_n_0 ));
  CARRY4 \cnt_reg[12]_i_2 
       (.CI(\cnt_reg[8]_i_2_n_0 ),
        .CO({\cnt_reg[12]_i_2_n_0 ,\cnt_reg[12]_i_2_n_1 ,\cnt_reg[12]_i_2_n_2 ,\cnt_reg[12]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(cnt0[12:9]),
        .S(Q[12:9]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[13]_i_1_n_0 ),
        .Q(Q[13]),
        .R(\cnt[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[14]_i_1_n_0 ),
        .Q(Q[14]),
        .R(\cnt[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[15] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[15]_i_2_n_0 ),
        .Q(Q[15]),
        .R(\cnt[15]_i_1_n_0 ));
  CARRY4 \cnt_reg[15]_i_5 
       (.CI(\cnt_reg[12]_i_2_n_0 ),
        .CO({\NLW_cnt_reg[15]_i_5_CO_UNCONNECTED [3:2],\cnt_reg[15]_i_5_n_2 ,\cnt_reg[15]_i_5_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_cnt_reg[15]_i_5_O_UNCONNECTED [3],cnt0[15:13]}),
        .S({1'b0,Q[15:13]}));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[1]_i_1_n_0 ),
        .Q(Q[1]),
        .R(\cnt[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[2]_i_1_n_0 ),
        .Q(Q[2]),
        .R(\cnt[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[3]_i_1_n_0 ),
        .Q(Q[3]),
        .R(\cnt[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[4]_i_1_n_0 ),
        .Q(Q[4]),
        .R(\cnt[15]_i_1_n_0 ));
  CARRY4 \cnt_reg[4]_i_2 
       (.CI(1'b0),
        .CO({\cnt_reg[4]_i_2_n_0 ,\cnt_reg[4]_i_2_n_1 ,\cnt_reg[4]_i_2_n_2 ,\cnt_reg[4]_i_2_n_3 }),
        .CYINIT(Q[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(cnt0[4:1]),
        .S(Q[4:1]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[5]_i_1_n_0 ),
        .Q(Q[5]),
        .R(\cnt[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[6]_i_1_n_0 ),
        .Q(Q[6]),
        .R(\cnt[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[7]_i_1_n_0 ),
        .Q(Q[7]),
        .R(\cnt[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[8]_i_1_n_0 ),
        .Q(Q[8]),
        .R(\cnt[15]_i_1_n_0 ));
  CARRY4 \cnt_reg[8]_i_2 
       (.CI(\cnt_reg[4]_i_2_n_0 ),
        .CO({\cnt_reg[8]_i_2_n_0 ,\cnt_reg[8]_i_2_n_1 ,\cnt_reg[8]_i_2_n_2 ,\cnt_reg[8]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(cnt0[8:5]),
        .S(Q[8:5]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[9]_i_1_n_0 ),
        .Q(Q[9]),
        .R(\cnt[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT1 #(
    .INIT(2'h1)) 
    direct_i_1
       (.I0(direct),
        .O(p_0_in));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    direct_reg
       (.C(reset),
        .CE(1'b1),
        .D(p_0_in),
        .Q(direct),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
