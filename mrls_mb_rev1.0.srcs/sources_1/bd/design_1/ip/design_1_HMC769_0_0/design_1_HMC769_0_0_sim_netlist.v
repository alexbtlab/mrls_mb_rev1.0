// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Mon Mar 22 13:00:55 2021
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               c:/project/mrls_mb_rev1.0/mrls_mb_rev1.0.srcs/sources_1/bd/design_1/ip/design_1_HMC769_0_0/design_1_HMC769_0_0_sim_netlist.v
// Design      : design_1_HMC769_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_HMC769_0_0,HMC769_v1_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "HMC769_v1_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module design_1_HMC769_0_0
   (s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    pll_sen,
    pll_sck,
    pll_mosi,
    pll_ld_sdo,
    pll_cen,
    pll_trig,
    ATTEN,
    start_adc_count,
    shift_front,
    s00_axi_aclk,
    s00_axi_aresetn);
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) input [5:0]s00_axi_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [5:0]s00_axi_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 16, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 6, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input s00_axi_rready;
  output pll_sen;
  output pll_sck;
  output pll_mosi;
  input pll_ld_sdo;
  output pll_cen;
  output pll_trig;
  output [5:0]ATTEN;
  output start_adc_count;
  input [15:0]shift_front;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, INSERT_VIP 0" *) input s00_axi_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axi_aresetn;

  wire \<const0> ;
  wire [5:0]ATTEN;
  wire pll_ld_sdo;
  wire pll_mosi;
  wire pll_sck;
  wire pll_sen;
  wire pll_trig;
  (* IBUF_LOW_PWR *) wire s00_axi_aclk;
  wire [5:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [5:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [15:0]shift_front;
  wire start_adc_count;

  assign pll_cen = \<const0> ;
  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  design_1_HMC769_0_0_HMC769_v1_0 inst
       (.ATTEN(ATTEN),
        .pll_ld_sdo(pll_ld_sdo),
        .pll_mosi(pll_mosi),
        .pll_sck(pll_sck),
        .pll_sen(pll_sen),
        .pll_trig(pll_trig),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[5:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arready(s00_axi_arready),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[5:2]),
        .s00_axi_awready(s00_axi_awready),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wready(s00_axi_wready),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .shift_front(shift_front),
        .start_adc_count(start_adc_count));
endmodule

(* ORIG_REF_NAME = "HMC769_v1_0" *) 
module design_1_HMC769_0_0_HMC769_v1_0
   (ATTEN,
    start_adc_count,
    s00_axi_wready,
    s00_axi_awready,
    s00_axi_arready,
    s00_axi_rdata,
    pll_sck,
    pll_sen,
    s00_axi_rvalid,
    pll_mosi,
    pll_trig,
    s00_axi_bvalid,
    s00_axi_aclk,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    pll_ld_sdo,
    shift_front,
    s00_axi_awvalid,
    s00_axi_wvalid,
    s00_axi_wstrb,
    s00_axi_arvalid,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output [5:0]ATTEN;
  output start_adc_count;
  output s00_axi_wready;
  output s00_axi_awready;
  output s00_axi_arready;
  output [31:0]s00_axi_rdata;
  output pll_sck;
  output pll_sen;
  output s00_axi_rvalid;
  output pll_mosi;
  output pll_trig;
  output s00_axi_bvalid;
  input s00_axi_aclk;
  input [3:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [3:0]s00_axi_araddr;
  input pll_ld_sdo;
  input [15:0]shift_front;
  input s00_axi_awvalid;
  input s00_axi_wvalid;
  input [3:0]s00_axi_wstrb;
  input s00_axi_arvalid;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire [5:0]ATTEN;
  wire HMC769_v1_0_S00_AXI_inst_n_10;
  wire HMC769_v1_0_S00_AXI_inst_n_11;
  wire HMC769_v1_0_S00_AXI_inst_n_12;
  wire HMC769_v1_0_S00_AXI_inst_n_13;
  wire HMC769_v1_0_S00_AXI_inst_n_5;
  wire HMC769_v1_0_S00_AXI_inst_n_6;
  wire HMC769_v1_0_S00_AXI_inst_n_7;
  wire HMC769_v1_0_S00_AXI_inst_n_9;
  wire [0:0]ip2mb_reg0;
  wire [23:0]ip2mb_reg1;
  wire pll_ld_sdo;
  wire pll_mosi;
  wire pll_sck;
  wire pll_sen;
  wire [1:0]\pll_send_i/bits_send_reg ;
  wire pll_trig;
  wire s00_axi_aclk;
  wire [3:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [3:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [15:0]shift_front;
  wire [1:0]slv_reg0;
  wire [4:0]slv_reg1;
  wire [23:23]slv_reg3;
  wire [5:0]slv_reg4;
  wire start_adc_count;
  wire top_PLL_control_i_n_29;
  wire top_PLL_control_i_n_30;

  design_1_HMC769_0_0_HMC769_v1_0_S00_AXI HMC769_v1_0_S00_AXI_inst
       (.D({top_PLL_control_i_n_29,top_PLL_control_i_n_30}),
        .Q(slv_reg3),
        .\axi_rdata_reg[23]_0 (ip2mb_reg1),
        .bits_send_reg(\pll_send_i/bits_send_reg ),
        .ip2mb_reg0(ip2mb_reg0),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arready(s00_axi_arready),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awready(s00_axi_awready),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wready(s00_axi_wready),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .slv_reg0(slv_reg0),
        .\slv_reg1_reg[4]_0 (slv_reg1),
        .\slv_reg2_reg[0]_0 (HMC769_v1_0_S00_AXI_inst_n_6),
        .\slv_reg2_reg[1]_0 (HMC769_v1_0_S00_AXI_inst_n_5),
        .\slv_reg3_reg[13]_0 (HMC769_v1_0_S00_AXI_inst_n_10),
        .\slv_reg3_reg[17]_0 (HMC769_v1_0_S00_AXI_inst_n_9),
        .\slv_reg3_reg[1]_0 (HMC769_v1_0_S00_AXI_inst_n_13),
        .\slv_reg3_reg[21]_0 (HMC769_v1_0_S00_AXI_inst_n_7),
        .\slv_reg3_reg[5]_0 (HMC769_v1_0_S00_AXI_inst_n_12),
        .\slv_reg3_reg[9]_0 (HMC769_v1_0_S00_AXI_inst_n_11),
        .\slv_reg4_reg[5]_0 (slv_reg4));
  design_1_HMC769_0_0_top_PLL_control top_PLL_control_i
       (.ATTEN(ATTEN),
        .D({top_PLL_control_i_n_29,top_PLL_control_i_n_30}),
        .Q(ip2mb_reg1),
        .\atten_reg_reg[5]_0 (slv_reg4),
        .\bits_send_reg[1] (\pll_send_i/bits_send_reg ),
        .ip2mb_reg0(ip2mb_reg0),
        .pll_ld_sdo(pll_ld_sdo),
        .pll_mosi(pll_mosi),
        .pll_mosi_reg_i_2__0(HMC769_v1_0_S00_AXI_inst_n_7),
        .pll_mosi_reg_i_2__0_0(HMC769_v1_0_S00_AXI_inst_n_9),
        .pll_mosi_reg_i_2__0_1(HMC769_v1_0_S00_AXI_inst_n_11),
        .pll_mosi_reg_i_2__0_2(HMC769_v1_0_S00_AXI_inst_n_10),
        .pll_mosi_reg_i_2__0_3(HMC769_v1_0_S00_AXI_inst_n_13),
        .pll_mosi_reg_i_2__0_4(HMC769_v1_0_S00_AXI_inst_n_12),
        .pll_mosi_reg_i_8(slv_reg1),
        .pll_mosi_reg_reg(HMC769_v1_0_S00_AXI_inst_n_5),
        .pll_mosi_reg_reg_0(HMC769_v1_0_S00_AXI_inst_n_6),
        .pll_mosi_reg_reg_1(slv_reg3),
        .pll_sck(pll_sck),
        .pll_sen(pll_sen),
        .pll_trig(pll_trig),
        .s00_axi_aclk(s00_axi_aclk),
        .shift_front(shift_front),
        .slv_reg0(slv_reg0),
        .start_adc_count(start_adc_count));
endmodule

(* ORIG_REF_NAME = "HMC769_v1_0_S00_AXI" *) 
module design_1_HMC769_0_0_HMC769_v1_0_S00_AXI
   (s00_axi_wready,
    s00_axi_awready,
    s00_axi_arready,
    s00_axi_bvalid,
    s00_axi_rvalid,
    \slv_reg2_reg[1]_0 ,
    \slv_reg2_reg[0]_0 ,
    \slv_reg3_reg[21]_0 ,
    Q,
    \slv_reg3_reg[17]_0 ,
    \slv_reg3_reg[13]_0 ,
    \slv_reg3_reg[9]_0 ,
    \slv_reg3_reg[5]_0 ,
    \slv_reg3_reg[1]_0 ,
    \slv_reg1_reg[4]_0 ,
    \slv_reg4_reg[5]_0 ,
    s00_axi_rdata,
    slv_reg0,
    s00_axi_aclk,
    D,
    bits_send_reg,
    \axi_rdata_reg[23]_0 ,
    ip2mb_reg0,
    s00_axi_aresetn,
    s00_axi_awvalid,
    s00_axi_wvalid,
    s00_axi_bready,
    s00_axi_arvalid,
    s00_axi_rready,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_wstrb);
  output s00_axi_wready;
  output s00_axi_awready;
  output s00_axi_arready;
  output s00_axi_bvalid;
  output s00_axi_rvalid;
  output \slv_reg2_reg[1]_0 ;
  output \slv_reg2_reg[0]_0 ;
  output \slv_reg3_reg[21]_0 ;
  output [0:0]Q;
  output \slv_reg3_reg[17]_0 ;
  output \slv_reg3_reg[13]_0 ;
  output \slv_reg3_reg[9]_0 ;
  output \slv_reg3_reg[5]_0 ;
  output \slv_reg3_reg[1]_0 ;
  output [4:0]\slv_reg1_reg[4]_0 ;
  output [5:0]\slv_reg4_reg[5]_0 ;
  output [31:0]s00_axi_rdata;
  output [1:0]slv_reg0;
  input s00_axi_aclk;
  input [1:0]D;
  input [1:0]bits_send_reg;
  input [23:0]\axi_rdata_reg[23]_0 ;
  input [0:0]ip2mb_reg0;
  input s00_axi_aresetn;
  input s00_axi_awvalid;
  input s00_axi_wvalid;
  input s00_axi_bready;
  input s00_axi_arvalid;
  input s00_axi_rready;
  input [3:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [3:0]s00_axi_araddr;
  input [3:0]s00_axi_wstrb;

  wire [1:0]D;
  wire [0:0]Q;
  wire aw_en_i_1_n_0;
  wire aw_en_reg_n_0;
  wire axi_arready0;
  wire axi_awready0;
  wire axi_awready_i_1_n_0;
  wire axi_bvalid_i_1_n_0;
  wire \axi_rdata[0]_i_2_n_0 ;
  wire \axi_rdata[0]_i_3_n_0 ;
  wire \axi_rdata[0]_i_4_n_0 ;
  wire \axi_rdata[10]_i_3_n_0 ;
  wire \axi_rdata[10]_i_4_n_0 ;
  wire \axi_rdata[11]_i_3_n_0 ;
  wire \axi_rdata[11]_i_4_n_0 ;
  wire \axi_rdata[12]_i_3_n_0 ;
  wire \axi_rdata[12]_i_4_n_0 ;
  wire \axi_rdata[13]_i_3_n_0 ;
  wire \axi_rdata[13]_i_4_n_0 ;
  wire \axi_rdata[14]_i_3_n_0 ;
  wire \axi_rdata[14]_i_4_n_0 ;
  wire \axi_rdata[15]_i_3_n_0 ;
  wire \axi_rdata[15]_i_4_n_0 ;
  wire \axi_rdata[16]_i_3_n_0 ;
  wire \axi_rdata[16]_i_4_n_0 ;
  wire \axi_rdata[17]_i_3_n_0 ;
  wire \axi_rdata[17]_i_4_n_0 ;
  wire \axi_rdata[18]_i_3_n_0 ;
  wire \axi_rdata[18]_i_4_n_0 ;
  wire \axi_rdata[19]_i_3_n_0 ;
  wire \axi_rdata[19]_i_4_n_0 ;
  wire \axi_rdata[1]_i_3_n_0 ;
  wire \axi_rdata[1]_i_4_n_0 ;
  wire \axi_rdata[20]_i_3_n_0 ;
  wire \axi_rdata[20]_i_4_n_0 ;
  wire \axi_rdata[21]_i_3_n_0 ;
  wire \axi_rdata[21]_i_4_n_0 ;
  wire \axi_rdata[22]_i_3_n_0 ;
  wire \axi_rdata[22]_i_4_n_0 ;
  wire \axi_rdata[23]_i_3_n_0 ;
  wire \axi_rdata[23]_i_4_n_0 ;
  wire \axi_rdata[24]_i_2_n_0 ;
  wire \axi_rdata[24]_i_3_n_0 ;
  wire \axi_rdata[25]_i_2_n_0 ;
  wire \axi_rdata[25]_i_3_n_0 ;
  wire \axi_rdata[26]_i_2_n_0 ;
  wire \axi_rdata[26]_i_3_n_0 ;
  wire \axi_rdata[27]_i_2_n_0 ;
  wire \axi_rdata[27]_i_3_n_0 ;
  wire \axi_rdata[28]_i_2_n_0 ;
  wire \axi_rdata[28]_i_3_n_0 ;
  wire \axi_rdata[29]_i_2_n_0 ;
  wire \axi_rdata[29]_i_3_n_0 ;
  wire \axi_rdata[2]_i_3_n_0 ;
  wire \axi_rdata[2]_i_4_n_0 ;
  wire \axi_rdata[30]_i_2_n_0 ;
  wire \axi_rdata[30]_i_3_n_0 ;
  wire \axi_rdata[31]_i_2_n_0 ;
  wire \axi_rdata[31]_i_3_n_0 ;
  wire \axi_rdata[3]_i_3_n_0 ;
  wire \axi_rdata[3]_i_4_n_0 ;
  wire \axi_rdata[4]_i_3_n_0 ;
  wire \axi_rdata[4]_i_4_n_0 ;
  wire \axi_rdata[5]_i_3_n_0 ;
  wire \axi_rdata[5]_i_4_n_0 ;
  wire \axi_rdata[6]_i_3_n_0 ;
  wire \axi_rdata[6]_i_4_n_0 ;
  wire \axi_rdata[7]_i_3_n_0 ;
  wire \axi_rdata[7]_i_4_n_0 ;
  wire \axi_rdata[8]_i_3_n_0 ;
  wire \axi_rdata[8]_i_4_n_0 ;
  wire \axi_rdata[9]_i_3_n_0 ;
  wire \axi_rdata[9]_i_4_n_0 ;
  wire \axi_rdata_reg[10]_i_2_n_0 ;
  wire \axi_rdata_reg[11]_i_2_n_0 ;
  wire \axi_rdata_reg[12]_i_2_n_0 ;
  wire \axi_rdata_reg[13]_i_2_n_0 ;
  wire \axi_rdata_reg[14]_i_2_n_0 ;
  wire \axi_rdata_reg[15]_i_2_n_0 ;
  wire \axi_rdata_reg[16]_i_2_n_0 ;
  wire \axi_rdata_reg[17]_i_2_n_0 ;
  wire \axi_rdata_reg[18]_i_2_n_0 ;
  wire \axi_rdata_reg[19]_i_2_n_0 ;
  wire \axi_rdata_reg[1]_i_2_n_0 ;
  wire \axi_rdata_reg[20]_i_2_n_0 ;
  wire \axi_rdata_reg[21]_i_2_n_0 ;
  wire \axi_rdata_reg[22]_i_2_n_0 ;
  wire [23:0]\axi_rdata_reg[23]_0 ;
  wire \axi_rdata_reg[23]_i_2_n_0 ;
  wire \axi_rdata_reg[2]_i_2_n_0 ;
  wire \axi_rdata_reg[3]_i_2_n_0 ;
  wire \axi_rdata_reg[4]_i_2_n_0 ;
  wire \axi_rdata_reg[5]_i_2_n_0 ;
  wire \axi_rdata_reg[6]_i_2_n_0 ;
  wire \axi_rdata_reg[7]_i_2_n_0 ;
  wire \axi_rdata_reg[8]_i_2_n_0 ;
  wire \axi_rdata_reg[9]_i_2_n_0 ;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready0;
  wire [1:0]bits_send_reg;
  wire [0:0]ip2mb_reg0;
  wire [3:0]p_0_in;
  wire [31:7]p_1_in;
  wire [31:0]reg_data_out;
  wire s00_axi_aclk;
  wire [3:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [3:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [3:0]sel0;
  wire [1:0]slv_reg0;
  wire \slv_reg0[0]_i_1_n_0 ;
  wire \slv_reg0[1]_i_1_n_0 ;
  wire \slv_reg0[1]_i_2_n_0 ;
  wire [31:5]slv_reg1;
  wire [4:0]\slv_reg1_reg[4]_0 ;
  wire [31:0]slv_reg2;
  wire \slv_reg2[15]_i_1_n_0 ;
  wire \slv_reg2[23]_i_1_n_0 ;
  wire \slv_reg2[31]_i_1_n_0 ;
  wire \slv_reg2[7]_i_1_n_0 ;
  wire \slv_reg2_reg[0]_0 ;
  wire \slv_reg2_reg[1]_0 ;
  wire [31:0]slv_reg3;
  wire \slv_reg3[15]_i_1_n_0 ;
  wire \slv_reg3[23]_i_1_n_0 ;
  wire \slv_reg3[31]_i_1_n_0 ;
  wire \slv_reg3[7]_i_1_n_0 ;
  wire \slv_reg3_reg[13]_0 ;
  wire \slv_reg3_reg[17]_0 ;
  wire \slv_reg3_reg[1]_0 ;
  wire \slv_reg3_reg[21]_0 ;
  wire \slv_reg3_reg[5]_0 ;
  wire \slv_reg3_reg[9]_0 ;
  wire [31:6]slv_reg4;
  wire \slv_reg4[15]_i_1_n_0 ;
  wire \slv_reg4[23]_i_1_n_0 ;
  wire \slv_reg4[31]_i_1_n_0 ;
  wire \slv_reg4[7]_i_1_n_0 ;
  wire [5:0]\slv_reg4_reg[5]_0 ;
  wire [31:0]slv_reg5;
  wire \slv_reg5[15]_i_1_n_0 ;
  wire \slv_reg5[23]_i_1_n_0 ;
  wire \slv_reg5[31]_i_1_n_0 ;
  wire \slv_reg5[7]_i_1_n_0 ;
  wire [31:0]slv_reg6;
  wire \slv_reg6[15]_i_1_n_0 ;
  wire \slv_reg6[23]_i_1_n_0 ;
  wire \slv_reg6[31]_i_1_n_0 ;
  wire \slv_reg6[7]_i_1_n_0 ;
  wire [31:0]slv_reg7;
  wire \slv_reg7[15]_i_1_n_0 ;
  wire \slv_reg7[23]_i_1_n_0 ;
  wire \slv_reg7[31]_i_1_n_0 ;
  wire \slv_reg7[7]_i_1_n_0 ;
  wire slv_reg_rden__0;
  wire slv_reg_wren__0;

  LUT6 #(
    .INIT(64'hF7FFC4CCC4CCC4CC)) 
    aw_en_i_1
       (.I0(s00_axi_awvalid),
        .I1(aw_en_reg_n_0),
        .I2(s00_axi_awready),
        .I3(s00_axi_wvalid),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(aw_en_i_1_n_0));
  FDSE aw_en_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(aw_en_i_1_n_0),
        .Q(aw_en_reg_n_0),
        .S(axi_awready_i_1_n_0));
  FDRE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[0]),
        .Q(sel0[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[1]),
        .Q(sel0[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_araddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[2]),
        .Q(sel0[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_araddr_reg[5] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[3]),
        .Q(sel0[3]),
        .R(axi_awready_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(s00_axi_arready),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(s00_axi_arready),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[0]),
        .Q(p_0_in[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[1]),
        .Q(p_0_in[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awaddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[2]),
        .Q(p_0_in[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awaddr_reg[5] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[3]),
        .Q(p_0_in[3]),
        .R(axi_awready_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s00_axi_aresetn),
        .O(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h2000)) 
    axi_awready_i_2
       (.I0(s00_axi_wvalid),
        .I1(s00_axi_awready),
        .I2(aw_en_reg_n_0),
        .I3(s00_axi_awvalid),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(s00_axi_awready),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awready),
        .I3(s00_axi_wready),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s00_axi_bvalid),
        .R(axi_awready_i_1_n_0));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[0]_i_1 
       (.I0(\axi_rdata[0]_i_2_n_0 ),
        .I1(sel0[3]),
        .I2(\axi_rdata[0]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[0]_i_4_n_0 ),
        .O(reg_data_out[0]));
  LUT5 #(
    .INIT(32'hFE11FE00)) 
    \axi_rdata[0]_i_2 
       (.I0(sel0[2]),
        .I1(sel0[1]),
        .I2(\axi_rdata_reg[23]_0 [0]),
        .I3(sel0[0]),
        .I4(ip2mb_reg0),
        .O(\axi_rdata[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_3 
       (.I0(slv_reg7[0]),
        .I1(slv_reg6[0]),
        .I2(sel0[1]),
        .I3(slv_reg5[0]),
        .I4(sel0[0]),
        .I5(\slv_reg4_reg[5]_0 [0]),
        .O(\axi_rdata[0]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[0]_i_4 
       (.I0(slv_reg3[0]),
        .I1(slv_reg2[0]),
        .I2(sel0[1]),
        .I3(\slv_reg1_reg[4]_0 [0]),
        .I4(sel0[0]),
        .O(\axi_rdata[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0040FFFF00400000)) 
    \axi_rdata[10]_i_1 
       (.I0(sel0[2]),
        .I1(\axi_rdata_reg[23]_0 [10]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[10]_i_2_n_0 ),
        .O(reg_data_out[10]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[10]_i_3 
       (.I0(slv_reg3[10]),
        .I1(slv_reg2[10]),
        .I2(sel0[1]),
        .I3(slv_reg1[10]),
        .I4(sel0[0]),
        .O(\axi_rdata[10]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_4 
       (.I0(slv_reg7[10]),
        .I1(slv_reg6[10]),
        .I2(sel0[1]),
        .I3(slv_reg5[10]),
        .I4(sel0[0]),
        .I5(slv_reg4[10]),
        .O(\axi_rdata[10]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0040FFFF00400000)) 
    \axi_rdata[11]_i_1 
       (.I0(sel0[2]),
        .I1(\axi_rdata_reg[23]_0 [11]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[11]_i_2_n_0 ),
        .O(reg_data_out[11]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[11]_i_3 
       (.I0(slv_reg3[11]),
        .I1(slv_reg2[11]),
        .I2(sel0[1]),
        .I3(slv_reg1[11]),
        .I4(sel0[0]),
        .O(\axi_rdata[11]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_4 
       (.I0(slv_reg7[11]),
        .I1(slv_reg6[11]),
        .I2(sel0[1]),
        .I3(slv_reg5[11]),
        .I4(sel0[0]),
        .I5(slv_reg4[11]),
        .O(\axi_rdata[11]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0040FFFF00400000)) 
    \axi_rdata[12]_i_1 
       (.I0(sel0[2]),
        .I1(\axi_rdata_reg[23]_0 [12]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[12]_i_2_n_0 ),
        .O(reg_data_out[12]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[12]_i_3 
       (.I0(slv_reg3[12]),
        .I1(slv_reg2[12]),
        .I2(sel0[1]),
        .I3(slv_reg1[12]),
        .I4(sel0[0]),
        .O(\axi_rdata[12]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_4 
       (.I0(slv_reg7[12]),
        .I1(slv_reg6[12]),
        .I2(sel0[1]),
        .I3(slv_reg5[12]),
        .I4(sel0[0]),
        .I5(slv_reg4[12]),
        .O(\axi_rdata[12]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0040FFFF00400000)) 
    \axi_rdata[13]_i_1 
       (.I0(sel0[2]),
        .I1(\axi_rdata_reg[23]_0 [13]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[13]_i_2_n_0 ),
        .O(reg_data_out[13]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[13]_i_3 
       (.I0(slv_reg3[13]),
        .I1(slv_reg2[13]),
        .I2(sel0[1]),
        .I3(slv_reg1[13]),
        .I4(sel0[0]),
        .O(\axi_rdata[13]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_4 
       (.I0(slv_reg7[13]),
        .I1(slv_reg6[13]),
        .I2(sel0[1]),
        .I3(slv_reg5[13]),
        .I4(sel0[0]),
        .I5(slv_reg4[13]),
        .O(\axi_rdata[13]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0040FFFF00400000)) 
    \axi_rdata[14]_i_1 
       (.I0(sel0[2]),
        .I1(\axi_rdata_reg[23]_0 [14]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[14]_i_2_n_0 ),
        .O(reg_data_out[14]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[14]_i_3 
       (.I0(slv_reg3[14]),
        .I1(slv_reg2[14]),
        .I2(sel0[1]),
        .I3(slv_reg1[14]),
        .I4(sel0[0]),
        .O(\axi_rdata[14]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_4 
       (.I0(slv_reg7[14]),
        .I1(slv_reg6[14]),
        .I2(sel0[1]),
        .I3(slv_reg5[14]),
        .I4(sel0[0]),
        .I5(slv_reg4[14]),
        .O(\axi_rdata[14]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0040FFFF00400000)) 
    \axi_rdata[15]_i_1 
       (.I0(sel0[2]),
        .I1(\axi_rdata_reg[23]_0 [15]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[15]_i_2_n_0 ),
        .O(reg_data_out[15]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[15]_i_3 
       (.I0(slv_reg3[15]),
        .I1(slv_reg2[15]),
        .I2(sel0[1]),
        .I3(slv_reg1[15]),
        .I4(sel0[0]),
        .O(\axi_rdata[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_4 
       (.I0(slv_reg7[15]),
        .I1(slv_reg6[15]),
        .I2(sel0[1]),
        .I3(slv_reg5[15]),
        .I4(sel0[0]),
        .I5(slv_reg4[15]),
        .O(\axi_rdata[15]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0040FFFF00400000)) 
    \axi_rdata[16]_i_1 
       (.I0(sel0[2]),
        .I1(\axi_rdata_reg[23]_0 [16]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[16]_i_2_n_0 ),
        .O(reg_data_out[16]));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[16]_i_3 
       (.I0(slv_reg3[16]),
        .I1(slv_reg2[16]),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(slv_reg1[16]),
        .O(\axi_rdata[16]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_4 
       (.I0(slv_reg7[16]),
        .I1(slv_reg6[16]),
        .I2(sel0[1]),
        .I3(slv_reg5[16]),
        .I4(sel0[0]),
        .I5(slv_reg4[16]),
        .O(\axi_rdata[16]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0040FFFF00400000)) 
    \axi_rdata[17]_i_1 
       (.I0(sel0[2]),
        .I1(\axi_rdata_reg[23]_0 [17]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[17]_i_2_n_0 ),
        .O(reg_data_out[17]));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[17]_i_3 
       (.I0(slv_reg3[17]),
        .I1(slv_reg2[17]),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(slv_reg1[17]),
        .O(\axi_rdata[17]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_4 
       (.I0(slv_reg7[17]),
        .I1(slv_reg6[17]),
        .I2(sel0[1]),
        .I3(slv_reg5[17]),
        .I4(sel0[0]),
        .I5(slv_reg4[17]),
        .O(\axi_rdata[17]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFEEEFFFFFEEE0000)) 
    \axi_rdata[18]_i_1 
       (.I0(sel0[1]),
        .I1(sel0[2]),
        .I2(sel0[0]),
        .I3(\axi_rdata_reg[23]_0 [18]),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[18]_i_2_n_0 ),
        .O(reg_data_out[18]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[18]_i_3 
       (.I0(slv_reg3[18]),
        .I1(slv_reg2[18]),
        .I2(sel0[1]),
        .I3(slv_reg1[18]),
        .I4(sel0[0]),
        .O(\axi_rdata[18]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_4 
       (.I0(slv_reg7[18]),
        .I1(slv_reg6[18]),
        .I2(sel0[1]),
        .I3(slv_reg5[18]),
        .I4(sel0[0]),
        .I5(slv_reg4[18]),
        .O(\axi_rdata[18]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFEEEFFFFFEEE0000)) 
    \axi_rdata[19]_i_1 
       (.I0(sel0[1]),
        .I1(sel0[2]),
        .I2(sel0[0]),
        .I3(\axi_rdata_reg[23]_0 [19]),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[19]_i_2_n_0 ),
        .O(reg_data_out[19]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[19]_i_3 
       (.I0(slv_reg3[19]),
        .I1(slv_reg2[19]),
        .I2(sel0[1]),
        .I3(slv_reg1[19]),
        .I4(sel0[0]),
        .O(\axi_rdata[19]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_4 
       (.I0(slv_reg7[19]),
        .I1(slv_reg6[19]),
        .I2(sel0[1]),
        .I3(slv_reg5[19]),
        .I4(sel0[0]),
        .I5(slv_reg4[19]),
        .O(\axi_rdata[19]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hBAAAFFFFBAAA0000)) 
    \axi_rdata[1]_i_1 
       (.I0(sel0[1]),
        .I1(sel0[2]),
        .I2(sel0[0]),
        .I3(\axi_rdata_reg[23]_0 [1]),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[1]_i_2_n_0 ),
        .O(reg_data_out[1]));
  LUT5 #(
    .INIT(32'hAFA0C0C0)) 
    \axi_rdata[1]_i_3 
       (.I0(slv_reg3[1]),
        .I1(slv_reg2[1]),
        .I2(sel0[1]),
        .I3(\slv_reg1_reg[4]_0 [1]),
        .I4(sel0[0]),
        .O(\axi_rdata[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_4 
       (.I0(slv_reg7[1]),
        .I1(slv_reg6[1]),
        .I2(sel0[1]),
        .I3(slv_reg5[1]),
        .I4(sel0[0]),
        .I5(\slv_reg4_reg[5]_0 [1]),
        .O(\axi_rdata[1]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFEEEFFFFFEEE0000)) 
    \axi_rdata[20]_i_1 
       (.I0(sel0[1]),
        .I1(sel0[2]),
        .I2(sel0[0]),
        .I3(\axi_rdata_reg[23]_0 [20]),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[20]_i_2_n_0 ),
        .O(reg_data_out[20]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[20]_i_3 
       (.I0(slv_reg3[20]),
        .I1(slv_reg2[20]),
        .I2(sel0[1]),
        .I3(slv_reg1[20]),
        .I4(sel0[0]),
        .O(\axi_rdata[20]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_4 
       (.I0(slv_reg7[20]),
        .I1(slv_reg6[20]),
        .I2(sel0[1]),
        .I3(slv_reg5[20]),
        .I4(sel0[0]),
        .I5(slv_reg4[20]),
        .O(\axi_rdata[20]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0040FFFF00400000)) 
    \axi_rdata[21]_i_1 
       (.I0(sel0[2]),
        .I1(\axi_rdata_reg[23]_0 [21]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[21]_i_2_n_0 ),
        .O(reg_data_out[21]));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[21]_i_3 
       (.I0(slv_reg3[21]),
        .I1(slv_reg2[21]),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(slv_reg1[21]),
        .O(\axi_rdata[21]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_4 
       (.I0(slv_reg7[21]),
        .I1(slv_reg6[21]),
        .I2(sel0[1]),
        .I3(slv_reg5[21]),
        .I4(sel0[0]),
        .I5(slv_reg4[21]),
        .O(\axi_rdata[21]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFEEEFFFFFEEE0000)) 
    \axi_rdata[22]_i_1 
       (.I0(sel0[1]),
        .I1(sel0[2]),
        .I2(sel0[0]),
        .I3(\axi_rdata_reg[23]_0 [22]),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[22]_i_2_n_0 ),
        .O(reg_data_out[22]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[22]_i_3 
       (.I0(slv_reg3[22]),
        .I1(slv_reg2[22]),
        .I2(sel0[1]),
        .I3(slv_reg1[22]),
        .I4(sel0[0]),
        .O(\axi_rdata[22]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_4 
       (.I0(slv_reg7[22]),
        .I1(slv_reg6[22]),
        .I2(sel0[1]),
        .I3(slv_reg5[22]),
        .I4(sel0[0]),
        .I5(slv_reg4[22]),
        .O(\axi_rdata[22]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFEEEFFFFFEEE0000)) 
    \axi_rdata[23]_i_1 
       (.I0(sel0[1]),
        .I1(sel0[2]),
        .I2(sel0[0]),
        .I3(\axi_rdata_reg[23]_0 [23]),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[23]_i_2_n_0 ),
        .O(reg_data_out[23]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[23]_i_3 
       (.I0(Q),
        .I1(slv_reg2[23]),
        .I2(sel0[1]),
        .I3(slv_reg1[23]),
        .I4(sel0[0]),
        .O(\axi_rdata[23]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_4 
       (.I0(slv_reg7[23]),
        .I1(slv_reg6[23]),
        .I2(sel0[1]),
        .I3(slv_reg5[23]),
        .I4(sel0[0]),
        .I5(slv_reg4[23]),
        .O(\axi_rdata[23]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \axi_rdata[24]_i_1 
       (.I0(\axi_rdata[24]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(\axi_rdata[24]_i_3_n_0 ),
        .I3(sel0[3]),
        .O(reg_data_out[24]));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[24]_i_2 
       (.I0(slv_reg3[24]),
        .I1(slv_reg2[24]),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(slv_reg1[24]),
        .O(\axi_rdata[24]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_3 
       (.I0(slv_reg7[24]),
        .I1(slv_reg6[24]),
        .I2(sel0[1]),
        .I3(slv_reg5[24]),
        .I4(sel0[0]),
        .I5(slv_reg4[24]),
        .O(\axi_rdata[24]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \axi_rdata[25]_i_1 
       (.I0(\axi_rdata[25]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(\axi_rdata[25]_i_3_n_0 ),
        .I3(sel0[3]),
        .O(reg_data_out[25]));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[25]_i_2 
       (.I0(slv_reg3[25]),
        .I1(slv_reg2[25]),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(slv_reg1[25]),
        .O(\axi_rdata[25]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_3 
       (.I0(slv_reg7[25]),
        .I1(slv_reg6[25]),
        .I2(sel0[1]),
        .I3(slv_reg5[25]),
        .I4(sel0[0]),
        .I5(slv_reg4[25]),
        .O(\axi_rdata[25]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFCBBFC88)) 
    \axi_rdata[26]_i_1 
       (.I0(sel0[1]),
        .I1(sel0[3]),
        .I2(\axi_rdata[26]_i_2_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[26]_i_3_n_0 ),
        .O(reg_data_out[26]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_2 
       (.I0(slv_reg7[26]),
        .I1(slv_reg6[26]),
        .I2(sel0[1]),
        .I3(slv_reg5[26]),
        .I4(sel0[0]),
        .I5(slv_reg4[26]),
        .O(\axi_rdata[26]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[26]_i_3 
       (.I0(slv_reg3[26]),
        .I1(slv_reg2[26]),
        .I2(sel0[1]),
        .I3(slv_reg1[26]),
        .I4(sel0[0]),
        .O(\axi_rdata[26]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFCBBFC88)) 
    \axi_rdata[27]_i_1 
       (.I0(sel0[1]),
        .I1(sel0[3]),
        .I2(\axi_rdata[27]_i_2_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[27]_i_3_n_0 ),
        .O(reg_data_out[27]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_2 
       (.I0(slv_reg7[27]),
        .I1(slv_reg6[27]),
        .I2(sel0[1]),
        .I3(slv_reg5[27]),
        .I4(sel0[0]),
        .I5(slv_reg4[27]),
        .O(\axi_rdata[27]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[27]_i_3 
       (.I0(slv_reg3[27]),
        .I1(slv_reg2[27]),
        .I2(sel0[1]),
        .I3(slv_reg1[27]),
        .I4(sel0[0]),
        .O(\axi_rdata[27]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \axi_rdata[28]_i_1 
       (.I0(\axi_rdata[28]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(\axi_rdata[28]_i_3_n_0 ),
        .I3(sel0[3]),
        .O(reg_data_out[28]));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[28]_i_2 
       (.I0(slv_reg3[28]),
        .I1(slv_reg2[28]),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(slv_reg1[28]),
        .O(\axi_rdata[28]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_3 
       (.I0(slv_reg7[28]),
        .I1(slv_reg6[28]),
        .I2(sel0[1]),
        .I3(slv_reg5[28]),
        .I4(sel0[0]),
        .I5(slv_reg4[28]),
        .O(\axi_rdata[28]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFCBBFC88)) 
    \axi_rdata[29]_i_1 
       (.I0(sel0[1]),
        .I1(sel0[3]),
        .I2(\axi_rdata[29]_i_2_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[29]_i_3_n_0 ),
        .O(reg_data_out[29]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_2 
       (.I0(slv_reg7[29]),
        .I1(slv_reg6[29]),
        .I2(sel0[1]),
        .I3(slv_reg5[29]),
        .I4(sel0[0]),
        .I5(slv_reg4[29]),
        .O(\axi_rdata[29]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[29]_i_3 
       (.I0(slv_reg3[29]),
        .I1(slv_reg2[29]),
        .I2(sel0[1]),
        .I3(slv_reg1[29]),
        .I4(sel0[0]),
        .O(\axi_rdata[29]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hBAAAFFFFBAAA0000)) 
    \axi_rdata[2]_i_1 
       (.I0(sel0[2]),
        .I1(sel0[1]),
        .I2(sel0[0]),
        .I3(\axi_rdata_reg[23]_0 [2]),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[2]_i_2_n_0 ),
        .O(reg_data_out[2]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[2]_i_3 
       (.I0(slv_reg3[2]),
        .I1(slv_reg2[2]),
        .I2(sel0[1]),
        .I3(\slv_reg1_reg[4]_0 [2]),
        .I4(sel0[0]),
        .O(\axi_rdata[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_4 
       (.I0(slv_reg7[2]),
        .I1(slv_reg6[2]),
        .I2(sel0[1]),
        .I3(slv_reg5[2]),
        .I4(sel0[0]),
        .I5(\slv_reg4_reg[5]_0 [2]),
        .O(\axi_rdata[2]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \axi_rdata[30]_i_1 
       (.I0(\axi_rdata[30]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(\axi_rdata[30]_i_3_n_0 ),
        .I3(sel0[3]),
        .O(reg_data_out[30]));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[30]_i_2 
       (.I0(slv_reg3[30]),
        .I1(slv_reg2[30]),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(slv_reg1[30]),
        .O(\axi_rdata[30]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_3 
       (.I0(slv_reg7[30]),
        .I1(slv_reg6[30]),
        .I2(sel0[1]),
        .I3(slv_reg5[30]),
        .I4(sel0[0]),
        .I5(slv_reg4[30]),
        .O(\axi_rdata[30]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFCBBFC88)) 
    \axi_rdata[31]_i_1 
       (.I0(sel0[1]),
        .I1(sel0[3]),
        .I2(\axi_rdata[31]_i_2_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[31]_i_3_n_0 ),
        .O(reg_data_out[31]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_2 
       (.I0(slv_reg7[31]),
        .I1(slv_reg6[31]),
        .I2(sel0[1]),
        .I3(slv_reg5[31]),
        .I4(sel0[0]),
        .I5(slv_reg4[31]),
        .O(\axi_rdata[31]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[31]_i_3 
       (.I0(slv_reg3[31]),
        .I1(slv_reg2[31]),
        .I2(sel0[1]),
        .I3(slv_reg1[31]),
        .I4(sel0[0]),
        .O(\axi_rdata[31]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0040FFFF00400000)) 
    \axi_rdata[3]_i_1 
       (.I0(sel0[2]),
        .I1(\axi_rdata_reg[23]_0 [3]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[3]_i_2_n_0 ),
        .O(reg_data_out[3]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[3]_i_3 
       (.I0(slv_reg3[3]),
        .I1(slv_reg2[3]),
        .I2(sel0[1]),
        .I3(\slv_reg1_reg[4]_0 [3]),
        .I4(sel0[0]),
        .O(\axi_rdata[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_4 
       (.I0(slv_reg7[3]),
        .I1(slv_reg6[3]),
        .I2(sel0[1]),
        .I3(slv_reg5[3]),
        .I4(sel0[0]),
        .I5(\slv_reg4_reg[5]_0 [3]),
        .O(\axi_rdata[3]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0040FFFF00400000)) 
    \axi_rdata[4]_i_1 
       (.I0(sel0[2]),
        .I1(\axi_rdata_reg[23]_0 [4]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[4]_i_2_n_0 ),
        .O(reg_data_out[4]));
  LUT5 #(
    .INIT(32'hAFA0C0C0)) 
    \axi_rdata[4]_i_3 
       (.I0(slv_reg3[4]),
        .I1(slv_reg2[4]),
        .I2(sel0[1]),
        .I3(\slv_reg1_reg[4]_0 [4]),
        .I4(sel0[0]),
        .O(\axi_rdata[4]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_4 
       (.I0(slv_reg7[4]),
        .I1(slv_reg6[4]),
        .I2(sel0[1]),
        .I3(slv_reg5[4]),
        .I4(sel0[0]),
        .I5(\slv_reg4_reg[5]_0 [4]),
        .O(\axi_rdata[4]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0040FFFF00400000)) 
    \axi_rdata[5]_i_1 
       (.I0(sel0[2]),
        .I1(\axi_rdata_reg[23]_0 [5]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[5]_i_2_n_0 ),
        .O(reg_data_out[5]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[5]_i_3 
       (.I0(slv_reg3[5]),
        .I1(slv_reg2[5]),
        .I2(sel0[1]),
        .I3(slv_reg1[5]),
        .I4(sel0[0]),
        .O(\axi_rdata[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_4 
       (.I0(slv_reg7[5]),
        .I1(slv_reg6[5]),
        .I2(sel0[1]),
        .I3(slv_reg5[5]),
        .I4(sel0[0]),
        .I5(\slv_reg4_reg[5]_0 [5]),
        .O(\axi_rdata[5]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0040FFFF00400000)) 
    \axi_rdata[6]_i_1 
       (.I0(sel0[2]),
        .I1(\axi_rdata_reg[23]_0 [6]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[6]_i_2_n_0 ),
        .O(reg_data_out[6]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[6]_i_3 
       (.I0(slv_reg3[6]),
        .I1(slv_reg2[6]),
        .I2(sel0[1]),
        .I3(slv_reg1[6]),
        .I4(sel0[0]),
        .O(\axi_rdata[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_4 
       (.I0(slv_reg7[6]),
        .I1(slv_reg6[6]),
        .I2(sel0[1]),
        .I3(slv_reg5[6]),
        .I4(sel0[0]),
        .I5(slv_reg4[6]),
        .O(\axi_rdata[6]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0040FFFF00400000)) 
    \axi_rdata[7]_i_1 
       (.I0(sel0[2]),
        .I1(\axi_rdata_reg[23]_0 [7]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[7]_i_2_n_0 ),
        .O(reg_data_out[7]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[7]_i_3 
       (.I0(slv_reg3[7]),
        .I1(slv_reg2[7]),
        .I2(sel0[1]),
        .I3(slv_reg1[7]),
        .I4(sel0[0]),
        .O(\axi_rdata[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_4 
       (.I0(slv_reg7[7]),
        .I1(slv_reg6[7]),
        .I2(sel0[1]),
        .I3(slv_reg5[7]),
        .I4(sel0[0]),
        .I5(slv_reg4[7]),
        .O(\axi_rdata[7]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0040FFFF00400000)) 
    \axi_rdata[8]_i_1 
       (.I0(sel0[2]),
        .I1(\axi_rdata_reg[23]_0 [8]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[8]_i_2_n_0 ),
        .O(reg_data_out[8]));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[8]_i_3 
       (.I0(slv_reg3[8]),
        .I1(slv_reg2[8]),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(slv_reg1[8]),
        .O(\axi_rdata[8]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_4 
       (.I0(slv_reg7[8]),
        .I1(slv_reg6[8]),
        .I2(sel0[1]),
        .I3(slv_reg5[8]),
        .I4(sel0[0]),
        .I5(slv_reg4[8]),
        .O(\axi_rdata[8]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0040FFFF00400000)) 
    \axi_rdata[9]_i_1 
       (.I0(sel0[2]),
        .I1(\axi_rdata_reg[23]_0 [9]),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(sel0[3]),
        .I5(\axi_rdata_reg[9]_i_2_n_0 ),
        .O(reg_data_out[9]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[9]_i_3 
       (.I0(slv_reg3[9]),
        .I1(slv_reg2[9]),
        .I2(sel0[1]),
        .I3(slv_reg1[9]),
        .I4(sel0[0]),
        .O(\axi_rdata[9]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_4 
       (.I0(slv_reg7[9]),
        .I1(slv_reg6[9]),
        .I2(sel0[1]),
        .I3(slv_reg5[9]),
        .I4(sel0[0]),
        .I5(slv_reg4[9]),
        .O(\axi_rdata[9]_i_4_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[0]),
        .Q(s00_axi_rdata[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[10]),
        .Q(s00_axi_rdata[10]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[10]_i_2 
       (.I0(\axi_rdata[10]_i_3_n_0 ),
        .I1(\axi_rdata[10]_i_4_n_0 ),
        .O(\axi_rdata_reg[10]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[11]),
        .Q(s00_axi_rdata[11]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[11]_i_2 
       (.I0(\axi_rdata[11]_i_3_n_0 ),
        .I1(\axi_rdata[11]_i_4_n_0 ),
        .O(\axi_rdata_reg[11]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[12]),
        .Q(s00_axi_rdata[12]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[12]_i_2 
       (.I0(\axi_rdata[12]_i_3_n_0 ),
        .I1(\axi_rdata[12]_i_4_n_0 ),
        .O(\axi_rdata_reg[12]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[13]),
        .Q(s00_axi_rdata[13]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[13]_i_2 
       (.I0(\axi_rdata[13]_i_3_n_0 ),
        .I1(\axi_rdata[13]_i_4_n_0 ),
        .O(\axi_rdata_reg[13]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[14]),
        .Q(s00_axi_rdata[14]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[14]_i_2 
       (.I0(\axi_rdata[14]_i_3_n_0 ),
        .I1(\axi_rdata[14]_i_4_n_0 ),
        .O(\axi_rdata_reg[14]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[15]),
        .Q(s00_axi_rdata[15]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[15]_i_2 
       (.I0(\axi_rdata[15]_i_3_n_0 ),
        .I1(\axi_rdata[15]_i_4_n_0 ),
        .O(\axi_rdata_reg[15]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[16]),
        .Q(s00_axi_rdata[16]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[16]_i_2 
       (.I0(\axi_rdata[16]_i_3_n_0 ),
        .I1(\axi_rdata[16]_i_4_n_0 ),
        .O(\axi_rdata_reg[16]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[17]),
        .Q(s00_axi_rdata[17]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[17]_i_2 
       (.I0(\axi_rdata[17]_i_3_n_0 ),
        .I1(\axi_rdata[17]_i_4_n_0 ),
        .O(\axi_rdata_reg[17]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[18]),
        .Q(s00_axi_rdata[18]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[18]_i_2 
       (.I0(\axi_rdata[18]_i_3_n_0 ),
        .I1(\axi_rdata[18]_i_4_n_0 ),
        .O(\axi_rdata_reg[18]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[19]),
        .Q(s00_axi_rdata[19]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[19]_i_2 
       (.I0(\axi_rdata[19]_i_3_n_0 ),
        .I1(\axi_rdata[19]_i_4_n_0 ),
        .O(\axi_rdata_reg[19]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[1]),
        .Q(s00_axi_rdata[1]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[1]_i_2 
       (.I0(\axi_rdata[1]_i_3_n_0 ),
        .I1(\axi_rdata[1]_i_4_n_0 ),
        .O(\axi_rdata_reg[1]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[20]),
        .Q(s00_axi_rdata[20]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[20]_i_2 
       (.I0(\axi_rdata[20]_i_3_n_0 ),
        .I1(\axi_rdata[20]_i_4_n_0 ),
        .O(\axi_rdata_reg[20]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[21]),
        .Q(s00_axi_rdata[21]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[21]_i_2 
       (.I0(\axi_rdata[21]_i_3_n_0 ),
        .I1(\axi_rdata[21]_i_4_n_0 ),
        .O(\axi_rdata_reg[21]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[22]),
        .Q(s00_axi_rdata[22]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[22]_i_2 
       (.I0(\axi_rdata[22]_i_3_n_0 ),
        .I1(\axi_rdata[22]_i_4_n_0 ),
        .O(\axi_rdata_reg[22]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[23]),
        .Q(s00_axi_rdata[23]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[23]_i_2 
       (.I0(\axi_rdata[23]_i_3_n_0 ),
        .I1(\axi_rdata[23]_i_4_n_0 ),
        .O(\axi_rdata_reg[23]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[24]),
        .Q(s00_axi_rdata[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[25]),
        .Q(s00_axi_rdata[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[26]),
        .Q(s00_axi_rdata[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[27]),
        .Q(s00_axi_rdata[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[28]),
        .Q(s00_axi_rdata[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[29]),
        .Q(s00_axi_rdata[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[2]),
        .Q(s00_axi_rdata[2]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[2]_i_2 
       (.I0(\axi_rdata[2]_i_3_n_0 ),
        .I1(\axi_rdata[2]_i_4_n_0 ),
        .O(\axi_rdata_reg[2]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[30]),
        .Q(s00_axi_rdata[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[31]),
        .Q(s00_axi_rdata[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[3]),
        .Q(s00_axi_rdata[3]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[3]_i_2 
       (.I0(\axi_rdata[3]_i_3_n_0 ),
        .I1(\axi_rdata[3]_i_4_n_0 ),
        .O(\axi_rdata_reg[3]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[4]),
        .Q(s00_axi_rdata[4]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[4]_i_2 
       (.I0(\axi_rdata[4]_i_3_n_0 ),
        .I1(\axi_rdata[4]_i_4_n_0 ),
        .O(\axi_rdata_reg[4]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[5]),
        .Q(s00_axi_rdata[5]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[5]_i_2 
       (.I0(\axi_rdata[5]_i_3_n_0 ),
        .I1(\axi_rdata[5]_i_4_n_0 ),
        .O(\axi_rdata_reg[5]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[6]),
        .Q(s00_axi_rdata[6]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[6]_i_2 
       (.I0(\axi_rdata[6]_i_3_n_0 ),
        .I1(\axi_rdata[6]_i_4_n_0 ),
        .O(\axi_rdata_reg[6]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[7]),
        .Q(s00_axi_rdata[7]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[7]_i_2 
       (.I0(\axi_rdata[7]_i_3_n_0 ),
        .I1(\axi_rdata[7]_i_4_n_0 ),
        .O(\axi_rdata_reg[7]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[8]),
        .Q(s00_axi_rdata[8]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[8]_i_2 
       (.I0(\axi_rdata[8]_i_3_n_0 ),
        .I1(\axi_rdata[8]_i_4_n_0 ),
        .O(\axi_rdata_reg[8]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[9]),
        .Q(s00_axi_rdata[9]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[9]_i_2 
       (.I0(\axi_rdata[9]_i_3_n_0 ),
        .I1(\axi_rdata[9]_i_4_n_0 ),
        .O(\axi_rdata_reg[9]_i_2_n_0 ),
        .S(sel0[2]));
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(s00_axi_arready),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s00_axi_rvalid),
        .R(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    axi_wready_i_1
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_wready),
        .I3(aw_en_reg_n_0),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(s00_axi_wready),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'hFACF0ACFFAC00AC0)) 
    pll_mosi_reg_i_10
       (.I0(slv_reg3[13]),
        .I1(slv_reg3[12]),
        .I2(bits_send_reg[1]),
        .I3(bits_send_reg[0]),
        .I4(slv_reg3[15]),
        .I5(slv_reg3[14]),
        .O(\slv_reg3_reg[13]_0 ));
  LUT6 #(
    .INIT(64'hFACF0ACFFAC00AC0)) 
    pll_mosi_reg_i_11
       (.I0(slv_reg3[1]),
        .I1(slv_reg3[0]),
        .I2(bits_send_reg[1]),
        .I3(bits_send_reg[0]),
        .I4(slv_reg3[3]),
        .I5(slv_reg3[2]),
        .O(\slv_reg3_reg[1]_0 ));
  LUT6 #(
    .INIT(64'hFACF0ACFFAC00AC0)) 
    pll_mosi_reg_i_12
       (.I0(slv_reg3[5]),
        .I1(slv_reg3[4]),
        .I2(bits_send_reg[1]),
        .I3(bits_send_reg[0]),
        .I4(slv_reg3[7]),
        .I5(slv_reg3[6]),
        .O(\slv_reg3_reg[5]_0 ));
  LUT6 #(
    .INIT(64'hFACF0ACFFAC00AC0)) 
    pll_mosi_reg_i_13
       (.I0(slv_reg3[21]),
        .I1(slv_reg3[20]),
        .I2(bits_send_reg[1]),
        .I3(bits_send_reg[0]),
        .I4(Q),
        .I5(slv_reg3[22]),
        .O(\slv_reg3_reg[21]_0 ));
  LUT6 #(
    .INIT(64'hFACF0ACFFAC00AC0)) 
    pll_mosi_reg_i_14
       (.I0(slv_reg3[17]),
        .I1(slv_reg3[16]),
        .I2(bits_send_reg[1]),
        .I3(bits_send_reg[0]),
        .I4(slv_reg3[19]),
        .I5(slv_reg3[18]),
        .O(\slv_reg3_reg[17]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    pll_mosi_reg_i_6
       (.I0(slv_reg2[1]),
        .I1(slv_reg2[5]),
        .I2(D[0]),
        .I3(slv_reg2[3]),
        .I4(D[1]),
        .I5(slv_reg2[7]),
        .O(\slv_reg2_reg[1]_0 ));
  LUT6 #(
    .INIT(64'h505F3030505F3F3F)) 
    pll_mosi_reg_i_7
       (.I0(slv_reg2[0]),
        .I1(slv_reg2[4]),
        .I2(D[0]),
        .I3(slv_reg2[2]),
        .I4(D[1]),
        .I5(slv_reg2[6]),
        .O(\slv_reg2_reg[0]_0 ));
  LUT6 #(
    .INIT(64'hFACF0ACFFAC00AC0)) 
    pll_mosi_reg_i_9
       (.I0(slv_reg3[9]),
        .I1(slv_reg3[8]),
        .I2(bits_send_reg[1]),
        .I3(bits_send_reg[0]),
        .I4(slv_reg3[11]),
        .I5(slv_reg3[10]),
        .O(\slv_reg3_reg[9]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \slv_reg0[0]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\slv_reg0[1]_i_2_n_0 ),
        .I2(slv_reg0[0]),
        .O(\slv_reg0[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \slv_reg0[1]_i_1 
       (.I0(s00_axi_wdata[1]),
        .I1(\slv_reg0[1]_i_2_n_0 ),
        .I2(slv_reg0[1]),
        .O(\slv_reg0[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \slv_reg0[1]_i_2 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s00_axi_wstrb[0]),
        .O(\slv_reg0[1]_i_2_n_0 ));
  FDRE \slv_reg0_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\slv_reg0[0]_i_1_n_0 ),
        .Q(slv_reg0[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\slv_reg0[1]_i_1_n_0 ),
        .Q(slv_reg0[1]),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg1[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(p_1_in[15]));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg1[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(p_1_in[23]));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg1[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(p_1_in[31]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg1[31]_i_2 
       (.I0(s00_axi_wready),
        .I1(s00_axi_awready),
        .I2(s00_axi_awvalid),
        .I3(s00_axi_wvalid),
        .O(slv_reg_wren__0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg1[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(p_1_in[7]));
  FDRE \slv_reg1_reg[0] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[0]),
        .Q(\slv_reg1_reg[4]_0 [0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[10] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg1[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[11] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg1[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[12] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg1[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[13] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg1[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[14] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg1[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[15] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg1[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[16] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg1[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[17] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg1[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[18] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg1[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[19] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg1[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[1] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[1]),
        .Q(\slv_reg1_reg[4]_0 [1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[20] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg1[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[21] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg1[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[22] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg1[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[23] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg1[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[24] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg1[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[25] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg1[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[26] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg1[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[27] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg1[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[28] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg1[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[29] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg1[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[2] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[2]),
        .Q(\slv_reg1_reg[4]_0 [2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[30] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg1[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[31] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg1[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[3] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[3]),
        .Q(\slv_reg1_reg[4]_0 [3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[4] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[4]),
        .Q(\slv_reg1_reg[4]_0 [4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[5] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg1[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[6] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg1[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[7] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg1[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[8] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg1[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[9] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg1[9]),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg2[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg2[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg2[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg2[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg2[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg2[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg2[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg2[7]_i_1_n_0 ));
  FDRE \slv_reg2_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg2[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg2[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg2[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg2[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg2[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg2[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg2[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg2[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg2[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg2[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg2[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg2[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg2[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg2[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg2[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg2[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg2[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg2[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg2[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg2[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg2[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg2[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg2[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg2[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg2[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg2[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg2[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg2[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg2[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg2[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg2[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg2[9]),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg3[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[3]),
        .O(\slv_reg3[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg3[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[3]),
        .O(\slv_reg3[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg3[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[3]),
        .O(\slv_reg3[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg3[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[3]),
        .O(\slv_reg3[7]_i_1_n_0 ));
  FDRE \slv_reg3_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg3[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg3[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg3[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg3[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg3[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg3[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg3[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg3[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg3[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg3[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg3[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg3[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg3[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg3[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg3[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(Q),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg3[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg3[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg3[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg3[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg3[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg3[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg3[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg3[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg3[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg3[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg3[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg3[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg3[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg3[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg3[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg3[9]),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg4[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[2]),
        .O(\slv_reg4[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg4[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[2]),
        .O(\slv_reg4[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg4[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[2]),
        .O(\slv_reg4[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg4[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[2]),
        .O(\slv_reg4[7]_i_1_n_0 ));
  FDRE \slv_reg4_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(\slv_reg4_reg[5]_0 [0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg4[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg4[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg4[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg4[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg4[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg4[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg4[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg4[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg4[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg4[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(\slv_reg4_reg[5]_0 [1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg4[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg4[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg4[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg4[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg4[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg4[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg4[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg4[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg4[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg4[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(\slv_reg4_reg[5]_0 [2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg4[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg4[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(\slv_reg4_reg[5]_0 [3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(\slv_reg4_reg[5]_0 [4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(\slv_reg4_reg[5]_0 [5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg4[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg4[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg4[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg4[9]),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg5[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg5[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg5[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg5[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg5[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg5[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg5[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg5[7]_i_1_n_0 ));
  FDRE \slv_reg5_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg5[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg5[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg5[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg5[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg5[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg5[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg5[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg5[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg5[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg5[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg5[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg5[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg5[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg5[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg5[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg5[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg5[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg5[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg5[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg5[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg5[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg5[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg5[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg5[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg5[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg5[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg5[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg5[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg5[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg5[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg5[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg5_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg5[9]),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg6[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[3]),
        .O(\slv_reg6[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg6[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[2]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[3]),
        .O(\slv_reg6[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg6[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[3]),
        .O(\slv_reg6[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg6[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[3]),
        .O(\slv_reg6[7]_i_1_n_0 ));
  FDRE \slv_reg6_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg6[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg6[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg6[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg6[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg6[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg6[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg6[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg6[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg6[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg6[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg6[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg6[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg6[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg6[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg6[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg6[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg6[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg6[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg6[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg6[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg6[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg6[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg6[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg6[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg6[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg6[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg6[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg6[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg6[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg6[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg6[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg6_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg6[9]),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg7[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[2]),
        .I2(s00_axi_wstrb[1]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg7[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg7[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[2]),
        .I2(s00_axi_wstrb[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg7[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg7[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[2]),
        .I2(s00_axi_wstrb[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg7[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg7[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[2]),
        .I2(s00_axi_wstrb[0]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg7[7]_i_1_n_0 ));
  FDRE \slv_reg7_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg7[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg7[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg7[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg7[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg7[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg7[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg7[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg7[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg7[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg7[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg7[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg7[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg7[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg7[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg7[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg7[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg7[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg7[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg7[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg7[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg7[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg7[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg7[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg7[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg7[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg7[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg7[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg7[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg7[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg7[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg7[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg7_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg7[9]),
        .R(axi_awready_i_1_n_0));
  LUT3 #(
    .INIT(8'h20)) 
    slv_reg_rden
       (.I0(s00_axi_arvalid),
        .I1(s00_axi_rvalid),
        .I2(s00_axi_arready),
        .O(slv_reg_rden__0));
endmodule

(* ORIG_REF_NAME = "clk_wiz_0" *) 
module design_1_HMC769_0_0_clk_wiz_0
   (clk_out1,
    clk_out2,
    clk_out3,
    clk_in1);
  output clk_out1;
  output clk_out2;
  output clk_out3;
  input clk_in1;

  wire clk_in1;
  wire clk_out1;
  wire clk_out2;
  wire clk_out3;

  design_1_HMC769_0_0_clk_wiz_0_clk_wiz inst
       (.clk_in1(clk_in1),
        .clk_out1(clk_out1),
        .clk_out2(clk_out2),
        .clk_out3(clk_out3));
endmodule

(* ORIG_REF_NAME = "clk_wiz_0_clk_wiz" *) 
module design_1_HMC769_0_0_clk_wiz_0_clk_wiz
   (clk_out1,
    clk_out2,
    clk_out3,
    clk_in1);
  output clk_out1;
  output clk_out2;
  output clk_out3;
  input clk_in1;

  wire clk_in1;
  wire clk_in1_clk_wiz_0;
  wire clk_out1;
  wire clk_out1_clk_wiz_0;
  wire clk_out2;
  wire clk_out2_clk_wiz_0;
  wire clk_out3;
  wire clk_out3_clk_wiz_0;
  wire clkfbout_buf_clk_wiz_0;
  wire clkfbout_clk_wiz_0;
  wire NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED;
  wire NLW_mmcm_adv_inst_DRDY_UNCONNECTED;
  wire NLW_mmcm_adv_inst_LOCKED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_PSDONE_UNCONNECTED;
  wire [15:0]NLW_mmcm_adv_inst_DO_UNCONNECTED;

  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkf_buf
       (.I(clkfbout_clk_wiz_0),
        .O(clkfbout_buf_clk_wiz_0));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUF #(
    .IOSTANDARD("DEFAULT")) 
    clkin1_ibufg
       (.I(clk_in1),
        .O(clk_in1_clk_wiz_0));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout1_buf
       (.I(clk_out1_clk_wiz_0),
        .O(clk_out1));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout2_buf
       (.I(clk_out2_clk_wiz_0),
        .O(clk_out2));
  (* BOX_TYPE = "PRIMITIVE" *) 
  BUFG clkout3_buf
       (.I(clk_out3_clk_wiz_0),
        .O(clk_out3));
  (* BOX_TYPE = "PRIMITIVE" *) 
  MMCME2_ADV #(
    .BANDWIDTH("OPTIMIZED"),
    .CLKFBOUT_MULT_F(10.000000),
    .CLKFBOUT_PHASE(0.000000),
    .CLKFBOUT_USE_FINE_PS("FALSE"),
    .CLKIN1_PERIOD(10.000000),
    .CLKIN2_PERIOD(0.000000),
    .CLKOUT0_DIVIDE_F(10.000000),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT0_USE_FINE_PS("FALSE"),
    .CLKOUT1_DIVIDE(125),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(0.000000),
    .CLKOUT1_USE_FINE_PS("FALSE"),
    .CLKOUT2_DIVIDE(100),
    .CLKOUT2_DUTY_CYCLE(0.500000),
    .CLKOUT2_PHASE(0.000000),
    .CLKOUT2_USE_FINE_PS("FALSE"),
    .CLKOUT3_DIVIDE(1),
    .CLKOUT3_DUTY_CYCLE(0.500000),
    .CLKOUT3_PHASE(0.000000),
    .CLKOUT3_USE_FINE_PS("FALSE"),
    .CLKOUT4_CASCADE("FALSE"),
    .CLKOUT4_DIVIDE(1),
    .CLKOUT4_DUTY_CYCLE(0.500000),
    .CLKOUT4_PHASE(0.000000),
    .CLKOUT4_USE_FINE_PS("FALSE"),
    .CLKOUT5_DIVIDE(1),
    .CLKOUT5_DUTY_CYCLE(0.500000),
    .CLKOUT5_PHASE(0.000000),
    .CLKOUT5_USE_FINE_PS("FALSE"),
    .CLKOUT6_DIVIDE(1),
    .CLKOUT6_DUTY_CYCLE(0.500000),
    .CLKOUT6_PHASE(0.000000),
    .CLKOUT6_USE_FINE_PS("FALSE"),
    .COMPENSATION("ZHOLD"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKINSEL_INVERTED(1'b0),
    .IS_PSEN_INVERTED(1'b0),
    .IS_PSINCDEC_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER1(0.010000),
    .REF_JITTER2(0.010000),
    .SS_EN("FALSE"),
    .SS_MODE("CENTER_HIGH"),
    .SS_MOD_PERIOD(10000),
    .STARTUP_WAIT("FALSE")) 
    mmcm_adv_inst
       (.CLKFBIN(clkfbout_buf_clk_wiz_0),
        .CLKFBOUT(clkfbout_clk_wiz_0),
        .CLKFBOUTB(NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED),
        .CLKFBSTOPPED(NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED),
        .CLKIN1(clk_in1_clk_wiz_0),
        .CLKIN2(1'b0),
        .CLKINSEL(1'b1),
        .CLKINSTOPPED(NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED),
        .CLKOUT0(clk_out1_clk_wiz_0),
        .CLKOUT0B(NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED),
        .CLKOUT1(clk_out2_clk_wiz_0),
        .CLKOUT1B(NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED),
        .CLKOUT2(clk_out3_clk_wiz_0),
        .CLKOUT2B(NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED),
        .CLKOUT3(NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED),
        .CLKOUT3B(NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED),
        .CLKOUT4(NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED),
        .CLKOUT5(NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED),
        .CLKOUT6(NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED),
        .DADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DCLK(1'b0),
        .DEN(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DO(NLW_mmcm_adv_inst_DO_UNCONNECTED[15:0]),
        .DRDY(NLW_mmcm_adv_inst_DRDY_UNCONNECTED),
        .DWE(1'b0),
        .LOCKED(NLW_mmcm_adv_inst_LOCKED_UNCONNECTED),
        .PSCLK(1'b0),
        .PSDONE(NLW_mmcm_adv_inst_PSDONE_UNCONNECTED),
        .PSEN(1'b0),
        .PSINCDEC(1'b0),
        .PWRDWN(1'b0),
        .RST(1'b0));
endmodule

(* ORIG_REF_NAME = "pll_receive" *) 
module design_1_HMC769_0_0_pll_receive
   (pll_mosi_read,
    ip2mb_reg0,
    Q,
    D,
    pll_sck,
    pll_sen,
    START_receive,
    clk_out3,
    pll_ld_sdo,
    pll_mosi_reg_reg_0,
    pll_mosi_reg_reg_1,
    is_clk_running,
    pll_sen_write);
  output pll_mosi_read;
  output [0:0]ip2mb_reg0;
  output [23:0]Q;
  output [1:0]D;
  output pll_sck;
  output pll_sen;
  input START_receive;
  input clk_out3;
  input pll_ld_sdo;
  input pll_mosi_reg_reg_0;
  input pll_mosi_reg_reg_1;
  input is_clk_running;
  input pll_sen_write;

  wire [1:0]D;
  wire [23:0]Q;
  wire START_receive;
  wire \bits_send[0]_i_1_n_0 ;
  wire \bits_send[3]_i_1_n_0 ;
  wire \bits_send[4]_i_2_n_0 ;
  wire \bits_send[4]_i_3_n_0 ;
  wire \bits_send_reg_n_0_[0] ;
  wire \bits_send_reg_n_0_[1] ;
  wire \bits_send_reg_n_0_[2] ;
  wire \bits_send_reg_n_0_[3] ;
  wire \bits_send_reg_n_0_[4] ;
  wire clk_out3;
  wire data_ready_reg7_out;
  wire data_ready_reg_i_1_n_0;
  wire [0:0]ip2mb_reg0;
  wire is_clk_running;
  wire is_first_cycle_going;
  wire is_first_cycle_going_i_1_n_0;
  wire is_second_cycle_going_i_1_n_0;
  wire is_second_cycle_going_reg_n_0;
  wire pll_cs_reg_i_1_n_0;
  wire pll_ld_sdo;
  wire pll_mosi_read;
  wire pll_mosi_reg_i_1_n_0;
  wire pll_mosi_reg_i_2_n_0;
  wire pll_mosi_reg_i_3_n_0;
  wire pll_mosi_reg_i_4_n_0;
  wire pll_mosi_reg_reg_0;
  wire pll_mosi_reg_reg_1;
  wire pll_sck;
  wire pll_sen;
  wire pll_sen_read;
  wire pll_sen_write;
  wire read_data_reg;
  wire read_data_reg025_out;
  wire \read_data_reg[0]_i_1_n_0 ;
  wire \read_data_reg[10]_i_1_n_0 ;
  wire \read_data_reg[11]_i_1_n_0 ;
  wire \read_data_reg[12]_i_1_n_0 ;
  wire \read_data_reg[13]_i_1_n_0 ;
  wire \read_data_reg[14]_i_1_n_0 ;
  wire \read_data_reg[15]_i_1_n_0 ;
  wire \read_data_reg[15]_i_2_n_0 ;
  wire \read_data_reg[16]_i_1_n_0 ;
  wire \read_data_reg[16]_i_2_n_0 ;
  wire \read_data_reg[17]_i_1_n_0 ;
  wire \read_data_reg[17]_i_2_n_0 ;
  wire \read_data_reg[18]_i_1_n_0 ;
  wire \read_data_reg[18]_i_2_n_0 ;
  wire \read_data_reg[19]_i_1_n_0 ;
  wire \read_data_reg[19]_i_2_n_0 ;
  wire \read_data_reg[1]_i_1_n_0 ;
  wire \read_data_reg[20]_i_1_n_0 ;
  wire \read_data_reg[20]_i_2_n_0 ;
  wire \read_data_reg[21]_i_1_n_0 ;
  wire \read_data_reg[22]_i_1_n_0 ;
  wire \read_data_reg[22]_i_2_n_0 ;
  wire \read_data_reg[23]_i_2_n_0 ;
  wire \read_data_reg[23]_i_3_n_0 ;
  wire \read_data_reg[2]_i_1_n_0 ;
  wire \read_data_reg[3]_i_1_n_0 ;
  wire \read_data_reg[4]_i_1_n_0 ;
  wire \read_data_reg[5]_i_1_n_0 ;
  wire \read_data_reg[6]_i_1_n_0 ;
  wire \read_data_reg[7]_i_1_n_0 ;
  wire \read_data_reg[8]_i_1_n_0 ;
  wire \read_data_reg[9]_i_1_n_0 ;
  wire start_prev;

  LUT1 #(
    .INIT(2'h1)) 
    \bits_send[0]_i_1 
       (.I0(\bits_send_reg_n_0_[0] ),
        .O(\bits_send[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \bits_send[1]_i_1 
       (.I0(\bits_send_reg_n_0_[1] ),
        .I1(\bits_send_reg_n_0_[0] ),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \bits_send[2]_i_1 
       (.I0(\bits_send_reg_n_0_[2] ),
        .I1(\bits_send_reg_n_0_[0] ),
        .I2(\bits_send_reg_n_0_[1] ),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \bits_send[3]_i_1 
       (.I0(\bits_send_reg_n_0_[3] ),
        .I1(\bits_send_reg_n_0_[2] ),
        .I2(\bits_send_reg_n_0_[0] ),
        .I3(\bits_send_reg_n_0_[1] ),
        .O(\bits_send[3]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \bits_send[4]_i_1 
       (.I0(START_receive),
        .I1(start_prev),
        .O(read_data_reg025_out));
  LUT2 #(
    .INIT(4'hE)) 
    \bits_send[4]_i_2 
       (.I0(is_second_cycle_going_reg_n_0),
        .I1(is_first_cycle_going),
        .O(\bits_send[4]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \bits_send[4]_i_3 
       (.I0(\bits_send_reg_n_0_[4] ),
        .I1(\bits_send_reg_n_0_[2] ),
        .I2(\bits_send_reg_n_0_[0] ),
        .I3(\bits_send_reg_n_0_[1] ),
        .I4(\bits_send_reg_n_0_[3] ),
        .O(\bits_send[4]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_send_reg[0] 
       (.C(clk_out3),
        .CE(\bits_send[4]_i_2_n_0 ),
        .D(\bits_send[0]_i_1_n_0 ),
        .Q(\bits_send_reg_n_0_[0] ),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_send_reg[1] 
       (.C(clk_out3),
        .CE(\bits_send[4]_i_2_n_0 ),
        .D(D[0]),
        .Q(\bits_send_reg_n_0_[1] ),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_send_reg[2] 
       (.C(clk_out3),
        .CE(\bits_send[4]_i_2_n_0 ),
        .D(D[1]),
        .Q(\bits_send_reg_n_0_[2] ),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_send_reg[3] 
       (.C(clk_out3),
        .CE(\bits_send[4]_i_2_n_0 ),
        .D(\bits_send[3]_i_1_n_0 ),
        .Q(\bits_send_reg_n_0_[3] ),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_send_reg[4] 
       (.C(clk_out3),
        .CE(\bits_send[4]_i_2_n_0 ),
        .D(\bits_send[4]_i_3_n_0 ),
        .Q(\bits_send_reg_n_0_[4] ),
        .R(read_data_reg025_out));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hAB00ABAB)) 
    data_ready_reg_i_1
       (.I0(ip2mb_reg0),
        .I1(is_first_cycle_going),
        .I2(is_second_cycle_going_reg_n_0),
        .I3(start_prev),
        .I4(START_receive),
        .O(data_ready_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    data_ready_reg_reg
       (.C(clk_out3),
        .CE(1'b1),
        .D(data_ready_reg_i_1_n_0),
        .Q(ip2mb_reg0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h7FFFFFFF00000000)) 
    is_first_cycle_going_i_1
       (.I0(\bits_send_reg_n_0_[3] ),
        .I1(\bits_send_reg_n_0_[4] ),
        .I2(\bits_send_reg_n_0_[1] ),
        .I3(\bits_send_reg_n_0_[0] ),
        .I4(\bits_send_reg_n_0_[2] ),
        .I5(is_first_cycle_going),
        .O(is_first_cycle_going_i_1_n_0));
  FDSE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    is_first_cycle_going_reg
       (.C(clk_out3),
        .CE(1'b1),
        .D(is_first_cycle_going_i_1_n_0),
        .Q(is_first_cycle_going),
        .S(read_data_reg025_out));
  LUT6 #(
    .INIT(64'hAAB8AAAAAAAAAAAA)) 
    is_second_cycle_going_i_1
       (.I0(is_second_cycle_going_reg_n_0),
        .I1(read_data_reg025_out),
        .I2(is_first_cycle_going),
        .I3(\read_data_reg[16]_i_2_n_0 ),
        .I4(\bits_send_reg_n_0_[4] ),
        .I5(\bits_send_reg_n_0_[3] ),
        .O(is_second_cycle_going_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    is_second_cycle_going_reg
       (.C(clk_out3),
        .CE(1'b1),
        .D(is_second_cycle_going_i_1_n_0),
        .Q(is_second_cycle_going_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hECECFFECA8A800A8)) 
    pll_cs_reg_i_1
       (.I0(pll_mosi_reg_i_4_n_0),
        .I1(is_first_cycle_going),
        .I2(is_second_cycle_going_reg_n_0),
        .I3(START_receive),
        .I4(start_prev),
        .I5(pll_sen_read),
        .O(pll_cs_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    pll_cs_reg_reg
       (.C(clk_out3),
        .CE(1'b1),
        .D(pll_cs_reg_i_1_n_0),
        .Q(pll_sen_read),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000000022200020)) 
    pll_mosi_reg_i_1
       (.I0(pll_mosi_reg_i_2_n_0),
        .I1(read_data_reg025_out),
        .I2(pll_mosi_reg_i_3_n_0),
        .I3(pll_mosi_reg_i_4_n_0),
        .I4(pll_mosi_read),
        .I5(data_ready_reg7_out),
        .O(pll_mosi_reg_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'hFEEE0000)) 
    pll_mosi_reg_i_2
       (.I0(\bits_send_reg_n_0_[3] ),
        .I1(\bits_send_reg_n_0_[2] ),
        .I2(\bits_send_reg_n_0_[0] ),
        .I3(\bits_send_reg_n_0_[1] ),
        .I4(\bits_send_reg_n_0_[4] ),
        .O(pll_mosi_reg_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000008B8B000000)) 
    pll_mosi_reg_i_3
       (.I0(pll_mosi_reg_reg_0),
        .I1(\bits_send_reg_n_0_[0] ),
        .I2(pll_mosi_reg_reg_1),
        .I3(\bits_send_reg_n_0_[4] ),
        .I4(\read_data_reg[16]_i_2_n_0 ),
        .I5(\bits_send_reg_n_0_[3] ),
        .O(pll_mosi_reg_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    pll_mosi_reg_i_4
       (.I0(\bits_send_reg_n_0_[3] ),
        .I1(\bits_send_reg_n_0_[4] ),
        .I2(\bits_send_reg_n_0_[1] ),
        .I3(\bits_send_reg_n_0_[0] ),
        .I4(\bits_send_reg_n_0_[2] ),
        .O(pll_mosi_reg_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h1)) 
    pll_mosi_reg_i_5
       (.I0(is_first_cycle_going),
        .I1(is_second_cycle_going_reg_n_0),
        .O(data_ready_reg7_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    pll_mosi_reg_reg
       (.C(clk_out3),
        .CE(1'b1),
        .D(pll_mosi_reg_i_1_n_0),
        .Q(pll_mosi_read),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hAAA8)) 
    pll_sck_INST_0
       (.I0(clk_out3),
        .I1(is_clk_running),
        .I2(is_first_cycle_going),
        .I3(is_second_cycle_going_reg_n_0),
        .O(pll_sck));
  LUT2 #(
    .INIT(4'hE)) 
    pll_sen_INST_0
       (.I0(pll_sen_read),
        .I1(pll_sen_write),
        .O(pll_sen));
  LUT5 #(
    .INIT(32'hAABAAAAA)) 
    \read_data_reg[0]_i_1 
       (.I0(Q[0]),
        .I1(\bits_send_reg_n_0_[3] ),
        .I2(pll_ld_sdo),
        .I3(\read_data_reg[16]_i_2_n_0 ),
        .I4(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAEAAAAAAAAAAAAAA)) 
    \read_data_reg[10]_i_1 
       (.I0(Q[10]),
        .I1(\bits_send_reg_n_0_[0] ),
        .I2(\bits_send_reg_n_0_[1] ),
        .I3(pll_ld_sdo),
        .I4(\bits_send_reg_n_0_[2] ),
        .I5(\read_data_reg[15]_i_2_n_0 ),
        .O(\read_data_reg[10]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAABAAAAAAAAAAAAA)) 
    \read_data_reg[11]_i_1 
       (.I0(Q[11]),
        .I1(\bits_send_reg_n_0_[0] ),
        .I2(pll_ld_sdo),
        .I3(\bits_send_reg_n_0_[1] ),
        .I4(\bits_send_reg_n_0_[2] ),
        .I5(\read_data_reg[15]_i_2_n_0 ),
        .O(\read_data_reg[11]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAEAAAAAAAAAAAAAA)) 
    \read_data_reg[12]_i_1 
       (.I0(Q[12]),
        .I1(pll_ld_sdo),
        .I2(\bits_send_reg_n_0_[2] ),
        .I3(\bits_send_reg_n_0_[0] ),
        .I4(\bits_send_reg_n_0_[1] ),
        .I5(\read_data_reg[15]_i_2_n_0 ),
        .O(\read_data_reg[12]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAEAAAAAAAAAA)) 
    \read_data_reg[13]_i_1 
       (.I0(Q[13]),
        .I1(\bits_send_reg_n_0_[1] ),
        .I2(\bits_send_reg_n_0_[0] ),
        .I3(pll_ld_sdo),
        .I4(\bits_send_reg_n_0_[2] ),
        .I5(\read_data_reg[15]_i_2_n_0 ),
        .O(\read_data_reg[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAEAAAAAAAAAA)) 
    \read_data_reg[14]_i_1 
       (.I0(Q[14]),
        .I1(\bits_send_reg_n_0_[0] ),
        .I2(\bits_send_reg_n_0_[1] ),
        .I3(pll_ld_sdo),
        .I4(\bits_send_reg_n_0_[2] ),
        .I5(\read_data_reg[15]_i_2_n_0 ),
        .O(\read_data_reg[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAABAAAAAAAAAA)) 
    \read_data_reg[15]_i_1 
       (.I0(Q[15]),
        .I1(\bits_send_reg_n_0_[2] ),
        .I2(\bits_send_reg_n_0_[0] ),
        .I3(pll_ld_sdo),
        .I4(\bits_send_reg_n_0_[1] ),
        .I5(\read_data_reg[15]_i_2_n_0 ),
        .O(\read_data_reg[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \read_data_reg[15]_i_2 
       (.I0(\bits_send_reg_n_0_[3] ),
        .I1(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[15]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'hAAAAAABA)) 
    \read_data_reg[16]_i_1 
       (.I0(Q[16]),
        .I1(\bits_send_reg_n_0_[3] ),
        .I2(pll_ld_sdo),
        .I3(\read_data_reg[16]_i_2_n_0 ),
        .I4(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[16]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \read_data_reg[16]_i_2 
       (.I0(\bits_send_reg_n_0_[2] ),
        .I1(\bits_send_reg_n_0_[0] ),
        .I2(\bits_send_reg_n_0_[1] ),
        .O(\read_data_reg[16]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hAB)) 
    \read_data_reg[17]_i_1 
       (.I0(Q[17]),
        .I1(\read_data_reg[17]_i_2_n_0 ),
        .I2(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[17]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'hFFBFFFFF)) 
    \read_data_reg[17]_i_2 
       (.I0(\bits_send_reg_n_0_[3] ),
        .I1(\bits_send_reg_n_0_[2] ),
        .I2(pll_ld_sdo),
        .I3(\bits_send_reg_n_0_[0] ),
        .I4(\bits_send_reg_n_0_[1] ),
        .O(\read_data_reg[17]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hAB)) 
    \read_data_reg[18]_i_1 
       (.I0(Q[18]),
        .I1(\read_data_reg[18]_i_2_n_0 ),
        .I2(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[18]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'hFFBFFFFF)) 
    \read_data_reg[18]_i_2 
       (.I0(\bits_send_reg_n_0_[3] ),
        .I1(\bits_send_reg_n_0_[2] ),
        .I2(pll_ld_sdo),
        .I3(\bits_send_reg_n_0_[1] ),
        .I4(\bits_send_reg_n_0_[0] ),
        .O(\read_data_reg[18]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hAB)) 
    \read_data_reg[19]_i_1 
       (.I0(Q[19]),
        .I1(\read_data_reg[19]_i_2_n_0 ),
        .I2(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[19]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'hFFFFFBFF)) 
    \read_data_reg[19]_i_2 
       (.I0(\bits_send_reg_n_0_[3] ),
        .I1(\bits_send_reg_n_0_[2] ),
        .I2(\bits_send_reg_n_0_[1] ),
        .I3(pll_ld_sdo),
        .I4(\bits_send_reg_n_0_[0] ),
        .O(\read_data_reg[19]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \read_data_reg[1]_i_1 
       (.I0(Q[1]),
        .I1(\read_data_reg[17]_i_2_n_0 ),
        .I2(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAABAAAAA)) 
    \read_data_reg[20]_i_1 
       (.I0(Q[20]),
        .I1(\bits_send_reg_n_0_[3] ),
        .I2(\read_data_reg[20]_i_2_n_0 ),
        .I3(\bits_send_reg_n_0_[2] ),
        .I4(pll_ld_sdo),
        .I5(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[20]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \read_data_reg[20]_i_2 
       (.I0(\bits_send_reg_n_0_[1] ),
        .I1(\bits_send_reg_n_0_[0] ),
        .O(\read_data_reg[20]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAABAAAA)) 
    \read_data_reg[21]_i_1 
       (.I0(Q[21]),
        .I1(\bits_send_reg_n_0_[3] ),
        .I2(\read_data_reg[22]_i_2_n_0 ),
        .I3(\bits_send_reg_n_0_[0] ),
        .I4(\bits_send_reg_n_0_[1] ),
        .I5(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[21]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAABAAAA)) 
    \read_data_reg[22]_i_1 
       (.I0(Q[22]),
        .I1(\bits_send_reg_n_0_[3] ),
        .I2(\read_data_reg[22]_i_2_n_0 ),
        .I3(\bits_send_reg_n_0_[1] ),
        .I4(\bits_send_reg_n_0_[0] ),
        .I5(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[22]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \read_data_reg[22]_i_2 
       (.I0(\bits_send_reg_n_0_[2] ),
        .I1(pll_ld_sdo),
        .O(\read_data_reg[22]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h002A)) 
    \read_data_reg[23]_i_1 
       (.I0(is_second_cycle_going_reg_n_0),
        .I1(\bits_send_reg_n_0_[4] ),
        .I2(\bits_send_reg_n_0_[3] ),
        .I3(is_first_cycle_going),
        .O(read_data_reg));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hAB)) 
    \read_data_reg[23]_i_2 
       (.I0(Q[23]),
        .I1(\read_data_reg[23]_i_3_n_0 ),
        .I2(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[23]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'hFFFFFFEF)) 
    \read_data_reg[23]_i_3 
       (.I0(\bits_send_reg_n_0_[3] ),
        .I1(\bits_send_reg_n_0_[1] ),
        .I2(pll_ld_sdo),
        .I3(\bits_send_reg_n_0_[0] ),
        .I4(\bits_send_reg_n_0_[2] ),
        .O(\read_data_reg[23]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \read_data_reg[2]_i_1 
       (.I0(Q[2]),
        .I1(\read_data_reg[18]_i_2_n_0 ),
        .I2(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \read_data_reg[3]_i_1 
       (.I0(Q[3]),
        .I1(\read_data_reg[19]_i_2_n_0 ),
        .I2(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAABAAAAAAAAAAAAA)) 
    \read_data_reg[4]_i_1 
       (.I0(Q[4]),
        .I1(\bits_send_reg_n_0_[3] ),
        .I2(\read_data_reg[20]_i_2_n_0 ),
        .I3(\bits_send_reg_n_0_[2] ),
        .I4(pll_ld_sdo),
        .I5(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAABAAAAAAAAAAAA)) 
    \read_data_reg[5]_i_1 
       (.I0(Q[5]),
        .I1(\bits_send_reg_n_0_[3] ),
        .I2(\read_data_reg[22]_i_2_n_0 ),
        .I3(\bits_send_reg_n_0_[0] ),
        .I4(\bits_send_reg_n_0_[1] ),
        .I5(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAABAAAAAAAAAAAA)) 
    \read_data_reg[6]_i_1 
       (.I0(Q[6]),
        .I1(\bits_send_reg_n_0_[3] ),
        .I2(\read_data_reg[22]_i_2_n_0 ),
        .I3(\bits_send_reg_n_0_[1] ),
        .I4(\bits_send_reg_n_0_[0] ),
        .I5(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \read_data_reg[7]_i_1 
       (.I0(Q[7]),
        .I1(\read_data_reg[23]_i_3_n_0 ),
        .I2(\bits_send_reg_n_0_[4] ),
        .O(\read_data_reg[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hEAAAAAAAAAAAAAAA)) 
    \read_data_reg[8]_i_1 
       (.I0(Q[8]),
        .I1(\bits_send_reg_n_0_[1] ),
        .I2(\bits_send_reg_n_0_[0] ),
        .I3(\bits_send_reg_n_0_[2] ),
        .I4(pll_ld_sdo),
        .I5(\read_data_reg[15]_i_2_n_0 ),
        .O(\read_data_reg[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAEAAAAAAAAAAAAAA)) 
    \read_data_reg[9]_i_1 
       (.I0(Q[9]),
        .I1(\bits_send_reg_n_0_[1] ),
        .I2(\bits_send_reg_n_0_[0] ),
        .I3(pll_ld_sdo),
        .I4(\bits_send_reg_n_0_[2] ),
        .I5(\read_data_reg[15]_i_2_n_0 ),
        .O(\read_data_reg[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[0] 
       (.C(clk_out3),
        .CE(read_data_reg),
        .D(\read_data_reg[0]_i_1_n_0 ),
        .Q(Q[0]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[10] 
       (.C(clk_out3),
        .CE(read_data_reg),
        .D(\read_data_reg[10]_i_1_n_0 ),
        .Q(Q[10]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[11] 
       (.C(clk_out3),
        .CE(read_data_reg),
        .D(\read_data_reg[11]_i_1_n_0 ),
        .Q(Q[11]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[12] 
       (.C(clk_out3),
        .CE(read_data_reg),
        .D(\read_data_reg[12]_i_1_n_0 ),
        .Q(Q[12]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[13] 
       (.C(clk_out3),
        .CE(read_data_reg),
        .D(\read_data_reg[13]_i_1_n_0 ),
        .Q(Q[13]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[14] 
       (.C(clk_out3),
        .CE(read_data_reg),
        .D(\read_data_reg[14]_i_1_n_0 ),
        .Q(Q[14]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[15] 
       (.C(clk_out3),
        .CE(read_data_reg),
        .D(\read_data_reg[15]_i_1_n_0 ),
        .Q(Q[15]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[16] 
       (.C(clk_out3),
        .CE(read_data_reg),
        .D(\read_data_reg[16]_i_1_n_0 ),
        .Q(Q[16]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[17] 
       (.C(clk_out3),
        .CE(read_data_reg),
        .D(\read_data_reg[17]_i_1_n_0 ),
        .Q(Q[17]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[18] 
       (.C(clk_out3),
        .CE(read_data_reg),
        .D(\read_data_reg[18]_i_1_n_0 ),
        .Q(Q[18]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[19] 
       (.C(clk_out3),
        .CE(read_data_reg),
        .D(\read_data_reg[19]_i_1_n_0 ),
        .Q(Q[19]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[1] 
       (.C(clk_out3),
        .CE(read_data_reg),
        .D(\read_data_reg[1]_i_1_n_0 ),
        .Q(Q[1]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[20] 
       (.C(clk_out3),
        .CE(read_data_reg),
        .D(\read_data_reg[20]_i_1_n_0 ),
        .Q(Q[20]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[21] 
       (.C(clk_out3),
        .CE(read_data_reg),
        .D(\read_data_reg[21]_i_1_n_0 ),
        .Q(Q[21]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[22] 
       (.C(clk_out3),
        .CE(read_data_reg),
        .D(\read_data_reg[22]_i_1_n_0 ),
        .Q(Q[22]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[23] 
       (.C(clk_out3),
        .CE(read_data_reg),
        .D(\read_data_reg[23]_i_2_n_0 ),
        .Q(Q[23]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[2] 
       (.C(clk_out3),
        .CE(read_data_reg),
        .D(\read_data_reg[2]_i_1_n_0 ),
        .Q(Q[2]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[3] 
       (.C(clk_out3),
        .CE(read_data_reg),
        .D(\read_data_reg[3]_i_1_n_0 ),
        .Q(Q[3]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[4] 
       (.C(clk_out3),
        .CE(read_data_reg),
        .D(\read_data_reg[4]_i_1_n_0 ),
        .Q(Q[4]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[5] 
       (.C(clk_out3),
        .CE(read_data_reg),
        .D(\read_data_reg[5]_i_1_n_0 ),
        .Q(Q[5]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[6] 
       (.C(clk_out3),
        .CE(read_data_reg),
        .D(\read_data_reg[6]_i_1_n_0 ),
        .Q(Q[6]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[7] 
       (.C(clk_out3),
        .CE(read_data_reg),
        .D(\read_data_reg[7]_i_1_n_0 ),
        .Q(Q[7]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[8] 
       (.C(clk_out3),
        .CE(read_data_reg),
        .D(\read_data_reg[8]_i_1_n_0 ),
        .Q(Q[8]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \read_data_reg_reg[9] 
       (.C(clk_out3),
        .CE(read_data_reg),
        .D(\read_data_reg[9]_i_1_n_0 ),
        .Q(Q[9]),
        .R(read_data_reg025_out));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    start_prev_reg
       (.C(clk_out3),
        .CE(1'b1),
        .D(START_receive),
        .Q(start_prev),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "pll_send" *) 
module design_1_HMC769_0_0_pll_send
   (\bits_send_reg[1]_0 ,
    \bits_send_reg[0]_0 ,
    is_clk_running,
    pll_sen_write,
    pll_mosi,
    START_send,
    clk_out3,
    pll_mosi_reg_i_2__0_0,
    pll_mosi_reg_i_2__0_1,
    pll_mosi_reg_i_2__0_2,
    pll_mosi_reg_i_2__0_3,
    pll_mosi_reg_i_2__0_4,
    pll_mosi_reg_i_2__0_5,
    pll_mosi_reg_i_8_0,
    pll_mosi_read,
    pll_mosi_reg_reg_0);
  output \bits_send_reg[1]_0 ;
  output \bits_send_reg[0]_0 ;
  output is_clk_running;
  output pll_sen_write;
  output pll_mosi;
  input START_send;
  input clk_out3;
  input pll_mosi_reg_i_2__0_0;
  input pll_mosi_reg_i_2__0_1;
  input pll_mosi_reg_i_2__0_2;
  input pll_mosi_reg_i_2__0_3;
  input pll_mosi_reg_i_2__0_4;
  input pll_mosi_reg_i_2__0_5;
  input [4:0]pll_mosi_reg_i_8_0;
  input pll_mosi_read;
  input [0:0]pll_mosi_reg_reg_0;

  wire START_send;
  wire bits_send0;
  wire \bits_send[0]_i_1__0_n_0 ;
  wire \bits_send[1]_i_1__0_n_0 ;
  wire \bits_send[2]_i_1__0_n_0 ;
  wire \bits_send[3]_i_1__0_n_0 ;
  wire \bits_send[4]_i_1__0_n_0 ;
  wire \bits_send[4]_i_2__0_n_0 ;
  wire [4:2]bits_send_reg;
  wire \bits_send_reg[0]_0 ;
  wire \bits_send_reg[1]_0 ;
  wire clk_out3;
  wire is_clk_running;
  wire is_clk_running_i_1_n_0;
  wire pll_cs_reg_i_1__0_n_0;
  wire pll_mosi;
  wire pll_mosi_read;
  wire pll_mosi_reg_i_15_n_0;
  wire pll_mosi_reg_i_16_n_0;
  wire pll_mosi_reg_i_1__0_n_0;
  wire pll_mosi_reg_i_2__0_0;
  wire pll_mosi_reg_i_2__0_1;
  wire pll_mosi_reg_i_2__0_2;
  wire pll_mosi_reg_i_2__0_3;
  wire pll_mosi_reg_i_2__0_4;
  wire pll_mosi_reg_i_2__0_5;
  wire pll_mosi_reg_i_2__0_n_0;
  wire pll_mosi_reg_i_3__0_n_0;
  wire pll_mosi_reg_i_4__0_n_0;
  wire pll_mosi_reg_i_5__0_n_0;
  wire pll_mosi_reg_i_6__0_n_0;
  wire pll_mosi_reg_i_7__0_n_0;
  wire [4:0]pll_mosi_reg_i_8_0;
  wire pll_mosi_reg_i_8_n_0;
  wire [0:0]pll_mosi_reg_reg_0;
  wire pll_mosi_write;
  wire pll_sen_write;
  wire start_prev;

  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT4 #(
    .INIT(16'h6066)) 
    \bits_send[0]_i_1__0 
       (.I0(\bits_send_reg[0]_0 ),
        .I1(is_clk_running),
        .I2(start_prev),
        .I3(START_send),
        .O(\bits_send[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT5 #(
    .INIT(32'h6A006A6A)) 
    \bits_send[1]_i_1__0 
       (.I0(\bits_send_reg[1]_0 ),
        .I1(is_clk_running),
        .I2(\bits_send_reg[0]_0 ),
        .I3(start_prev),
        .I4(START_send),
        .O(\bits_send[1]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h6AAA00006AAA6AAA)) 
    \bits_send[2]_i_1__0 
       (.I0(bits_send_reg[2]),
        .I1(\bits_send_reg[0]_0 ),
        .I2(\bits_send_reg[1]_0 ),
        .I3(is_clk_running),
        .I4(start_prev),
        .I5(START_send),
        .O(\bits_send[2]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h1230303030303030)) 
    \bits_send[3]_i_1__0 
       (.I0(is_clk_running),
        .I1(bits_send0),
        .I2(bits_send_reg[3]),
        .I3(\bits_send_reg[1]_0 ),
        .I4(\bits_send_reg[0]_0 ),
        .I5(bits_send_reg[2]),
        .O(\bits_send[3]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h00000000FF7F0080)) 
    \bits_send[4]_i_1__0 
       (.I0(is_clk_running),
        .I1(bits_send_reg[3]),
        .I2(bits_send_reg[2]),
        .I3(\bits_send[4]_i_2__0_n_0 ),
        .I4(bits_send_reg[4]),
        .I5(bits_send0),
        .O(\bits_send[4]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \bits_send[4]_i_2__0 
       (.I0(\bits_send_reg[1]_0 ),
        .I1(\bits_send_reg[0]_0 ),
        .O(\bits_send[4]_i_2__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \bits_send[4]_i_3__0 
       (.I0(START_send),
        .I1(start_prev),
        .O(bits_send0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_send_reg[0] 
       (.C(clk_out3),
        .CE(1'b1),
        .D(\bits_send[0]_i_1__0_n_0 ),
        .Q(\bits_send_reg[0]_0 ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_send_reg[1] 
       (.C(clk_out3),
        .CE(1'b1),
        .D(\bits_send[1]_i_1__0_n_0 ),
        .Q(\bits_send_reg[1]_0 ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_send_reg[2] 
       (.C(clk_out3),
        .CE(1'b1),
        .D(\bits_send[2]_i_1__0_n_0 ),
        .Q(bits_send_reg[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_send_reg[3] 
       (.C(clk_out3),
        .CE(1'b1),
        .D(\bits_send[3]_i_1__0_n_0 ),
        .Q(bits_send_reg[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \bits_send_reg[4] 
       (.C(clk_out3),
        .CE(1'b1),
        .D(\bits_send[4]_i_1__0_n_0 ),
        .Q(bits_send_reg[4]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFFAA2AAAAA)) 
    is_clk_running_i_1
       (.I0(is_clk_running),
        .I1(bits_send_reg[3]),
        .I2(bits_send_reg[2]),
        .I3(\bits_send[4]_i_2__0_n_0 ),
        .I4(bits_send_reg[4]),
        .I5(bits_send0),
        .O(is_clk_running_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    is_clk_running_reg
       (.C(clk_out3),
        .CE(1'b1),
        .D(is_clk_running_i_1_n_0),
        .Q(is_clk_running),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT5 #(
    .INIT(32'hB0AAB0B0)) 
    pll_cs_reg_i_1__0
       (.I0(pll_sen_write),
        .I1(pll_mosi_reg_i_3__0_n_0),
        .I2(is_clk_running),
        .I3(start_prev),
        .I4(START_send),
        .O(pll_cs_reg_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    pll_cs_reg_reg
       (.C(clk_out3),
        .CE(1'b1),
        .D(pll_cs_reg_i_1__0_n_0),
        .Q(pll_sen_write),
        .R(1'b0));
  LUT2 #(
    .INIT(4'hE)) 
    pll_mosi_INST_0
       (.I0(pll_mosi_write),
        .I1(pll_mosi_read),
        .O(pll_mosi));
  LUT6 #(
    .INIT(64'h00800000FF000000)) 
    pll_mosi_reg_i_15
       (.I0(\bits_send_reg[0]_0 ),
        .I1(\bits_send_reg[1]_0 ),
        .I2(pll_mosi_reg_i_8_0[4]),
        .I3(bits_send_reg[3]),
        .I4(bits_send_reg[4]),
        .I5(bits_send_reg[2]),
        .O(pll_mosi_reg_i_15_n_0));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'hC808C8C8)) 
    pll_mosi_reg_i_16
       (.I0(pll_mosi_reg_i_8_0[1]),
        .I1(\bits_send_reg[1]_0 ),
        .I2(\bits_send_reg[0]_0 ),
        .I3(pll_mosi_reg_i_8_0[0]),
        .I4(bits_send_reg[3]),
        .O(pll_mosi_reg_i_16_n_0));
  LUT6 #(
    .INIT(64'hACA0AFA0ACA0A0A0)) 
    pll_mosi_reg_i_1__0
       (.I0(pll_mosi_reg_reg_0),
        .I1(pll_mosi_reg_i_2__0_n_0),
        .I2(bits_send0),
        .I3(is_clk_running),
        .I4(pll_mosi_reg_i_3__0_n_0),
        .I5(pll_mosi_write),
        .O(pll_mosi_reg_i_1__0_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFF0DDF0DD0)) 
    pll_mosi_reg_i_2__0
       (.I0(pll_mosi_reg_i_4__0_n_0),
        .I1(pll_mosi_reg_i_5__0_n_0),
        .I2(bits_send_reg[4]),
        .I3(pll_mosi_reg_i_6__0_n_0),
        .I4(pll_mosi_reg_i_7__0_n_0),
        .I5(pll_mosi_reg_i_8_n_0),
        .O(pll_mosi_reg_i_2__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'h7FFFFFFF)) 
    pll_mosi_reg_i_3__0
       (.I0(bits_send_reg[3]),
        .I1(bits_send_reg[2]),
        .I2(\bits_send_reg[1]_0 ),
        .I3(\bits_send_reg[0]_0 ),
        .I4(bits_send_reg[4]),
        .O(pll_mosi_reg_i_3__0_n_0));
  LUT6 #(
    .INIT(64'hA7777555F7777FFF)) 
    pll_mosi_reg_i_4__0
       (.I0(bits_send_reg[3]),
        .I1(pll_mosi_reg_i_2__0_2),
        .I2(\bits_send_reg[1]_0 ),
        .I3(\bits_send_reg[0]_0 ),
        .I4(bits_send_reg[2]),
        .I5(pll_mosi_reg_i_2__0_3),
        .O(pll_mosi_reg_i_4__0_n_0));
  LUT6 #(
    .INIT(64'hC00000000AACACAC)) 
    pll_mosi_reg_i_5__0
       (.I0(pll_mosi_reg_i_2__0_4),
        .I1(pll_mosi_reg_i_2__0_5),
        .I2(bits_send_reg[2]),
        .I3(\bits_send_reg[0]_0 ),
        .I4(\bits_send_reg[1]_0 ),
        .I5(bits_send_reg[3]),
        .O(pll_mosi_reg_i_5__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'hEAAA)) 
    pll_mosi_reg_i_6__0
       (.I0(bits_send_reg[3]),
        .I1(\bits_send_reg[1]_0 ),
        .I2(\bits_send_reg[0]_0 ),
        .I3(bits_send_reg[2]),
        .O(pll_mosi_reg_i_6__0_n_0));
  LUT5 #(
    .INIT(32'hBEEE8222)) 
    pll_mosi_reg_i_7__0
       (.I0(pll_mosi_reg_i_2__0_0),
        .I1(bits_send_reg[2]),
        .I2(\bits_send_reg[0]_0 ),
        .I3(\bits_send_reg[1]_0 ),
        .I4(pll_mosi_reg_i_2__0_1),
        .O(pll_mosi_reg_i_7__0_n_0));
  LUT6 #(
    .INIT(64'h88A888AA88A88888)) 
    pll_mosi_reg_i_8
       (.I0(pll_mosi_reg_i_15_n_0),
        .I1(pll_mosi_reg_i_16_n_0),
        .I2(pll_mosi_reg_i_8_0[2]),
        .I3(\bits_send_reg[1]_0 ),
        .I4(\bits_send_reg[0]_0 ),
        .I5(pll_mosi_reg_i_8_0[3]),
        .O(pll_mosi_reg_i_8_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    pll_mosi_reg_reg
       (.C(clk_out3),
        .CE(1'b1),
        .D(pll_mosi_reg_i_1__0_n_0),
        .Q(pll_mosi_write),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    start_prev_reg
       (.C(clk_out3),
        .CE(1'b1),
        .D(START_send),
        .Q(start_prev),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "top_PLL_control" *) 
module design_1_HMC769_0_0_top_PLL_control
   (start_adc_count,
    \bits_send_reg[1] ,
    ip2mb_reg0,
    pll_trig,
    Q,
    D,
    pll_sck,
    pll_sen,
    pll_mosi,
    ATTEN,
    s00_axi_aclk,
    slv_reg0,
    pll_ld_sdo,
    pll_mosi_reg_reg,
    pll_mosi_reg_reg_0,
    pll_mosi_reg_i_2__0,
    pll_mosi_reg_i_2__0_0,
    pll_mosi_reg_i_2__0_1,
    pll_mosi_reg_i_2__0_2,
    pll_mosi_reg_i_2__0_3,
    pll_mosi_reg_i_2__0_4,
    pll_mosi_reg_i_8,
    pll_mosi_reg_reg_1,
    \atten_reg_reg[5]_0 ,
    shift_front);
  output start_adc_count;
  output [1:0]\bits_send_reg[1] ;
  output [0:0]ip2mb_reg0;
  output pll_trig;
  output [23:0]Q;
  output [1:0]D;
  output pll_sck;
  output pll_sen;
  output pll_mosi;
  output [5:0]ATTEN;
  input s00_axi_aclk;
  input [1:0]slv_reg0;
  input pll_ld_sdo;
  input pll_mosi_reg_reg;
  input pll_mosi_reg_reg_0;
  input pll_mosi_reg_i_2__0;
  input pll_mosi_reg_i_2__0_0;
  input pll_mosi_reg_i_2__0_1;
  input pll_mosi_reg_i_2__0_2;
  input pll_mosi_reg_i_2__0_3;
  input pll_mosi_reg_i_2__0_4;
  input [4:0]pll_mosi_reg_i_8;
  input [0:0]pll_mosi_reg_reg_1;
  input [5:0]\atten_reg_reg[5]_0 ;
  input [15:0]shift_front;

  wire [5:0]ATTEN;
  wire [1:0]D;
  wire [23:0]Q;
  wire START_receive;
  wire START_send;
  wire [5:0]\atten_reg_reg[5]_0 ;
  wire [1:0]\bits_send_reg[1] ;
  wire clk_8MHz;
  wire clk_pll_spi;
  wire [0:0]ip2mb_reg0;
  wire is_clk_running;
  wire [15:1]p_2_in;
  wire pll_ld_sdo;
  wire pll_mosi;
  wire pll_mosi_read;
  wire pll_mosi_reg_i_2__0;
  wire pll_mosi_reg_i_2__0_0;
  wire pll_mosi_reg_i_2__0_1;
  wire pll_mosi_reg_i_2__0_2;
  wire pll_mosi_reg_i_2__0_3;
  wire pll_mosi_reg_i_2__0_4;
  wire [4:0]pll_mosi_reg_i_8;
  wire pll_mosi_reg_reg;
  wire pll_mosi_reg_reg_0;
  wire [0:0]pll_mosi_reg_reg_1;
  wire pll_sck;
  wire pll_sen;
  wire pll_sen_write;
  wire pll_trig;
  wire pll_trig_reg_i_1_n_0;
  wire s00_axi_aclk;
  wire [15:0]shift_front;
  wire [1:0]slv_reg0;
  wire start_adc_count;
  wire start_adc_count_r0;
  wire start_adc_count_r1;
  wire start_adc_count_r1_carry__0_i_1_n_0;
  wire start_adc_count_r1_carry__0_i_2_n_0;
  wire start_adc_count_r1_carry__0_i_3_n_0;
  wire start_adc_count_r1_carry__0_i_4_n_0;
  wire start_adc_count_r1_carry__0_i_5_n_0;
  wire start_adc_count_r1_carry__0_i_6_n_0;
  wire start_adc_count_r1_carry__0_i_7_n_0;
  wire start_adc_count_r1_carry__0_i_8_n_0;
  wire start_adc_count_r1_carry__0_n_1;
  wire start_adc_count_r1_carry__0_n_2;
  wire start_adc_count_r1_carry__0_n_3;
  wire start_adc_count_r1_carry_i_1_n_0;
  wire start_adc_count_r1_carry_i_2_n_0;
  wire start_adc_count_r1_carry_i_3_n_0;
  wire start_adc_count_r1_carry_i_4_n_0;
  wire start_adc_count_r1_carry_i_5_n_0;
  wire start_adc_count_r1_carry_i_6_n_0;
  wire start_adc_count_r1_carry_i_7_n_0;
  wire start_adc_count_r1_carry_i_8_n_0;
  wire start_adc_count_r1_carry_n_0;
  wire start_adc_count_r1_carry_n_1;
  wire start_adc_count_r1_carry_n_2;
  wire start_adc_count_r1_carry_n_3;
  wire start_adc_count_r_i_2_n_0;
  wire start_adc_count_r_i_3_n_0;
  wire start_adc_count_r_i_4_n_0;
  wire start_adc_count_r_i_5_n_0;
  wire trig_tguard_counter;
  wire \trig_tguard_counter[0]_i_1_n_0 ;
  wire \trig_tguard_counter[0]_i_2_n_0 ;
  wire \trig_tguard_counter[0]_i_3_n_0 ;
  wire \trig_tguard_counter[0]_i_4_n_0 ;
  wire \trig_tguard_counter[15]_i_10_n_0 ;
  wire \trig_tguard_counter[15]_i_11_n_0 ;
  wire \trig_tguard_counter[15]_i_12_n_0 ;
  wire \trig_tguard_counter[15]_i_13_n_0 ;
  wire \trig_tguard_counter[15]_i_14_n_0 ;
  wire \trig_tguard_counter[15]_i_15_n_0 ;
  wire \trig_tguard_counter[15]_i_16_n_0 ;
  wire \trig_tguard_counter[15]_i_17_n_0 ;
  wire \trig_tguard_counter[15]_i_18_n_0 ;
  wire \trig_tguard_counter[15]_i_19_n_0 ;
  wire \trig_tguard_counter[15]_i_1_n_0 ;
  wire \trig_tguard_counter[15]_i_4_n_0 ;
  wire \trig_tguard_counter[15]_i_5_n_0 ;
  wire \trig_tguard_counter[15]_i_6_n_0 ;
  wire \trig_tguard_counter[15]_i_7_n_0 ;
  wire \trig_tguard_counter[15]_i_8_n_0 ;
  wire \trig_tguard_counter[15]_i_9_n_0 ;
  wire \trig_tguard_counter_reg[12]_i_1_n_0 ;
  wire \trig_tguard_counter_reg[12]_i_1_n_1 ;
  wire \trig_tguard_counter_reg[12]_i_1_n_2 ;
  wire \trig_tguard_counter_reg[12]_i_1_n_3 ;
  wire \trig_tguard_counter_reg[15]_i_3_n_2 ;
  wire \trig_tguard_counter_reg[15]_i_3_n_3 ;
  wire \trig_tguard_counter_reg[4]_i_1_n_0 ;
  wire \trig_tguard_counter_reg[4]_i_1_n_1 ;
  wire \trig_tguard_counter_reg[4]_i_1_n_2 ;
  wire \trig_tguard_counter_reg[4]_i_1_n_3 ;
  wire \trig_tguard_counter_reg[8]_i_1_n_0 ;
  wire \trig_tguard_counter_reg[8]_i_1_n_1 ;
  wire \trig_tguard_counter_reg[8]_i_1_n_2 ;
  wire \trig_tguard_counter_reg[8]_i_1_n_3 ;
  wire \trig_tguard_counter_reg_n_0_[0] ;
  wire \trig_tguard_counter_reg_n_0_[10] ;
  wire \trig_tguard_counter_reg_n_0_[11] ;
  wire \trig_tguard_counter_reg_n_0_[12] ;
  wire \trig_tguard_counter_reg_n_0_[13] ;
  wire \trig_tguard_counter_reg_n_0_[14] ;
  wire \trig_tguard_counter_reg_n_0_[15] ;
  wire \trig_tguard_counter_reg_n_0_[1] ;
  wire \trig_tguard_counter_reg_n_0_[2] ;
  wire \trig_tguard_counter_reg_n_0_[3] ;
  wire \trig_tguard_counter_reg_n_0_[4] ;
  wire \trig_tguard_counter_reg_n_0_[5] ;
  wire \trig_tguard_counter_reg_n_0_[6] ;
  wire \trig_tguard_counter_reg_n_0_[7] ;
  wire \trig_tguard_counter_reg_n_0_[8] ;
  wire \trig_tguard_counter_reg_n_0_[9] ;
  wire trig_tsweep_counter07_out;
  wire \trig_tsweep_counter[0]_i_1_n_0 ;
  wire \trig_tsweep_counter[0]_i_4_n_0 ;
  wire \trig_tsweep_counter[0]_i_5_n_0 ;
  wire \trig_tsweep_counter[0]_i_6_n_0 ;
  wire [15:0]trig_tsweep_counter_reg;
  wire \trig_tsweep_counter_reg[0]_i_3_n_0 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_1 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_2 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_3 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_4 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_5 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_6 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_7 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_1 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_2 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_3 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_4 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_5 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_6 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_7 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_0 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_1 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_2 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_3 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_4 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_5 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_6 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_7 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_0 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_1 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_2 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_3 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_4 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_5 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_6 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_7 ;
  wire NLW_clk_wizard_main_clk_out1_UNCONNECTED;
  wire [3:0]NLW_start_adc_count_r1_carry_O_UNCONNECTED;
  wire [3:0]NLW_start_adc_count_r1_carry__0_O_UNCONNECTED;
  wire [3:2]\NLW_trig_tguard_counter_reg[15]_i_3_CO_UNCONNECTED ;
  wire [3:3]\NLW_trig_tguard_counter_reg[15]_i_3_O_UNCONNECTED ;
  wire [3:3]\NLW_trig_tsweep_counter_reg[12]_i_1_CO_UNCONNECTED ;

  FDRE START_receive_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(slv_reg0[1]),
        .Q(START_receive),
        .R(1'b0));
  FDRE START_send_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(slv_reg0[0]),
        .Q(START_send),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \atten_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\atten_reg_reg[5]_0 [0]),
        .Q(ATTEN[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \atten_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\atten_reg_reg[5]_0 [1]),
        .Q(ATTEN[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \atten_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\atten_reg_reg[5]_0 [2]),
        .Q(ATTEN[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \atten_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\atten_reg_reg[5]_0 [3]),
        .Q(ATTEN[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \atten_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\atten_reg_reg[5]_0 [4]),
        .Q(ATTEN[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \atten_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\atten_reg_reg[5]_0 [5]),
        .Q(ATTEN[5]),
        .R(1'b0));
  design_1_HMC769_0_0_clk_wiz_0 clk_wizard_main
       (.clk_in1(s00_axi_aclk),
        .clk_out1(NLW_clk_wizard_main_clk_out1_UNCONNECTED),
        .clk_out2(clk_8MHz),
        .clk_out3(clk_pll_spi));
  design_1_HMC769_0_0_pll_receive pll_receive
       (.D(D),
        .Q(Q),
        .START_receive(START_receive),
        .clk_out3(clk_pll_spi),
        .ip2mb_reg0(ip2mb_reg0),
        .is_clk_running(is_clk_running),
        .pll_ld_sdo(pll_ld_sdo),
        .pll_mosi_read(pll_mosi_read),
        .pll_mosi_reg_reg_0(pll_mosi_reg_reg),
        .pll_mosi_reg_reg_1(pll_mosi_reg_reg_0),
        .pll_sck(pll_sck),
        .pll_sen(pll_sen),
        .pll_sen_write(pll_sen_write));
  design_1_HMC769_0_0_pll_send pll_send_i
       (.START_send(START_send),
        .\bits_send_reg[0]_0 (\bits_send_reg[1] [0]),
        .\bits_send_reg[1]_0 (\bits_send_reg[1] [1]),
        .clk_out3(clk_pll_spi),
        .is_clk_running(is_clk_running),
        .pll_mosi(pll_mosi),
        .pll_mosi_read(pll_mosi_read),
        .pll_mosi_reg_i_2__0_0(pll_mosi_reg_i_2__0),
        .pll_mosi_reg_i_2__0_1(pll_mosi_reg_i_2__0_0),
        .pll_mosi_reg_i_2__0_2(pll_mosi_reg_i_2__0_1),
        .pll_mosi_reg_i_2__0_3(pll_mosi_reg_i_2__0_2),
        .pll_mosi_reg_i_2__0_4(pll_mosi_reg_i_2__0_3),
        .pll_mosi_reg_i_2__0_5(pll_mosi_reg_i_2__0_4),
        .pll_mosi_reg_i_8_0(pll_mosi_reg_i_8),
        .pll_mosi_reg_reg_0(pll_mosi_reg_reg_1),
        .pll_sen_write(pll_sen_write));
  LUT4 #(
    .INIT(16'h00CE)) 
    pll_trig_reg_i_1
       (.I0(pll_trig),
        .I1(\trig_tguard_counter[15]_i_1_n_0 ),
        .I2(trig_tguard_counter),
        .I3(trig_tsweep_counter07_out),
        .O(pll_trig_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    pll_trig_reg_reg
       (.C(clk_8MHz),
        .CE(1'b1),
        .D(pll_trig_reg_i_1_n_0),
        .Q(pll_trig),
        .R(1'b0));
  CARRY4 start_adc_count_r1_carry
       (.CI(1'b0),
        .CO({start_adc_count_r1_carry_n_0,start_adc_count_r1_carry_n_1,start_adc_count_r1_carry_n_2,start_adc_count_r1_carry_n_3}),
        .CYINIT(1'b0),
        .DI({start_adc_count_r1_carry_i_1_n_0,start_adc_count_r1_carry_i_2_n_0,start_adc_count_r1_carry_i_3_n_0,start_adc_count_r1_carry_i_4_n_0}),
        .O(NLW_start_adc_count_r1_carry_O_UNCONNECTED[3:0]),
        .S({start_adc_count_r1_carry_i_5_n_0,start_adc_count_r1_carry_i_6_n_0,start_adc_count_r1_carry_i_7_n_0,start_adc_count_r1_carry_i_8_n_0}));
  CARRY4 start_adc_count_r1_carry__0
       (.CI(start_adc_count_r1_carry_n_0),
        .CO({start_adc_count_r1,start_adc_count_r1_carry__0_n_1,start_adc_count_r1_carry__0_n_2,start_adc_count_r1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({start_adc_count_r1_carry__0_i_1_n_0,start_adc_count_r1_carry__0_i_2_n_0,start_adc_count_r1_carry__0_i_3_n_0,start_adc_count_r1_carry__0_i_4_n_0}),
        .O(NLW_start_adc_count_r1_carry__0_O_UNCONNECTED[3:0]),
        .S({start_adc_count_r1_carry__0_i_5_n_0,start_adc_count_r1_carry__0_i_6_n_0,start_adc_count_r1_carry__0_i_7_n_0,start_adc_count_r1_carry__0_i_8_n_0}));
  LUT4 #(
    .INIT(16'h22B2)) 
    start_adc_count_r1_carry__0_i_1
       (.I0(trig_tsweep_counter_reg[15]),
        .I1(shift_front[15]),
        .I2(trig_tsweep_counter_reg[14]),
        .I3(shift_front[14]),
        .O(start_adc_count_r1_carry__0_i_1_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    start_adc_count_r1_carry__0_i_2
       (.I0(trig_tsweep_counter_reg[13]),
        .I1(shift_front[13]),
        .I2(trig_tsweep_counter_reg[12]),
        .I3(shift_front[12]),
        .O(start_adc_count_r1_carry__0_i_2_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    start_adc_count_r1_carry__0_i_3
       (.I0(trig_tsweep_counter_reg[11]),
        .I1(shift_front[11]),
        .I2(trig_tsweep_counter_reg[10]),
        .I3(shift_front[10]),
        .O(start_adc_count_r1_carry__0_i_3_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    start_adc_count_r1_carry__0_i_4
       (.I0(trig_tsweep_counter_reg[9]),
        .I1(shift_front[9]),
        .I2(trig_tsweep_counter_reg[8]),
        .I3(shift_front[8]),
        .O(start_adc_count_r1_carry__0_i_4_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    start_adc_count_r1_carry__0_i_5
       (.I0(shift_front[15]),
        .I1(trig_tsweep_counter_reg[15]),
        .I2(shift_front[14]),
        .I3(trig_tsweep_counter_reg[14]),
        .O(start_adc_count_r1_carry__0_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    start_adc_count_r1_carry__0_i_6
       (.I0(shift_front[13]),
        .I1(trig_tsweep_counter_reg[13]),
        .I2(shift_front[12]),
        .I3(trig_tsweep_counter_reg[12]),
        .O(start_adc_count_r1_carry__0_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    start_adc_count_r1_carry__0_i_7
       (.I0(shift_front[11]),
        .I1(trig_tsweep_counter_reg[11]),
        .I2(shift_front[10]),
        .I3(trig_tsweep_counter_reg[10]),
        .O(start_adc_count_r1_carry__0_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    start_adc_count_r1_carry__0_i_8
       (.I0(shift_front[9]),
        .I1(trig_tsweep_counter_reg[9]),
        .I2(shift_front[8]),
        .I3(trig_tsweep_counter_reg[8]),
        .O(start_adc_count_r1_carry__0_i_8_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    start_adc_count_r1_carry_i_1
       (.I0(trig_tsweep_counter_reg[7]),
        .I1(shift_front[7]),
        .I2(trig_tsweep_counter_reg[6]),
        .I3(shift_front[6]),
        .O(start_adc_count_r1_carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    start_adc_count_r1_carry_i_2
       (.I0(trig_tsweep_counter_reg[5]),
        .I1(shift_front[5]),
        .I2(trig_tsweep_counter_reg[4]),
        .I3(shift_front[4]),
        .O(start_adc_count_r1_carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    start_adc_count_r1_carry_i_3
       (.I0(trig_tsweep_counter_reg[3]),
        .I1(shift_front[3]),
        .I2(trig_tsweep_counter_reg[2]),
        .I3(shift_front[2]),
        .O(start_adc_count_r1_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    start_adc_count_r1_carry_i_4
       (.I0(trig_tsweep_counter_reg[1]),
        .I1(shift_front[1]),
        .I2(trig_tsweep_counter_reg[0]),
        .I3(shift_front[0]),
        .O(start_adc_count_r1_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    start_adc_count_r1_carry_i_5
       (.I0(shift_front[7]),
        .I1(trig_tsweep_counter_reg[7]),
        .I2(shift_front[6]),
        .I3(trig_tsweep_counter_reg[6]),
        .O(start_adc_count_r1_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    start_adc_count_r1_carry_i_6
       (.I0(shift_front[5]),
        .I1(trig_tsweep_counter_reg[5]),
        .I2(shift_front[4]),
        .I3(trig_tsweep_counter_reg[4]),
        .O(start_adc_count_r1_carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    start_adc_count_r1_carry_i_7
       (.I0(shift_front[3]),
        .I1(trig_tsweep_counter_reg[3]),
        .I2(shift_front[2]),
        .I3(trig_tsweep_counter_reg[2]),
        .O(start_adc_count_r1_carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    start_adc_count_r1_carry_i_8
       (.I0(shift_front[1]),
        .I1(trig_tsweep_counter_reg[1]),
        .I2(shift_front[0]),
        .I3(trig_tsweep_counter_reg[0]),
        .O(start_adc_count_r1_carry_i_8_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    start_adc_count_r_i_1
       (.I0(start_adc_count_r1),
        .I1(start_adc_count_r_i_2_n_0),
        .O(start_adc_count_r0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    start_adc_count_r_i_2
       (.I0(start_adc_count_r_i_3_n_0),
        .I1(start_adc_count_r_i_4_n_0),
        .I2(\trig_tguard_counter_reg_n_0_[7] ),
        .I3(start_adc_count_r_i_5_n_0),
        .I4(\trig_tguard_counter[15]_i_11_n_0 ),
        .O(start_adc_count_r_i_2_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    start_adc_count_r_i_3
       (.I0(\trig_tguard_counter_reg_n_0_[12] ),
        .I1(\trig_tguard_counter_reg_n_0_[13] ),
        .I2(\trig_tguard_counter_reg_n_0_[14] ),
        .I3(\trig_tguard_counter_reg_n_0_[15] ),
        .O(start_adc_count_r_i_3_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    start_adc_count_r_i_4
       (.I0(\trig_tguard_counter_reg_n_0_[10] ),
        .I1(\trig_tguard_counter_reg_n_0_[11] ),
        .I2(\trig_tguard_counter_reg_n_0_[8] ),
        .I3(\trig_tguard_counter_reg_n_0_[9] ),
        .O(start_adc_count_r_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    start_adc_count_r_i_5
       (.I0(\trig_tguard_counter_reg_n_0_[1] ),
        .I1(\trig_tguard_counter_reg_n_0_[0] ),
        .I2(\trig_tguard_counter_reg_n_0_[5] ),
        .I3(\trig_tguard_counter_reg_n_0_[6] ),
        .O(start_adc_count_r_i_5_n_0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    start_adc_count_r_reg
       (.C(clk_8MHz),
        .CE(1'b1),
        .D(start_adc_count_r0),
        .Q(start_adc_count),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hFF12)) 
    \trig_tguard_counter[0]_i_1 
       (.I0(\trig_tguard_counter_reg_n_0_[0] ),
        .I1(\trig_tguard_counter[0]_i_2_n_0 ),
        .I2(trig_tguard_counter),
        .I3(\trig_tguard_counter[0]_i_3_n_0 ),
        .O(\trig_tguard_counter[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \trig_tguard_counter[0]_i_2 
       (.I0(\trig_tguard_counter[15]_i_7_n_0 ),
        .I1(\trig_tguard_counter[15]_i_6_n_0 ),
        .I2(\trig_tguard_counter[15]_i_5_n_0 ),
        .I3(\trig_tguard_counter[15]_i_4_n_0 ),
        .O(\trig_tguard_counter[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \trig_tguard_counter[0]_i_3 
       (.I0(start_adc_count_r_i_2_n_0),
        .I1(\trig_tguard_counter[0]_i_4_n_0 ),
        .I2(trig_tsweep_counter_reg[2]),
        .I3(trig_tsweep_counter_reg[4]),
        .I4(\trig_tguard_counter[15]_i_14_n_0 ),
        .I5(\trig_tguard_counter[15]_i_16_n_0 ),
        .O(\trig_tguard_counter[0]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT5 #(
    .INIT(32'hFFBFFFFF)) 
    \trig_tguard_counter[0]_i_4 
       (.I0(\trig_tguard_counter[15]_i_13_n_0 ),
        .I1(trig_tsweep_counter_reg[3]),
        .I2(trig_tsweep_counter_reg[5]),
        .I3(trig_tsweep_counter_reg[12]),
        .I4(trig_tsweep_counter_reg[13]),
        .O(\trig_tguard_counter[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h000100010001FFFF)) 
    \trig_tguard_counter[15]_i_1 
       (.I0(\trig_tguard_counter[15]_i_4_n_0 ),
        .I1(\trig_tguard_counter[15]_i_5_n_0 ),
        .I2(\trig_tguard_counter[15]_i_6_n_0 ),
        .I3(\trig_tguard_counter[15]_i_7_n_0 ),
        .I4(\trig_tguard_counter[15]_i_8_n_0 ),
        .I5(start_adc_count_r_i_2_n_0),
        .O(\trig_tguard_counter[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \trig_tguard_counter[15]_i_10 
       (.I0(\trig_tguard_counter[15]_i_18_n_0 ),
        .I1(\trig_tguard_counter[15]_i_14_n_0 ),
        .I2(trig_tsweep_counter_reg[1]),
        .I3(trig_tsweep_counter_reg[0]),
        .I4(\trig_tguard_counter[15]_i_19_n_0 ),
        .I5(\trig_tguard_counter[15]_i_13_n_0 ),
        .O(\trig_tguard_counter[15]_i_10_n_0 ));
  LUT3 #(
    .INIT(8'hFE)) 
    \trig_tguard_counter[15]_i_11 
       (.I0(\trig_tguard_counter_reg_n_0_[2] ),
        .I1(\trig_tguard_counter_reg_n_0_[3] ),
        .I2(\trig_tguard_counter_reg_n_0_[4] ),
        .O(\trig_tguard_counter[15]_i_11_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \trig_tguard_counter[15]_i_12 
       (.I0(\trig_tguard_counter_reg_n_0_[6] ),
        .I1(\trig_tguard_counter_reg_n_0_[5] ),
        .O(\trig_tguard_counter[15]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \trig_tguard_counter[15]_i_13 
       (.I0(trig_tsweep_counter_reg[11]),
        .I1(trig_tsweep_counter_reg[10]),
        .I2(trig_tsweep_counter_reg[15]),
        .I3(trig_tsweep_counter_reg[14]),
        .O(\trig_tguard_counter[15]_i_13_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \trig_tguard_counter[15]_i_14 
       (.I0(trig_tsweep_counter_reg[6]),
        .I1(trig_tsweep_counter_reg[7]),
        .O(\trig_tguard_counter[15]_i_14_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \trig_tguard_counter[15]_i_15 
       (.I0(trig_tsweep_counter_reg[3]),
        .I1(trig_tsweep_counter_reg[4]),
        .O(\trig_tguard_counter[15]_i_15_n_0 ));
  LUT4 #(
    .INIT(16'hEFFF)) 
    \trig_tguard_counter[15]_i_16 
       (.I0(trig_tsweep_counter_reg[1]),
        .I1(trig_tsweep_counter_reg[0]),
        .I2(trig_tsweep_counter_reg[9]),
        .I3(trig_tsweep_counter_reg[8]),
        .O(\trig_tguard_counter[15]_i_16_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT4 #(
    .INIT(16'hDFFF)) 
    \trig_tguard_counter[15]_i_17 
       (.I0(trig_tsweep_counter_reg[13]),
        .I1(trig_tsweep_counter_reg[12]),
        .I2(trig_tsweep_counter_reg[5]),
        .I3(trig_tsweep_counter_reg[3]),
        .O(\trig_tguard_counter[15]_i_17_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \trig_tguard_counter[15]_i_18 
       (.I0(trig_tsweep_counter_reg[4]),
        .I1(trig_tsweep_counter_reg[3]),
        .I2(trig_tsweep_counter_reg[9]),
        .I3(trig_tsweep_counter_reg[8]),
        .O(\trig_tguard_counter[15]_i_18_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \trig_tguard_counter[15]_i_19 
       (.I0(trig_tsweep_counter_reg[13]),
        .I1(trig_tsweep_counter_reg[12]),
        .I2(trig_tsweep_counter_reg[5]),
        .I3(trig_tsweep_counter_reg[2]),
        .O(\trig_tguard_counter[15]_i_19_n_0 ));
  LUT6 #(
    .INIT(64'h0003030303030302)) 
    \trig_tguard_counter[15]_i_2 
       (.I0(\trig_tguard_counter[15]_i_9_n_0 ),
        .I1(\trig_tguard_counter[15]_i_5_n_0 ),
        .I2(\trig_tguard_counter[15]_i_10_n_0 ),
        .I3(\trig_tguard_counter_reg_n_0_[5] ),
        .I4(\trig_tguard_counter_reg_n_0_[6] ),
        .I5(\trig_tguard_counter[15]_i_11_n_0 ),
        .O(trig_tguard_counter));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFFFF)) 
    \trig_tguard_counter[15]_i_4 
       (.I0(\trig_tguard_counter_reg_n_0_[0] ),
        .I1(\trig_tguard_counter_reg_n_0_[1] ),
        .I2(\trig_tguard_counter_reg_n_0_[4] ),
        .I3(\trig_tguard_counter_reg_n_0_[3] ),
        .I4(\trig_tguard_counter_reg_n_0_[2] ),
        .I5(\trig_tguard_counter[15]_i_12_n_0 ),
        .O(\trig_tguard_counter[15]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \trig_tguard_counter[15]_i_5 
       (.I0(\trig_tguard_counter_reg_n_0_[7] ),
        .I1(start_adc_count_r_i_4_n_0),
        .I2(\trig_tguard_counter_reg_n_0_[12] ),
        .I3(\trig_tguard_counter_reg_n_0_[13] ),
        .I4(\trig_tguard_counter_reg_n_0_[14] ),
        .I5(\trig_tguard_counter_reg_n_0_[15] ),
        .O(\trig_tguard_counter[15]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \trig_tguard_counter[15]_i_6 
       (.I0(\trig_tguard_counter[15]_i_13_n_0 ),
        .I1(trig_tsweep_counter_reg[2]),
        .I2(trig_tsweep_counter_reg[5]),
        .I3(trig_tsweep_counter_reg[12]),
        .I4(trig_tsweep_counter_reg[13]),
        .O(\trig_tguard_counter[15]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFFFFFF)) 
    \trig_tguard_counter[15]_i_7 
       (.I0(trig_tsweep_counter_reg[0]),
        .I1(trig_tsweep_counter_reg[1]),
        .I2(\trig_tguard_counter[15]_i_14_n_0 ),
        .I3(trig_tsweep_counter_reg[8]),
        .I4(trig_tsweep_counter_reg[9]),
        .I5(\trig_tguard_counter[15]_i_15_n_0 ),
        .O(\trig_tguard_counter[15]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \trig_tguard_counter[15]_i_8 
       (.I0(\trig_tguard_counter[15]_i_16_n_0 ),
        .I1(\trig_tguard_counter[15]_i_14_n_0 ),
        .I2(trig_tsweep_counter_reg[4]),
        .I3(trig_tsweep_counter_reg[2]),
        .I4(\trig_tguard_counter[15]_i_17_n_0 ),
        .I5(\trig_tguard_counter[15]_i_13_n_0 ),
        .O(\trig_tguard_counter[15]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \trig_tguard_counter[15]_i_9 
       (.I0(\trig_tguard_counter_reg_n_0_[0] ),
        .I1(\trig_tguard_counter_reg_n_0_[1] ),
        .O(\trig_tguard_counter[15]_i_9_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[0] 
       (.C(clk_8MHz),
        .CE(1'b1),
        .D(\trig_tguard_counter[0]_i_1_n_0 ),
        .Q(\trig_tguard_counter_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[10] 
       (.C(clk_8MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[10]),
        .Q(\trig_tguard_counter_reg_n_0_[10] ),
        .R(\trig_tguard_counter[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[11] 
       (.C(clk_8MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[11]),
        .Q(\trig_tguard_counter_reg_n_0_[11] ),
        .R(\trig_tguard_counter[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[12] 
       (.C(clk_8MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[12]),
        .Q(\trig_tguard_counter_reg_n_0_[12] ),
        .R(\trig_tguard_counter[15]_i_1_n_0 ));
  CARRY4 \trig_tguard_counter_reg[12]_i_1 
       (.CI(\trig_tguard_counter_reg[8]_i_1_n_0 ),
        .CO({\trig_tguard_counter_reg[12]_i_1_n_0 ,\trig_tguard_counter_reg[12]_i_1_n_1 ,\trig_tguard_counter_reg[12]_i_1_n_2 ,\trig_tguard_counter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(p_2_in[12:9]),
        .S({\trig_tguard_counter_reg_n_0_[12] ,\trig_tguard_counter_reg_n_0_[11] ,\trig_tguard_counter_reg_n_0_[10] ,\trig_tguard_counter_reg_n_0_[9] }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[13] 
       (.C(clk_8MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[13]),
        .Q(\trig_tguard_counter_reg_n_0_[13] ),
        .R(\trig_tguard_counter[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[14] 
       (.C(clk_8MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[14]),
        .Q(\trig_tguard_counter_reg_n_0_[14] ),
        .R(\trig_tguard_counter[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[15] 
       (.C(clk_8MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[15]),
        .Q(\trig_tguard_counter_reg_n_0_[15] ),
        .R(\trig_tguard_counter[15]_i_1_n_0 ));
  CARRY4 \trig_tguard_counter_reg[15]_i_3 
       (.CI(\trig_tguard_counter_reg[12]_i_1_n_0 ),
        .CO({\NLW_trig_tguard_counter_reg[15]_i_3_CO_UNCONNECTED [3:2],\trig_tguard_counter_reg[15]_i_3_n_2 ,\trig_tguard_counter_reg[15]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_trig_tguard_counter_reg[15]_i_3_O_UNCONNECTED [3],p_2_in[15:13]}),
        .S({1'b0,\trig_tguard_counter_reg_n_0_[15] ,\trig_tguard_counter_reg_n_0_[14] ,\trig_tguard_counter_reg_n_0_[13] }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[1] 
       (.C(clk_8MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[1]),
        .Q(\trig_tguard_counter_reg_n_0_[1] ),
        .R(\trig_tguard_counter[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[2] 
       (.C(clk_8MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[2]),
        .Q(\trig_tguard_counter_reg_n_0_[2] ),
        .R(\trig_tguard_counter[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[3] 
       (.C(clk_8MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[3]),
        .Q(\trig_tguard_counter_reg_n_0_[3] ),
        .R(\trig_tguard_counter[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[4] 
       (.C(clk_8MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[4]),
        .Q(\trig_tguard_counter_reg_n_0_[4] ),
        .R(\trig_tguard_counter[15]_i_1_n_0 ));
  CARRY4 \trig_tguard_counter_reg[4]_i_1 
       (.CI(1'b0),
        .CO({\trig_tguard_counter_reg[4]_i_1_n_0 ,\trig_tguard_counter_reg[4]_i_1_n_1 ,\trig_tguard_counter_reg[4]_i_1_n_2 ,\trig_tguard_counter_reg[4]_i_1_n_3 }),
        .CYINIT(\trig_tguard_counter_reg_n_0_[0] ),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(p_2_in[4:1]),
        .S({\trig_tguard_counter_reg_n_0_[4] ,\trig_tguard_counter_reg_n_0_[3] ,\trig_tguard_counter_reg_n_0_[2] ,\trig_tguard_counter_reg_n_0_[1] }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[5] 
       (.C(clk_8MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[5]),
        .Q(\trig_tguard_counter_reg_n_0_[5] ),
        .R(\trig_tguard_counter[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[6] 
       (.C(clk_8MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[6]),
        .Q(\trig_tguard_counter_reg_n_0_[6] ),
        .R(\trig_tguard_counter[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[7] 
       (.C(clk_8MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[7]),
        .Q(\trig_tguard_counter_reg_n_0_[7] ),
        .R(\trig_tguard_counter[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[8] 
       (.C(clk_8MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[8]),
        .Q(\trig_tguard_counter_reg_n_0_[8] ),
        .R(\trig_tguard_counter[15]_i_1_n_0 ));
  CARRY4 \trig_tguard_counter_reg[8]_i_1 
       (.CI(\trig_tguard_counter_reg[4]_i_1_n_0 ),
        .CO({\trig_tguard_counter_reg[8]_i_1_n_0 ,\trig_tguard_counter_reg[8]_i_1_n_1 ,\trig_tguard_counter_reg[8]_i_1_n_2 ,\trig_tguard_counter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(p_2_in[8:5]),
        .S({\trig_tguard_counter_reg_n_0_[8] ,\trig_tguard_counter_reg_n_0_[7] ,\trig_tguard_counter_reg_n_0_[6] ,\trig_tguard_counter_reg_n_0_[5] }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[9] 
       (.C(clk_8MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[9]),
        .Q(\trig_tguard_counter_reg_n_0_[9] ),
        .R(\trig_tguard_counter[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \trig_tsweep_counter[0]_i_1 
       (.I0(trig_tguard_counter),
        .I1(\trig_tguard_counter[15]_i_1_n_0 ),
        .O(\trig_tsweep_counter[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00000007)) 
    \trig_tsweep_counter[0]_i_2 
       (.I0(\trig_tsweep_counter[0]_i_4_n_0 ),
        .I1(trig_tsweep_counter_reg[13]),
        .I2(trig_tsweep_counter_reg[14]),
        .I3(trig_tsweep_counter_reg[15]),
        .I4(start_adc_count_r_i_2_n_0),
        .O(trig_tsweep_counter07_out));
  LUT6 #(
    .INIT(64'hFEFFFEFEFEFEFEFE)) 
    \trig_tsweep_counter[0]_i_4 
       (.I0(trig_tsweep_counter_reg[10]),
        .I1(trig_tsweep_counter_reg[11]),
        .I2(trig_tsweep_counter_reg[12]),
        .I3(\trig_tsweep_counter[0]_i_6_n_0 ),
        .I4(trig_tsweep_counter_reg[8]),
        .I5(trig_tsweep_counter_reg[9]),
        .O(\trig_tsweep_counter[0]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \trig_tsweep_counter[0]_i_5 
       (.I0(trig_tsweep_counter_reg[0]),
        .O(\trig_tsweep_counter[0]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT5 #(
    .INIT(32'h00000057)) 
    \trig_tsweep_counter[0]_i_6 
       (.I0(trig_tsweep_counter_reg[5]),
        .I1(trig_tsweep_counter_reg[3]),
        .I2(trig_tsweep_counter_reg[4]),
        .I3(trig_tsweep_counter_reg[7]),
        .I4(trig_tsweep_counter_reg[6]),
        .O(\trig_tsweep_counter[0]_i_6_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[0] 
       (.C(clk_8MHz),
        .CE(trig_tsweep_counter07_out),
        .D(\trig_tsweep_counter_reg[0]_i_3_n_7 ),
        .Q(trig_tsweep_counter_reg[0]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  CARRY4 \trig_tsweep_counter_reg[0]_i_3 
       (.CI(1'b0),
        .CO({\trig_tsweep_counter_reg[0]_i_3_n_0 ,\trig_tsweep_counter_reg[0]_i_3_n_1 ,\trig_tsweep_counter_reg[0]_i_3_n_2 ,\trig_tsweep_counter_reg[0]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\trig_tsweep_counter_reg[0]_i_3_n_4 ,\trig_tsweep_counter_reg[0]_i_3_n_5 ,\trig_tsweep_counter_reg[0]_i_3_n_6 ,\trig_tsweep_counter_reg[0]_i_3_n_7 }),
        .S({trig_tsweep_counter_reg[3:1],\trig_tsweep_counter[0]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[10] 
       (.C(clk_8MHz),
        .CE(trig_tsweep_counter07_out),
        .D(\trig_tsweep_counter_reg[8]_i_1_n_5 ),
        .Q(trig_tsweep_counter_reg[10]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[11] 
       (.C(clk_8MHz),
        .CE(trig_tsweep_counter07_out),
        .D(\trig_tsweep_counter_reg[8]_i_1_n_4 ),
        .Q(trig_tsweep_counter_reg[11]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[12] 
       (.C(clk_8MHz),
        .CE(trig_tsweep_counter07_out),
        .D(\trig_tsweep_counter_reg[12]_i_1_n_7 ),
        .Q(trig_tsweep_counter_reg[12]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  CARRY4 \trig_tsweep_counter_reg[12]_i_1 
       (.CI(\trig_tsweep_counter_reg[8]_i_1_n_0 ),
        .CO({\NLW_trig_tsweep_counter_reg[12]_i_1_CO_UNCONNECTED [3],\trig_tsweep_counter_reg[12]_i_1_n_1 ,\trig_tsweep_counter_reg[12]_i_1_n_2 ,\trig_tsweep_counter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\trig_tsweep_counter_reg[12]_i_1_n_4 ,\trig_tsweep_counter_reg[12]_i_1_n_5 ,\trig_tsweep_counter_reg[12]_i_1_n_6 ,\trig_tsweep_counter_reg[12]_i_1_n_7 }),
        .S(trig_tsweep_counter_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[13] 
       (.C(clk_8MHz),
        .CE(trig_tsweep_counter07_out),
        .D(\trig_tsweep_counter_reg[12]_i_1_n_6 ),
        .Q(trig_tsweep_counter_reg[13]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[14] 
       (.C(clk_8MHz),
        .CE(trig_tsweep_counter07_out),
        .D(\trig_tsweep_counter_reg[12]_i_1_n_5 ),
        .Q(trig_tsweep_counter_reg[14]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[15] 
       (.C(clk_8MHz),
        .CE(trig_tsweep_counter07_out),
        .D(\trig_tsweep_counter_reg[12]_i_1_n_4 ),
        .Q(trig_tsweep_counter_reg[15]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[1] 
       (.C(clk_8MHz),
        .CE(trig_tsweep_counter07_out),
        .D(\trig_tsweep_counter_reg[0]_i_3_n_6 ),
        .Q(trig_tsweep_counter_reg[1]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[2] 
       (.C(clk_8MHz),
        .CE(trig_tsweep_counter07_out),
        .D(\trig_tsweep_counter_reg[0]_i_3_n_5 ),
        .Q(trig_tsweep_counter_reg[2]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[3] 
       (.C(clk_8MHz),
        .CE(trig_tsweep_counter07_out),
        .D(\trig_tsweep_counter_reg[0]_i_3_n_4 ),
        .Q(trig_tsweep_counter_reg[3]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[4] 
       (.C(clk_8MHz),
        .CE(trig_tsweep_counter07_out),
        .D(\trig_tsweep_counter_reg[4]_i_1_n_7 ),
        .Q(trig_tsweep_counter_reg[4]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  CARRY4 \trig_tsweep_counter_reg[4]_i_1 
       (.CI(\trig_tsweep_counter_reg[0]_i_3_n_0 ),
        .CO({\trig_tsweep_counter_reg[4]_i_1_n_0 ,\trig_tsweep_counter_reg[4]_i_1_n_1 ,\trig_tsweep_counter_reg[4]_i_1_n_2 ,\trig_tsweep_counter_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\trig_tsweep_counter_reg[4]_i_1_n_4 ,\trig_tsweep_counter_reg[4]_i_1_n_5 ,\trig_tsweep_counter_reg[4]_i_1_n_6 ,\trig_tsweep_counter_reg[4]_i_1_n_7 }),
        .S(trig_tsweep_counter_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[5] 
       (.C(clk_8MHz),
        .CE(trig_tsweep_counter07_out),
        .D(\trig_tsweep_counter_reg[4]_i_1_n_6 ),
        .Q(trig_tsweep_counter_reg[5]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[6] 
       (.C(clk_8MHz),
        .CE(trig_tsweep_counter07_out),
        .D(\trig_tsweep_counter_reg[4]_i_1_n_5 ),
        .Q(trig_tsweep_counter_reg[6]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[7] 
       (.C(clk_8MHz),
        .CE(trig_tsweep_counter07_out),
        .D(\trig_tsweep_counter_reg[4]_i_1_n_4 ),
        .Q(trig_tsweep_counter_reg[7]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[8] 
       (.C(clk_8MHz),
        .CE(trig_tsweep_counter07_out),
        .D(\trig_tsweep_counter_reg[8]_i_1_n_7 ),
        .Q(trig_tsweep_counter_reg[8]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  CARRY4 \trig_tsweep_counter_reg[8]_i_1 
       (.CI(\trig_tsweep_counter_reg[4]_i_1_n_0 ),
        .CO({\trig_tsweep_counter_reg[8]_i_1_n_0 ,\trig_tsweep_counter_reg[8]_i_1_n_1 ,\trig_tsweep_counter_reg[8]_i_1_n_2 ,\trig_tsweep_counter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\trig_tsweep_counter_reg[8]_i_1_n_4 ,\trig_tsweep_counter_reg[8]_i_1_n_5 ,\trig_tsweep_counter_reg[8]_i_1_n_6 ,\trig_tsweep_counter_reg[8]_i_1_n_7 }),
        .S(trig_tsweep_counter_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[9] 
       (.C(clk_8MHz),
        .CE(trig_tsweep_counter07_out),
        .D(\trig_tsweep_counter_reg[8]_i_1_n_6 ),
        .Q(trig_tsweep_counter_reg[9]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
