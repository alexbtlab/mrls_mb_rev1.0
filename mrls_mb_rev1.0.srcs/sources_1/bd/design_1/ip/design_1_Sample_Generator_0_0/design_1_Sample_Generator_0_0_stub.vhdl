-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Fri Mar 19 16:09:46 2021
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               C:/project/mrls_mb_rev1.0/mrls_mb_rev1.0.srcs/sources_1/bd/design_1/ip/design_1_Sample_Generator_0_0/design_1_Sample_Generator_0_0_stub.vhdl
-- Design      : design_1_Sample_Generator_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_Sample_Generator_0_0 is
  Port ( 
    data_clk : in STD_LOGIC;
    clk_5MHz : in STD_LOGIC;
    data_in_IF1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    data_in_IF2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 47 downto 0 );
    m00_axis_tstrb : out STD_LOGIC_VECTOR ( 5 downto 0 );
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tready : in STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    m00_axis_config_tvalid : out STD_LOGIC;
    m00_axis_config_tdata : out STD_LOGIC_VECTOR ( 47 downto 0 );
    m00_axis_config_tready : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC
  );

end design_1_Sample_Generator_0_0;

architecture stub of design_1_Sample_Generator_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "data_clk,clk_5MHz,data_in_IF1[15:0],data_in_IF2[15:0],m00_axis_tdata[47:0],m00_axis_tstrb[5:0],m00_axis_tlast,m00_axis_tvalid,m00_axis_tready,m00_axis_aclk,m00_axis_config_tvalid,m00_axis_config_tdata[47:0],m00_axis_config_tready,m00_axis_aresetn";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "Sample_Generator_v3_0,Vivado 2019.1";
begin
end;
