// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Fri Mar 19 16:09:46 2021
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               C:/project/mrls_mb_rev1.0/mrls_mb_rev1.0.srcs/sources_1/bd/design_1/ip/design_1_Sample_Generator_0_0/design_1_Sample_Generator_0_0_stub.v
// Design      : design_1_Sample_Generator_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "Sample_Generator_v3_0,Vivado 2019.1" *)
module design_1_Sample_Generator_0_0(data_clk, clk_5MHz, data_in_IF1, data_in_IF2, 
  m00_axis_tdata, m00_axis_tstrb, m00_axis_tlast, m00_axis_tvalid, m00_axis_tready, 
  m00_axis_aclk, m00_axis_config_tvalid, m00_axis_config_tdata, m00_axis_config_tready, 
  m00_axis_aresetn)
/* synthesis syn_black_box black_box_pad_pin="data_clk,clk_5MHz,data_in_IF1[15:0],data_in_IF2[15:0],m00_axis_tdata[47:0],m00_axis_tstrb[5:0],m00_axis_tlast,m00_axis_tvalid,m00_axis_tready,m00_axis_aclk,m00_axis_config_tvalid,m00_axis_config_tdata[47:0],m00_axis_config_tready,m00_axis_aresetn" */;
  input data_clk;
  input clk_5MHz;
  input [15:0]data_in_IF1;
  input [15:0]data_in_IF2;
  output [47:0]m00_axis_tdata;
  output [5:0]m00_axis_tstrb;
  output m00_axis_tlast;
  output m00_axis_tvalid;
  input m00_axis_tready;
  input m00_axis_aclk;
  output m00_axis_config_tvalid;
  output [47:0]m00_axis_config_tdata;
  input m00_axis_config_tready;
  input m00_axis_aresetn;
endmodule
