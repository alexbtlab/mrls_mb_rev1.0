set_property PACKAGE_PIN K2 [get_ports {DDR3_dqs_p[0]}]
set_property PACKAGE_PIN J2 [get_ports {DDR3_dqs_n[0]}]
set_property PACKAGE_PIN E1 [get_ports {DDR3_dqs_p[1]}]
set_property PACKAGE_PIN D1 [get_ports {DDR3_dqs_n[1]}]
set_property PACKAGE_PIN J1 [get_ports {DDR3_dq[0]}]
set_property PACKAGE_PIN H3 [get_ports {DDR3_dq[1]}]
set_property PACKAGE_PIN K1 [get_ports {DDR3_dq[2]}]
set_property PACKAGE_PIN G3 [get_ports {DDR3_dq[3]}]
set_property PACKAGE_PIN G4 [get_ports {DDR3_dq[4]}]
set_property PACKAGE_PIN G2 [get_ports {DDR3_dq[5]}]
set_property PACKAGE_PIN J5 [get_ports {DDR3_dq[6]}]
set_property PACKAGE_PIN H2 [get_ports {DDR3_dq[7]}]
set_property PACKAGE_PIN E2 [get_ports {DDR3_dq[8]}]
set_property PACKAGE_PIN F1 [get_ports {DDR3_dq[9]}]
set_property PACKAGE_PIN D2 [get_ports {DDR3_dq[10]}]
set_property PACKAGE_PIN E3 [get_ports {DDR3_dq[11]}]
set_property PACKAGE_PIN B1 [get_ports {DDR3_dq[12]}]
set_property PACKAGE_PIN B2 [get_ports {DDR3_dq[13]}]
set_property PACKAGE_PIN C2 [get_ports {DDR3_dq[14]}]
set_property PACKAGE_PIN A1 [get_ports {DDR3_dq[15]}]
set_property PACKAGE_PIN P6 [get_ports {DDR3_addr[13]}]
set_property PACKAGE_PIN L1 [get_ports {DDR3_addr[12]}]
set_property PACKAGE_PIN L3 [get_ports {DDR3_addr[11]}]
set_property PACKAGE_PIN L4 [get_ports {DDR3_addr[10]}]
set_property PACKAGE_PIN P1 [get_ports {DDR3_addr[9]}]
set_property PACKAGE_PIN M6 [get_ports {DDR3_addr[8]}]
set_property PACKAGE_PIN P2 [get_ports {DDR3_addr[7]}]
set_property PACKAGE_PIN N2 [get_ports {DDR3_addr[6]}]
set_property PACKAGE_PIN N4 [get_ports {DDR3_addr[5]}]
set_property PACKAGE_PIN M1 [get_ports {DDR3_addr[4]}]
set_property PACKAGE_PIN M3 [get_ports {DDR3_addr[3]}]
set_property PACKAGE_PIN N3 [get_ports {DDR3_addr[2]}]
set_property PACKAGE_PIN N5 [get_ports {DDR3_addr[1]}]
set_property PACKAGE_PIN M5 [get_ports {DDR3_addr[0]}]
set_property PACKAGE_PIN K4 [get_ports {DDR3_ba[2]}]
set_property PACKAGE_PIN M2 [get_ports {DDR3_ba[1]}]
set_property PACKAGE_PIN K3 [get_ports {DDR3_ba[0]}]
set_property PACKAGE_PIN L6 [get_ports DDR3_ras_n]
set_property PACKAGE_PIN J6 [get_ports DDR3_cas_n]
set_property PACKAGE_PIN K6 [get_ports DDR3_we_n]
set_property PACKAGE_PIN G1 [get_ports DDR3_reset_n]
set_property PACKAGE_PIN L5 [get_ports {DDR3_cke[0]}]
set_property PACKAGE_PIN R1 [get_ports {DDR3_odt[0]}]
set_property PACKAGE_PIN J4 [get_ports {DDR3_cs_n[0]}]
set_property PACKAGE_PIN H5 [get_ports {DDR3_dm[0]}]
set_property PACKAGE_PIN F3 [get_ports {DDR3_dm[1]}]
set_property PACKAGE_PIN P5 [get_ports {DDR3_ck_p[0]}]
set_property PACKAGE_PIN P4 [get_ports {DDR3_ck_n[0]}]
set_property INTERNAL_VREF 0.75 [get_iobanks 35]

set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[15]}]
set_property SLEW FAST [get_ports {DDR3_dq[15]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[15]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[14]}]
set_property SLEW FAST [get_ports {DDR3_dq[14]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[14]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[13]}]
set_property SLEW FAST [get_ports {DDR3_dq[13]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[13]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[12]}]
set_property SLEW FAST [get_ports {DDR3_dq[12]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[12]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[11]}]
set_property SLEW FAST [get_ports {DDR3_dq[11]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[11]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[10]}]
set_property SLEW FAST [get_ports {DDR3_dq[10]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[10]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[9]}]
set_property SLEW FAST [get_ports {DDR3_dq[9]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[9]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[8]}]
set_property SLEW FAST [get_ports {DDR3_dq[8]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[8]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[7]}]
set_property SLEW FAST [get_ports {DDR3_dq[7]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[7]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[6]}]
set_property SLEW FAST [get_ports {DDR3_dq[6]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[6]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[5]}]
set_property SLEW FAST [get_ports {DDR3_dq[5]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[5]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[4]}]
set_property SLEW FAST [get_ports {DDR3_dq[4]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[4]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[3]}]
set_property SLEW FAST [get_ports {DDR3_dq[3]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[3]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[2]}]
set_property SLEW FAST [get_ports {DDR3_dq[2]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[2]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[1]}]
set_property SLEW FAST [get_ports {DDR3_dq[1]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[1]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dq[0]}]
set_property SLEW FAST [get_ports {DDR3_dq[0]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dq[0]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_cke[0]}]
set_property SLEW FAST [get_ports {DDR3_cke[0]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_ba[2]}]
set_property SLEW FAST [get_ports {DDR3_ba[2]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_ba[1]}]
set_property SLEW FAST [get_ports {DDR3_ba[1]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_ba[0]}]
set_property SLEW FAST [get_ports {DDR3_ba[0]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[13]}]
set_property SLEW FAST [get_ports {DDR3_addr[13]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[12]}]
set_property SLEW FAST [get_ports {DDR3_addr[12]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[11]}]
set_property SLEW FAST [get_ports {DDR3_addr[11]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[10]}]
set_property SLEW FAST [get_ports {DDR3_addr[10]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[9]}]
set_property SLEW FAST [get_ports {DDR3_addr[9]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[8]}]
set_property SLEW FAST [get_ports {DDR3_addr[8]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[7]}]
set_property SLEW FAST [get_ports {DDR3_addr[7]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[6]}]
set_property SLEW FAST [get_ports {DDR3_addr[6]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[5]}]
set_property SLEW FAST [get_ports {DDR3_addr[5]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[4]}]
set_property SLEW FAST [get_ports {DDR3_addr[4]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[3]}]
set_property SLEW FAST [get_ports {DDR3_addr[3]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[2]}]
set_property SLEW FAST [get_ports {DDR3_addr[2]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[1]}]
set_property SLEW FAST [get_ports {DDR3_addr[1]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_addr[0]}]
set_property SLEW FAST [get_ports {DDR3_addr[0]}]
set_property IOSTANDARD DIFF_SSTL15 [get_ports {DDR3_ck_n[0]}]
set_property SLEW FAST [get_ports {DDR3_ck_n[0]}]
set_property IOSTANDARD DIFF_SSTL15 [get_ports {DDR3_ck_p[0]}]
set_property SLEW FAST [get_ports {DDR3_ck_p[0]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_cs_n[0]}]
set_property SLEW FAST [get_ports {DDR3_cs_n[0]}]
set_property IOSTANDARD DIFF_SSTL15 [get_ports {DDR3_dqs_n[1]}]
set_property SLEW FAST [get_ports {DDR3_dqs_n[1]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dqs_n[1]}]
set_property DIFF_TERM FALSE [get_ports {DDR3_dqs_n[1]}]
set_property IOSTANDARD DIFF_SSTL15 [get_ports {DDR3_dqs_n[0]}]
set_property SLEW FAST [get_ports {DDR3_dqs_n[0]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dqs_n[0]}]
set_property DIFF_TERM FALSE [get_ports {DDR3_dqs_n[0]}]
set_property IOSTANDARD DIFF_SSTL15 [get_ports {DDR3_dqs_p[1]}]
set_property SLEW FAST [get_ports {DDR3_dqs_p[1]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dqs_p[1]}]
set_property DIFF_TERM FALSE [get_ports {DDR3_dqs_p[1]}]
set_property IOSTANDARD DIFF_SSTL15 [get_ports {DDR3_dqs_p[0]}]
set_property SLEW FAST [get_ports {DDR3_dqs_p[0]}]
set_property IN_TERM UNTUNED_SPLIT_50 [get_ports {DDR3_dqs_p[0]}]
set_property DIFF_TERM FALSE [get_ports {DDR3_dqs_p[0]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dm[1]}]
set_property SLEW FAST [get_ports {DDR3_dm[1]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_dm[0]}]
set_property SLEW FAST [get_ports {DDR3_dm[0]}]
set_property IOSTANDARD SSTL15 [get_ports {DDR3_odt[0]}]
set_property SLEW FAST [get_ports {DDR3_odt[0]}]
set_property IOSTANDARD SSTL15 [get_ports DDR3_cas_n]
set_property SLEW FAST [get_ports DDR3_cas_n]
set_property IOSTANDARD SSTL15 [get_ports DDR3_ras_n]
set_property SLEW FAST [get_ports DDR3_ras_n]
set_property IOSTANDARD LVCMOS15 [get_ports DDR3_reset_n]
set_property DRIVE 12 [get_ports DDR3_reset_n]
set_property SLEW FAST [get_ports DDR3_reset_n]
set_property IOSTANDARD SSTL15 [get_ports DDR3_we_n]
set_property SLEW FAST [get_ports DDR3_we_n]

