set_property PACKAGE_PIN AA1 [get_ports mdio_mdc]
set_property PACKAGE_PIN Y1 [get_ports mdio_mdio_io]
set_property PACKAGE_PIN R6 [get_ports {rmii_rxd[0]}]
set_property PACKAGE_PIN T6 [get_ports {rmii_rxd[1]}]
set_property PACKAGE_PIN W6 [get_ports {rmii_txd[0]}]
set_property PACKAGE_PIN W5 [get_ports {rmii_txd[1]}]
set_property PACKAGE_PIN V5 [get_ports rmii_crs_dv]
set_property PACKAGE_PIN AB3 [get_ports rmii_rx_er]
set_property PACKAGE_PIN AB1 [get_ports rmii_tx_en]

set_property IOSTANDARD LVCMOS33 [get_ports {rmii_rxd[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {rmii_rxd[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {rmii_txd[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {rmii_txd[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports rmii_crs_dv]
set_property IOSTANDARD LVCMOS33 [get_ports rmii_rx_er]
set_property IOSTANDARD LVCMOS33 [get_ports rmii_tx_en]
set_property IOSTANDARD LVCMOS33 [get_ports mdio_mdc]
set_property IOSTANDARD LVCMOS33 [get_ports mdio_mdio_io]

set_property DRIVE 12 [get_ports mdio_mdc]
set_property DRIVE 12 [get_ports {rmii_txd[1]}]
set_property DRIVE 12 [get_ports rmii_tx_en]
set_property DRIVE 12 [get_ports {rmii_txd[0]}]
set_property DRIVE 12 [get_ports mdio_mdio_io]

set_property SLEW SLOW [get_ports rmii_tx_en]
set_property SLEW SLOW [get_ports {rmii_txd[1]}]
set_property SLEW SLOW [get_ports {rmii_txd[0]}]
set_property SLEW SLOW [get_ports mdio_mdc]
set_property SLEW SLOW [get_ports mdio_mdio_io]



