/*
 * xadc.h
 *
 *  Created on: 23 ���. 2020 �.
 *      Author: 24
 */

#ifndef SRC_XADC_H_
#define SRC_XADC_H_

#include "xil_printf.h"
#include "xsysmon.h"
#include "parameters.h"

#define SYSMON_DEVICE_ID XPAR_SYSMON_0_DEVICE_ID

	int XADC_Init(void);
	u16 XADC_Read(u8 channel);
	int SysMonFractionToInt(float FloatNum);

#define RawToVoltage(AdcData) \
	((((float)(AdcData)) * (1.0f)) / 65536.0f)

#endif /* SRC_XADC_H_ */
