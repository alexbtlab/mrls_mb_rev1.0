#include "parameters.h"
#include "main.h"

Param params[MAX_NUM_PARAM+1];
BT_STATE state_mrls;

bool tmp_dhcp_val = false;
u16 tmp_port_val = listenport;
u8 tmp_ATTEN_val = 0;
bool tmp_PAMP_val = false;
bool tmp_UseCable_val = false;

extern bool useCable;

/**
 * @brief addParam
 * Функция добавления параметров в массив параметров
 * @param param Добавляемый параметр хранит индекс параметра для дальнейшего обращения к нему и записи в нужное место массива
 */
void addParam(Param* param) {
	params[param->param.param_index] = *param;		// Какой идекс имеет паратетр, в ту ячейку массива кладем параметр
}
/**
 * @brief sendAllParams
 * Функция передачи всех параметров"
 * @param none
 */
void sendAllParams() {

	char bufTx[1024];
	mavlink_message_t 	msgTx_value;
    for(u8 i = 1; i <= MAX_NUM_PARAM; i++){
		mavlink_msg_param_value_encode(SYS_ID, COMPONENT_ID, &msgTx_value, &(params[i].param));
		u16 len_val = mavlink_msg_to_send_buffer((uint8_t *)bufTx, &msgTx_value);
		my_udp_send(bufTx, len_val, &ip_addr_broadcast);
    }
    needed_send_param = false;
}
/**
 * @brief Set the Param object
 * Функция установки неоходимого параметра после приема от ПК команды "Установить параметр"
 * @param newParam - новое значение параметра (в структуре хранится номер необходимого)
 */
void setParam(mavlink_param_value_t *newParam) {

	xil_printf_BLE("INFO_P: setParam\r\n");
	params[(newParam->param_index)].setter((uint64_t *)&(newParam->param_value));
}
/**
 * @brief 
 * Функция установки параметра "IP адрес"
 * @param value Принятое значение от ПК для установки параметра
 */
void setterIP(void *value) {

	ip_addr_t ipaddr_mp;

	xil_printf_BLE("INFO_P: setterIP\r\n");

	IP4_ADDR(&ipaddr_mp,    (*((uint64_t *)value) & 0xFF000000) >> 24,
							(*((uint64_t *)value) & 0xFF0000) >> 16,
							(*((uint64_t *)value) & 0xFF00) >> 8,
							(*((uint64_t *)value) & 0xFF));

	xil_printf_BLE("%s\r\n", 	  ip4addr_ntoa(&ipaddr_mp));
	IP_Param = (Param){{ipaddr_mp.addr, PARAM_COUNT_NET_GROUP, 1, "IP_addres", "NetGroup", PARAM_TYPE_IP}, setterIP, getterIP};
	addParam(&IP_Param);

	WriteSD_setNetData(ipaddr_mp, netmask, gw, tmp_port_val, tmp_dhcp_val, useCable);
	state_mrls = STATE_NEED_REBOOT;
}
/**
 * @brief 
 * Функция установки параметра "NM адрес"
 * @param value Принятое значение от ПК для установки параметра
 */
void setterNM(void *value) {

	ip_addr_t netmask;

	xil_printf_BLE("INFO_P: setterNM\r\n");
	IP4_ADDR(&netmask,   (*((uint64_t *)value) & 0xFF),
			      	  	((*((uint64_t *)value) & 0xFF00)   >> 8),
				        ((*((uint64_t *)value) & 0xFF0000) >> 16),
				        ((*((uint64_t *)value) & 0xFF0000) >> 24));
	NM_Param   = (Param){{netmask.addr, PARAM_COUNT_NET_GROUP, 2, "NM_addres", "NetGroup", PARAM_TYPE_IP},		setterNM,	getterNM};
	addParam(&NM_Param);
	//WriteSD_setNetData(ipaddr, netmask, gw, listenport, tmp_dhcp_val);
	state_mrls = STATE_NEED_REBOOT;
}
/**
 * @brief 
 * Функция установки параметра "GW адрес"
 * @param value Принятое значение от ПК для установки параметра
 */
void setterGW(void *value) {

	ip_addr_t gw;

	xil_printf_BLE("INFO_P: setterGW\r\n");
	IP4_ADDR(&gw,        (*((uint64_t *)value) &0xFF),
			            ((*((uint64_t *)value) &0xFF00)   >> 8),
				        ((*((uint64_t *)value) &0xFF0000) >> 16),
				        ((*((uint64_t *)value) &0xFF0000) >> 24));

	GW_Param   = (Param){{gw.addr, PARAM_COUNT_NET_GROUP, 3, "GW_addres", "NetGroup", PARAM_TYPE_IP},		setterGW,	getterGW};
	addParam(&GW_Param);
	//WriteSD_setNetData(ipaddr, netmask, gw, listenport, tmp_dhcp_val);
	state_mrls = STATE_NEED_REBOOT;
}
/**
 * @brief 
 * Функция установки параметра "UDP Port"
 * @param value Принятое значение от ПК для установки параметра
 */
void setterPort(void *value) {

	tmp_port_val = (u16)(*(uint16_t *)value);
	xil_printf_BLE("INFO_P: setterPort->%d\r\n", tmp_port_val);

	port_Param = (Param){{tmp_port_val, PARAM_COUNT_NET_GROUP, 4, "Port_val",  "NetGroup", PARAM_TYPE_UINT16},	setterPort,	getterPort};
	addParam(&port_Param);

	WriteSD_setNetData(ipaddr, netmask, gw, tmp_port_val, tmp_dhcp_val, useCable);
	state_mrls = STATE_NEED_REBOOT;
}
/**
 * @brief 
 * Функция установки параметра "DHCP use"
 * @param value Принятое значение от ПК для установки параметра
 */
void setterDHCP(void *value) {

	tmp_dhcp_val = (bool)*((uint32_t *)value);
	xil_printf_BLE("INFO_P: setterDHCP\r\n");

	DHCP_Param = (Param){{tmp_dhcp_val, PARAM_COUNT_NET_GROUP, 5, "DHCP_val",  "NetGroup", PARAM_TYPE_BIT},		setterDHCP,	getterDHCP};
	addParam(&DHCP_Param);
	WriteSD_setNetData(ipaddr, netmask, gw, tmp_port_val, tmp_dhcp_val, useCable);
	state_mrls = STATE_NEED_REBOOT;
}
/**
 * @brief 
 * Функция установки параметра "Аттенюатор передатчика"
 * @param value Принятое значение от ПК для установки параметра
 */
void setterATTEN(void *value) {

	tmp_ATTEN_val = (u8)*((uint8_t *)value);
	xil_printf_BLE("INFO_P: setterATTEN->%d\r\n", tmp_ATTEN_val);

	TX_atten_Param = (Param){{tmp_ATTEN_val, PARAM_COUNT_TX_GROUP, 1, "TX_atten",  "TXGroup", PARAM_TYPE_UINT8}, setterATTEN,	getterATTEN};
	addParam(&TX_atten_Param);
	HMC769_setAtten(tmp_ATTEN_val);
}
/**
 * @brief
 *
 * @param value
 */
void setterRXnumAMP(void *value) {}
/**
 * @brief Set the terPAMP State object
 * Функция установки параметра "PAMP state"
 * @param value Принятое значение от ПК для установки параметра
 */
void setterPAMP_State(void *value) {

	tmp_PAMP_val = (bool)*((bool *)value);
	xil_printf_BLE("INFO_P: setterPAMP->%d\r\n", tmp_PAMP_val);

	if(tmp_PAMP_val)
		configReciever(PAMP_state, 	ON, 0);
	else
		configReciever(PAMP_state, 	OFF, 0);
}
void setterUseCable(void *value) {

	tmp_UseCable_val = (bool)(*(bool *)value);
	xil_printf_BLE("INFO_P: setterUseCable->%d\r\n", tmp_UseCable_val);

	WriteSD_setNetData(ipaddr, netmask, gw, tmp_port_val, tmp_dhcp_val, tmp_UseCable_val);
	//HMC769_configIC(tmp_UseCable_val);
}
/****************************************************************/
/**
 * @brief 
 * Функция чтения параметра "IP адрес" из МРЛС и передача на ПК
 * @param value  
 */
void getterIP(void *value) {}
/**
 * @brief 
 * Функция чтения параметра "NM адрес" из МРЛС и передача на ПК
 * @param value 
 */
void getterNM(void *value) {}
/**
 * @brief 
 * Функция чтения параметра "GW адрес" из МРЛС и передача на ПК
 * @param value 
 */
void getterGW(void *value) {}
/**
 * @brief 
 * Функция чтения параметра "UDP Port" из МРЛС и передача на ПК
 * @param value 
 */
void getterPort(void *value) {}
/**
 * @brief 
 * Функция чтения параметра "DHCP use" из МРЛС и передача на ПК
 * @param value 
 */
void getterDHCP(void *value) {}
/**
 * @brief 
 * Функция взятия параметра из МРЛС и передача его на ПК
 * @param value 
 */
void getterRXnumAMP(void *value) {}
/**
 * @brief Get the Param object
 * Функция взятия параметра из МРЛС и передача его на ПК
 * @param Param Параметр который необходимо взять из МРЛС и передать на ПК
 * @param data 
 */
void getParam(mavlink_param_value_t *Param, void* data){

	xil_printf_BLE("INFO_P: getParam->%d\r\n", (Param->param_index));
	params[(Param->param_index)].getter(data);
}
/**
 * @brief 
 * Функция чтения параметра "Аттенюатор" из МРЛС и передача на ПК
 * @param value Принятое значение от ПК для установки параметра
 */
void getterATTEN(void *value) {}
/**
 * @brief Get the terPAMP State object
 *
 * @param value
 */
void getterPAMP_State(void *value) {}
void getterUseCable(void *value) {

	tmp_PAMP_val = (bool)*((bool *)value);
	xil_printf_BLE("INFO_P: setterPAMP->%d\r\n", tmp_PAMP_val);

	if(tmp_PAMP_val)
		configReciever(PAMP_state, 	ON, 0);
	else
		configReciever(PAMP_state, 	OFF, 0);
}
