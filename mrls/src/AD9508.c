

/***************************** Include Files *******************************/
#include "AD9508.h"

/************************** Function Definitions ***************************/
void AD9508_writeData(u8 adr, u8 data){
    // Первый байт, первого входного регистра IP-ядра связан с шиной ДАННЫХ модуля передачи конфигурации в МС AD9508 (clk_send)
    // Второй байт, первого входного регистра IP-ядра связан с шиной АДРЕСА модуля передачи конфигурации в МС AD9508 (clk_send)
    AD9508_mWriteReg(XPAR_AD9508_0_S00_AXI_BASEADDR, AD9508_S00_AXI_SLV_REG0_OFFSET, (adr << 8) | data);
       for(u32 i = 0; i<100; i++){}  // Задержка между формированием адреса\данных и сигналом начала передачи конфинурации
    // Второй бит, второго входного регистра IP-ядра связан с сигналом старта модуля передачи конфигурации
    AD9508_mWriteReg(XPAR_AD9508_0_S00_AXI_BASEADDR, AD9508_S00_AXI_SLV_REG1_OFFSET, 0x1);
    	for(u32 i = 0; i<1000; i++){}
    AD9508_mWriteReg(XPAR_AD9508_0_S00_AXI_BASEADDR, AD9508_S00_AXI_SLV_REG1_OFFSET, 0x0);
    	for(u32 i = 0; i<1000; i++){}
}
void ADC_Init(void){

	    AD9508_writeData(0x00, 0x00);
	    AD9508_writeData(0x0A, 0x00);
	    AD9508_writeData(0x0B, 0x00);
	    AD9508_writeData(0x0C, 0x05);
	    AD9508_writeData(0x0D, 0x00);
	    AD9508_writeData(0x12, 0x02);
	    AD9508_writeData(0x13, 0x00);
	    AD9508_writeData(0x14, 0x01);

	    AD9508_writeData(0x15, 0x09);
	    AD9508_writeData(0x16, 0x00);
	    AD9508_writeData(0x17, 0);
	    AD9508_writeData(0x18, 0x00);
	    AD9508_writeData(0x19, 0x14);
	    AD9508_writeData(0x1A, 0x00);

	    AD9508_writeData(0x1B, 0x09);
	    AD9508_writeData(0x1C, 0x00);
	    AD9508_writeData(0x1D, 0x00);
	    AD9508_writeData(0x1E, 0x00);
	    AD9508_writeData(0x1F, 0x14);
	    AD9508_writeData(0x20, 0x00);

	    AD9508_writeData(0x21, 0x09);
	    AD9508_writeData(0x22, 0x00);
	    AD9508_writeData(0x23, 0x00);
	    AD9508_writeData(0x24, 0x00);
	    AD9508_writeData(0x25, 0x14);
	    AD9508_writeData(0x26, 0x00);

	    AD9508_writeData(0x27, 0x09);
	    AD9508_writeData(0x28, 0x00);
	    AD9508_writeData(0x29, 0x00);
	    AD9508_writeData(0x2A, 0x00);
	    AD9508_writeData(0x2B, 0x14);
	    AD9508_writeData(0x2C, 0x00);

	    xil_printf_BLE("INFO: AD9508 force clk 10MHz \r\n");
}
void AD9508_setDebugLed(bool on) {
	AD9508_mWriteReg(XPAR_AD9508_0_S00_AXI_BASEADDR, AD9508_S00_AXI_SLV_REG1_OFFSET, on? 0x2 : 0x0);
}
