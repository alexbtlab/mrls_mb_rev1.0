#include "SendData.h"

bool GPS_data_Recieved_Done 	= false;
bool GPS_data_tramsmit_to_Diman = false;
bool UART_Interrupt_Enable 		= true;
bool flag_HB_done_to_transmit   = false;
bool flag_XADC_Data_Done        = false;

u32 V_temp_Raw_Data;
u16 Vaux_01_RawData;
u32 Vaux_09_RawData;
float Temp_FPGA;
float Vaux_01_Data;
float Vaux_09_Data;

void ReadAndSend_TemperatureAndPowerToPC(){

	mavlink_message_t 	msgTx;
	mavlink_amplifier_t msgTx_amp;
	mavlink_temp_raw_t  msgTx_temp_raw;

	char bufTx_Temp[64];
	char bufTx_AMP[64];

	// Температуру FPGA
	V_temp_Raw_Data = XSysMon_GetAdcData(SysMonInstPtr, XSM_CH_TEMP);
	Temp_FPGA = XSysMon_RawToTemperature(V_temp_Raw_Data);
//	xil_printf_BLE("INFO: Temp_FPGA    : %d.%d Grad   ", (int)(Temp_FPGA), SysMonFractionToInt(Temp_FPGA));

	// Температуру передатчика AMP
	Vaux_01_RawData = XSysMon_GetAdcData(SysMonInstPtr, 16 + 1);
	Vaux_01_Data = RawToVoltage(Vaux_01_RawData);
//	xil_printf_BLE("Temp_PAMP-> %2d.%2d Grad   ", (int)(Vaux_01_Data * 100), SysMonFractionToInt(Vaux_01_Data * 100));

	// Мощность передатчика в dBm
	Vaux_09_RawData = XSysMon_GetAdcData(SysMonInstPtr, 16 + 9);
	Vaux_09_Data = RawToVoltage(Vaux_09_RawData);
//	xil_printf_BLE("Power : %2d dbm \r\n", (u32)(Vaux_09_Data * 33));

	msgTx_amp.time   = sys_tick;
	msgTx_amp.power  = (int8_t)(Vaux_09_Data * 33);
	msgTx_amp.temp   = (int8_t)(Vaux_01_Data * 100);

	mavlink_msg_amplifier_encode(SYS_ID, COMPONENT_ID, &msgTx, &msgTx_amp);
	u16 len_amp = mavlink_msg_to_send_buffer((uint8_t *)bufTx_AMP, &msgTx);
	my_udp_send(bufTx_AMP, len_amp, &ip_addr_broadcast);

	msgTx_temp_raw.time 	 = sys_tick;
	msgTx_temp_raw.fpga 	 = (int8_t)Temp_FPGA;
	msgTx_temp_raw.amplifier = (int8_t)(Vaux_01_Data * 100);
	msgTx_temp_raw.drive 	 = (int8_t)(Vaux_01_Data * 100);
	msgTx_temp_raw.emitter	 = (int8_t)(Vaux_01_Data * 100);

	mavlink_msg_temp_raw_encode(SYS_ID, COMPONENT_ID, &msgTx, &msgTx_temp_raw);
	u16 len_temp = mavlink_msg_to_send_buffer((uint8_t *)bufTx_Temp, &msgTx);
	my_udp_send(bufTx_Temp, len_temp, &ip_addr_broadcast);

}
void SendGpsToPC(){

	mavlink_message_t 	msgTx;
	mavlink_gps_raw_t 	msgTx_gps;
	char bufTx_GPS[64];

	msgTx_gps.time = sys_tick;
	msgTx_gps.lat  = (int32_t)(lat * 10000000);	//557825400;
	msgTx_gps.lon  = (int32_t)(lon * 10000000);	//375819660;
	msgTx_gps.sat  = sat;

	mavlink_msg_gps_raw_encode(SYS_ID, COMPONENT_ID, &msgTx, &msgTx_gps);
	u16 len_gps = mavlink_msg_to_send_buffer((uint8_t *)bufTx_GPS, &msgTx);
	my_udp_send(bufTx_GPS, len_gps, &ip_addr_broadcast);

//		xil_printf_BLE("lat:%d.%d    ", (int)lat,	SysMonFractionToInt(lat));
//		xil_printf_BLE("lon:%d.%d    ", (int)lon,	SysMonFractionToInt(lon));
//		xil_printf_BLE("sat:%d.%d\r\n", (int)sat,	SysMonFractionToInt(sat));

	GPS_data_Recieved_Done = false;
	GPS_data_tramsmit_to_Diman = true;
}
void SendHeartBeat(){

	mavlink_message_t 	msgTx;
	char bufTx_HB[16];

	//HeartBeat
	mavlink_msg_heartbeat_pack(SYS_ID, COMPONENT_ID, &msgTx, state_mrls, VERSION);
	u16 len_hb = mavlink_msg_to_send_buffer((uint8_t *)bufTx_HB, &msgTx);
	my_udp_send((char *)bufTx_HB, len_hb, &ip_addr_broadcast);
}
void SendMeasurementDataToPC(){

	u64 temp_time = 0;
	/* Если приняли от Димана (с ПК) запрос на передачу всех параметров, то отдаем все имеющиеся параметры */
	if(needed_send_param){
		sendAllParams();
	}
	/* Раз в секунду передаем  */
	if(sys_tick - temp_time > INTERVAL_TRANSMIT_MEASUREMENT_DATA){
		temp_time = sys_tick;
		ReadAndSend_TemperatureAndPowerToPC();	// Читаем и передам  измеренные даные
		SendHeartBeat();						// Передаем HB
	}
	/* Если приняли данные GPS то необходимо отдать их Диману (на ПК) */
	if(GPS_data_Recieved_Done){
		SendGpsToPC();				// Передаем GPS полученные по UART
	}
}
void SendRawDataToPC(){

	if(!mem_empty){
		my_udp_send_RAWD(praw, FRAME_SIZE_DDR2PC*sizeof(uint32_t), &ip_addr_broadcast);
		mem_empty = true;
	}
	/* Проверка состояния модуля DMA на наличие ошибки передачи данных  */
	int	Status = readDmaStatusRegister();
	if (Status != XST_SUCCESS){
		xil_printf_BLE("ERROR: readDmaStatusRegister Failed\r\n");
		while(true){}
	}
}
