#ifndef SRC_NET_MRLS_H_
#define SRC_NET_MRLS_H_

#include <assert.h>
#include "PmodSD.h"
#include "lwip/init.h"
#include "lwip/priv/tcp_priv.h"
#include "lwip/tcp.h"
#include "mavlink_parser/mavlink_parser.h"
#include "netif/xadapter.h"
#include "platform.h"
#include <stdio.h>
#include <stddef.h>
#include "platform_config.h"
#include "uart.h"
#include "configReciever.h"
//#include "HMC769_v1.0.h"
#include "lwip/udp.h"
#include "lwip/dhcp.h"
#include "SD.h"
#include "parameters.h"
#include "parameters.h"
#include "lwip/udp.h"
#include "xil_printf_BLE.h"

#define MAX_LEN_INI 10
#define PARAM_COUNT_NET_GROUP 5

extern u32 BuffSizeMavLink;
extern const int * base_adr_mem_data_from_PL;
extern struct netif *echo_netif;
extern volatile int dhcp_timoutcntr;
extern u64 sys_tick;
extern uint32_t cnt_azimut;
extern uint32_t cnt_azimut_1ms;

#ifdef __cplusplus
extern "C" {
#endif

	void print_ip(char *msg, ip_addr_t *ip);
	void print_ip_settings(ip_addr_t *ip, ip_addr_t *mask, ip_addr_t *gw);
	void start_tcp_server(void);
	void tcp_server_init(void);
	int my_udp_server_init(void* buffer, size_t bufferlen);
	ssize_t my_udp_send(void* data, size_t datalen, const ip_addr_t* dest_ip);
	ssize_t my_udp_send_RAWD(void* data, size_t datalen, const ip_addr_t* dest_ip);

#ifdef __cplusplus
}
#endif

void ReadSD_HMCConfig(void);

#endif /* SRC_NET_MRLS_H_ */
