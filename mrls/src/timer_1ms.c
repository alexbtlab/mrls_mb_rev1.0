#include "timer_1ms.h"

u64 sys_tick = 0;
u64 cnt_azimut_1ms = 0;
uint32_t cnt_azimut;
bool clear_angel = true;

static void timer_handler(void *p);

void init_timer_1ms(){

	/* set the number of cycles the timer counts before interrupting */
	/* 100 Mhz clock => .01us for 1 clk tick. For 100ms, 10000000 clk ticks need to elapse  */
	XTmrCtr_SetLoadReg(XPAR_AXI_TIMER_3_BASEADDR, 0, 100000);
	//XTmrCtr_GetLoadReg(XPAR_AXI_TIMER_2_BASEADDR, 0);
	/* reset the timers, and clear interrupts */
	XTmrCtr_SetControlStatusReg(XPAR_AXI_TIMER_3_BASEADDR, 0, XTC_CSR_INT_OCCURED_MASK | XTC_CSR_LOAD_MASK );

	/* start the timers */
	XTmrCtr_SetControlStatusReg(XPAR_AXI_TIMER_3_BASEADDR, 0,
			   XTC_CSR_ENABLE_TMR_MASK | XTC_CSR_ENABLE_INT_MASK
			| XTC_CSR_AUTO_RELOAD_MASK | XTC_CSR_DOWN_COUNT_MASK);

	/* Register Timer handler */
	XIntc_RegisterHandler(XPAR_INTC_0_BASEADDR,
			XPAR_MICROBLAZE_0_AXI_INTC_AXI_TIMER_3_INTERRUPT_INTR,
			(XInterruptHandler)timer_handler,
			0);
}
static void timer_handler(void *p){

	timer_1ms_callback();

	/* Load timer, clear interrupt bit */
	XTmrCtr_SetControlStatusReg(XPAR_AXI_TIMER_3_BASEADDR, 0,
			XTC_CSR_INT_OCCURED_MASK
			| XTC_CSR_LOAD_MASK);

	XTmrCtr_SetControlStatusReg(XPAR_AXI_TIMER_3_BASEADDR, 0,
			XTC_CSR_ENABLE_TMR_MASK
			| XTC_CSR_ENABLE_INT_MASK
			| XTC_CSR_AUTO_RELOAD_MASK
			| XTC_CSR_DOWN_COUNT_MASK);

	XIntc_AckIntr(XPAR_INTC_0_BASEADDR, PLATFORM_TIMER_3_INTERRUPT_MASK);
}
void timer_1ms_callback(){

	if(sys_tick == 0xFFFFFFFFFFFFFFFF)
		sys_tick = 0;
	else
		sys_tick++;
}

void InterruptHandler_ENCODE(){

//	clear_angel = true;				// Приняли сигнал "АЗИМУТ 0" от АЦЧ.
	cnt_azimut = 0;
	cnt_azimut_1ms = sys_tick;
}

