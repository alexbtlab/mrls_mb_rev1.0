#ifndef SRC_DMA_MY_H_
#define SRC_DMA_MY_H_

#include "xparameters.h"
#include "xil_io.h"
#include "xstatus.h"
//#include "HMC769_v1.0.h"

#define FRAME_SIZE					8192
#define SIZE_BYTE_SEND_ARRAY		FRAME_SIZE*4
/*--------DMA IP Register-------------------------------*/
#define S2MM_DMASR_OFFSET			0x34
#define S2MM_DA_OFFSET		    	0x48
#define S2MM_LENGTH_OFFSET		    0x58
#define S2MM_DMACR_OFFSET		    0x30

extern volatile bool mem_empty;

	void InitializeAxiDMA(bool enable_DMA);
	void startDmaTransfer(unsigned int dstAddr, unsigned int len);
	void InterruptHandler_DMA(void);
	void InterruptHandler_DMA_readBackground(void);
	void InterruptHandler_GPIO(void);
	void InterruptHandler_GPIO_readBackground(void);
	int readDmaStatusRegister(void);

#endif /* SRC_DMA_MY_H_ */
