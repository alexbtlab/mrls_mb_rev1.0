#ifndef SRC_INIT_H_
#define SRC_INIT_H_

//#include "HMC769_v1.0.h"
#include "xadc.h"
//#include "AD9508.h"
#include "configReciever.h"
#include "net_mrls.h"
#include "DMA_my.h"
#include "net_mrls.h"
#include "timer_8ms.h"
#include "timer_1s.h"
#include "timer_1ms.h"
#include "mavlink_my.h"
#include "parameters.h"
#include "SD.h"
#include "xil_printf_BLE.h"
#include "SendData.h"

extern Param TX_atten_Param;
extern Param PAMP_state_Param;
extern Param RX_Num_Amp_Param;

extern int32_t raw;
extern int32_t* praw;

	void init_mb(void);
	void switchGpio(void);

#endif /* SRC_INIT_H_ */
