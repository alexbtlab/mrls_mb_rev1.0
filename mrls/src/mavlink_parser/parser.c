#include "mavlink_parser.h"


mavlink_message_t rxMessage;
mavlink_status_t rxStatus;

void parser(const char* buffer, size_t len)
{
    mavlink_message_t message;
    mavlink_status_t status;
    for (int pos = 0; pos < len; pos++) {
        uint8_t res = mavlink_frame_char_buffer(&rxMessage, &rxStatus, (uint8_t)buffer[pos], &message, &status);
        switch (res) {
        case MAVLINK_FRAMING_INCOMPLETE:
            break;
        case MAVLINK_FRAMING_OK: {
            handler(&message);
        } break;
        case MAVLINK_FRAMING_BAD_CRC:
            printf("Receive message error: BAD_CRC\n");
            break;
        case MAVLINK_FRAMING_BAD_SIGNATURE:
            printf("Receive message error: BAD_SIGNATURE\n");
            break;
        }
    }
}
