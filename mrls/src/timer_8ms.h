#ifndef SRC_TIMER_8MS_H_
#define SRC_TIMER_8MS_H_

#include "xtmrctr_l.h"
#include "xintc.h"
#include "platform_config.h"
#include "AD9508.h"

	void init_timer_8ms(void);
	void timer_8ms_callback(void);
	void InterruptHandler_ENCODE(void);

#endif /* SRC_TIMER_8MS_H_ */
