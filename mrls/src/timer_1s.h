#ifndef SRC_TIMER_1S_H_
#define SRC_TIMER_1S_H_

#include "xtmrctr_l.h"
#include "xintc.h"
#include "platform_config.h"
#include "mavlink_parser/mavlink_parser.h"
#include "lwip/udp.h"
#include "net_mrls.h"
#include "xadc.h"

	void init_timer_1s(void);
	void timer_1s_callback(void);

#endif /* SRC_TIMER_1S_H_ */
