#pragma once
// MESSAGE TEMP_RAW PACKING

#define MAVLINK_MSG_ID_TEMP_RAW 17


typedef struct __mavlink_temp_raw_t {
 uint32_t time; /*<  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.*/
 int8_t fpga; /*<   .*/
 int8_t amplifier; /*<   .*/
 int8_t drive; /*<   .*/
 int8_t emitter; /*<   .*/
 int8_t reserv_1; /*<  .*/
 int8_t reserv_2; /*<  .*/
} mavlink_temp_raw_t;

#define MAVLINK_MSG_ID_TEMP_RAW_LEN 10
#define MAVLINK_MSG_ID_TEMP_RAW_MIN_LEN 10
#define MAVLINK_MSG_ID_17_LEN 10
#define MAVLINK_MSG_ID_17_MIN_LEN 10

#define MAVLINK_MSG_ID_TEMP_RAW_CRC 235
#define MAVLINK_MSG_ID_17_CRC 235



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_TEMP_RAW { \
    17, \
    "TEMP_RAW", \
    7, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_temp_raw_t, time) }, \
         { "fpga", NULL, MAVLINK_TYPE_INT8_T, 0, 4, offsetof(mavlink_temp_raw_t, fpga) }, \
         { "amplifier", NULL, MAVLINK_TYPE_INT8_T, 0, 5, offsetof(mavlink_temp_raw_t, amplifier) }, \
         { "drive", NULL, MAVLINK_TYPE_INT8_T, 0, 6, offsetof(mavlink_temp_raw_t, drive) }, \
         { "emitter", NULL, MAVLINK_TYPE_INT8_T, 0, 7, offsetof(mavlink_temp_raw_t, emitter) }, \
         { "reserv_1", NULL, MAVLINK_TYPE_INT8_T, 0, 8, offsetof(mavlink_temp_raw_t, reserv_1) }, \
         { "reserv_2", NULL, MAVLINK_TYPE_INT8_T, 0, 9, offsetof(mavlink_temp_raw_t, reserv_2) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_TEMP_RAW { \
    "TEMP_RAW", \
    7, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_temp_raw_t, time) }, \
         { "fpga", NULL, MAVLINK_TYPE_INT8_T, 0, 4, offsetof(mavlink_temp_raw_t, fpga) }, \
         { "amplifier", NULL, MAVLINK_TYPE_INT8_T, 0, 5, offsetof(mavlink_temp_raw_t, amplifier) }, \
         { "drive", NULL, MAVLINK_TYPE_INT8_T, 0, 6, offsetof(mavlink_temp_raw_t, drive) }, \
         { "emitter", NULL, MAVLINK_TYPE_INT8_T, 0, 7, offsetof(mavlink_temp_raw_t, emitter) }, \
         { "reserv_1", NULL, MAVLINK_TYPE_INT8_T, 0, 8, offsetof(mavlink_temp_raw_t, reserv_1) }, \
         { "reserv_2", NULL, MAVLINK_TYPE_INT8_T, 0, 9, offsetof(mavlink_temp_raw_t, reserv_2) }, \
         } \
}
#endif

/**
 * @brief Pack a temp_raw message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param fpga   .
 * @param amplifier   .
 * @param drive   .
 * @param emitter   .
 * @param reserv_1  .
 * @param reserv_2  .
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_temp_raw_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t time, int8_t fpga, int8_t amplifier, int8_t drive, int8_t emitter, int8_t reserv_1, int8_t reserv_2)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_TEMP_RAW_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int8_t(buf, 4, fpga);
    _mav_put_int8_t(buf, 5, amplifier);
    _mav_put_int8_t(buf, 6, drive);
    _mav_put_int8_t(buf, 7, emitter);
    _mav_put_int8_t(buf, 8, reserv_1);
    _mav_put_int8_t(buf, 9, reserv_2);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_TEMP_RAW_LEN);
#else
    mavlink_temp_raw_t packet;
    packet.time = time;
    packet.fpga = fpga;
    packet.amplifier = amplifier;
    packet.drive = drive;
    packet.emitter = emitter;
    packet.reserv_1 = reserv_1;
    packet.reserv_2 = reserv_2;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_TEMP_RAW_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_TEMP_RAW;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_TEMP_RAW_MIN_LEN, MAVLINK_MSG_ID_TEMP_RAW_LEN, MAVLINK_MSG_ID_TEMP_RAW_CRC);
}

/**
 * @brief Pack a temp_raw message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param fpga   .
 * @param amplifier   .
 * @param drive   .
 * @param emitter   .
 * @param reserv_1  .
 * @param reserv_2  .
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_temp_raw_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t time,int8_t fpga,int8_t amplifier,int8_t drive,int8_t emitter,int8_t reserv_1,int8_t reserv_2)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_TEMP_RAW_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int8_t(buf, 4, fpga);
    _mav_put_int8_t(buf, 5, amplifier);
    _mav_put_int8_t(buf, 6, drive);
    _mav_put_int8_t(buf, 7, emitter);
    _mav_put_int8_t(buf, 8, reserv_1);
    _mav_put_int8_t(buf, 9, reserv_2);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_TEMP_RAW_LEN);
#else
    mavlink_temp_raw_t packet;
    packet.time = time;
    packet.fpga = fpga;
    packet.amplifier = amplifier;
    packet.drive = drive;
    packet.emitter = emitter;
    packet.reserv_1 = reserv_1;
    packet.reserv_2 = reserv_2;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_TEMP_RAW_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_TEMP_RAW;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_TEMP_RAW_MIN_LEN, MAVLINK_MSG_ID_TEMP_RAW_LEN, MAVLINK_MSG_ID_TEMP_RAW_CRC);
}

/**
 * @brief Encode a temp_raw struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param temp_raw C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_temp_raw_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_temp_raw_t* temp_raw)
{
    return mavlink_msg_temp_raw_pack(system_id, component_id, msg, temp_raw->time, temp_raw->fpga, temp_raw->amplifier, temp_raw->drive, temp_raw->emitter, temp_raw->reserv_1, temp_raw->reserv_2);
}

/**
 * @brief Encode a temp_raw struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param temp_raw C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_temp_raw_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_temp_raw_t* temp_raw)
{
    return mavlink_msg_temp_raw_pack_chan(system_id, component_id, chan, msg, temp_raw->time, temp_raw->fpga, temp_raw->amplifier, temp_raw->drive, temp_raw->emitter, temp_raw->reserv_1, temp_raw->reserv_2);
}

/**
 * @brief Send a temp_raw message
 * @param chan MAVLink channel to send the message
 *
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param fpga   .
 * @param amplifier   .
 * @param drive   .
 * @param emitter   .
 * @param reserv_1  .
 * @param reserv_2  .
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_temp_raw_send(mavlink_channel_t chan, uint32_t time, int8_t fpga, int8_t amplifier, int8_t drive, int8_t emitter, int8_t reserv_1, int8_t reserv_2)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_TEMP_RAW_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int8_t(buf, 4, fpga);
    _mav_put_int8_t(buf, 5, amplifier);
    _mav_put_int8_t(buf, 6, drive);
    _mav_put_int8_t(buf, 7, emitter);
    _mav_put_int8_t(buf, 8, reserv_1);
    _mav_put_int8_t(buf, 9, reserv_2);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_TEMP_RAW, buf, MAVLINK_MSG_ID_TEMP_RAW_MIN_LEN, MAVLINK_MSG_ID_TEMP_RAW_LEN, MAVLINK_MSG_ID_TEMP_RAW_CRC);
#else
    mavlink_temp_raw_t packet;
    packet.time = time;
    packet.fpga = fpga;
    packet.amplifier = amplifier;
    packet.drive = drive;
    packet.emitter = emitter;
    packet.reserv_1 = reserv_1;
    packet.reserv_2 = reserv_2;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_TEMP_RAW, (const char *)&packet, MAVLINK_MSG_ID_TEMP_RAW_MIN_LEN, MAVLINK_MSG_ID_TEMP_RAW_LEN, MAVLINK_MSG_ID_TEMP_RAW_CRC);
#endif
}

/**
 * @brief Send a temp_raw message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_temp_raw_send_struct(mavlink_channel_t chan, const mavlink_temp_raw_t* temp_raw)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_temp_raw_send(chan, temp_raw->time, temp_raw->fpga, temp_raw->amplifier, temp_raw->drive, temp_raw->emitter, temp_raw->reserv_1, temp_raw->reserv_2);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_TEMP_RAW, (const char *)temp_raw, MAVLINK_MSG_ID_TEMP_RAW_MIN_LEN, MAVLINK_MSG_ID_TEMP_RAW_LEN, MAVLINK_MSG_ID_TEMP_RAW_CRC);
#endif
}

#if MAVLINK_MSG_ID_TEMP_RAW_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_temp_raw_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t time, int8_t fpga, int8_t amplifier, int8_t drive, int8_t emitter, int8_t reserv_1, int8_t reserv_2)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int8_t(buf, 4, fpga);
    _mav_put_int8_t(buf, 5, amplifier);
    _mav_put_int8_t(buf, 6, drive);
    _mav_put_int8_t(buf, 7, emitter);
    _mav_put_int8_t(buf, 8, reserv_1);
    _mav_put_int8_t(buf, 9, reserv_2);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_TEMP_RAW, buf, MAVLINK_MSG_ID_TEMP_RAW_MIN_LEN, MAVLINK_MSG_ID_TEMP_RAW_LEN, MAVLINK_MSG_ID_TEMP_RAW_CRC);
#else
    mavlink_temp_raw_t *packet = (mavlink_temp_raw_t *)msgbuf;
    packet->time = time;
    packet->fpga = fpga;
    packet->amplifier = amplifier;
    packet->drive = drive;
    packet->emitter = emitter;
    packet->reserv_1 = reserv_1;
    packet->reserv_2 = reserv_2;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_TEMP_RAW, (const char *)packet, MAVLINK_MSG_ID_TEMP_RAW_MIN_LEN, MAVLINK_MSG_ID_TEMP_RAW_LEN, MAVLINK_MSG_ID_TEMP_RAW_CRC);
#endif
}
#endif

#endif

// MESSAGE TEMP_RAW UNPACKING


/**
 * @brief Get field time from temp_raw message
 *
 * @return  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 */
static inline uint32_t mavlink_msg_temp_raw_get_time(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field fpga from temp_raw message
 *
 * @return   .
 */
static inline int8_t mavlink_msg_temp_raw_get_fpga(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int8_t(msg,  4);
}

/**
 * @brief Get field amplifier from temp_raw message
 *
 * @return   .
 */
static inline int8_t mavlink_msg_temp_raw_get_amplifier(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int8_t(msg,  5);
}

/**
 * @brief Get field drive from temp_raw message
 *
 * @return   .
 */
static inline int8_t mavlink_msg_temp_raw_get_drive(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int8_t(msg,  6);
}

/**
 * @brief Get field emitter from temp_raw message
 *
 * @return   .
 */
static inline int8_t mavlink_msg_temp_raw_get_emitter(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int8_t(msg,  7);
}

/**
 * @brief Get field reserv_1 from temp_raw message
 *
 * @return  .
 */
static inline int8_t mavlink_msg_temp_raw_get_reserv_1(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int8_t(msg,  8);
}

/**
 * @brief Get field reserv_2 from temp_raw message
 *
 * @return  .
 */
static inline int8_t mavlink_msg_temp_raw_get_reserv_2(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int8_t(msg,  9);
}

/**
 * @brief Decode a temp_raw message into a struct
 *
 * @param msg The message to decode
 * @param temp_raw C-struct to decode the message contents into
 */
static inline void mavlink_msg_temp_raw_decode(const mavlink_message_t* msg, mavlink_temp_raw_t* temp_raw)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    temp_raw->time = mavlink_msg_temp_raw_get_time(msg);
    temp_raw->fpga = mavlink_msg_temp_raw_get_fpga(msg);
    temp_raw->amplifier = mavlink_msg_temp_raw_get_amplifier(msg);
    temp_raw->drive = mavlink_msg_temp_raw_get_drive(msg);
    temp_raw->emitter = mavlink_msg_temp_raw_get_emitter(msg);
    temp_raw->reserv_1 = mavlink_msg_temp_raw_get_reserv_1(msg);
    temp_raw->reserv_2 = mavlink_msg_temp_raw_get_reserv_2(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_TEMP_RAW_LEN? msg->len : MAVLINK_MSG_ID_TEMP_RAW_LEN;
        memset(temp_raw, 0, MAVLINK_MSG_ID_TEMP_RAW_LEN);
    memcpy(temp_raw, _MAV_PAYLOAD(msg), len);
#endif
}
