#pragma once
// MESSAGE GPS_RAW PACKING

#define MAVLINK_MSG_ID_GPS_RAW 16


typedef struct __mavlink_gps_raw_t {
 uint32_t time; /*<  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.*/
 int32_t lat; /*<  Latitude (WGS84, EGM96 ellipsoid)*/
 int32_t lon; /*<  Longitude (WGS84, EGM96 ellipsoid)*/
 int32_t alt; /*<  Altitude (MSL). Positive for up. Note that virtually all GPS modules provide the MSL altitude in addition to the WGS84 altitude.*/
 uint16_t vel; /*<  GPS ground speed. If unknown, set to: UINT16_MAX*/
 uint8_t sat; /*<  Number of satellites visible. If unknown, set to 255*/
 uint8_t fix; /*<  GPS fix type.*/
} mavlink_gps_raw_t;

#define MAVLINK_MSG_ID_GPS_RAW_LEN 20
#define MAVLINK_MSG_ID_GPS_RAW_MIN_LEN 20
#define MAVLINK_MSG_ID_16_LEN 20
#define MAVLINK_MSG_ID_16_MIN_LEN 20

#define MAVLINK_MSG_ID_GPS_RAW_CRC 75
#define MAVLINK_MSG_ID_16_CRC 75



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_GPS_RAW { \
    16, \
    "GPS_RAW", \
    7, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_gps_raw_t, time) }, \
         { "lat", NULL, MAVLINK_TYPE_INT32_T, 0, 4, offsetof(mavlink_gps_raw_t, lat) }, \
         { "lon", NULL, MAVLINK_TYPE_INT32_T, 0, 8, offsetof(mavlink_gps_raw_t, lon) }, \
         { "alt", NULL, MAVLINK_TYPE_INT32_T, 0, 12, offsetof(mavlink_gps_raw_t, alt) }, \
         { "vel", NULL, MAVLINK_TYPE_UINT16_T, 0, 16, offsetof(mavlink_gps_raw_t, vel) }, \
         { "sat", NULL, MAVLINK_TYPE_UINT8_T, 0, 18, offsetof(mavlink_gps_raw_t, sat) }, \
         { "fix", NULL, MAVLINK_TYPE_UINT8_T, 0, 19, offsetof(mavlink_gps_raw_t, fix) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_GPS_RAW { \
    "GPS_RAW", \
    7, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_gps_raw_t, time) }, \
         { "lat", NULL, MAVLINK_TYPE_INT32_T, 0, 4, offsetof(mavlink_gps_raw_t, lat) }, \
         { "lon", NULL, MAVLINK_TYPE_INT32_T, 0, 8, offsetof(mavlink_gps_raw_t, lon) }, \
         { "alt", NULL, MAVLINK_TYPE_INT32_T, 0, 12, offsetof(mavlink_gps_raw_t, alt) }, \
         { "vel", NULL, MAVLINK_TYPE_UINT16_T, 0, 16, offsetof(mavlink_gps_raw_t, vel) }, \
         { "sat", NULL, MAVLINK_TYPE_UINT8_T, 0, 18, offsetof(mavlink_gps_raw_t, sat) }, \
         { "fix", NULL, MAVLINK_TYPE_UINT8_T, 0, 19, offsetof(mavlink_gps_raw_t, fix) }, \
         } \
}
#endif

/**
 * @brief Pack a gps_raw message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param lat  Latitude (WGS84, EGM96 ellipsoid)
 * @param lon  Longitude (WGS84, EGM96 ellipsoid)
 * @param alt  Altitude (MSL). Positive for up. Note that virtually all GPS modules provide the MSL altitude in addition to the WGS84 altitude.
 * @param vel  GPS ground speed. If unknown, set to: UINT16_MAX
 * @param sat  Number of satellites visible. If unknown, set to 255
 * @param fix  GPS fix type.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_gps_raw_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t time, int32_t lat, int32_t lon, int32_t alt, uint16_t vel, uint8_t sat, uint8_t fix)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_GPS_RAW_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int32_t(buf, 4, lat);
    _mav_put_int32_t(buf, 8, lon);
    _mav_put_int32_t(buf, 12, alt);
    _mav_put_uint16_t(buf, 16, vel);
    _mav_put_uint8_t(buf, 18, sat);
    _mav_put_uint8_t(buf, 19, fix);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_GPS_RAW_LEN);
#else
    mavlink_gps_raw_t packet;
    packet.time = time;
    packet.lat = lat;
    packet.lon = lon;
    packet.alt = alt;
    packet.vel = vel;
    packet.sat = sat;
    packet.fix = fix;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_GPS_RAW_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_GPS_RAW;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_GPS_RAW_MIN_LEN, MAVLINK_MSG_ID_GPS_RAW_LEN, MAVLINK_MSG_ID_GPS_RAW_CRC);
}

/**
 * @brief Pack a gps_raw message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param lat  Latitude (WGS84, EGM96 ellipsoid)
 * @param lon  Longitude (WGS84, EGM96 ellipsoid)
 * @param alt  Altitude (MSL). Positive for up. Note that virtually all GPS modules provide the MSL altitude in addition to the WGS84 altitude.
 * @param vel  GPS ground speed. If unknown, set to: UINT16_MAX
 * @param sat  Number of satellites visible. If unknown, set to 255
 * @param fix  GPS fix type.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_gps_raw_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t time,int32_t lat,int32_t lon,int32_t alt,uint16_t vel,uint8_t sat,uint8_t fix)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_GPS_RAW_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int32_t(buf, 4, lat);
    _mav_put_int32_t(buf, 8, lon);
    _mav_put_int32_t(buf, 12, alt);
    _mav_put_uint16_t(buf, 16, vel);
    _mav_put_uint8_t(buf, 18, sat);
    _mav_put_uint8_t(buf, 19, fix);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_GPS_RAW_LEN);
#else
    mavlink_gps_raw_t packet;
    packet.time = time;
    packet.lat = lat;
    packet.lon = lon;
    packet.alt = alt;
    packet.vel = vel;
    packet.sat = sat;
    packet.fix = fix;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_GPS_RAW_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_GPS_RAW;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_GPS_RAW_MIN_LEN, MAVLINK_MSG_ID_GPS_RAW_LEN, MAVLINK_MSG_ID_GPS_RAW_CRC);
}

/**
 * @brief Encode a gps_raw struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param gps_raw C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_gps_raw_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_gps_raw_t* gps_raw)
{
    return mavlink_msg_gps_raw_pack(system_id, component_id, msg, gps_raw->time, gps_raw->lat, gps_raw->lon, gps_raw->alt, gps_raw->vel, gps_raw->sat, gps_raw->fix);
}

/**
 * @brief Encode a gps_raw struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param gps_raw C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_gps_raw_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_gps_raw_t* gps_raw)
{
    return mavlink_msg_gps_raw_pack_chan(system_id, component_id, chan, msg, gps_raw->time, gps_raw->lat, gps_raw->lon, gps_raw->alt, gps_raw->vel, gps_raw->sat, gps_raw->fix);
}

/**
 * @brief Send a gps_raw message
 * @param chan MAVLink channel to send the message
 *
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param lat  Latitude (WGS84, EGM96 ellipsoid)
 * @param lon  Longitude (WGS84, EGM96 ellipsoid)
 * @param alt  Altitude (MSL). Positive for up. Note that virtually all GPS modules provide the MSL altitude in addition to the WGS84 altitude.
 * @param vel  GPS ground speed. If unknown, set to: UINT16_MAX
 * @param sat  Number of satellites visible. If unknown, set to 255
 * @param fix  GPS fix type.
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_gps_raw_send(mavlink_channel_t chan, uint32_t time, int32_t lat, int32_t lon, int32_t alt, uint16_t vel, uint8_t sat, uint8_t fix)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_GPS_RAW_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int32_t(buf, 4, lat);
    _mav_put_int32_t(buf, 8, lon);
    _mav_put_int32_t(buf, 12, alt);
    _mav_put_uint16_t(buf, 16, vel);
    _mav_put_uint8_t(buf, 18, sat);
    _mav_put_uint8_t(buf, 19, fix);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_GPS_RAW, buf, MAVLINK_MSG_ID_GPS_RAW_MIN_LEN, MAVLINK_MSG_ID_GPS_RAW_LEN, MAVLINK_MSG_ID_GPS_RAW_CRC);
#else
    mavlink_gps_raw_t packet;
    packet.time = time;
    packet.lat = lat;
    packet.lon = lon;
    packet.alt = alt;
    packet.vel = vel;
    packet.sat = sat;
    packet.fix = fix;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_GPS_RAW, (const char *)&packet, MAVLINK_MSG_ID_GPS_RAW_MIN_LEN, MAVLINK_MSG_ID_GPS_RAW_LEN, MAVLINK_MSG_ID_GPS_RAW_CRC);
#endif
}

/**
 * @brief Send a gps_raw message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_gps_raw_send_struct(mavlink_channel_t chan, const mavlink_gps_raw_t* gps_raw)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_gps_raw_send(chan, gps_raw->time, gps_raw->lat, gps_raw->lon, gps_raw->alt, gps_raw->vel, gps_raw->sat, gps_raw->fix);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_GPS_RAW, (const char *)gps_raw, MAVLINK_MSG_ID_GPS_RAW_MIN_LEN, MAVLINK_MSG_ID_GPS_RAW_LEN, MAVLINK_MSG_ID_GPS_RAW_CRC);
#endif
}

#if MAVLINK_MSG_ID_GPS_RAW_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_gps_raw_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t time, int32_t lat, int32_t lon, int32_t alt, uint16_t vel, uint8_t sat, uint8_t fix)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int32_t(buf, 4, lat);
    _mav_put_int32_t(buf, 8, lon);
    _mav_put_int32_t(buf, 12, alt);
    _mav_put_uint16_t(buf, 16, vel);
    _mav_put_uint8_t(buf, 18, sat);
    _mav_put_uint8_t(buf, 19, fix);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_GPS_RAW, buf, MAVLINK_MSG_ID_GPS_RAW_MIN_LEN, MAVLINK_MSG_ID_GPS_RAW_LEN, MAVLINK_MSG_ID_GPS_RAW_CRC);
#else
    mavlink_gps_raw_t *packet = (mavlink_gps_raw_t *)msgbuf;
    packet->time = time;
    packet->lat = lat;
    packet->lon = lon;
    packet->alt = alt;
    packet->vel = vel;
    packet->sat = sat;
    packet->fix = fix;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_GPS_RAW, (const char *)packet, MAVLINK_MSG_ID_GPS_RAW_MIN_LEN, MAVLINK_MSG_ID_GPS_RAW_LEN, MAVLINK_MSG_ID_GPS_RAW_CRC);
#endif
}
#endif

#endif

// MESSAGE GPS_RAW UNPACKING


/**
 * @brief Get field time from gps_raw message
 *
 * @return  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 */
static inline uint32_t mavlink_msg_gps_raw_get_time(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field lat from gps_raw message
 *
 * @return  Latitude (WGS84, EGM96 ellipsoid)
 */
static inline int32_t mavlink_msg_gps_raw_get_lat(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int32_t(msg,  4);
}

/**
 * @brief Get field lon from gps_raw message
 *
 * @return  Longitude (WGS84, EGM96 ellipsoid)
 */
static inline int32_t mavlink_msg_gps_raw_get_lon(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int32_t(msg,  8);
}

/**
 * @brief Get field alt from gps_raw message
 *
 * @return  Altitude (MSL). Positive for up. Note that virtually all GPS modules provide the MSL altitude in addition to the WGS84 altitude.
 */
static inline int32_t mavlink_msg_gps_raw_get_alt(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int32_t(msg,  12);
}

/**
 * @brief Get field vel from gps_raw message
 *
 * @return  GPS ground speed. If unknown, set to: UINT16_MAX
 */
static inline uint16_t mavlink_msg_gps_raw_get_vel(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  16);
}

/**
 * @brief Get field sat from gps_raw message
 *
 * @return  Number of satellites visible. If unknown, set to 255
 */
static inline uint8_t mavlink_msg_gps_raw_get_sat(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  18);
}

/**
 * @brief Get field fix from gps_raw message
 *
 * @return  GPS fix type.
 */
static inline uint8_t mavlink_msg_gps_raw_get_fix(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  19);
}

/**
 * @brief Decode a gps_raw message into a struct
 *
 * @param msg The message to decode
 * @param gps_raw C-struct to decode the message contents into
 */
static inline void mavlink_msg_gps_raw_decode(const mavlink_message_t* msg, mavlink_gps_raw_t* gps_raw)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    gps_raw->time = mavlink_msg_gps_raw_get_time(msg);
    gps_raw->lat = mavlink_msg_gps_raw_get_lat(msg);
    gps_raw->lon = mavlink_msg_gps_raw_get_lon(msg);
    gps_raw->alt = mavlink_msg_gps_raw_get_alt(msg);
    gps_raw->vel = mavlink_msg_gps_raw_get_vel(msg);
    gps_raw->sat = mavlink_msg_gps_raw_get_sat(msg);
    gps_raw->fix = mavlink_msg_gps_raw_get_fix(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_GPS_RAW_LEN? msg->len : MAVLINK_MSG_ID_GPS_RAW_LEN;
        memset(gps_raw, 0, MAVLINK_MSG_ID_GPS_RAW_LEN);
    memcpy(gps_raw, _MAV_PAYLOAD(msg), len);
#endif
}
