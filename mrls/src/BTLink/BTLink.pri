INCLUDEPATH += \
    $$PWD/src/ \
    $$PWD/src/BTLink/

HEADERS += \
    $$PWD/src/BTLink/BTLink.h \
    $$PWD/src/BTLink/mavlink.h \
    $$PWD/src/BTLink/mavlink_msg_amplifier.h \
    $$PWD/src/BTLink/mavlink_msg_auth_key.h \
    $$PWD/src/BTLink/mavlink_msg_cmd.h \
    $$PWD/src/BTLink/mavlink_msg_cmd_ack.h \
    $$PWD/src/BTLink/mavlink_msg_drive.h \
    $$PWD/src/BTLink/mavlink_msg_error.h \
    $$PWD/src/BTLink/mavlink_msg_gps_raw.h \
    $$PWD/src/BTLink/mavlink_msg_heartbeat.h \
    $$PWD/src/BTLink/mavlink_msg_param_get.h \
    $$PWD/src/BTLink/mavlink_msg_param_set.h \
    $$PWD/src/BTLink/mavlink_msg_param_set_ack.h \
    $$PWD/src/BTLink/mavlink_msg_param_value.h \
    $$PWD/src/BTLink/mavlink_msg_ping.h \
    $$PWD/src/BTLink/mavlink_msg_power.h \
    $$PWD/src/BTLink/mavlink_msg_raw_data.h \
    $$PWD/src/BTLink/mavlink_msg_target_data.h \
    $$PWD/src/BTLink/mavlink_msg_target_id_data.h \
    $$PWD/src/BTLink/mavlink_msg_temp_raw.h \
    $$PWD/src/BTLink/testsuite.h \
    $$PWD/src/BTLink/version.h \
    $$PWD/src/checksum.h \
    $$PWD/src/mavlink_conversions.h \
    $$PWD/src/mavlink_get_info.h \
    $$PWD/src/mavlink_helpers.h \
    $$PWD/src/mavlink_sha256.h \
    $$PWD/src/mavlink_types.h \
    $$PWD/src/protocol.h

DISTFILES += \
    $$PWD/message_definitions/BTLink.xml
