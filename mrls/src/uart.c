#include "uart.h"

XUartLite UartLiteInst;
XUartLite UartLiteInst_BLE;
INTC IntcInstance;
#define SIZE_BUF_FROM_JEKA 256
volatile char buff_UART[SIZE_BUF_FROM_JEKA] = {0,};
volatile u16 cntByte = 0;



u64 lon_dec;
u64 lat_dec;



u8 sat_dec;
//u8 latitude_cnt_data = 0;
//u8 longitude_cnt_data = 0;
u8 satelite_cnt_data = 0;

float lat_done = 0;
float lon_done = 0;
float sat_done = 0;

float lat_sum = 0;
float lon_sum = 0;
float sat_sum = 0;
   u8 lat_cnt = 0;
   u8 lon_cnt = 0;
   u8 sat_cnt = 0;

   float lat = 0;
   float lon = 0;
   u8 sat = 0;

void UartLiteSendHandler_BLE(void *CallBackRef, unsigned int EventData){}
void UartLiteSendHandler(void *CallBackRef, unsigned int EventData){}
void parse_data_from_jeka(){

    char  end[128];
    char*  p_end = end;

	static char lat_degrees_str[2] = {0,};
	static char lat_minutes_str[7] = {0,};
	static char lon_degrees_str[3] = {0,};
	static char lon_minutes_str[7] = {0,};
	static char sat_str[7] = {0,};
	cntByte = 0;

	/*------LATITUDE---------------------------------------*/

		while (FIND_STR('L','a','t'))				// Ищем Latitude
		{
			if(cntByte != SIZE_BUF_FROM_JEKA)		cntByte++;	// Опасно!! можем обратится к несуществующему элементу массива
			else									return;		// Если не нашли вываливаемся и не парсим мусор
		}
			// Из ГГ°MM.ммм' / DD°MM.mmm'	в	ГГ.гггггг° / DD.dddddd°
			strncpy((char *)lat_degrees_str, 		BUFF_UART(LAT_DEGREES_POS),			LAT_SIZE_DEGREES_CELL);
			strncpy((char *)lat_minutes_str, 		BUFF_UART(LAT_MINUTE_POS),			LAT_SIZE_MINUTE_CELL);

			float lat_minutes = (float)strtod(lat_minutes_str, &p_end)/60;
			float lat_degrees = (float)strtod(lat_degrees_str, &p_end);
			lat = lat_degrees + lat_minutes;

//			if(lat != 0){
//				lat_sum += lat;
//				lat_cnt++;
//			}
//			else{
//				lat_cnt = 0;
//			}

			//xil_printf_BLE("lat:%d.%d         ", (int)lat,	SysMonFractionToInt(lat));
	/*----LON------------------------------------------------------------------------------*/
		while ( FIND_STR('L','o','n'))				// Ищем Longitude
		{
			if(cntByte != SIZE_BUF_FROM_JEKA)		cntByte++;	// Опасно!! можем обратится к несуществующему элементу массива
			else									return;		// Если не нашли вываливаемся и не парсим мусор
		}
		// Из ГГ°MM.ммм' / DD°MM.mmm'	в	ГГ.гггггг° / DD.dddddd°
		strncpy((char *)lon_degrees_str, 		BUFF_UART(LON_DEGREES_POS),			LON_SIZE_DEGREES_CELL);
		strncpy((char *)lon_minutes_str, 		BUFF_UART(LON_MINUTE_POS),			LON_SIZE_MINUTE_CELL);

		float lon_minutes = (float)strtod(lon_minutes_str, &p_end)/60;
		float lon_degrees = (float)strtod(lon_degrees_str, &p_end);
		lon = lon_degrees + lon_minutes;

//		if(lon != 0){
//			lon_sum += lon;
//			lon_cnt++;
//		}
//		else{
//			lon_cnt = 0;
//		}
		//il_printf_BLE("lon:%d.%d\r\n", (int)lon,	SysMonFractionToInt(lon));
	/*------SATELLITE-----------------------------------------------------*/
		while (!( (buff_UART[cntByte]     == 'S')  &&		// Ищем количество спутников
				  (buff_UART[cntByte + 1] == 'a')  &&
				  (buff_UART[cntByte + 2] == 't')))
		{
			if(cntByte != SIZE_BUF_FROM_JEKA)		cntByte++;
			else									return;
		}

		memcpy((void *)sat_str, (void *)&buff_UART[cntByte+4],  2);
		sat = strtol ((char *)sat_str, &p_end, 10);

//		if(sat != 0){
//			sat_sum += sat;
//			sat_cnt++;
//		}
//		else{
//			sat_cnt = 0;
//		}
	/*------ClearBuff-----------------------------------------------------*/
		memset((void *)buff_UART, 0, SIZE_BUF_FROM_JEKA+1);

		GPS_data_Recieved_Done = true;

//		if(sat_cnt >= 5 && lon_cnt >= 5 && lat_cnt >= 5){
//			lat_done = lat_sum/(float)lat_cnt;
//			lon_done = lon_sum/(float)lon_cnt;
//			sat_done = sat_sum/(float)sat_cnt;
//
//			xil_printf_BLE("INFO_GPS: ");
//			xil_printf_BLE("lat:%d.%d    ", (int)lat_done,	SysMonFractionToInt(lat_done));
//			xil_printf_BLE("lon:%d.%d\r\n", (int)lon_done,	SysMonFractionToInt(lon_done));
//
//			XIntc_Disable(intcp, XPAR_INTC_0_UARTLITE_0_VEC_ID);
//			XIntc_Disable(intcp, XPAR_INTC_0_UARTLITE_1_VEC_ID);
//
//			GPS_data_Recieved_Done = true;
//
//		}
}
void UartLiteRecvHandler(void *CallBackRef, unsigned int EventData){

//	static bool  led = false;
//	led = !led;
//	AD9508_setDebugLed(led);
//	buff_UART[cntByte] = XUartLite_RecvByte(XPAR_AXI_UARTLITE_0_BASEADDR);	// Принимаем по байту
//
//		if((buff_UART[cntByte] == '\n'))	parse_data_from_jeka();		// Приняли строку от Жеки (Нижняя часть МРЛС там stm32)
//		else								cntByte++;					// Продолжаем принимать... Еще не конец строки
}
void UartLiteRecvHandler_BLE(void *CallBackRef, unsigned int EventData){

	buff_UART[cntByte] = XUartLite_RecvByte(XPAR_AXI_UARTLITE_BLE_BASEADDR);	// Принимаем по байту

		if((buff_UART[cntByte] == '\n'))	xil_printf_BLE("str_BLE:%s\r\n", buff_UART);	//Возвращаем то что приняли
		else								cntByte++;										// Продолжаем принимать... Еще не конец строки
}



