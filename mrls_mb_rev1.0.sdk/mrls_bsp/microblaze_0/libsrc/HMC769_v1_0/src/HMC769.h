#ifndef SRC_HMC769_H_
#define SRC_HMC769_H_

#include "HMC769.h"
//#include "platform.h"
#include "xil_printf.h"

#include <stdio.h>
#include "xil_cache.h"
#include "xil_types.h"

#define HMC769_ID_REG 0x0
#define HMC769_RST_Register_REG 0x1
#define HMC769_REFDIV_REG 0x2
#define HMC769_Frequency_Register_REG 0x3
#define HMC769_Frequency_Register_Fractional_Part_REG 0x4
#define HMC769_Seed_REG 0x5
#define HMC769_SD_CFG_REG 0x6
#define HMC769_Lock_Detect_REG 0x7
#define HMC769_Analog_EN_REG 0x8
#define HMC769_Charge_Pump_REG 0x9
#define HMC769_Modulation_Step_REG 0xA
#define HMC769_PD_REG 0xB
#define HMC769_ALTINT_REG 0xC
#define HMC769_ALTFRAC_REG 0xD
#define HMC769_SPI_TRIG_REG 0xE
#define HMC769_GPO_REG 0xF
#define HMC769_Reserve_REG 0x10
#define HMC769_Reserve2_REG 0x11
#define HMC769_GPO2_REG 0x12
#define HMC769_BIST_REG 0x13
#define HMC769_Lock_Detect_Timer_Status_REG 0x14

#pragma pack(push, 1)

typedef struct { //HMC769_ID_REG 0x0
    u32 chip_ID : 24; //----------------------[23:0]
} t_HMC769_ID_REG;
/*---------------------------------------------------------------------*/
typedef struct { //HMC769_RST_Register_REG 0x1
    u8 EnPinSel : 1; //-----------------------[0]
    u8 EnFromSPI : 1; //----------------------[1]
    u8 EnKeepOns : 8; //----------------------[9:2]
    u8 EnSyncChpDis : 1; //-------------------[10]
} t_HMC769_RST_Register_REG;
/*---------------------------------------------------------------------*/
typedef struct { //HMC769_REFDIV_REG 0x2
    u16 rdiv : 14; //-------------------------[13:0]
} t_HMC769_REFDIV_REG;
/*---------------------------------------------------------------------*/
typedef struct { //HMC769_Frequency_Register_REG 0x3
    u16 intg : 16; //-------------------------[15:0]
} t_HMC769_Frequency_Register_REG;
/*---------------------------------------------------------------------*/
typedef struct { //HMC769_Frequency_Register_Fractional_Part_REG 0x4
    u32 frac : 24; //-------------------------[15:0]
} t_HMC769_Frequency_Register_Fractional_Part_REG;
/*---------------------------------------------------------------------*/
typedef struct { //HMC769_Seed_REG 0x5
    u32 SEED : 23; //-------------------------[15:0]
} t_HMC769_Seed_REG;
/*---------------------------------------------------------------------*/
typedef struct { //HMC769_SD_CFG_REG 0x6
    u8 Modulator_Type : 1; //----------------[0]
    u8 Reserved_1 : 4; //--------------------[4:1]
    u8 SD_Mode : 3; //-----------------------[7:5]
    u8 autoseed : 1; //----------------------[8]
    u8 External_Trigger_Enable : 1; //-------[9]
    u8 Reserved_7 : 3; //--------------------[12:10]
    u8 Force_DSM_Clock_n : 1; //-------------[13]
    u8 BIST_Enable : 1; //-------------------[14]
    u8 Number_of_Bist_Cycles : 2; //---------[16:15]
    u8 DSM_Clock_Source : 2; //--------------[18:17]
    u8 Invert_DSM_Clock : 1; //--------------[19]
    u8 Reserved_0 : 1; //--------------------[20]
    u8 Force_RDIV_bypass : 1; //-------------[21]
    u8 Disable_Reset : 1; //-----------------[22]
    u8 Single_Step_Ramp_Mode : 1; //---------[23]
} t_HMC769_SD_CFG_REG;
/*---------------------------------------------------------------------*/
typedef struct { //HMC769_Lock_Detect_REG 0x7
    u8 LKDCounts : 3; //---------------------[2:0]
    u8 Reserved_x : 8; //--------------------[10:3]
    u8 LockDetect_Counters_Enable : 3; //----[11]
    u8 Reserved_z : 2; //--------------------[13:12]
    u8 Lock_Detect_Timer_Enable : 1; //------[14]
    u8 Cycle_Slip_Prevention_Enable : 1; //--[15]
    u8 Reserved_0 : 4; //--------------------[19:16]
    u8 Train_Lock_Detect_Timer : 1; //-------[20]
    u8 Reserved_y : 1; //--------------------[21]
} t_HMC769_Lock_Detect_REG;
/*---------------------------------------------------------------------*/
typedef struct { // HMC769_Analog_EN_REG 0x8
    u8 EnBias : 1; //------------------------[0]
    u8 EnCP : 1; //--------------------------[1]
    u8 EnPFD : 1; //-------------------------[2]
    u8 EnXtal : 1; //------------------------[3]
    u8 EnVCO : 1; //-------------------------[4]
    u8 EnGPO : 1; //-------------------------[5]
    u8 EnMcnt : 1; //------------------------[6]
    u8 EnPS : 1; //--------------------------[7]
    u8 EnVCOBias : 1; //---------------------[8]
    u8 EnOpAmp : 1; //-----------------------[9]
    u8 VCOOutBiasA : 3; //-------------------[12:10]
    u8 VCOOutBiasB : 3; //-------------------[15:13]
    u8 VCOBWSel : 1; //----------------------[16]
    u8 RFDiv2Sel : 1; //---------------------[17]
    u8 XtalLowGain : 1; //-------------------[18]
    u8 XtalDisSat : 1; //---------------------[19]
} t_HMC769_Analog_EN_REG;
/*---------------------------------------------------------------------*/
typedef struct { //HMC769_Charge_Pump_REG 0x9
    u8 CPIdn : 7; //-------------------------[0:6]
    u8 CPIup : 7; //-------------------------[13:7]
    u8 CPOffset : 7; //----------------------[20:14]
    u8 CPSrcEn : 1; //-----------------------[21]
    u8 CPSnkEn : 1; //-----------------------[22]
    u8 CPHiK : 1; //-------------------------[23]
} t_HMC769_Charge_Pump_REG;
/*---------------------------------------------------------------------*/
typedef struct { //HMC769_Modulation_Step_REG 0xA
    u32 MODSTEP : 23;
} t_HMC769_Modulation_Step_REG;
/*---------------------------------------------------------------------*/
typedef struct { // HMC769_PD_REG 0xB
    u8 PFDDly : 3; //------------------------[2:0]
    u8 PFDShort : 1; //----------------------[3]
    u8 PFDInv : 1; //------------------------[4]
    u8 PFDUpEn : 1; //-----------------------[5]
    u8 PFDDnEN : 1; //-----------------------[6]
    u8 PFDForceUp : 1; //---------------------[7]
    u8 PFDForceDn : 1; //---------------------[8]
    u8 PFDForceMid : 1; //--------------------[9]
    u8 PSBiasSel : 3; //----------------------[12:10]
    u8 OpAmpBiasSel : 2; //-------------------[14:13]
    u8 McntClkGateSel : 2; //-----------------[16:15]
    u8 VDIVExt : 1; //------------------------[17]
    u8 LKDProcTesttoCP : 1; //----------------[18]
} t_HMC769_PD_REG;
/*---------------------------------------------------------------------*/
typedef struct { //HMC769_ALTINT_REG 0xC
    u16 ALTINT : 16; //-----------------------[15:0]
} t_HMC769_ALTINT_REG;
/*---------------------------------------------------------------------*/
typedef struct { //HMC769_ALTFRAC_REG 0xD
    u32 ALTFRAC : 23; //----------------------[23:0]
} t_HMC769_ALTFRAC_REG;
/*---------------------------------------------------------------------*/
typedef struct { //HMC769_SPI_TRIG_REG 0xE
    u8 SPITRIG : 1; //------------------------[0]
} t_HMC769_SPI_TRIG_REG;
/*---------------------------------------------------------------------*/
typedef struct { //HMC769_GPO_REG 0xF
    u8 GPOSel : 3; //------------------------[4:0]
    u8 GPOTest : 1; //-----------------------[5]
    u8 GPOAlways : 1; //---------------------[6]
    u8 GPOOn : 1; //-------------------------[7]
    u8 GPOPullUpDis : 1; //------------------[8]
    u8 GPOPullDnDis : 1; //------------------[9]
} t_HMC769_GPO_REG;
/*---------------------------------------------------------------------*/
typedef struct { //HMC769_GPO2_REG 0x12
    u8 GPO : 1; //---------------------------[0]
    u8 Lock_Detect : 1; //-------------------[1]
    u8 Ramp_Busy : 1; //---------------------[2]
} t_HMC769_GPO2_REG;
/*---------------------------------------------------------------------*/
typedef struct { //HMC769_BIST_REG 0x13
    u32 BIST_Signature : 16; //---------------[15:0]
    u32 BIST_Busy : 1; //---------------------[16]
} t_HMC769_BIST_REG;
/*---------------------------------------------------------------------*/
typedef struct { //HMC769_Lock_Detect_Timer_Status_REG 0x14
    u32 LkdSpeed : 3; //----------------------[2:0]
    u32 LkdTraining : 1; //-------------------[3]
} t_HMC769_Lock_Detect_Timer_Status_REG;

void HMC769_init(void);
void HMC769_setRegAdr(u8 adr);
void HMC769_startReceive(void);
void HMC769_startSend(void);
void HMC769_waitReady(void);
u32 HMC769_getData(void);
u32 HMC769_read(u8 adrReg);
void HMC769_write(u8 adrRegHMC, u32 dataSend);
void HMC769_readBitMap(u8 adrRegHMC, void* rxReg);
void HMC769_configIC(void);
void HMC769_viewAllDataPLL(void);

#endif /* SRC_HMC769_H_ */
