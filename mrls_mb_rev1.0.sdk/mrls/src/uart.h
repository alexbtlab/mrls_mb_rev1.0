#ifndef __UART_H_
#define __UART_H_

#include "xintc.h"
#include "xuartlite.h"
#include "xuartlite_l.h"
#include "AD9508.h"
#include "xil_printf.h"
#include "xparameters.h"
#include "xadc.h"

#define BUFF_UART(pos)	(char *)&buff_UART[cntByte + pos]
#define FIND_STR(a, b, c)		!( (buff_UART[cntByte] == a) && (buff_UART[cntByte + 1] == b) && (buff_UART[cntByte + 2] == c))

#define LAT_SIZE_DEGREES_CELL	2
#define LAT_SIZE_MINUTE_CELL	7
#define LAT_DEGREES_POS			4
#define LAT_MINUTE_POS			6
#define LAT_SECOND_WHOLE_POS	9
#define LAT_SECOND_FRACT_POS	11

#define LON_SIZE_DEGREES_CELL   3
#define LON_SIZE_MINUTE_CELL    7
#define LON_DEGREES_POS			4
#define LON_MINUTE_POS			7
#define LON_SECOND_WHOLE_POS	10
#define LON_SECOND_FRACT_POS	12

#define INTC					XIntc
#define INTC_HANDLER			XIntc_InterruptHandler
#define INTC_DEVICE_ID			XPAR_INTC_0_DEVICE_ID
#define UARTLITE_IRPT_INTR		XPAR_INTC_0_UARTLITE_0_VEC_ID
#define UARTLITE_DEVICE_ID	  	XPAR_UARTLITE_0_DEVICE_ID

extern u32 BuffSizeMavLink;
extern XIntc *intcp;
extern bool GPS_data_tramsmit_to_Diman;
extern bool GPS_data_Recieved_Done;

	void UartLiteSendHandler(void *CallBackRef, unsigned int EventData);
	void UartLiteRecvHandler(void *CallBackRef, unsigned int EventData);
	void UartLiteSendHandler_BLE(void *CallBackRef, unsigned int EventData);
	void UartLiteRecvHandler_BLE(void *CallBackRef, unsigned int EventData);
	void mb_reset(void);

#endif
