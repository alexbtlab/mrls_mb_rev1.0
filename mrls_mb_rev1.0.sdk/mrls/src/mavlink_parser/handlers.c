#include "mavlink_parser.h"
#include "net_mrls.h"
#include <string.h>
#include "parameters.h"

extern struct tcp_pcb *server_pcb;
extern mavlink_message_t 	msgTx;
extern Param params[MAX_NUM_PARAM];

const char err1[] = "Packet error 1";
const char err2[] = "Packet error 2";
const char err3[] = "Packet error 3";
const char err4[] = "Packet error 4";

bool needed_send_param = false;


void sendError(int errorNum);

void handler(mavlink_message_t* message){

    switch (message->msgid) {
    case MAVLINK_MSG_ID_HEARTBEAT:
        handler_heartbeat(message);
        break;
//    case MAVLINK_MSG_ID_PARAM_GET:
//        handler_param_get(message);
//        break;
    case MAVLINK_MSG_ID_PARAM_SET:
        handler_PARAM_SET(message);
        break;
    case MAVLINK_MSG_ID_COMMAND_INT:
        handler_COMMAND_INT(message);
        break;
    case MAVLINK_MSG_ID_PARAM_VALUE:
        handler_PARAM_GET(message);
        break;
    case MAVLINK_MSG_ID_PARAM_REQUEST_LIST:
        handler_PARAM_REQUEST_LIST(message);
        break;
    case MAVLINK_MSG_ID_PARAM_REQUEST_GROUP:
        //handler_param_set(message);
        break;
    case MAVLINK_MSG_ID_PARAM_REQUEST_READ:
//        handler_param_REQUEST_READ(message);
        break;
    default:
        //sendError(0);
        break;
    }
}

/**  
 * @brief Обработчик по приему HeartBeat
 *
 * @param message
 */
mavlink_heartbeat_t heartbeat;
void handler_heartbeat(mavlink_message_t* message){

    mavlink_msg_heartbeat_decode(message, &heartbeat);

    xil_printf_BLE("handler_heartbeat ---%x,%x\r\n", heartbeat.state, heartbeat.version);
}
/**
 * @brief Функция обработчика по приему пакета "Запрос ВСЕХ параметров"
 * Затем МРЛС отвечает ПК пакетами mavlink_param_value_t
 * И только после того как ПК будет иметь все параметры МРЛС, ПК может запрашивать установку каких либо параметров
 * пакетами PARAM_SET
 *
 * @param message
 * @return int
 */
int handler_PARAM_REQUEST_LIST(mavlink_message_t* message)
{ 
	mavlink_param_request_list_t param_request_list;
	// Декодир

	xil_printf_BLE("INFO: handler_PARAM_REQUEST_LIST\r\n");

	mavlink_msg_param_request_list_decode(message, &param_request_list);
    // Проверяем нам ли это сообщение
    if (param_request_list.target_system != CURRENT_SYSID)
        return -1;

    needed_send_param = true;

    return 1;
}
/**
 * @brief
 * Обработчик по приему пакета "Установить параметр"
 * ПК может передать эту команду МРЛС только в случае если ПК знает о всех параметрах МРЛС
 * Для этого ПК сначала запрвщивает ВСЕ параметры у МРЛС командой REQUEST_PARAM_LIST("Запрос листа параметров")
 * Проверяем нам ли это сообщение и уснанавливаем необходимый параметр
 * @param message  Принятое сообщение mavlink_message_t
 * @return int
 */
int handler_PARAM_SET(mavlink_message_t* message){

	mavlink_param_set_t param_set;
	mavlink_param_value_t param_value;

	xil_printf_BLE("INFO: handler_PARAM_SET\r\n");

	mavlink_msg_param_set_decode(message, &param_set);
	// Проверяем нам ли это сообщение
	if (param_set.target_system != CURRENT_SYSID)
		return -1;

	param_value.param_value = param_set.param_value;
	param_value.param_index = param_set.param_index;

	setParam(&param_value);
		return 1;
}
int handler_PARAM_GET(mavlink_message_t* message){

	mavlink_param_value_t param_value;
	char bufTx[128];
	xil_printf_BLE("INFO: handler_PARAM_GET\r\n");

	mavlink_msg_param_value_decode(message, &param_value);
	// Проверяем нам ли это сообщение
//	if (param_value.target_system != CURRENT_SYSID)
//		return -1;

//	param_value.param_value = param_set.param_value;
//	param_value.param_index = param_set.param_index;


	u64 data;
	getParam(&param_value, (uint64_t *)&data);
	xil_printf_BLE("INFO:%d\r\n", data);
	mavlink_message_t 	msgTx;

		mavlink_msg_param_value_encode(SYS_ID, COMPONENT_ID, &msgTx, &(params[(param_value.param_index) - 1].param));
		u16 len_val = mavlink_msg_to_send_buffer((uint8_t *)bufTx, &msgTx);
		my_udp_send(bufTx, len_val, &ip_addr_broadcast);

		return 1;
}
//int handler_param_REQUEST_READ(mavlink_message_t* message){
//
//	xil_printf_BLE("INFO: handler_param_REQUEST_READ\r\n");
//}
/**
 * @brief Обработчик по приему команды "КОМАНДА"
 * Здесь может быть переданна команда о перезапуске МРЛС,
 * сбросе параметров, или отладочные команды
 *
 * @param message Принятое сообщение mavlink_message_t
 * @return int
 */
int handler_COMMAND_INT(mavlink_message_t* message){

	mavlink_command_int_t command_int;
	u16 len_cmd_ack;
	xil_printf_BLE("INFO: handler_COMMAND_INT\r\n");

	mavlink_msg_command_int_decode(message, &command_int);
	if(command_int.target_system != CURRENT_SYSID)
			return -1;

	    switch (command_int.command)    // Смотрим принятую команду
	    {
			case CUSTOM:	 switch (command_int.param1)    // Смотрим принятую команду
		    				{
								case 0: 	xil_printf_BLE("CMD: Enable AMP\n\r");		// Приняли команду включить усилитель
									break;
								case 1:		xil_printf_BLE("CMD: Set ATTEN\n\r");
											xil_printf_BLE("CMD: ATTEN:%d\n\r", command_int.param2);
											if(command_int.param2 >=0 && command_int.param2 <= 63)
												HMC769_setAtten(command_int.param2);
											else	
												xil_printf_BLE("ERR_CMD: val atten is not valid ->%d\n\r", command_int.param2);
											
									// Приняли команду включить усилительь
									break;
								case 2:		xil_printf_BLE("CMD: Enable TRIG\n\r");		// Приняли команду включить усилительь
											xil_printf_BLE("CMD: VAL_TRIG:%d\n\r", command_int.param2);
									break;
								case 3:		xil_printf_BLE("CMD: AMP\n\r");		// Приняли команду включить усилительь
											xil_printf_BLE("CMD: AMP_VAL:%d\n\r", command_int.param2);
									break;
								case 4:		xil_printf_BLE("CMD: frameSizeML\n\r");		// Приняли команду включить усилительь
									break;
								case 5: break;
								case 6: break;
								case 7: break;
								case 8: break;
								case 9: break;
								default : break;
		    				}






			break;
			case REBOOT:				send_cmdACK_mavLink();
										mb_reset();    			break;
			case RESET_PARAM:	    	/*------------------*/	break;
			case BT_CMD_TYPE_ENUM_END:  /*------------------*/	break;
			default:   					sendError(0);			break;
	    }
	return 1;
}
/**
 * @brief Функция передачи ошибок ПК
 *
 * @param errorNum - Номер ошибки
 */
void sendError(int errorNum) {

	mavlink_message_t msg;
    char error[128];
    char buf[MAVLINK_MAX_PACKET_LEN];
    switch (errorNum)
    {
    case 0:
        strncpy(error, err1, strlen(err1));
        break;
    default:
        break;
    }
    mavlink_msg_error_pack(0, 0, &msg, 0, 5526, error);
    int len = mavlink_msg_to_send_buffer((uint8_t *)buf, &msg);
    tcp_write(server_pcb, &buf, len, 1);
}
/**
 * @brief Функция перезагрузки ПО
 * Необходима при смене настроек сети
 *
 */
void mb_reset (){
    
	microblaze_disable_interrupts ();
    (* ((void (*) ()) (0x80000000))) (); // ����������
}
