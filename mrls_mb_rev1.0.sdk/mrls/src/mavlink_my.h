#ifndef SRC_MAVLINK_MY_H_
#define SRC_MAVLINK_MY_H_

#include "net_mrls.h"

#define DATA_MAX_LEN 32

#define SYS_ID 55
#define COMPONENT_ID 0
#define VERSION 211

	void send_rawData_mavLink(const int32_t* points, uint32_t nPoints);
	void send_rawData_mavLink(const int32_t* points, uint32_t nPoints);
	void send_rawData_tcp(void);
	void send_cmdACK_mavLink();

#endif /* SRC_MAVLINK_MY_H_ */
