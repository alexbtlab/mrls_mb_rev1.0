#include "HMC769_v1.0.h"

Param TX_atten_Param;
Param HMC_useCable_Param;

void HMC769_init()
{ //Функция инициализации IP ядра. Проверка идентификатора МС HMC769
    xil_printf_BLE("\r\n<---Start init AXI_HMC769--->\r\n");

    if (HMC769_read(HMC769_ID_REG) == 0x97370) // Проверка идентификатора МС
        xil_printf_BLE("INFO: init AXI_HMC769 completed \r\n");
    else
        xil_printf_BLE("ERROR: init AXI_HMC769 is fault\r\n");

    HMC769_write(HMC769_REFDIV_REG, 0x4); // Проверка работы интерфейса SPI. Запись значения 4 регистра делителя, затем чтение..

    if (HMC769_read(HMC769_REFDIV_REG) == 0x4) // Проверка записанного значения
        xil_printf_BLE("INFO: compare value is completed\r\n");
    else
        xil_printf_BLE("ERROR: compare value is fault\r\n");
}
void HMC769_setRegAdr(u8 adr)
{ //Функция установки адреса регистра МС HMC769
    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // Базовый адрес регистра IP ядра МС HMC769
    volatile unsigned int* slv_reg1 = hmcbaseaddr + 1; // Адрес 1-го регистра для передачи данных в IP ядро из MB
    volatile unsigned int* slv_reg2 = hmcbaseaddr + 2; // Адрес 2-го регистра для передачи данных в IP ядро из MB
    // т.к. модуль SPI имеет 2 входа установки адреса (на ПРМ и ПРД)
    *slv_reg1 = adr; // Запись адреса регистра МС HMC769, в регистр IP ядра (slv_reg1 - адреса регистра МС для передачи)
    *slv_reg2 = adr; // Запись адреса регистра МС HMC769, в регистр IP ядра (slv_reg2 - адреса регистра МС для приема)
}
void HMC769_startSend()
{ // Функция формирования сигнала старта записи данных из МС HMC769
    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // Базовый адрес регистра IP ядра МС HMC769
    (*hmcbaseaddr) = 0x1;
    for (u32 i = 0; i < 10000; i++) {
    }
    (*hmcbaseaddr) = 0x0;
}
void HMC769_startReceive()
{ // Функция формирования сигнала старта чтения данных из МС HMC769

    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // Базовый адрес регистра IP ядра МС HMC769
    (*hmcbaseaddr) = 0x2;
    for (u32 i = 0; i < 10000; i++) {
    }
    (*hmcbaseaddr) = 0x0;
}
void HMC769_startTrig(bool on)
{ // Функция формирования сигнала старта чтения данных из МС HMC769

    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // Базовый адрес регистра IP ядра МС HMC769
    (*hmcbaseaddr) = on ? 0x4 : 0;
//    for (u32 i = 0; i < 10000; i++) {
//    }
//    (*hmcbaseaddr) = 0x0;
}
void HMC769_setAtten(u8 val)
{ // Функция формирования сигнала старта чтения данных из МС HMC769

	volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // Базовый адрес регистра IP ядра МС HMC769
	volatile unsigned int* slv_reg4 = hmcbaseaddr + 4; // Адрес 1-го регистра для передачи данных в IP ядро из MB

	TX_atten_Param = (Param){{val, PARAM_COUNT_TX_GROUP, 1, "TX_atten",  "TXGroup", PARAM_TYPE_UINT8}, setterATTEN,	getterATTEN};
	addParam(&TX_atten_Param);

	*slv_reg4 = val; // Запись адреса регистра МС HMC769, в регистр IP ядра (slv_reg2 - адреса регистра МС для приема)
}
void HMC769_setSweepVal(u8 val)
{ // Функция формирования сигнала старта чтения данных из МС HMC769

	volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // Базовый адрес регистра IP ядра МС HMC769
	volatile unsigned int* slv_reg5 = hmcbaseaddr + 5; // Адрес 1-го регистра для передачи данных в IP ядро из MB

	*slv_reg5 = val; // Запись адреса регистра МС HMC769, в регистр IP ядра (slv_reg2 - адреса регистра МС для приема)
}
void HMC769_EnClk8(bool state)
{ // Функция формирования сигнала старта чтения данных из МС HMC769

	volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // Базовый адрес регистра IP ядра МС HMC769
	volatile unsigned int* slv_reg1 = hmcbaseaddr + 1; // Адрес 1-го регистра для передачи данных в IP ядро из MB

	*slv_reg1 = state; // Запись адреса регистра МС HMC769, в регистр IP ядра (slv_reg2 - адреса регистра МС для приема)
}
void HMC769_waitReady()
{ //Функция ожидания сигнала ready (окончания обмена)

    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // Базовый адрес регистра IP ядра МС HMC769
    volatile unsigned int* ip2mb_reg0 = hmcbaseaddr + 8; // Адрес 8-го регистра для приема данных из IP ядра в МС HMC769
    while ((*ip2mb_reg0) != 1) {
    }
}
u32 HMC769_getData()
{ // Функция чтения данных из МС HMC769

    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // Базовый адрес регистра IP ядра МС HMC769
    volatile unsigned int* ip2mb_reg1 = hmcbaseaddr + 9; // Адрес 9-го регистра для приема данных из IP ядра в МС HMC769
    return *ip2mb_reg1;
}
void HMC769_setSendSata(u32 dataSend)
{ // Функция формирования данных для передачу в МС HMC769

    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // Базовый адрес регистра IP ядра МС HMC769
    volatile unsigned int* slv_reg3 = hmcbaseaddr + 3; // Адрес 3-го регистра для передачи данных в IP ядро из MB
    *slv_reg3 = dataSend;
}
void HMC769_write(u8 adrRegHMC, u32 dataSend)
{ // Функция записи данных u32 dataSend по адресу u8 adrRegH в МС HMC769

    HMC769_setRegAdr(adrRegHMC); // установка адреса регистра МС HMC769
    HMC769_setSendSata(dataSend); // Формирование данных для передачу в МС HMC769
    HMC769_startSend(); // Формирование сигнала старта записи данных из МС HMC769
    HMC769_waitReady(); // Ожидание сигнала ready (окончания обмена)
}
u32 HMC769_read(u8 adrRegHMC)
{ // Функция чтения данных u32 по адресу u8 adrRegH из МС HMC769

    HMC769_setRegAdr(adrRegHMC); // установка адреса регистра МС HMC769
    HMC769_startReceive(); // Формирование сигнала старта чтения данных из МС HMC769
    HMC769_waitReady(); // Ожидание сигнала ready (окончания обмена)
    return HMC769_getData(); // Чтение данных из МС HMC769
}
void HMC769_readBitMap(u8 adrRegHMC, void* rxReg)
{ // Функция чтения битовой карты регистра adrRegHMC МС HMC769
    u32 dataReg;
    rxReg = &dataReg; // Указываем регистр dataReg принятых данных от МС, на необходимую структуру (void*)(созданную во время входа в функцию)
                      // т.к. на каждый регистр принятых данных от МС есть своя структура

    dataReg = HMC769_read(adrRegHMC); // Чтение данных по адресу u8 adrRegH из МС HMC769
    xil_printf_BLE("\r\nHMC769 ADR_REG:%x DATA_REG:%x\r\n", adrRegHMC, *(u32*)rxReg);

    switch (adrRegHMC) {
    case HMC769_ID_REG:
        xil_printf_BLE("chip_ID-%x\r\n", ((t_HMC769_ID_REG*)rxReg)->chip_ID);
        break;
    case HMC769_RST_Register_REG:
        xil_printf_BLE("EnFromSPI-%x\r\n", ((t_HMC769_RST_Register_REG*)rxReg)->EnFromSPI);
        xil_printf_BLE("EnKeepOns-%x\r\n", ((t_HMC769_RST_Register_REG*)rxReg)->EnKeepOns);
        xil_printf_BLE("EnPinSel-%x\r\n", ((t_HMC769_RST_Register_REG*)rxReg)->EnPinSel);
        xil_printf_BLE("EnSyncChpDis-%x\r\n", ((t_HMC769_RST_Register_REG*)rxReg)->EnSyncChpDis);
        break;
    case HMC769_REFDIV_REG:
        xil_printf_BLE("rdiv:%x\r\n", ((t_HMC769_REFDIV_REG*)rxReg)->rdiv);
        break;
    case HMC769_Frequency_Register_REG:
        xil_printf_BLE("intg-%x\r\n", ((t_HMC769_Frequency_Register_REG*)rxReg)->intg);
        break;
    case HMC769_Frequency_Register_Fractional_Part_REG:
        xil_printf_BLE("frac-%x\r\n", ((t_HMC769_Frequency_Register_Fractional_Part_REG*)rxReg)->frac);
        break;
    case HMC769_Seed_REG:
        xil_printf_BLE("SEED:%x\r\n", ((t_HMC769_Seed_REG*)rxReg)->SEED);
        break;
    case HMC769_SD_CFG_REG:
        xil_printf_BLE("autoseed:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->autoseed);
        xil_printf_BLE("BIST_Enable:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->BIST_Enable);
        xil_printf_BLE("Disable_Reset:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Disable_Reset);
        xil_printf_BLE("DSM_Clock_Source:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->DSM_Clock_Source);
        xil_printf_BLE("External_Trigger_Enable:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->External_Trigger_Enable);
        xil_printf_BLE("Force_DSM_Clock_n:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Force_DSM_Clock_n);
        xil_printf_BLE("Force_RDIV_bypass:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Force_RDIV_bypass);
        xil_printf_BLE("Invert_DSM_Clock:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Invert_DSM_Clock);
        xil_printf_BLE("Modulator_Type:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Modulator_Type);
        xil_printf_BLE("Number_of_Bist_Cycles:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Number_of_Bist_Cycles);
        xil_printf_BLE("Reserved_0:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Reserved_0);
        xil_printf_BLE("Reserved_1:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Reserved_1);
        xil_printf_BLE("Reserved_7:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Reserved_7);
        xil_printf_BLE("SD_Mode:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->SD_Mode);
        xil_printf_BLE("Single_Step_Ramp_Mode:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Single_Step_Ramp_Mode);
        break;
    case HMC769_Lock_Detect_REG:
        xil_printf_BLE("Cycle_Slip_Prevention_Enable:%x\r\n", ((t_HMC769_Lock_Detect_REG*)rxReg)->Cycle_Slip_Prevention_Enable);
        xil_printf_BLE("LKDCounts:%x\r\n", ((t_HMC769_Lock_Detect_REG*)rxReg)->LKDCounts);
        xil_printf_BLE("Lock_Detect_Timer_Enable:%x\r\n", ((t_HMC769_Lock_Detect_REG*)rxReg)->Lock_Detect_Timer_Enable);
        xil_printf_BLE("LockDetect_Counters_Enable:%x\r\n", ((t_HMC769_Lock_Detect_REG*)rxReg)->LockDetect_Counters_Enable);
        xil_printf_BLE("Train_Lock_Detect_Timer:%x\r\n", ((t_HMC769_Lock_Detect_REG*)rxReg)->Train_Lock_Detect_Timer);
        break;
    case HMC769_Analog_EN_REG:
        xil_printf_BLE("EnBias:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnBias);
        xil_printf_BLE("EnCP:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnCP);
        xil_printf_BLE("EnMcnt:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnMcnt);
        xil_printf_BLE("EnOpAmp:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnOpAmp);
        xil_printf_BLE("EnPFD:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnPFD);
        xil_printf_BLE("EnPS:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnPS);
        xil_printf_BLE("EnVCO:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnVCO);
        xil_printf_BLE("EnVCOBias:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnVCOBias);
        xil_printf_BLE("EnXtal:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnXtal);
        xil_printf_BLE("RFDiv2Sel:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->RFDiv2Sel);
        xil_printf_BLE("VCOBWSel:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->VCOBWSel);
        xil_printf_BLE("VCOOutBiasA:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->VCOOutBiasA);
        xil_printf_BLE("VCOOutBiasB:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->VCOOutBiasB);
        xil_printf_BLE("XtalDisSat:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->XtalDisSat);
        xil_printf_BLE("XtalLowGain:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->XtalLowGain);
        break;
    case HMC769_Charge_Pump_REG:
        xil_printf_BLE("CPHiK:%x\r\n", ((t_HMC769_Charge_Pump_REG*)rxReg)->CPHiK);
        xil_printf_BLE("CPIdn:%x\r\n", ((t_HMC769_Charge_Pump_REG*)rxReg)->CPIdn);
        xil_printf_BLE("CPIup:%x\r\n", ((t_HMC769_Charge_Pump_REG*)rxReg)->CPIup);
        xil_printf_BLE("CPOffset:%x\r\n", ((t_HMC769_Charge_Pump_REG*)rxReg)->CPOffset);
        xil_printf_BLE("CPSnkEn:%x\r\n", ((t_HMC769_Charge_Pump_REG*)rxReg)->CPSnkEn);
        xil_printf_BLE("CPSrcEn:%x\r\n", ((t_HMC769_Charge_Pump_REG*)rxReg)->CPSrcEn);
        break;
    case HMC769_Modulation_Step_REG:
        xil_printf_BLE("MODSTEP:%x\r\n", ((t_HMC769_Modulation_Step_REG*)rxReg)->MODSTEP);
        break;
    case HMC769_PD_REG:
        xil_printf_BLE("LKDProcTesttoCP:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->LKDProcTesttoCP);
        xil_printf_BLE("McntClkGateSel:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->McntClkGateSel);
        xil_printf_BLE("PFDDly:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDDly);
        xil_printf_BLE("PFDDnEN:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDDnEN);
        xil_printf_BLE("PFDForceDn:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDForceDn);
        xil_printf_BLE("PFDForceMid:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDForceMid);
        xil_printf_BLE("PFDForceUp:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDForceUp);
        xil_printf_BLE("PFDInv:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDInv);
        xil_printf_BLE("PFDShort:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDShort);
        xil_printf_BLE("PFDUpEn:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDUpEn);
        xil_printf_BLE("PSBiasSel:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PSBiasSel);
        xil_printf_BLE("VDIVExt:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->VDIVExt);
        break;
    case HMC769_ALTINT_REG:
        xil_printf_BLE("ALTINT:%x\r\n", ((t_HMC769_ALTINT_REG*)rxReg)->ALTINT);
        break;
    case HMC769_ALTFRAC_REG:
        xil_printf_BLE("ALTFRAC:%x\r\n", ((t_HMC769_ALTFRAC_REG*)rxReg)->ALTFRAC);
        break;
    case HMC769_SPI_TRIG_REG:
        xil_printf_BLE("SPITRIG:%x\r\n", ((t_HMC769_SPI_TRIG_REG*)rxReg)->SPITRIG);
        break;
    case HMC769_GPO_REG:
        xil_printf_BLE("GPOAlways:%x\r\n", ((t_HMC769_GPO_REG*)rxReg)->GPOAlways);
        xil_printf_BLE("GPOOn:%x\r\n", ((t_HMC769_GPO_REG*)rxReg)->GPOOn);
        xil_printf_BLE("GPOPullDnDis:%x\r\n", ((t_HMC769_GPO_REG*)rxReg)->GPOPullDnDis);
        xil_printf_BLE("GPOPullUpDis:%x\r\n", ((t_HMC769_GPO_REG*)rxReg)->GPOPullUpDis);
        xil_printf_BLE("GPOPullUpDis:%x\r\n", ((t_HMC769_GPO_REG*)rxReg)->GPOPullUpDis);
        xil_printf_BLE("GPOTest:%x\r\n", ((t_HMC769_GPO_REG*)rxReg)->GPOTest);
        break;
    case HMC769_GPO2_REG:
        xil_printf_BLE("GPO:%x\r\n", ((t_HMC769_GPO2_REG*)rxReg)->GPO);
        xil_printf_BLE("Lock_Detect:%x\r\n", ((t_HMC769_GPO2_REG*)rxReg)->Lock_Detect);
        xil_printf_BLE("Ramp_Busy:%x\r\n", ((t_HMC769_GPO2_REG*)rxReg)->Ramp_Busy);
        break;
    case HMC769_BIST_REG:
        xil_printf_BLE("BIST_Busy:%x\r\n", ((t_HMC769_BIST_REG*)rxReg)->BIST_Busy);
        xil_printf_BLE("BIST_Signature:%x\r\n", ((t_HMC769_BIST_REG*)rxReg)->BIST_Signature);
        break;
    case HMC769_Lock_Detect_Timer_Status_REG:
        xil_printf_BLE("LkdSpeed:%x\r\n", ((t_HMC769_Lock_Detect_Timer_Status_REG*)rxReg)->LkdSpeed);
        xil_printf_BLE("LkdTraining:%x\r\n", ((t_HMC769_Lock_Detect_Timer_Status_REG*)rxReg)->LkdTraining);
        break;

    default:
        xil_printf_BLE("ERROR: no register with the given address u8 adrRegHMC\r\n");
        break;
    }
}
void HMC769_configIC(bool use_cable)
{
	if(use_cable)
		xil_printf_BLE("INFO: use_cable - ON  dF=240MHz \r\n");
	else
		xil_printf_BLE("INFO: use_cable - OFF dF=120MHz \r\n");

    HMC769_write(0x01, 0x000002);
    HMC769_write(0x02, 0x000001);
    HMC769_write(0x03, 0x00001D);
    HMC769_write(0x04, 1048576);
    HMC769_write(0x05, 0x000000);
    HMC769_write(0x07, 0x204865);
    HMC769_write(0x08, 0x036FFF);
    HMC769_write(0x09, 0x003264);

	if(use_cable){
		HMC769_write(0x0A, 140);
		HMC769_write(0x0B, 0x01E071);
		HMC769_write(0x0C, 0x00001D);
		HMC769_write(0x0D, 13648576);
	}
	else{
		HMC769_write(0x0A, 70);
		HMC769_write(0x0B, 0x01E071);
		HMC769_write(0x0C, 0x00001D);
		HMC769_write(0x0D, 7348576);
}
	HMC769_write(0x0E, 0x000000);
	HMC769_write(0x0F, 0x000001);
	HMC769_write(0x06, 0x001FBF);

    xil_printf_BLE("INFO: HMC769 init completed \r\n");

    HMC_useCable_Param   = (Param){{use_cable,  PARAM_COUNT_TX_GROUP, 3, "useCable", "TXGroup", PARAM_TYPE_BIT},		setterUseCable,	getterUseCable};
    addParam(&HMC_useCable_Param);

    //HMC796_readAllReg();
}
void HMC769_viewAllDataPLL()
{ // Функция чтения карты регистров МС HMC769
    static t_HMC769_ID_REG* idReg;
    static t_HMC769_RST_Register_REG* rstReg;
    static t_HMC769_REFDIV_REG* refdivReg;
    static t_HMC769_Frequency_Register_REG* freqReg;
    static t_HMC769_Frequency_Register_Fractional_Part_REG* freqFracReg;
    static t_HMC769_Seed_REG* seedReg;
    static t_HMC769_SD_CFG_REG* sdCfgReg;
    static t_HMC769_Lock_Detect_REG* lockDetReg;
    static t_HMC769_Analog_EN_REG* AnEnReg;
    static t_HMC769_Charge_Pump_REG* ChargePumpReg;
    static t_HMC769_Modulation_Step_REG* modStepReg;
    static t_HMC769_PD_REG* pdReg;
    static t_HMC769_ALTINT_REG* altintReg;
    static t_HMC769_ALTFRAC_REG* altFracReg;
    static t_HMC769_SPI_TRIG_REG* spiTrigReg;
    static t_HMC769_GPO_REG* gpoReg;
    static t_HMC769_GPO2_REG* gpo2Reg;
    static t_HMC769_BIST_REG* bistReg;
    static t_HMC769_Lock_Detect_Timer_Status_REG* lockDetTimStatReg;

    HMC769_readBitMap(HMC769_ID_REG, idReg);
    HMC769_readBitMap(HMC769_RST_Register_REG, rstReg);
    HMC769_readBitMap(HMC769_REFDIV_REG, refdivReg);
    HMC769_readBitMap(HMC769_Frequency_Register_REG, freqReg);
    HMC769_readBitMap(HMC769_Frequency_Register_Fractional_Part_REG, freqFracReg);
    HMC769_readBitMap(HMC769_Seed_REG, seedReg);
    HMC769_readBitMap(HMC769_SD_CFG_REG, sdCfgReg);
    HMC769_readBitMap(HMC769_Lock_Detect_REG, lockDetReg);
    HMC769_readBitMap(HMC769_Analog_EN_REG, AnEnReg);
    HMC769_readBitMap(HMC769_Charge_Pump_REG, ChargePumpReg);
    HMC769_readBitMap(HMC769_Modulation_Step_REG, modStepReg);
    HMC769_readBitMap(HMC769_PD_REG, pdReg);
    HMC769_readBitMap(HMC769_ALTINT_REG, altintReg);
    HMC769_readBitMap(HMC769_ALTFRAC_REG, altFracReg);
    HMC769_readBitMap(HMC769_SPI_TRIG_REG, spiTrigReg);
    HMC769_readBitMap(HMC769_GPO_REG, gpoReg);
    HMC769_readBitMap(HMC769_GPO2_REG, gpo2Reg);
    HMC769_readBitMap(HMC769_BIST_REG, bistReg);
    HMC769_readBitMap(HMC769_Lock_Detect_Timer_Status_REG, lockDetTimStatReg);
}
void HMC796_readAllReg(){

    xil_printf_BLE("REG:0 data:%8x\r\n", HMC769_read(0));
    xil_printf_BLE("REG:1 data:%8x\r\n", HMC769_read(1));
    xil_printf_BLE("REG:2 data:%8x\r\n", HMC769_read(2));
    xil_printf_BLE("REG:3 data:%8x\r\n", HMC769_read(3));
    xil_printf_BLE("REG:4 data:%8x\r\n", HMC769_read(4));
    xil_printf_BLE("REG:5 data:%8x\r\n", HMC769_read(5));
    xil_printf_BLE("REG:6 data:%8x\r\n", HMC769_read(6));
    xil_printf_BLE("REG:7 data:%8x\r\n", HMC769_read(7));
    xil_printf_BLE("REG:8 data:%8x\r\n", HMC769_read(8));
    xil_printf_BLE("REG:9 data:%8x\r\n", HMC769_read(9));
    xil_printf_BLE("REG:A data:%8x\r\n", HMC769_read(10));
    xil_printf_BLE("REG:B data:%8x\r\n", HMC769_read(11));
    xil_printf_BLE("REG:C data:%8x\r\n", HMC769_read(12));
    xil_printf_BLE("REG:D data:%8x\r\n", HMC769_read(13));
    xil_printf_BLE("REG:E data:%8x\r\n", HMC769_read(14));
    xil_printf_BLE("REG:F data:%8x\r\n", HMC769_read(15));
    xil_printf_BLE("REG:10 data:%8x\r\n", HMC769_read(16));
    xil_printf_BLE("REG:11 data:%8x\r\n", HMC769_read(17));
    xil_printf_BLE("REG:12 data:%8x\r\n", HMC769_read(18));
    xil_printf_BLE("REG:13 data:%8x\r\n", HMC769_read(19));
    xil_printf_BLE("REG:14 data:%8x\r\n", HMC769_read(20));
}

