#include "DMA_my.h"
#include "platform.h"

extern int32_t raw;
extern int32_t* praw;


u8 part_mem = 0;
extern uint32_t cnt_azimut;


void InitializeAxiDMA(bool enable_DMA){
    unsigned int tmpVal;

    Xil_Out32(XPAR_AXI_DMA_0_BASEADDR + S2MM_DMACR_OFFSET, 0x2);	//RESET DMA
    	tmpVal = Xil_In32(XPAR_AXI_DMA_0_BASEADDR + S2MM_DMACR_OFFSET);
    	if(enable_DMA)
    		tmpVal |= 0x1001;
    	else
    		tmpVal |= 0x1000;
    Xil_Out32(XPAR_AXI_DMA_0_BASEADDR + S2MM_DMACR_OFFSET, tmpVal);
    	tmpVal = Xil_In32(XPAR_AXI_DMA_0_BASEADDR + S2MM_DMACR_OFFSET);
}
void InterruptHandler_GPIO(){

	AD9508_setDebugLed(true);

	if(mem_empty){
		if(!part_mem){
			startDmaTransfer(0x80070000, SIZE_BYTE_SEND_ARRAY);
		}else {
			startDmaTransfer(0x80080000, SIZE_BYTE_SEND_ARRAY);
		}
	}
	cnt_azimut++;
}
void InterruptHandler_DMA(){

	AD9508_setDebugLed(false);

	u32 tmpVal = Xil_In32(XPAR_AXI_DMA_0_BASEADDR + S2MM_DMASR_OFFSET);
    tmpVal = tmpVal | 0x1000;
    Xil_Out32(XPAR_AXI_DMA_0_BASEADDR + S2MM_DMASR_OFFSET, tmpVal);

    if(mem_empty){
		if(part_mem == 0){
			part_mem = 1;
			praw = (int32_t *)0x80070000;			//MEMCPY(raw , (u32 *)0x80070000, FRAME_SIZE*4);
		}
		else{
			part_mem = 0;
			praw = (int32_t *)0x80080000;			//MEMCPY(raw ,  (u32 *)0x80080000, FRAME_SIZE*4);
		}
		mem_empty = false;
    }
}
void startDmaTransfer(unsigned int dstAddr, unsigned int len){

    Xil_Out32 ( XPAR_AXI_DMA_0_BASEADDR + S2MM_DA_OFFSET, dstAddr);
    Xil_Out32 ( XPAR_AXI_DMA_0_BASEADDR + S2MM_LENGTH_OFFSET, len);
}
int readDmaStatusRegister(){

	if((Xil_In32(XPAR_AXI_DMA_0_BASEADDR + S2MM_DMASR_OFFSET) & 0x10) == 0x10){
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}
