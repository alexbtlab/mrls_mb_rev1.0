#ifndef SRC_PARAMETERS_H_
#define SRC_PARAMETERS_H_

#include "xil_types.h"
#include "xil_io.h"
#include "mavlink.h"
#include "net_mrls.h"

#define MAX_NUM_PARAM 9

typedef void (*paramFuncSet)(void *);
typedef void (*paramFuncGet)(void *);

#define IP4_ADDR(ipaddr, a,b,c,d)  (ipaddr)->addr = PP_HTONL(LWIP_MAKEU32(a,b,c,d))

struct Param {
	mavlink_param_value_t param;
	paramFuncSet setter;
	paramFuncGet getter;
};

extern ip_addr_t ipaddr, netmask, gw;
extern BT_STATE state_mrls;
extern u16 listenport;
extern bool needed_send_param;
extern Param IP_Param;
extern Param NM_Param;
extern Param GW_Param;
extern Param port_Param;
extern Param DHCP_Param;
extern Param TX_atten_Param;
extern Param PAMP_state_Param;

void addParam(Param *param);
void sendAllParams();
void setParam(mavlink_param_value_t *newParam);
void getParam(mavlink_param_value_t *newParam, void* data);

void setterIP(void *value);
void setterNM(void *value);
void setterGW(void *value);
void setterPort(void *value);
void setterCoof(void *value);
void setterDHCP(void *value);
void setterATTEN(void *value);
void setterPAMP_State(void *value);
void setterPAMP_State(void *value);
void setterRXnumAMP(void *value);
void setterUseCable(void *value);

void getterIP(void *readData);
void getterNM(void* readData);
void getterGW(void* readData);
void getterPort(void* readData);
void getterCoof(void* readData);
void getterDHCP(void* readData);
void getterATTEN(void *value);
void getterPAMP_State(void *value);
void getterPAMP_State(void *value);
void getterRXnumAMP(void *value);
void getterUseCable(void *value);

void sendBy(int index);
float getter_Vdet(void);
//#ifdef __cplusplus
//extern "C" {
//#endif
//
//
//
//#ifdef __cplusplus
//}
//#endif

#endif /* SRC_PARAMETERS_H_ */
