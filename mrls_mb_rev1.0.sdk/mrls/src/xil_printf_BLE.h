/*
 * xil_printf_BLE.h
 *
 *  Created on: 16 мар. 2021 г.
 *      Author: 24
 */

#ifndef SRC_XIL_PRINTF_BLE_H_
#define SRC_XIL_PRINTF_BLE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <ctype.h>
#include <string.h>
#include <stdarg.h>
#include "xil_types.h"
#include "xparameters.h"
#include "bspconfig.h"

typedef char8* charptr;
typedef s32 (*func_ptr)(int c);

void outbyte_BLE(char c);
void xil_printf_BLE( const char8 *ctrl1, ...);

#ifdef __cplusplus
}
#endif



#endif /* SRC_XIL_PRINTF_BLE_H_ */
