#include "configReciever.h"

Param PAMP_state_Param;
Param RX_Num_Amp_Param;

void configReciever(param_t param, stateConfigReciever_t state, u8 data){

	volatile unsigned int* configRecieverbaseaddr = (unsigned int*)XPAR_CONFIGRECIEVER_0_S00_AXI_BASEADDR;

	volatile unsigned int* slv_reg1 = configRecieverbaseaddr + 1;
	volatile unsigned int* slv_reg2 = configRecieverbaseaddr + 2;
	volatile unsigned int* slv_reg3 = configRecieverbaseaddr + 3;
	volatile unsigned int* slv_reg4 = configRecieverbaseaddr + 4;

	switch (param){
		case ATTEN: 						            		*slv_reg1 = data;										break;
		/*-------------------------------------------------------------------------------------------------------------------*/
		case PAMP_state:
		if (state == ON){	*slv_reg3  |=   0x1;
							PAMP_state_Param = (Param){{true,  PARAM_COUNT_TX_GROUP, 2, "PAMP_state", "TXGroup", PARAM_TYPE_BIT}, setterPAMP_State, getterPAMP_State};
		}
		else{				*slv_reg3  &=  ~0x1;
							PAMP_state_Param = (Param){{false, PARAM_COUNT_TX_GROUP, 2, "PAMP_state", "TXGroup", PARAM_TYPE_BIT}, setterPAMP_State, getterPAMP_State};
		}
		addParam(&PAMP_state_Param);
		break;
		/*-------------------------------------------------------------------------------------------------------------------*/
		case ADC_MODE_PDn: 		                        		*slv_reg2 = data; 	 	break;
		/*-------------------------------------------------------------------------------------------------------------------*/
		case ETH_REF_CLK_EN_and_RST: 		if (state == ON) 	*slv_reg3  |=   0x2;
											else				*slv_reg3  &=  ~0x2;	break;
		/*-------------------------------------------------------------------------------------------------------------------*/
		case RECIEVER_NUM_AMP: 	*slv_reg4 =  data;
								RX_Num_Amp_Param = (Param){{data, 1, 4, "RX_Num_AMP",  "RXGroup", PARAM_TYPE_UINT32}, setterRXnumAMP,	getterRXnumAMP};
								addParam(&RX_Num_Amp_Param);							break;
		/*-------------------------------------------------------------------------------------------------------------------*/
		default : break;
	}



}
