#ifndef SRC_SD_H_
#define SRC_SD_H_

#include "PmodSD.h"
#include "netif/xadapter.h"
#include "ini.h"
#include "xil_printf_BLE.h"

typedef struct{

    int dhcp;
    const char* ip;
    const char* nm;
    const char* gw;
    int port;
    int cable_use;
} configuration;

extern  u16 listenport;

void ReadSD_HMCConfig(void);
void WriteSD_NetConfig(void);
void ReadSD_file(char * file_name);
int WriteSD_file(char* file_name, char* data);
int WriteSD_log(char* data);
int WriteSD_setNetData(ip4_addr_t ip, ip4_addr_t nm, ip4_addr_t gw, u16 port, u8 dhcp_use, bool useCable);
int WriteSD_setNM(char* IP);
int WriteSD_setGW(char* IP);
int WriteSD_setPort(char* IP);
bool ReadNetConfigFromSD(ip_addr_t *ipaddr, ip_addr_t *netmask, ip_addr_t *gw, u16* port);

#endif /* SRC_SD_H_ */
