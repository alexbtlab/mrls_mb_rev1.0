#ifndef SRC_SENDDATA_H_
#define SRC_SENDDATA_H_

#include "xadc.h"
#include "xil_printf_BLE.h"

#define INTERVAL_TRANSMIT_MEASUREMENT_DATA 2500
#define FRAME_SIZE_DDR2PC 2048

extern u64 sys_tick;
extern u16 count_intrr;
extern int state_dataBackround_int;
extern int64_t adr_DDR;
extern int32_t* ppraw_data_azimut[512];
extern XSysMon *SysMonInstPtr;
extern bool needed_send_param;
extern float lat_done;
extern float lon_done;
extern float lat;
extern float lon;
extern u8 sat;
extern u8 sat_done;
extern int32_t raw;
extern int32_t* praw;
extern XSysMon *SysMonInstPtr;

void ReadAndSend_TemperatureAndPowerToPC();
void SendGpsToPC();
void SendHeartBeat();
void SendMeasurementDataToPC();
void SendRawDataToPC();

#endif /* SRC_SENDDATA_H_ */
