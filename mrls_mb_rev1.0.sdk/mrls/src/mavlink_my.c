#include "mavlink_my.h"

static void send_mavlink_pkt(uint8_t iPacket, uint8_t nPackets, const int32_t* points, uint32_t nPoints);

int32_t raw[2048];
int32_t* praw = raw;

char bufTx[4096];
volatile bool mem_empty = true;
 /**
  * @brief Функция формирования и передачи mavLink пакетов по 64 байта из массива сырых данных
  *
  * @param iPacket позиция посылки в наборе
  * @param nPackets количество посылок в наборе
  * @param points Указатель на массив принятых данных которые необходимо завернуть в mavLink
  * @param nPoints Количество передаваемых слов (32b)
  */
static void send_mavlink_pkt(uint8_t iPacket, uint8_t nPackets, const int32_t* points, uint32_t nPoints) {

	char bufTx[8192];
	mavlink_message_t msgTx;
	mavlink_raw_data_t mavData;
	for (uint32_t j = 0; j < nPoints; j++) {
    	mavData.data[j] = (int32_t)points[j];
    	//mavData.data[j] = cnt_azimut;
    }
    mavData.time = 0; // тут нужно дать уникальный индекс для посылки (хорошо время)
    mavData.azimuth = 0; // тут нужно дать уникальный индекс для набора данных
    mavData.count = nPackets; // количество посылок в наборе
    mavData.index = iPacket; // позиция посылки в наборе

	mavlink_msg_raw_data_encode(SYS_ID, COMPONENT_ID, &msgTx, &mavData);
	u16 len = mavlink_msg_to_send_buffer((uint8_t *)bufTx, &msgTx);

	if (my_udp_send(bufTx, len, &ip_addr_broadcast) < 0) {
		//setDebugLed(1);
	} else {
		//setDebugLed(0);
	}
}
void send_rawData_mavLink(const int32_t* points, uint32_t nPoints) {

	if(!mem_empty) {
	//if(1) {
		unsigned i = 0;
		uint8_t iPacket = 0;
		uint8_t nPackets = (nPoints + DATA_MAX_LEN - 1) / DATA_MAX_LEN; // - Деление с округлением вверх

		while(i < nPoints) {
			if (i + DATA_MAX_LEN > nPoints) {
				// Есть "хвост" - данные, не уместившиеся в размер DATA_MAX_LEN
				uint32_t nTail = nPoints - i;
				send_mavlink_pkt(iPacket, nPackets, points + i, nTail);
				break;
			} else {
				send_mavlink_pkt(iPacket, nPackets, points + i, DATA_MAX_LEN);
				iPacket += 1;
				i += DATA_MAX_LEN;
			}
		}
	}
	mem_empty = true;
}
void send_rawData_tcp(){

//	if(flag){
//		 flag = 0;
//				if(tcp_write(server_pcb, praw, 1024, 1) == 0){
//	     	    	tcp_output(server_pcb);
//	     	    }
//	     	    else{
////	     	    	AD9508_mWriteReg(XPAR_AD9508_0_S00_AXI_BASEADDR, AD9508_S00_AXI_SLV_REG1_OFFSET, 0x2);   //LED
//	     	    }
//	 }
}
void send_cmdACK_mavLink() {

	mavlink_command_ack_t msgTx_cmd_ack;
	mavlink_message_t 	msgTx;
	msgTx_cmd_ack.command = sys_tick;
	msgTx_cmd_ack.result  = CMD_ACK_OK;
	msgTx_cmd_ack.progress  = 1;
	mavlink_msg_command_ack_encode(SYS_ID, COMPONENT_ID, &msgTx, &msgTx_cmd_ack);
	u16 len_cmd_ack = mavlink_msg_to_send_buffer((uint8_t *)bufTx, &msgTx);
	my_udp_send(bufTx, len_cmd_ack, &ip_addr_broadcast);
}
void send_rawData_udp(){

	if (my_udp_send(praw, 1024, &ip_addr_broadcast) < 0) {
		//setDebugLed(1);
	} else {
		//setDebugLed(0);
	}
}






