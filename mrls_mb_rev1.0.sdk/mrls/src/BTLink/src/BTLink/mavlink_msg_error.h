#pragma once
// MESSAGE ERROR PACKING

#define MAVLINK_MSG_ID_ERROR 3


typedef struct __mavlink_error_t {
 uint32_t time; /*<  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.*/
 uint32_t error_id; /*<   .*/
 char text[128]; /*<   .*/
} mavlink_error_t;

#define MAVLINK_MSG_ID_ERROR_LEN 136
#define MAVLINK_MSG_ID_ERROR_MIN_LEN 136
#define MAVLINK_MSG_ID_3_LEN 136
#define MAVLINK_MSG_ID_3_MIN_LEN 136

#define MAVLINK_MSG_ID_ERROR_CRC 67
#define MAVLINK_MSG_ID_3_CRC 67

#define MAVLINK_MSG_ERROR_FIELD_TEXT_LEN 128

#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_ERROR { \
    3, \
    "ERROR", \
    3, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_error_t, time) }, \
         { "error_id", NULL, MAVLINK_TYPE_UINT32_T, 0, 4, offsetof(mavlink_error_t, error_id) }, \
         { "text", NULL, MAVLINK_TYPE_CHAR, 128, 8, offsetof(mavlink_error_t, text) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_ERROR { \
    "ERROR", \
    3, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_error_t, time) }, \
         { "error_id", NULL, MAVLINK_TYPE_UINT32_T, 0, 4, offsetof(mavlink_error_t, error_id) }, \
         { "text", NULL, MAVLINK_TYPE_CHAR, 128, 8, offsetof(mavlink_error_t, text) }, \
         } \
}
#endif

/**
 * @brief Pack a error message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param error_id   .
 * @param text   .
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_error_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t time, uint32_t error_id, const char *text)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_ERROR_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint32_t(buf, 4, error_id);
    _mav_put_char_array(buf, 8, text, 128);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_ERROR_LEN);
#else
    mavlink_error_t packet;
    packet.time = time;
    packet.error_id = error_id;
    mav_array_memcpy(packet.text, text, sizeof(char)*128);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_ERROR_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_ERROR;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_ERROR_MIN_LEN, MAVLINK_MSG_ID_ERROR_LEN, MAVLINK_MSG_ID_ERROR_CRC);
}

/**
 * @brief Pack a error message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param error_id   .
 * @param text   .
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_error_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t time,uint32_t error_id,const char *text)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_ERROR_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint32_t(buf, 4, error_id);
    _mav_put_char_array(buf, 8, text, 128);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_ERROR_LEN);
#else
    mavlink_error_t packet;
    packet.time = time;
    packet.error_id = error_id;
    mav_array_memcpy(packet.text, text, sizeof(char)*128);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_ERROR_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_ERROR;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_ERROR_MIN_LEN, MAVLINK_MSG_ID_ERROR_LEN, MAVLINK_MSG_ID_ERROR_CRC);
}

/**
 * @brief Encode a error struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param error C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_error_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_error_t* error)
{
    return mavlink_msg_error_pack(system_id, component_id, msg, error->time, error->error_id, error->text);
}

/**
 * @brief Encode a error struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param error C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_error_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_error_t* error)
{
    return mavlink_msg_error_pack_chan(system_id, component_id, chan, msg, error->time, error->error_id, error->text);
}

/**
 * @brief Send a error message
 * @param chan MAVLink channel to send the message
 *
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param error_id   .
 * @param text   .
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_error_send(mavlink_channel_t chan, uint32_t time, uint32_t error_id, const char *text)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_ERROR_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint32_t(buf, 4, error_id);
    _mav_put_char_array(buf, 8, text, 128);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ERROR, buf, MAVLINK_MSG_ID_ERROR_MIN_LEN, MAVLINK_MSG_ID_ERROR_LEN, MAVLINK_MSG_ID_ERROR_CRC);
#else
    mavlink_error_t packet;
    packet.time = time;
    packet.error_id = error_id;
    mav_array_memcpy(packet.text, text, sizeof(char)*128);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ERROR, (const char *)&packet, MAVLINK_MSG_ID_ERROR_MIN_LEN, MAVLINK_MSG_ID_ERROR_LEN, MAVLINK_MSG_ID_ERROR_CRC);
#endif
}

/**
 * @brief Send a error message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_error_send_struct(mavlink_channel_t chan, const mavlink_error_t* error)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_error_send(chan, error->time, error->error_id, error->text);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ERROR, (const char *)error, MAVLINK_MSG_ID_ERROR_MIN_LEN, MAVLINK_MSG_ID_ERROR_LEN, MAVLINK_MSG_ID_ERROR_CRC);
#endif
}

#if MAVLINK_MSG_ID_ERROR_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_error_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t time, uint32_t error_id, const char *text)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint32_t(buf, 4, error_id);
    _mav_put_char_array(buf, 8, text, 128);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ERROR, buf, MAVLINK_MSG_ID_ERROR_MIN_LEN, MAVLINK_MSG_ID_ERROR_LEN, MAVLINK_MSG_ID_ERROR_CRC);
#else
    mavlink_error_t *packet = (mavlink_error_t *)msgbuf;
    packet->time = time;
    packet->error_id = error_id;
    mav_array_memcpy(packet->text, text, sizeof(char)*128);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ERROR, (const char *)packet, MAVLINK_MSG_ID_ERROR_MIN_LEN, MAVLINK_MSG_ID_ERROR_LEN, MAVLINK_MSG_ID_ERROR_CRC);
#endif
}
#endif

#endif

// MESSAGE ERROR UNPACKING


/**
 * @brief Get field time from error message
 *
 * @return  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 */
static inline uint32_t mavlink_msg_error_get_time(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field error_id from error message
 *
 * @return   .
 */
static inline uint32_t mavlink_msg_error_get_error_id(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  4);
}

/**
 * @brief Get field text from error message
 *
 * @return   .
 */
static inline uint16_t mavlink_msg_error_get_text(const mavlink_message_t* msg, char *text)
{
    return _MAV_RETURN_char_array(msg, text, 128,  8);
}

/**
 * @brief Decode a error message into a struct
 *
 * @param msg The message to decode
 * @param error C-struct to decode the message contents into
 */
static inline void mavlink_msg_error_decode(const mavlink_message_t* msg, mavlink_error_t* error)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    error->time = mavlink_msg_error_get_time(msg);
    error->error_id = mavlink_msg_error_get_error_id(msg);
    mavlink_msg_error_get_text(msg, error->text);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_ERROR_LEN? msg->len : MAVLINK_MSG_ID_ERROR_LEN;
        memset(error, 0, MAVLINK_MSG_ID_ERROR_LEN);
    memcpy(error, _MAV_PAYLOAD(msg), len);
#endif
}
