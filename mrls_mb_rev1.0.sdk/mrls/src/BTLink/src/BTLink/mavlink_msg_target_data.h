#pragma once
// MESSAGE TARGET_DATA PACKING

#define MAVLINK_MSG_ID_TARGET_DATA 33


typedef struct __mavlink_target_data_t {
 uint32_t time; /*<  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.*/
 float azimuth; /*<  .*/
 float distance; /*<  .*/
 float erp; /*<  .*/
} mavlink_target_data_t;

#define MAVLINK_MSG_ID_TARGET_DATA_LEN 16
#define MAVLINK_MSG_ID_TARGET_DATA_MIN_LEN 16
#define MAVLINK_MSG_ID_33_LEN 16
#define MAVLINK_MSG_ID_33_MIN_LEN 16

#define MAVLINK_MSG_ID_TARGET_DATA_CRC 148
#define MAVLINK_MSG_ID_33_CRC 148



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_TARGET_DATA { \
    33, \
    "TARGET_DATA", \
    4, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_target_data_t, time) }, \
         { "azimuth", NULL, MAVLINK_TYPE_FLOAT, 0, 4, offsetof(mavlink_target_data_t, azimuth) }, \
         { "distance", NULL, MAVLINK_TYPE_FLOAT, 0, 8, offsetof(mavlink_target_data_t, distance) }, \
         { "erp", NULL, MAVLINK_TYPE_FLOAT, 0, 12, offsetof(mavlink_target_data_t, erp) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_TARGET_DATA { \
    "TARGET_DATA", \
    4, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_target_data_t, time) }, \
         { "azimuth", NULL, MAVLINK_TYPE_FLOAT, 0, 4, offsetof(mavlink_target_data_t, azimuth) }, \
         { "distance", NULL, MAVLINK_TYPE_FLOAT, 0, 8, offsetof(mavlink_target_data_t, distance) }, \
         { "erp", NULL, MAVLINK_TYPE_FLOAT, 0, 12, offsetof(mavlink_target_data_t, erp) }, \
         } \
}
#endif

/**
 * @brief Pack a target_data message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param azimuth  .
 * @param distance  .
 * @param erp  .
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_target_data_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t time, float azimuth, float distance, float erp)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_TARGET_DATA_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_float(buf, 4, azimuth);
    _mav_put_float(buf, 8, distance);
    _mav_put_float(buf, 12, erp);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_TARGET_DATA_LEN);
#else
    mavlink_target_data_t packet;
    packet.time = time;
    packet.azimuth = azimuth;
    packet.distance = distance;
    packet.erp = erp;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_TARGET_DATA_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_TARGET_DATA;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_TARGET_DATA_MIN_LEN, MAVLINK_MSG_ID_TARGET_DATA_LEN, MAVLINK_MSG_ID_TARGET_DATA_CRC);
}

/**
 * @brief Pack a target_data message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param azimuth  .
 * @param distance  .
 * @param erp  .
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_target_data_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t time,float azimuth,float distance,float erp)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_TARGET_DATA_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_float(buf, 4, azimuth);
    _mav_put_float(buf, 8, distance);
    _mav_put_float(buf, 12, erp);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_TARGET_DATA_LEN);
#else
    mavlink_target_data_t packet;
    packet.time = time;
    packet.azimuth = azimuth;
    packet.distance = distance;
    packet.erp = erp;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_TARGET_DATA_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_TARGET_DATA;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_TARGET_DATA_MIN_LEN, MAVLINK_MSG_ID_TARGET_DATA_LEN, MAVLINK_MSG_ID_TARGET_DATA_CRC);
}

/**
 * @brief Encode a target_data struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param target_data C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_target_data_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_target_data_t* target_data)
{
    return mavlink_msg_target_data_pack(system_id, component_id, msg, target_data->time, target_data->azimuth, target_data->distance, target_data->erp);
}

/**
 * @brief Encode a target_data struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param target_data C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_target_data_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_target_data_t* target_data)
{
    return mavlink_msg_target_data_pack_chan(system_id, component_id, chan, msg, target_data->time, target_data->azimuth, target_data->distance, target_data->erp);
}

/**
 * @brief Send a target_data message
 * @param chan MAVLink channel to send the message
 *
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param azimuth  .
 * @param distance  .
 * @param erp  .
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_target_data_send(mavlink_channel_t chan, uint32_t time, float azimuth, float distance, float erp)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_TARGET_DATA_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_float(buf, 4, azimuth);
    _mav_put_float(buf, 8, distance);
    _mav_put_float(buf, 12, erp);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_TARGET_DATA, buf, MAVLINK_MSG_ID_TARGET_DATA_MIN_LEN, MAVLINK_MSG_ID_TARGET_DATA_LEN, MAVLINK_MSG_ID_TARGET_DATA_CRC);
#else
    mavlink_target_data_t packet;
    packet.time = time;
    packet.azimuth = azimuth;
    packet.distance = distance;
    packet.erp = erp;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_TARGET_DATA, (const char *)&packet, MAVLINK_MSG_ID_TARGET_DATA_MIN_LEN, MAVLINK_MSG_ID_TARGET_DATA_LEN, MAVLINK_MSG_ID_TARGET_DATA_CRC);
#endif
}

/**
 * @brief Send a target_data message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_target_data_send_struct(mavlink_channel_t chan, const mavlink_target_data_t* target_data)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_target_data_send(chan, target_data->time, target_data->azimuth, target_data->distance, target_data->erp);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_TARGET_DATA, (const char *)target_data, MAVLINK_MSG_ID_TARGET_DATA_MIN_LEN, MAVLINK_MSG_ID_TARGET_DATA_LEN, MAVLINK_MSG_ID_TARGET_DATA_CRC);
#endif
}

#if MAVLINK_MSG_ID_TARGET_DATA_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_target_data_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t time, float azimuth, float distance, float erp)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_float(buf, 4, azimuth);
    _mav_put_float(buf, 8, distance);
    _mav_put_float(buf, 12, erp);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_TARGET_DATA, buf, MAVLINK_MSG_ID_TARGET_DATA_MIN_LEN, MAVLINK_MSG_ID_TARGET_DATA_LEN, MAVLINK_MSG_ID_TARGET_DATA_CRC);
#else
    mavlink_target_data_t *packet = (mavlink_target_data_t *)msgbuf;
    packet->time = time;
    packet->azimuth = azimuth;
    packet->distance = distance;
    packet->erp = erp;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_TARGET_DATA, (const char *)packet, MAVLINK_MSG_ID_TARGET_DATA_MIN_LEN, MAVLINK_MSG_ID_TARGET_DATA_LEN, MAVLINK_MSG_ID_TARGET_DATA_CRC);
#endif
}
#endif

#endif

// MESSAGE TARGET_DATA UNPACKING


/**
 * @brief Get field time from target_data message
 *
 * @return  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 */
static inline uint32_t mavlink_msg_target_data_get_time(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field azimuth from target_data message
 *
 * @return  .
 */
static inline float mavlink_msg_target_data_get_azimuth(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  4);
}

/**
 * @brief Get field distance from target_data message
 *
 * @return  .
 */
static inline float mavlink_msg_target_data_get_distance(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  8);
}

/**
 * @brief Get field erp from target_data message
 *
 * @return  .
 */
static inline float mavlink_msg_target_data_get_erp(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  12);
}

/**
 * @brief Decode a target_data message into a struct
 *
 * @param msg The message to decode
 * @param target_data C-struct to decode the message contents into
 */
static inline void mavlink_msg_target_data_decode(const mavlink_message_t* msg, mavlink_target_data_t* target_data)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    target_data->time = mavlink_msg_target_data_get_time(msg);
    target_data->azimuth = mavlink_msg_target_data_get_azimuth(msg);
    target_data->distance = mavlink_msg_target_data_get_distance(msg);
    target_data->erp = mavlink_msg_target_data_get_erp(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_TARGET_DATA_LEN? msg->len : MAVLINK_MSG_ID_TARGET_DATA_LEN;
        memset(target_data, 0, MAVLINK_MSG_ID_TARGET_DATA_LEN);
    memcpy(target_data, _MAV_PAYLOAD(msg), len);
#endif
}
