#pragma once
// MESSAGE PARAM_REQUEST_GROUP PACKING

#define MAVLINK_MSG_ID_PARAM_REQUEST_GROUP 11


typedef struct __mavlink_param_request_group_t {
 uint8_t target_system; /*<  System ID*/
 uint8_t target_component; /*<  Component ID*/
 char param_group[16]; /*<  Onboard group parameters*/
} mavlink_param_request_group_t;

#define MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_LEN 18
#define MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_MIN_LEN 18
#define MAVLINK_MSG_ID_11_LEN 18
#define MAVLINK_MSG_ID_11_MIN_LEN 18

#define MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_CRC 206
#define MAVLINK_MSG_ID_11_CRC 206

#define MAVLINK_MSG_PARAM_REQUEST_GROUP_FIELD_PARAM_GROUP_LEN 16

#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_PARAM_REQUEST_GROUP { \
    11, \
    "PARAM_REQUEST_GROUP", \
    3, \
    {  { "target_system", NULL, MAVLINK_TYPE_UINT8_T, 0, 0, offsetof(mavlink_param_request_group_t, target_system) }, \
         { "target_component", NULL, MAVLINK_TYPE_UINT8_T, 0, 1, offsetof(mavlink_param_request_group_t, target_component) }, \
         { "param_group", NULL, MAVLINK_TYPE_CHAR, 16, 2, offsetof(mavlink_param_request_group_t, param_group) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_PARAM_REQUEST_GROUP { \
    "PARAM_REQUEST_GROUP", \
    3, \
    {  { "target_system", NULL, MAVLINK_TYPE_UINT8_T, 0, 0, offsetof(mavlink_param_request_group_t, target_system) }, \
         { "target_component", NULL, MAVLINK_TYPE_UINT8_T, 0, 1, offsetof(mavlink_param_request_group_t, target_component) }, \
         { "param_group", NULL, MAVLINK_TYPE_CHAR, 16, 2, offsetof(mavlink_param_request_group_t, param_group) }, \
         } \
}
#endif

/**
 * @brief Pack a param_request_group message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param target_system  System ID
 * @param target_component  Component ID
 * @param param_group  Onboard group parameters
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_param_request_group_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint8_t target_system, uint8_t target_component, const char *param_group)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_LEN];
    _mav_put_uint8_t(buf, 0, target_system);
    _mav_put_uint8_t(buf, 1, target_component);
    _mav_put_char_array(buf, 2, param_group, 16);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_LEN);
#else
    mavlink_param_request_group_t packet;
    packet.target_system = target_system;
    packet.target_component = target_component;
    mav_array_memcpy(packet.param_group, param_group, sizeof(char)*16);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_PARAM_REQUEST_GROUP;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_MIN_LEN, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_LEN, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_CRC);
}

/**
 * @brief Pack a param_request_group message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param target_system  System ID
 * @param target_component  Component ID
 * @param param_group  Onboard group parameters
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_param_request_group_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint8_t target_system,uint8_t target_component,const char *param_group)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_LEN];
    _mav_put_uint8_t(buf, 0, target_system);
    _mav_put_uint8_t(buf, 1, target_component);
    _mav_put_char_array(buf, 2, param_group, 16);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_LEN);
#else
    mavlink_param_request_group_t packet;
    packet.target_system = target_system;
    packet.target_component = target_component;
    mav_array_memcpy(packet.param_group, param_group, sizeof(char)*16);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_PARAM_REQUEST_GROUP;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_MIN_LEN, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_LEN, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_CRC);
}

/**
 * @brief Encode a param_request_group struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param param_request_group C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_param_request_group_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_param_request_group_t* param_request_group)
{
    return mavlink_msg_param_request_group_pack(system_id, component_id, msg, param_request_group->target_system, param_request_group->target_component, param_request_group->param_group);
}

/**
 * @brief Encode a param_request_group struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param param_request_group C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_param_request_group_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_param_request_group_t* param_request_group)
{
    return mavlink_msg_param_request_group_pack_chan(system_id, component_id, chan, msg, param_request_group->target_system, param_request_group->target_component, param_request_group->param_group);
}

/**
 * @brief Send a param_request_group message
 * @param chan MAVLink channel to send the message
 *
 * @param target_system  System ID
 * @param target_component  Component ID
 * @param param_group  Onboard group parameters
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_param_request_group_send(mavlink_channel_t chan, uint8_t target_system, uint8_t target_component, const char *param_group)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_LEN];
    _mav_put_uint8_t(buf, 0, target_system);
    _mav_put_uint8_t(buf, 1, target_component);
    _mav_put_char_array(buf, 2, param_group, 16);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP, buf, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_MIN_LEN, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_LEN, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_CRC);
#else
    mavlink_param_request_group_t packet;
    packet.target_system = target_system;
    packet.target_component = target_component;
    mav_array_memcpy(packet.param_group, param_group, sizeof(char)*16);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP, (const char *)&packet, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_MIN_LEN, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_LEN, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_CRC);
#endif
}

/**
 * @brief Send a param_request_group message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_param_request_group_send_struct(mavlink_channel_t chan, const mavlink_param_request_group_t* param_request_group)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_param_request_group_send(chan, param_request_group->target_system, param_request_group->target_component, param_request_group->param_group);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP, (const char *)param_request_group, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_MIN_LEN, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_LEN, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_CRC);
#endif
}

#if MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_param_request_group_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint8_t target_system, uint8_t target_component, const char *param_group)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint8_t(buf, 0, target_system);
    _mav_put_uint8_t(buf, 1, target_component);
    _mav_put_char_array(buf, 2, param_group, 16);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP, buf, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_MIN_LEN, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_LEN, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_CRC);
#else
    mavlink_param_request_group_t *packet = (mavlink_param_request_group_t *)msgbuf;
    packet->target_system = target_system;
    packet->target_component = target_component;
    mav_array_memcpy(packet->param_group, param_group, sizeof(char)*16);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP, (const char *)packet, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_MIN_LEN, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_LEN, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_CRC);
#endif
}
#endif

#endif

// MESSAGE PARAM_REQUEST_GROUP UNPACKING


/**
 * @brief Get field target_system from param_request_group message
 *
 * @return  System ID
 */
static inline uint8_t mavlink_msg_param_request_group_get_target_system(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  0);
}

/**
 * @brief Get field target_component from param_request_group message
 *
 * @return  Component ID
 */
static inline uint8_t mavlink_msg_param_request_group_get_target_component(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  1);
}

/**
 * @brief Get field param_group from param_request_group message
 *
 * @return  Onboard group parameters
 */
static inline uint16_t mavlink_msg_param_request_group_get_param_group(const mavlink_message_t* msg, char *param_group)
{
    return _MAV_RETURN_char_array(msg, param_group, 16,  2);
}

/**
 * @brief Decode a param_request_group message into a struct
 *
 * @param msg The message to decode
 * @param param_request_group C-struct to decode the message contents into
 */
static inline void mavlink_msg_param_request_group_decode(const mavlink_message_t* msg, mavlink_param_request_group_t* param_request_group)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    param_request_group->target_system = mavlink_msg_param_request_group_get_target_system(msg);
    param_request_group->target_component = mavlink_msg_param_request_group_get_target_component(msg);
    mavlink_msg_param_request_group_get_param_group(msg, param_request_group->param_group);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_LEN? msg->len : MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_LEN;
        memset(param_request_group, 0, MAVLINK_MSG_ID_PARAM_REQUEST_GROUP_LEN);
    memcpy(param_request_group, _MAV_PAYLOAD(msg), len);
#endif
}
