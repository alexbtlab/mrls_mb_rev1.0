#ifndef SRC_TIMER_1MS_H_
#define SRC_TIMER_1MS_H_

#include "xtmrctr_l.h"
#include "xil_printf_BLE.h"
#include "xintc.h"
#include "platform_config.h"

	void init_timer_1ms(void);
	void timer_1ms_callback(void);

#endif /* SRC_TIMER_1MS_H_ */
