-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Mon Mar 22 13:00:54 2021
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_HMC769_0_0_sim_netlist.vhdl
-- Design      : design_1_HMC769_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HMC769_v1_0_S00_AXI is
  port (
    s00_axi_wready : out STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_rvalid : out STD_LOGIC;
    \slv_reg2_reg[1]_0\ : out STD_LOGIC;
    \slv_reg2_reg[0]_0\ : out STD_LOGIC;
    \slv_reg3_reg[21]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg3_reg[17]_0\ : out STD_LOGIC;
    \slv_reg3_reg[13]_0\ : out STD_LOGIC;
    \slv_reg3_reg[9]_0\ : out STD_LOGIC;
    \slv_reg3_reg[5]_0\ : out STD_LOGIC;
    \slv_reg3_reg[1]_0\ : out STD_LOGIC;
    \slv_reg1_reg[4]_0\ : out STD_LOGIC_VECTOR ( 4 downto 0 );
    \slv_reg4_reg[5]_0\ : out STD_LOGIC_VECTOR ( 5 downto 0 );
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    slv_reg0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_aclk : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 1 downto 0 );
    bits_send_reg : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \axi_rdata_reg[23]_0\ : in STD_LOGIC_VECTOR ( 23 downto 0 );
    ip2mb_reg0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HMC769_v1_0_S00_AXI;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HMC769_v1_0_S00_AXI is
  signal \^q\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal aw_en_i_1_n_0 : STD_LOGIC;
  signal aw_en_reg_n_0 : STD_LOGIC;
  signal axi_arready0 : STD_LOGIC;
  signal axi_awready0 : STD_LOGIC;
  signal axi_awready_i_1_n_0 : STD_LOGIC;
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal \axi_rdata[0]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[10]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[11]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[12]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[13]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[14]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[15]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[16]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[17]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[18]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[19]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[1]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[20]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[21]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[22]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[23]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[2]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[3]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[4]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[5]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[6]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[7]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[8]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[9]_i_2_n_0\ : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal p_1_in : STD_LOGIC_VECTOR ( 31 downto 7 );
  signal reg_data_out : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^s00_axi_arready\ : STD_LOGIC;
  signal \^s00_axi_awready\ : STD_LOGIC;
  signal \^s00_axi_bvalid\ : STD_LOGIC;
  signal \^s00_axi_rvalid\ : STD_LOGIC;
  signal \^s00_axi_wready\ : STD_LOGIC;
  signal sel0 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \^slv_reg0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \slv_reg0[0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg0[1]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg0[1]_i_2_n_0\ : STD_LOGIC;
  signal slv_reg1 : STD_LOGIC_VECTOR ( 31 downto 5 );
  signal \^slv_reg1_reg[4]_0\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal slv_reg2 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg2[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg3 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg3[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg4 : STD_LOGIC_VECTOR ( 31 downto 6 );
  signal \slv_reg4[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg4[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg4[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg4[7]_i_1_n_0\ : STD_LOGIC;
  signal \^slv_reg4_reg[5]_0\ : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal slv_reg5 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg5[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg5[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg5[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg5[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg6 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg6[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg6[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg6[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg6[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg7 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg7[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg7[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg7[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg7[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_rden__0\ : STD_LOGIC;
  signal \slv_reg_wren__0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of axi_wready_i_1 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \slv_reg0[0]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \slv_reg0[1]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \slv_reg1[31]_i_2\ : label is "soft_lutpair0";
begin
  Q(0) <= \^q\(0);
  s00_axi_arready <= \^s00_axi_arready\;
  s00_axi_awready <= \^s00_axi_awready\;
  s00_axi_bvalid <= \^s00_axi_bvalid\;
  s00_axi_rvalid <= \^s00_axi_rvalid\;
  s00_axi_wready <= \^s00_axi_wready\;
  slv_reg0(1 downto 0) <= \^slv_reg0\(1 downto 0);
  \slv_reg1_reg[4]_0\(4 downto 0) <= \^slv_reg1_reg[4]_0\(4 downto 0);
  \slv_reg4_reg[5]_0\(5 downto 0) <= \^slv_reg4_reg[5]_0\(5 downto 0);
aw_en_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F7FFC4CCC4CCC4CC"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => aw_en_reg_n_0,
      I2 => \^s00_axi_awready\,
      I3 => s00_axi_wvalid,
      I4 => s00_axi_bready,
      I5 => \^s00_axi_bvalid\,
      O => aw_en_i_1_n_0
    );
aw_en_reg: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => aw_en_i_1_n_0,
      Q => aw_en_reg_n_0,
      S => axi_awready_i_1_n_0
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(0),
      Q => sel0(0),
      R => axi_awready_i_1_n_0
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(1),
      Q => sel0(1),
      R => axi_awready_i_1_n_0
    );
\axi_araddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(2),
      Q => sel0(2),
      R => axi_awready_i_1_n_0
    );
\axi_araddr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(3),
      Q => sel0(3),
      R => axi_awready_i_1_n_0
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s00_axi_arready\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_arready0,
      Q => \^s00_axi_arready\,
      R => axi_awready_i_1_n_0
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(0),
      Q => p_0_in(0),
      R => axi_awready_i_1_n_0
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(1),
      Q => p_0_in(1),
      R => axi_awready_i_1_n_0
    );
\axi_awaddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(2),
      Q => p_0_in(2),
      R => axi_awready_i_1_n_0
    );
\axi_awaddr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(3),
      Q => p_0_in(3),
      R => axi_awready_i_1_n_0
    );
axi_awready_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s00_axi_aresetn,
      O => axi_awready_i_1_n_0
    );
axi_awready_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2000"
    )
        port map (
      I0 => s00_axi_wvalid,
      I1 => \^s00_axi_awready\,
      I2 => aw_en_reg_n_0,
      I3 => s00_axi_awvalid,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_awready0,
      Q => \^s00_axi_awready\,
      R => axi_awready_i_1_n_0
    );
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF80008000"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => s00_axi_wvalid,
      I2 => \^s00_axi_awready\,
      I3 => \^s00_axi_wready\,
      I4 => s00_axi_bready,
      I5 => \^s00_axi_bvalid\,
      O => axi_bvalid_i_1_n_0
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_bvalid_i_1_n_0,
      Q => \^s00_axi_bvalid\,
      R => axi_awready_i_1_n_0
    );
\axi_rdata[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \axi_rdata[0]_i_2_n_0\,
      I1 => sel0(3),
      I2 => \axi_rdata[0]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[0]_i_4_n_0\,
      O => reg_data_out(0)
    );
\axi_rdata[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FE11FE00"
    )
        port map (
      I0 => sel0(2),
      I1 => sel0(1),
      I2 => \axi_rdata_reg[23]_0\(0),
      I3 => sel0(0),
      I4 => ip2mb_reg0(0),
      O => \axi_rdata[0]_i_2_n_0\
    );
\axi_rdata[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(0),
      I1 => slv_reg6(0),
      I2 => sel0(1),
      I3 => slv_reg5(0),
      I4 => sel0(0),
      I5 => \^slv_reg4_reg[5]_0\(0),
      O => \axi_rdata[0]_i_3_n_0\
    );
\axi_rdata[0]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => slv_reg3(0),
      I1 => slv_reg2(0),
      I2 => sel0(1),
      I3 => \^slv_reg1_reg[4]_0\(0),
      I4 => sel0(0),
      O => \axi_rdata[0]_i_4_n_0\
    );
\axi_rdata[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040FFFF00400000"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_rdata_reg[23]_0\(10),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => sel0(3),
      I5 => \axi_rdata_reg[10]_i_2_n_0\,
      O => reg_data_out(10)
    );
\axi_rdata[10]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => slv_reg3(10),
      I1 => slv_reg2(10),
      I2 => sel0(1),
      I3 => slv_reg1(10),
      I4 => sel0(0),
      O => \axi_rdata[10]_i_3_n_0\
    );
\axi_rdata[10]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(10),
      I1 => slv_reg6(10),
      I2 => sel0(1),
      I3 => slv_reg5(10),
      I4 => sel0(0),
      I5 => slv_reg4(10),
      O => \axi_rdata[10]_i_4_n_0\
    );
\axi_rdata[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040FFFF00400000"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_rdata_reg[23]_0\(11),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => sel0(3),
      I5 => \axi_rdata_reg[11]_i_2_n_0\,
      O => reg_data_out(11)
    );
\axi_rdata[11]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => slv_reg3(11),
      I1 => slv_reg2(11),
      I2 => sel0(1),
      I3 => slv_reg1(11),
      I4 => sel0(0),
      O => \axi_rdata[11]_i_3_n_0\
    );
\axi_rdata[11]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(11),
      I1 => slv_reg6(11),
      I2 => sel0(1),
      I3 => slv_reg5(11),
      I4 => sel0(0),
      I5 => slv_reg4(11),
      O => \axi_rdata[11]_i_4_n_0\
    );
\axi_rdata[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040FFFF00400000"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_rdata_reg[23]_0\(12),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => sel0(3),
      I5 => \axi_rdata_reg[12]_i_2_n_0\,
      O => reg_data_out(12)
    );
\axi_rdata[12]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => slv_reg3(12),
      I1 => slv_reg2(12),
      I2 => sel0(1),
      I3 => slv_reg1(12),
      I4 => sel0(0),
      O => \axi_rdata[12]_i_3_n_0\
    );
\axi_rdata[12]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(12),
      I1 => slv_reg6(12),
      I2 => sel0(1),
      I3 => slv_reg5(12),
      I4 => sel0(0),
      I5 => slv_reg4(12),
      O => \axi_rdata[12]_i_4_n_0\
    );
\axi_rdata[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040FFFF00400000"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_rdata_reg[23]_0\(13),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => sel0(3),
      I5 => \axi_rdata_reg[13]_i_2_n_0\,
      O => reg_data_out(13)
    );
\axi_rdata[13]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => slv_reg3(13),
      I1 => slv_reg2(13),
      I2 => sel0(1),
      I3 => slv_reg1(13),
      I4 => sel0(0),
      O => \axi_rdata[13]_i_3_n_0\
    );
\axi_rdata[13]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(13),
      I1 => slv_reg6(13),
      I2 => sel0(1),
      I3 => slv_reg5(13),
      I4 => sel0(0),
      I5 => slv_reg4(13),
      O => \axi_rdata[13]_i_4_n_0\
    );
\axi_rdata[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040FFFF00400000"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_rdata_reg[23]_0\(14),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => sel0(3),
      I5 => \axi_rdata_reg[14]_i_2_n_0\,
      O => reg_data_out(14)
    );
\axi_rdata[14]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => slv_reg3(14),
      I1 => slv_reg2(14),
      I2 => sel0(1),
      I3 => slv_reg1(14),
      I4 => sel0(0),
      O => \axi_rdata[14]_i_3_n_0\
    );
\axi_rdata[14]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(14),
      I1 => slv_reg6(14),
      I2 => sel0(1),
      I3 => slv_reg5(14),
      I4 => sel0(0),
      I5 => slv_reg4(14),
      O => \axi_rdata[14]_i_4_n_0\
    );
\axi_rdata[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040FFFF00400000"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_rdata_reg[23]_0\(15),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => sel0(3),
      I5 => \axi_rdata_reg[15]_i_2_n_0\,
      O => reg_data_out(15)
    );
\axi_rdata[15]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => slv_reg3(15),
      I1 => slv_reg2(15),
      I2 => sel0(1),
      I3 => slv_reg1(15),
      I4 => sel0(0),
      O => \axi_rdata[15]_i_3_n_0\
    );
\axi_rdata[15]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(15),
      I1 => slv_reg6(15),
      I2 => sel0(1),
      I3 => slv_reg5(15),
      I4 => sel0(0),
      I5 => slv_reg4(15),
      O => \axi_rdata[15]_i_4_n_0\
    );
\axi_rdata[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040FFFF00400000"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_rdata_reg[23]_0\(16),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => sel0(3),
      I5 => \axi_rdata_reg[16]_i_2_n_0\,
      O => reg_data_out(16)
    );
\axi_rdata[16]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(16),
      I1 => slv_reg2(16),
      I2 => sel0(1),
      I3 => sel0(0),
      I4 => slv_reg1(16),
      O => \axi_rdata[16]_i_3_n_0\
    );
\axi_rdata[16]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(16),
      I1 => slv_reg6(16),
      I2 => sel0(1),
      I3 => slv_reg5(16),
      I4 => sel0(0),
      I5 => slv_reg4(16),
      O => \axi_rdata[16]_i_4_n_0\
    );
\axi_rdata[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040FFFF00400000"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_rdata_reg[23]_0\(17),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => sel0(3),
      I5 => \axi_rdata_reg[17]_i_2_n_0\,
      O => reg_data_out(17)
    );
\axi_rdata[17]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(17),
      I1 => slv_reg2(17),
      I2 => sel0(1),
      I3 => sel0(0),
      I4 => slv_reg1(17),
      O => \axi_rdata[17]_i_3_n_0\
    );
\axi_rdata[17]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(17),
      I1 => slv_reg6(17),
      I2 => sel0(1),
      I3 => slv_reg5(17),
      I4 => sel0(0),
      I5 => slv_reg4(17),
      O => \axi_rdata[17]_i_4_n_0\
    );
\axi_rdata[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEEEFFFFFEEE0000"
    )
        port map (
      I0 => sel0(1),
      I1 => sel0(2),
      I2 => sel0(0),
      I3 => \axi_rdata_reg[23]_0\(18),
      I4 => sel0(3),
      I5 => \axi_rdata_reg[18]_i_2_n_0\,
      O => reg_data_out(18)
    );
\axi_rdata[18]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => slv_reg3(18),
      I1 => slv_reg2(18),
      I2 => sel0(1),
      I3 => slv_reg1(18),
      I4 => sel0(0),
      O => \axi_rdata[18]_i_3_n_0\
    );
\axi_rdata[18]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(18),
      I1 => slv_reg6(18),
      I2 => sel0(1),
      I3 => slv_reg5(18),
      I4 => sel0(0),
      I5 => slv_reg4(18),
      O => \axi_rdata[18]_i_4_n_0\
    );
\axi_rdata[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEEEFFFFFEEE0000"
    )
        port map (
      I0 => sel0(1),
      I1 => sel0(2),
      I2 => sel0(0),
      I3 => \axi_rdata_reg[23]_0\(19),
      I4 => sel0(3),
      I5 => \axi_rdata_reg[19]_i_2_n_0\,
      O => reg_data_out(19)
    );
\axi_rdata[19]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => slv_reg3(19),
      I1 => slv_reg2(19),
      I2 => sel0(1),
      I3 => slv_reg1(19),
      I4 => sel0(0),
      O => \axi_rdata[19]_i_3_n_0\
    );
\axi_rdata[19]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(19),
      I1 => slv_reg6(19),
      I2 => sel0(1),
      I3 => slv_reg5(19),
      I4 => sel0(0),
      I5 => slv_reg4(19),
      O => \axi_rdata[19]_i_4_n_0\
    );
\axi_rdata[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BAAAFFFFBAAA0000"
    )
        port map (
      I0 => sel0(1),
      I1 => sel0(2),
      I2 => sel0(0),
      I3 => \axi_rdata_reg[23]_0\(1),
      I4 => sel0(3),
      I5 => \axi_rdata_reg[1]_i_2_n_0\,
      O => reg_data_out(1)
    );
\axi_rdata[1]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0C0C0"
    )
        port map (
      I0 => slv_reg3(1),
      I1 => slv_reg2(1),
      I2 => sel0(1),
      I3 => \^slv_reg1_reg[4]_0\(1),
      I4 => sel0(0),
      O => \axi_rdata[1]_i_3_n_0\
    );
\axi_rdata[1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(1),
      I1 => slv_reg6(1),
      I2 => sel0(1),
      I3 => slv_reg5(1),
      I4 => sel0(0),
      I5 => \^slv_reg4_reg[5]_0\(1),
      O => \axi_rdata[1]_i_4_n_0\
    );
\axi_rdata[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEEEFFFFFEEE0000"
    )
        port map (
      I0 => sel0(1),
      I1 => sel0(2),
      I2 => sel0(0),
      I3 => \axi_rdata_reg[23]_0\(20),
      I4 => sel0(3),
      I5 => \axi_rdata_reg[20]_i_2_n_0\,
      O => reg_data_out(20)
    );
\axi_rdata[20]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => slv_reg3(20),
      I1 => slv_reg2(20),
      I2 => sel0(1),
      I3 => slv_reg1(20),
      I4 => sel0(0),
      O => \axi_rdata[20]_i_3_n_0\
    );
\axi_rdata[20]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(20),
      I1 => slv_reg6(20),
      I2 => sel0(1),
      I3 => slv_reg5(20),
      I4 => sel0(0),
      I5 => slv_reg4(20),
      O => \axi_rdata[20]_i_4_n_0\
    );
\axi_rdata[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040FFFF00400000"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_rdata_reg[23]_0\(21),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => sel0(3),
      I5 => \axi_rdata_reg[21]_i_2_n_0\,
      O => reg_data_out(21)
    );
\axi_rdata[21]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(21),
      I1 => slv_reg2(21),
      I2 => sel0(1),
      I3 => sel0(0),
      I4 => slv_reg1(21),
      O => \axi_rdata[21]_i_3_n_0\
    );
\axi_rdata[21]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(21),
      I1 => slv_reg6(21),
      I2 => sel0(1),
      I3 => slv_reg5(21),
      I4 => sel0(0),
      I5 => slv_reg4(21),
      O => \axi_rdata[21]_i_4_n_0\
    );
\axi_rdata[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEEEFFFFFEEE0000"
    )
        port map (
      I0 => sel0(1),
      I1 => sel0(2),
      I2 => sel0(0),
      I3 => \axi_rdata_reg[23]_0\(22),
      I4 => sel0(3),
      I5 => \axi_rdata_reg[22]_i_2_n_0\,
      O => reg_data_out(22)
    );
\axi_rdata[22]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => slv_reg3(22),
      I1 => slv_reg2(22),
      I2 => sel0(1),
      I3 => slv_reg1(22),
      I4 => sel0(0),
      O => \axi_rdata[22]_i_3_n_0\
    );
\axi_rdata[22]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(22),
      I1 => slv_reg6(22),
      I2 => sel0(1),
      I3 => slv_reg5(22),
      I4 => sel0(0),
      I5 => slv_reg4(22),
      O => \axi_rdata[22]_i_4_n_0\
    );
\axi_rdata[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEEEFFFFFEEE0000"
    )
        port map (
      I0 => sel0(1),
      I1 => sel0(2),
      I2 => sel0(0),
      I3 => \axi_rdata_reg[23]_0\(23),
      I4 => sel0(3),
      I5 => \axi_rdata_reg[23]_i_2_n_0\,
      O => reg_data_out(23)
    );
\axi_rdata[23]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => \^q\(0),
      I1 => slv_reg2(23),
      I2 => sel0(1),
      I3 => slv_reg1(23),
      I4 => sel0(0),
      O => \axi_rdata[23]_i_3_n_0\
    );
\axi_rdata[23]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(23),
      I1 => slv_reg6(23),
      I2 => sel0(1),
      I3 => slv_reg5(23),
      I4 => sel0(0),
      I5 => slv_reg4(23),
      O => \axi_rdata[23]_i_4_n_0\
    );
\axi_rdata[24]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00E2"
    )
        port map (
      I0 => \axi_rdata[24]_i_2_n_0\,
      I1 => sel0(2),
      I2 => \axi_rdata[24]_i_3_n_0\,
      I3 => sel0(3),
      O => reg_data_out(24)
    );
\axi_rdata[24]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(24),
      I1 => slv_reg2(24),
      I2 => sel0(1),
      I3 => sel0(0),
      I4 => slv_reg1(24),
      O => \axi_rdata[24]_i_2_n_0\
    );
\axi_rdata[24]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(24),
      I1 => slv_reg6(24),
      I2 => sel0(1),
      I3 => slv_reg5(24),
      I4 => sel0(0),
      I5 => slv_reg4(24),
      O => \axi_rdata[24]_i_3_n_0\
    );
\axi_rdata[25]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00E2"
    )
        port map (
      I0 => \axi_rdata[25]_i_2_n_0\,
      I1 => sel0(2),
      I2 => \axi_rdata[25]_i_3_n_0\,
      I3 => sel0(3),
      O => reg_data_out(25)
    );
\axi_rdata[25]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(25),
      I1 => slv_reg2(25),
      I2 => sel0(1),
      I3 => sel0(0),
      I4 => slv_reg1(25),
      O => \axi_rdata[25]_i_2_n_0\
    );
\axi_rdata[25]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(25),
      I1 => slv_reg6(25),
      I2 => sel0(1),
      I3 => slv_reg5(25),
      I4 => sel0(0),
      I5 => slv_reg4(25),
      O => \axi_rdata[25]_i_3_n_0\
    );
\axi_rdata[26]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FCBBFC88"
    )
        port map (
      I0 => sel0(1),
      I1 => sel0(3),
      I2 => \axi_rdata[26]_i_2_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[26]_i_3_n_0\,
      O => reg_data_out(26)
    );
\axi_rdata[26]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(26),
      I1 => slv_reg6(26),
      I2 => sel0(1),
      I3 => slv_reg5(26),
      I4 => sel0(0),
      I5 => slv_reg4(26),
      O => \axi_rdata[26]_i_2_n_0\
    );
\axi_rdata[26]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => slv_reg3(26),
      I1 => slv_reg2(26),
      I2 => sel0(1),
      I3 => slv_reg1(26),
      I4 => sel0(0),
      O => \axi_rdata[26]_i_3_n_0\
    );
\axi_rdata[27]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FCBBFC88"
    )
        port map (
      I0 => sel0(1),
      I1 => sel0(3),
      I2 => \axi_rdata[27]_i_2_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[27]_i_3_n_0\,
      O => reg_data_out(27)
    );
\axi_rdata[27]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(27),
      I1 => slv_reg6(27),
      I2 => sel0(1),
      I3 => slv_reg5(27),
      I4 => sel0(0),
      I5 => slv_reg4(27),
      O => \axi_rdata[27]_i_2_n_0\
    );
\axi_rdata[27]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => slv_reg3(27),
      I1 => slv_reg2(27),
      I2 => sel0(1),
      I3 => slv_reg1(27),
      I4 => sel0(0),
      O => \axi_rdata[27]_i_3_n_0\
    );
\axi_rdata[28]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00E2"
    )
        port map (
      I0 => \axi_rdata[28]_i_2_n_0\,
      I1 => sel0(2),
      I2 => \axi_rdata[28]_i_3_n_0\,
      I3 => sel0(3),
      O => reg_data_out(28)
    );
\axi_rdata[28]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(28),
      I1 => slv_reg2(28),
      I2 => sel0(1),
      I3 => sel0(0),
      I4 => slv_reg1(28),
      O => \axi_rdata[28]_i_2_n_0\
    );
\axi_rdata[28]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(28),
      I1 => slv_reg6(28),
      I2 => sel0(1),
      I3 => slv_reg5(28),
      I4 => sel0(0),
      I5 => slv_reg4(28),
      O => \axi_rdata[28]_i_3_n_0\
    );
\axi_rdata[29]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FCBBFC88"
    )
        port map (
      I0 => sel0(1),
      I1 => sel0(3),
      I2 => \axi_rdata[29]_i_2_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[29]_i_3_n_0\,
      O => reg_data_out(29)
    );
\axi_rdata[29]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(29),
      I1 => slv_reg6(29),
      I2 => sel0(1),
      I3 => slv_reg5(29),
      I4 => sel0(0),
      I5 => slv_reg4(29),
      O => \axi_rdata[29]_i_2_n_0\
    );
\axi_rdata[29]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => slv_reg3(29),
      I1 => slv_reg2(29),
      I2 => sel0(1),
      I3 => slv_reg1(29),
      I4 => sel0(0),
      O => \axi_rdata[29]_i_3_n_0\
    );
\axi_rdata[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BAAAFFFFBAAA0000"
    )
        port map (
      I0 => sel0(2),
      I1 => sel0(1),
      I2 => sel0(0),
      I3 => \axi_rdata_reg[23]_0\(2),
      I4 => sel0(3),
      I5 => \axi_rdata_reg[2]_i_2_n_0\,
      O => reg_data_out(2)
    );
\axi_rdata[2]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => slv_reg3(2),
      I1 => slv_reg2(2),
      I2 => sel0(1),
      I3 => \^slv_reg1_reg[4]_0\(2),
      I4 => sel0(0),
      O => \axi_rdata[2]_i_3_n_0\
    );
\axi_rdata[2]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(2),
      I1 => slv_reg6(2),
      I2 => sel0(1),
      I3 => slv_reg5(2),
      I4 => sel0(0),
      I5 => \^slv_reg4_reg[5]_0\(2),
      O => \axi_rdata[2]_i_4_n_0\
    );
\axi_rdata[30]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00E2"
    )
        port map (
      I0 => \axi_rdata[30]_i_2_n_0\,
      I1 => sel0(2),
      I2 => \axi_rdata[30]_i_3_n_0\,
      I3 => sel0(3),
      O => reg_data_out(30)
    );
\axi_rdata[30]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(30),
      I1 => slv_reg2(30),
      I2 => sel0(1),
      I3 => sel0(0),
      I4 => slv_reg1(30),
      O => \axi_rdata[30]_i_2_n_0\
    );
\axi_rdata[30]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(30),
      I1 => slv_reg6(30),
      I2 => sel0(1),
      I3 => slv_reg5(30),
      I4 => sel0(0),
      I5 => slv_reg4(30),
      O => \axi_rdata[30]_i_3_n_0\
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FCBBFC88"
    )
        port map (
      I0 => sel0(1),
      I1 => sel0(3),
      I2 => \axi_rdata[31]_i_2_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[31]_i_3_n_0\,
      O => reg_data_out(31)
    );
\axi_rdata[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(31),
      I1 => slv_reg6(31),
      I2 => sel0(1),
      I3 => slv_reg5(31),
      I4 => sel0(0),
      I5 => slv_reg4(31),
      O => \axi_rdata[31]_i_2_n_0\
    );
\axi_rdata[31]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => slv_reg3(31),
      I1 => slv_reg2(31),
      I2 => sel0(1),
      I3 => slv_reg1(31),
      I4 => sel0(0),
      O => \axi_rdata[31]_i_3_n_0\
    );
\axi_rdata[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040FFFF00400000"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_rdata_reg[23]_0\(3),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => sel0(3),
      I5 => \axi_rdata_reg[3]_i_2_n_0\,
      O => reg_data_out(3)
    );
\axi_rdata[3]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => slv_reg3(3),
      I1 => slv_reg2(3),
      I2 => sel0(1),
      I3 => \^slv_reg1_reg[4]_0\(3),
      I4 => sel0(0),
      O => \axi_rdata[3]_i_3_n_0\
    );
\axi_rdata[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(3),
      I1 => slv_reg6(3),
      I2 => sel0(1),
      I3 => slv_reg5(3),
      I4 => sel0(0),
      I5 => \^slv_reg4_reg[5]_0\(3),
      O => \axi_rdata[3]_i_4_n_0\
    );
\axi_rdata[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040FFFF00400000"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_rdata_reg[23]_0\(4),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => sel0(3),
      I5 => \axi_rdata_reg[4]_i_2_n_0\,
      O => reg_data_out(4)
    );
\axi_rdata[4]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0C0C0"
    )
        port map (
      I0 => slv_reg3(4),
      I1 => slv_reg2(4),
      I2 => sel0(1),
      I3 => \^slv_reg1_reg[4]_0\(4),
      I4 => sel0(0),
      O => \axi_rdata[4]_i_3_n_0\
    );
\axi_rdata[4]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(4),
      I1 => slv_reg6(4),
      I2 => sel0(1),
      I3 => slv_reg5(4),
      I4 => sel0(0),
      I5 => \^slv_reg4_reg[5]_0\(4),
      O => \axi_rdata[4]_i_4_n_0\
    );
\axi_rdata[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040FFFF00400000"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_rdata_reg[23]_0\(5),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => sel0(3),
      I5 => \axi_rdata_reg[5]_i_2_n_0\,
      O => reg_data_out(5)
    );
\axi_rdata[5]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => slv_reg3(5),
      I1 => slv_reg2(5),
      I2 => sel0(1),
      I3 => slv_reg1(5),
      I4 => sel0(0),
      O => \axi_rdata[5]_i_3_n_0\
    );
\axi_rdata[5]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(5),
      I1 => slv_reg6(5),
      I2 => sel0(1),
      I3 => slv_reg5(5),
      I4 => sel0(0),
      I5 => \^slv_reg4_reg[5]_0\(5),
      O => \axi_rdata[5]_i_4_n_0\
    );
\axi_rdata[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040FFFF00400000"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_rdata_reg[23]_0\(6),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => sel0(3),
      I5 => \axi_rdata_reg[6]_i_2_n_0\,
      O => reg_data_out(6)
    );
\axi_rdata[6]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => slv_reg3(6),
      I1 => slv_reg2(6),
      I2 => sel0(1),
      I3 => slv_reg1(6),
      I4 => sel0(0),
      O => \axi_rdata[6]_i_3_n_0\
    );
\axi_rdata[6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(6),
      I1 => slv_reg6(6),
      I2 => sel0(1),
      I3 => slv_reg5(6),
      I4 => sel0(0),
      I5 => slv_reg4(6),
      O => \axi_rdata[6]_i_4_n_0\
    );
\axi_rdata[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040FFFF00400000"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_rdata_reg[23]_0\(7),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => sel0(3),
      I5 => \axi_rdata_reg[7]_i_2_n_0\,
      O => reg_data_out(7)
    );
\axi_rdata[7]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => slv_reg3(7),
      I1 => slv_reg2(7),
      I2 => sel0(1),
      I3 => slv_reg1(7),
      I4 => sel0(0),
      O => \axi_rdata[7]_i_3_n_0\
    );
\axi_rdata[7]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(7),
      I1 => slv_reg6(7),
      I2 => sel0(1),
      I3 => slv_reg5(7),
      I4 => sel0(0),
      I5 => slv_reg4(7),
      O => \axi_rdata[7]_i_4_n_0\
    );
\axi_rdata[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040FFFF00400000"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_rdata_reg[23]_0\(8),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => sel0(3),
      I5 => \axi_rdata_reg[8]_i_2_n_0\,
      O => reg_data_out(8)
    );
\axi_rdata[8]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(8),
      I1 => slv_reg2(8),
      I2 => sel0(1),
      I3 => sel0(0),
      I4 => slv_reg1(8),
      O => \axi_rdata[8]_i_3_n_0\
    );
\axi_rdata[8]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(8),
      I1 => slv_reg6(8),
      I2 => sel0(1),
      I3 => slv_reg5(8),
      I4 => sel0(0),
      I5 => slv_reg4(8),
      O => \axi_rdata[8]_i_4_n_0\
    );
\axi_rdata[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040FFFF00400000"
    )
        port map (
      I0 => sel0(2),
      I1 => \axi_rdata_reg[23]_0\(9),
      I2 => sel0(0),
      I3 => sel0(1),
      I4 => sel0(3),
      I5 => \axi_rdata_reg[9]_i_2_n_0\,
      O => reg_data_out(9)
    );
\axi_rdata[9]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => slv_reg3(9),
      I1 => slv_reg2(9),
      I2 => sel0(1),
      I3 => slv_reg1(9),
      I4 => sel0(0),
      O => \axi_rdata[9]_i_3_n_0\
    );
\axi_rdata[9]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(9),
      I1 => slv_reg6(9),
      I2 => sel0(1),
      I3 => slv_reg5(9),
      I4 => sel0(0),
      I5 => slv_reg4(9),
      O => \axi_rdata[9]_i_4_n_0\
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(0),
      Q => s00_axi_rdata(0),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(10),
      Q => s00_axi_rdata(10),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[10]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[10]_i_3_n_0\,
      I1 => \axi_rdata[10]_i_4_n_0\,
      O => \axi_rdata_reg[10]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(11),
      Q => s00_axi_rdata(11),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[11]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[11]_i_3_n_0\,
      I1 => \axi_rdata[11]_i_4_n_0\,
      O => \axi_rdata_reg[11]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(12),
      Q => s00_axi_rdata(12),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[12]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[12]_i_3_n_0\,
      I1 => \axi_rdata[12]_i_4_n_0\,
      O => \axi_rdata_reg[12]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(13),
      Q => s00_axi_rdata(13),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[13]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[13]_i_3_n_0\,
      I1 => \axi_rdata[13]_i_4_n_0\,
      O => \axi_rdata_reg[13]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(14),
      Q => s00_axi_rdata(14),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[14]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[14]_i_3_n_0\,
      I1 => \axi_rdata[14]_i_4_n_0\,
      O => \axi_rdata_reg[14]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(15),
      Q => s00_axi_rdata(15),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[15]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[15]_i_3_n_0\,
      I1 => \axi_rdata[15]_i_4_n_0\,
      O => \axi_rdata_reg[15]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(16),
      Q => s00_axi_rdata(16),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[16]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[16]_i_3_n_0\,
      I1 => \axi_rdata[16]_i_4_n_0\,
      O => \axi_rdata_reg[16]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(17),
      Q => s00_axi_rdata(17),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[17]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[17]_i_3_n_0\,
      I1 => \axi_rdata[17]_i_4_n_0\,
      O => \axi_rdata_reg[17]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(18),
      Q => s00_axi_rdata(18),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[18]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[18]_i_3_n_0\,
      I1 => \axi_rdata[18]_i_4_n_0\,
      O => \axi_rdata_reg[18]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(19),
      Q => s00_axi_rdata(19),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[19]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[19]_i_3_n_0\,
      I1 => \axi_rdata[19]_i_4_n_0\,
      O => \axi_rdata_reg[19]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(1),
      Q => s00_axi_rdata(1),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[1]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[1]_i_3_n_0\,
      I1 => \axi_rdata[1]_i_4_n_0\,
      O => \axi_rdata_reg[1]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(20),
      Q => s00_axi_rdata(20),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[20]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[20]_i_3_n_0\,
      I1 => \axi_rdata[20]_i_4_n_0\,
      O => \axi_rdata_reg[20]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(21),
      Q => s00_axi_rdata(21),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[21]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[21]_i_3_n_0\,
      I1 => \axi_rdata[21]_i_4_n_0\,
      O => \axi_rdata_reg[21]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(22),
      Q => s00_axi_rdata(22),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[22]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[22]_i_3_n_0\,
      I1 => \axi_rdata[22]_i_4_n_0\,
      O => \axi_rdata_reg[22]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(23),
      Q => s00_axi_rdata(23),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[23]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[23]_i_3_n_0\,
      I1 => \axi_rdata[23]_i_4_n_0\,
      O => \axi_rdata_reg[23]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(24),
      Q => s00_axi_rdata(24),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(25),
      Q => s00_axi_rdata(25),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(26),
      Q => s00_axi_rdata(26),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(27),
      Q => s00_axi_rdata(27),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(28),
      Q => s00_axi_rdata(28),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(29),
      Q => s00_axi_rdata(29),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(2),
      Q => s00_axi_rdata(2),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[2]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[2]_i_3_n_0\,
      I1 => \axi_rdata[2]_i_4_n_0\,
      O => \axi_rdata_reg[2]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(30),
      Q => s00_axi_rdata(30),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(31),
      Q => s00_axi_rdata(31),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(3),
      Q => s00_axi_rdata(3),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[3]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[3]_i_3_n_0\,
      I1 => \axi_rdata[3]_i_4_n_0\,
      O => \axi_rdata_reg[3]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(4),
      Q => s00_axi_rdata(4),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[4]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[4]_i_3_n_0\,
      I1 => \axi_rdata[4]_i_4_n_0\,
      O => \axi_rdata_reg[4]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(5),
      Q => s00_axi_rdata(5),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[5]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[5]_i_3_n_0\,
      I1 => \axi_rdata[5]_i_4_n_0\,
      O => \axi_rdata_reg[5]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(6),
      Q => s00_axi_rdata(6),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[6]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[6]_i_3_n_0\,
      I1 => \axi_rdata[6]_i_4_n_0\,
      O => \axi_rdata_reg[6]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(7),
      Q => s00_axi_rdata(7),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[7]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[7]_i_3_n_0\,
      I1 => \axi_rdata[7]_i_4_n_0\,
      O => \axi_rdata_reg[7]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(8),
      Q => s00_axi_rdata(8),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[8]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[8]_i_3_n_0\,
      I1 => \axi_rdata[8]_i_4_n_0\,
      O => \axi_rdata_reg[8]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(9),
      Q => s00_axi_rdata(9),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[9]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[9]_i_3_n_0\,
      I1 => \axi_rdata[9]_i_4_n_0\,
      O => \axi_rdata_reg[9]_i_2_n_0\,
      S => sel0(2)
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"08F8"
    )
        port map (
      I0 => \^s00_axi_arready\,
      I1 => s00_axi_arvalid,
      I2 => \^s00_axi_rvalid\,
      I3 => s00_axi_rready,
      O => axi_rvalid_i_1_n_0
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_rvalid_i_1_n_0,
      Q => \^s00_axi_rvalid\,
      R => axi_awready_i_1_n_0
    );
axi_wready_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => s00_axi_wvalid,
      I2 => \^s00_axi_wready\,
      I3 => aw_en_reg_n_0,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_wready0,
      Q => \^s00_axi_wready\,
      R => axi_awready_i_1_n_0
    );
pll_mosi_reg_i_10: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FACF0ACFFAC00AC0"
    )
        port map (
      I0 => slv_reg3(13),
      I1 => slv_reg3(12),
      I2 => bits_send_reg(1),
      I3 => bits_send_reg(0),
      I4 => slv_reg3(15),
      I5 => slv_reg3(14),
      O => \slv_reg3_reg[13]_0\
    );
pll_mosi_reg_i_11: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FACF0ACFFAC00AC0"
    )
        port map (
      I0 => slv_reg3(1),
      I1 => slv_reg3(0),
      I2 => bits_send_reg(1),
      I3 => bits_send_reg(0),
      I4 => slv_reg3(3),
      I5 => slv_reg3(2),
      O => \slv_reg3_reg[1]_0\
    );
pll_mosi_reg_i_12: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FACF0ACFFAC00AC0"
    )
        port map (
      I0 => slv_reg3(5),
      I1 => slv_reg3(4),
      I2 => bits_send_reg(1),
      I3 => bits_send_reg(0),
      I4 => slv_reg3(7),
      I5 => slv_reg3(6),
      O => \slv_reg3_reg[5]_0\
    );
pll_mosi_reg_i_13: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FACF0ACFFAC00AC0"
    )
        port map (
      I0 => slv_reg3(21),
      I1 => slv_reg3(20),
      I2 => bits_send_reg(1),
      I3 => bits_send_reg(0),
      I4 => \^q\(0),
      I5 => slv_reg3(22),
      O => \slv_reg3_reg[21]_0\
    );
pll_mosi_reg_i_14: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FACF0ACFFAC00AC0"
    )
        port map (
      I0 => slv_reg3(17),
      I1 => slv_reg3(16),
      I2 => bits_send_reg(1),
      I3 => bits_send_reg(0),
      I4 => slv_reg3(19),
      I5 => slv_reg3(18),
      O => \slv_reg3_reg[17]_0\
    );
pll_mosi_reg_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg2(1),
      I1 => slv_reg2(5),
      I2 => D(0),
      I3 => slv_reg2(3),
      I4 => D(1),
      I5 => slv_reg2(7),
      O => \slv_reg2_reg[1]_0\
    );
pll_mosi_reg_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"505F3030505F3F3F"
    )
        port map (
      I0 => slv_reg2(0),
      I1 => slv_reg2(4),
      I2 => D(0),
      I3 => slv_reg2(2),
      I4 => D(1),
      I5 => slv_reg2(6),
      O => \slv_reg2_reg[0]_0\
    );
pll_mosi_reg_i_9: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FACF0ACFFAC00AC0"
    )
        port map (
      I0 => slv_reg3(9),
      I1 => slv_reg3(8),
      I2 => bits_send_reg(1),
      I3 => bits_send_reg(0),
      I4 => slv_reg3(11),
      I5 => slv_reg3(10),
      O => \slv_reg3_reg[9]_0\
    );
\slv_reg0[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \slv_reg0[1]_i_2_n_0\,
      I2 => \^slv_reg0\(0),
      O => \slv_reg0[0]_i_1_n_0\
    );
\slv_reg0[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(1),
      I1 => \slv_reg0[1]_i_2_n_0\,
      I2 => \^slv_reg0\(1),
      O => \slv_reg0[1]_i_1_n_0\
    );
\slv_reg0[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(0),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => s00_axi_wstrb(0),
      O => \slv_reg0[1]_i_2_n_0\
    );
\slv_reg0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \slv_reg0[0]_i_1_n_0\,
      Q => \^slv_reg0\(0),
      R => axi_awready_i_1_n_0
    );
\slv_reg0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \slv_reg0[1]_i_1_n_0\,
      Q => \^slv_reg0\(1),
      R => axi_awready_i_1_n_0
    );
\slv_reg1[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(1),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => p_0_in(0),
      O => p_1_in(15)
    );
\slv_reg1[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(2),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => p_0_in(0),
      O => p_1_in(23)
    );
\slv_reg1[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(3),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => p_0_in(0),
      O => p_1_in(31)
    );
\slv_reg1[31]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \^s00_axi_wready\,
      I1 => \^s00_axi_awready\,
      I2 => s00_axi_awvalid,
      I3 => s00_axi_wvalid,
      O => \slv_reg_wren__0\
    );
\slv_reg1[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(0),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => p_0_in(0),
      O => p_1_in(7)
    );
\slv_reg1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(0),
      Q => \^slv_reg1_reg[4]_0\(0),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(10),
      Q => slv_reg1(10),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(11),
      Q => slv_reg1(11),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(12),
      Q => slv_reg1(12),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(13),
      Q => slv_reg1(13),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(14),
      Q => slv_reg1(14),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(15),
      Q => slv_reg1(15),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(16),
      Q => slv_reg1(16),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(17),
      Q => slv_reg1(17),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(18),
      Q => slv_reg1(18),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(19),
      Q => slv_reg1(19),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(1),
      Q => \^slv_reg1_reg[4]_0\(1),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(20),
      Q => slv_reg1(20),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(21),
      Q => slv_reg1(21),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(22),
      Q => slv_reg1(22),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(23),
      Q => slv_reg1(23),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(24),
      Q => slv_reg1(24),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(25),
      Q => slv_reg1(25),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(26),
      Q => slv_reg1(26),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(27),
      Q => slv_reg1(27),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(28),
      Q => slv_reg1(28),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(29),
      Q => slv_reg1(29),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(2),
      Q => \^slv_reg1_reg[4]_0\(2),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(30),
      Q => slv_reg1(30),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(31),
      Q => slv_reg1(31),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(3),
      Q => \^slv_reg1_reg[4]_0\(3),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(4),
      Q => \^slv_reg1_reg[4]_0\(4),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(5),
      Q => slv_reg1(5),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(6),
      Q => slv_reg1(6),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(7),
      Q => slv_reg1(7),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(8),
      Q => slv_reg1(8),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(9),
      Q => slv_reg1(9),
      R => axi_awready_i_1_n_0
    );
\slv_reg2[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(1),
      I2 => p_0_in(3),
      I3 => p_0_in(0),
      I4 => p_0_in(2),
      I5 => p_0_in(1),
      O => \slv_reg2[15]_i_1_n_0\
    );
\slv_reg2[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(2),
      I2 => p_0_in(3),
      I3 => p_0_in(0),
      I4 => p_0_in(2),
      I5 => p_0_in(1),
      O => \slv_reg2[23]_i_1_n_0\
    );
\slv_reg2[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(3),
      I2 => p_0_in(3),
      I3 => p_0_in(0),
      I4 => p_0_in(2),
      I5 => p_0_in(1),
      O => \slv_reg2[31]_i_1_n_0\
    );
\slv_reg2[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(0),
      I2 => p_0_in(3),
      I3 => p_0_in(0),
      I4 => p_0_in(2),
      I5 => p_0_in(1),
      O => \slv_reg2[7]_i_1_n_0\
    );
\slv_reg2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg2(0),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg2(10),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg2(11),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg2(12),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg2(13),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg2(14),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg2(15),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg2(16),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg2(17),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg2(18),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg2(19),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg2(1),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg2(20),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg2(21),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg2(22),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg2(23),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg2(24),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg2(25),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg2(26),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg2(27),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg2(28),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg2(29),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg2(2),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg2(30),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg2(31),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg2(3),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg2(4),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg2(5),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg2(6),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg2(7),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg2(8),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg2(9),
      R => axi_awready_i_1_n_0
    );
\slv_reg3[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(1),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => p_0_in(3),
      O => \slv_reg3[15]_i_1_n_0\
    );
\slv_reg3[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(2),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => p_0_in(3),
      O => \slv_reg3[23]_i_1_n_0\
    );
\slv_reg3[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(3),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => p_0_in(3),
      O => \slv_reg3[31]_i_1_n_0\
    );
\slv_reg3[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(0),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => p_0_in(3),
      O => \slv_reg3[7]_i_1_n_0\
    );
\slv_reg3_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg3(0),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg3(10),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg3(11),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg3(12),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg3(13),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg3(14),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg3(15),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg3(16),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg3(17),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg3(18),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg3(19),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg3(1),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg3(20),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg3(21),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg3(22),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \^q\(0),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg3(24),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg3(25),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg3(26),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg3(27),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg3(28),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg3(29),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg3(2),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg3(30),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg3(31),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg3(3),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg3(4),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg3(5),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg3(6),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg3(7),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg3(8),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg3(9),
      R => axi_awready_i_1_n_0
    );
\slv_reg4[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(1),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(2),
      O => \slv_reg4[15]_i_1_n_0\
    );
\slv_reg4[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(2),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(2),
      O => \slv_reg4[23]_i_1_n_0\
    );
\slv_reg4[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(3),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(2),
      O => \slv_reg4[31]_i_1_n_0\
    );
\slv_reg4[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(0),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(2),
      O => \slv_reg4[7]_i_1_n_0\
    );
\slv_reg4_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => \^slv_reg4_reg[5]_0\(0),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg4(10),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg4(11),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg4(12),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg4(13),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg4(14),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg4(15),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg4(16),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg4(17),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg4(18),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg4(19),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => \^slv_reg4_reg[5]_0\(1),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg4(20),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg4(21),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg4(22),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg4(23),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg4(24),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg4(25),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg4(26),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg4(27),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg4(28),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg4(29),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => \^slv_reg4_reg[5]_0\(2),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg4(30),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg4(31),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => \^slv_reg4_reg[5]_0\(3),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => \^slv_reg4_reg[5]_0\(4),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => \^slv_reg4_reg[5]_0\(5),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg4(6),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg4(7),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg4(8),
      R => axi_awready_i_1_n_0
    );
\slv_reg4_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg4(9),
      R => axi_awready_i_1_n_0
    );
\slv_reg5[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg5[15]_i_1_n_0\
    );
\slv_reg5[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(2),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg5[23]_i_1_n_0\
    );
\slv_reg5[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(3),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg5[31]_i_1_n_0\
    );
\slv_reg5[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(0),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg5[7]_i_1_n_0\
    );
\slv_reg5_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg5(0),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg5(10),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg5(11),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg5(12),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg5(13),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg5(14),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg5(15),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg5(16),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg5(17),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg5(18),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg5(19),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg5(1),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg5(20),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg5(21),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg5(22),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg5(23),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg5(24),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg5(25),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg5(26),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg5(27),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg5(28),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg5(29),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg5(2),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg5(30),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg5(31),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg5(3),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg5(4),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg5(5),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg5(6),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg5(7),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg5(8),
      R => axi_awready_i_1_n_0
    );
\slv_reg5_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg5(9),
      R => axi_awready_i_1_n_0
    );
\slv_reg6[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(1),
      I2 => p_0_in(2),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(3),
      O => \slv_reg6[15]_i_1_n_0\
    );
\slv_reg6[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(2),
      I2 => p_0_in(2),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(3),
      O => \slv_reg6[23]_i_1_n_0\
    );
\slv_reg6[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(3),
      I2 => p_0_in(2),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(3),
      O => \slv_reg6[31]_i_1_n_0\
    );
\slv_reg6[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(0),
      I2 => p_0_in(2),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(3),
      O => \slv_reg6[7]_i_1_n_0\
    );
\slv_reg6_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg6(0),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg6(10),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg6(11),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg6(12),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg6(13),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg6(14),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg6(15),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg6(16),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg6(17),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg6(18),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg6(19),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg6(1),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg6(20),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg6(21),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg6(22),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg6(23),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg6(24),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg6(25),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg6(26),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg6(27),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg6(28),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg6(29),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg6(2),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg6(30),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg6(31),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg6(3),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg6(4),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg6(5),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg6(6),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg6(7),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg6(8),
      R => axi_awready_i_1_n_0
    );
\slv_reg6_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg6(9),
      R => axi_awready_i_1_n_0
    );
\slv_reg7[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(2),
      I2 => s00_axi_wstrb(1),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg7[15]_i_1_n_0\
    );
\slv_reg7[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(2),
      I2 => s00_axi_wstrb(2),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg7[23]_i_1_n_0\
    );
\slv_reg7[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(2),
      I2 => s00_axi_wstrb(3),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg7[31]_i_1_n_0\
    );
\slv_reg7[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(2),
      I2 => s00_axi_wstrb(0),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg7[7]_i_1_n_0\
    );
\slv_reg7_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg7(0),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg7(10),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg7(11),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg7(12),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg7(13),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg7(14),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg7(15),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg7(16),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg7(17),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg7(18),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg7(19),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg7(1),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg7(20),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg7(21),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg7(22),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg7(23),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg7(24),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg7(25),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg7(26),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg7(27),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg7(28),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg7(29),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg7(2),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg7(30),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg7(31),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg7(3),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg7(4),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg7(5),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg7(6),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg7(7),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg7(8),
      R => axi_awready_i_1_n_0
    );
\slv_reg7_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg7(9),
      R => axi_awready_i_1_n_0
    );
slv_reg_rden: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s00_axi_rvalid\,
      I2 => \^s00_axi_arready\,
      O => \slv_reg_rden__0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz is
  port (
    clk_out1 : out STD_LOGIC;
    clk_out2 : out STD_LOGIC;
    clk_out3 : out STD_LOGIC;
    clk_in1 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz is
  signal clk_in1_clk_wiz_0 : STD_LOGIC;
  signal clk_out1_clk_wiz_0 : STD_LOGIC;
  signal clk_out2_clk_wiz_0 : STD_LOGIC;
  signal clk_out3_clk_wiz_0 : STD_LOGIC;
  signal clkfbout_buf_clk_wiz_0 : STD_LOGIC;
  signal clkfbout_clk_wiz_0 : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DRDY_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_LOCKED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_PSDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of clkf_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkin1_ibufg : label is "PRIMITIVE";
  attribute CAPACITANCE : string;
  attribute CAPACITANCE of clkin1_ibufg : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE : string;
  attribute IBUF_DELAY_VALUE of clkin1_ibufg : label is "0";
  attribute IFD_DELAY_VALUE : string;
  attribute IFD_DELAY_VALUE of clkin1_ibufg : label is "AUTO";
  attribute BOX_TYPE of clkout1_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout2_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of clkout3_buf : label is "PRIMITIVE";
  attribute BOX_TYPE of mmcm_adv_inst : label is "PRIMITIVE";
begin
clkf_buf: unisim.vcomponents.BUFG
     port map (
      I => clkfbout_clk_wiz_0,
      O => clkfbout_buf_clk_wiz_0
    );
clkin1_ibufg: unisim.vcomponents.IBUF
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => clk_in1,
      O => clk_in1_clk_wiz_0
    );
clkout1_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out1_clk_wiz_0,
      O => clk_out1
    );
clkout2_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out2_clk_wiz_0,
      O => clk_out2
    );
clkout3_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out3_clk_wiz_0,
      O => clk_out3
    );
mmcm_adv_inst: unisim.vcomponents.MMCME2_ADV
    generic map(
      BANDWIDTH => "OPTIMIZED",
      CLKFBOUT_MULT_F => 10.000000,
      CLKFBOUT_PHASE => 0.000000,
      CLKFBOUT_USE_FINE_PS => false,
      CLKIN1_PERIOD => 10.000000,
      CLKIN2_PERIOD => 0.000000,
      CLKOUT0_DIVIDE_F => 10.000000,
      CLKOUT0_DUTY_CYCLE => 0.500000,
      CLKOUT0_PHASE => 0.000000,
      CLKOUT0_USE_FINE_PS => false,
      CLKOUT1_DIVIDE => 125,
      CLKOUT1_DUTY_CYCLE => 0.500000,
      CLKOUT1_PHASE => 0.000000,
      CLKOUT1_USE_FINE_PS => false,
      CLKOUT2_DIVIDE => 100,
      CLKOUT2_DUTY_CYCLE => 0.500000,
      CLKOUT2_PHASE => 0.000000,
      CLKOUT2_USE_FINE_PS => false,
      CLKOUT3_DIVIDE => 1,
      CLKOUT3_DUTY_CYCLE => 0.500000,
      CLKOUT3_PHASE => 0.000000,
      CLKOUT3_USE_FINE_PS => false,
      CLKOUT4_CASCADE => false,
      CLKOUT4_DIVIDE => 1,
      CLKOUT4_DUTY_CYCLE => 0.500000,
      CLKOUT4_PHASE => 0.000000,
      CLKOUT4_USE_FINE_PS => false,
      CLKOUT5_DIVIDE => 1,
      CLKOUT5_DUTY_CYCLE => 0.500000,
      CLKOUT5_PHASE => 0.000000,
      CLKOUT5_USE_FINE_PS => false,
      CLKOUT6_DIVIDE => 1,
      CLKOUT6_DUTY_CYCLE => 0.500000,
      CLKOUT6_PHASE => 0.000000,
      CLKOUT6_USE_FINE_PS => false,
      COMPENSATION => "ZHOLD",
      DIVCLK_DIVIDE => 1,
      IS_CLKINSEL_INVERTED => '0',
      IS_PSEN_INVERTED => '0',
      IS_PSINCDEC_INVERTED => '0',
      IS_PWRDWN_INVERTED => '0',
      IS_RST_INVERTED => '0',
      REF_JITTER1 => 0.010000,
      REF_JITTER2 => 0.010000,
      SS_EN => "FALSE",
      SS_MODE => "CENTER_HIGH",
      SS_MOD_PERIOD => 10000,
      STARTUP_WAIT => false
    )
        port map (
      CLKFBIN => clkfbout_buf_clk_wiz_0,
      CLKFBOUT => clkfbout_clk_wiz_0,
      CLKFBOUTB => NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED,
      CLKFBSTOPPED => NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED,
      CLKIN1 => clk_in1_clk_wiz_0,
      CLKIN2 => '0',
      CLKINSEL => '1',
      CLKINSTOPPED => NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED,
      CLKOUT0 => clk_out1_clk_wiz_0,
      CLKOUT0B => NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED,
      CLKOUT1 => clk_out2_clk_wiz_0,
      CLKOUT1B => NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED,
      CLKOUT2 => clk_out3_clk_wiz_0,
      CLKOUT2B => NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED,
      CLKOUT3 => NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED,
      CLKOUT3B => NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED,
      CLKOUT4 => NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED,
      CLKOUT5 => NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED,
      CLKOUT6 => NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED,
      DADDR(6 downto 0) => B"0000000",
      DCLK => '0',
      DEN => '0',
      DI(15 downto 0) => B"0000000000000000",
      DO(15 downto 0) => NLW_mmcm_adv_inst_DO_UNCONNECTED(15 downto 0),
      DRDY => NLW_mmcm_adv_inst_DRDY_UNCONNECTED,
      DWE => '0',
      LOCKED => NLW_mmcm_adv_inst_LOCKED_UNCONNECTED,
      PSCLK => '0',
      PSDONE => NLW_mmcm_adv_inst_PSDONE_UNCONNECTED,
      PSEN => '0',
      PSINCDEC => '0',
      PWRDWN => '0',
      RST => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll_receive is
  port (
    pll_mosi_read : out STD_LOGIC;
    ip2mb_reg0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : out STD_LOGIC_VECTOR ( 23 downto 0 );
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    pll_sck : out STD_LOGIC;
    pll_sen : out STD_LOGIC;
    START_receive : in STD_LOGIC;
    clk_out3 : in STD_LOGIC;
    pll_ld_sdo : in STD_LOGIC;
    pll_mosi_reg_reg_0 : in STD_LOGIC;
    pll_mosi_reg_reg_1 : in STD_LOGIC;
    is_clk_running : in STD_LOGIC;
    pll_sen_write : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll_receive;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll_receive is
  signal \^d\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^q\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \bits_send[0]_i_1_n_0\ : STD_LOGIC;
  signal \bits_send[3]_i_1_n_0\ : STD_LOGIC;
  signal \bits_send[4]_i_2_n_0\ : STD_LOGIC;
  signal \bits_send[4]_i_3_n_0\ : STD_LOGIC;
  signal \bits_send_reg_n_0_[0]\ : STD_LOGIC;
  signal \bits_send_reg_n_0_[1]\ : STD_LOGIC;
  signal \bits_send_reg_n_0_[2]\ : STD_LOGIC;
  signal \bits_send_reg_n_0_[3]\ : STD_LOGIC;
  signal \bits_send_reg_n_0_[4]\ : STD_LOGIC;
  signal data_ready_reg7_out : STD_LOGIC;
  signal data_ready_reg_i_1_n_0 : STD_LOGIC;
  signal \^ip2mb_reg0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal is_first_cycle_going : STD_LOGIC;
  signal is_first_cycle_going_i_1_n_0 : STD_LOGIC;
  signal is_second_cycle_going_i_1_n_0 : STD_LOGIC;
  signal is_second_cycle_going_reg_n_0 : STD_LOGIC;
  signal pll_cs_reg_i_1_n_0 : STD_LOGIC;
  signal \^pll_mosi_read\ : STD_LOGIC;
  signal pll_mosi_reg_i_1_n_0 : STD_LOGIC;
  signal pll_mosi_reg_i_2_n_0 : STD_LOGIC;
  signal pll_mosi_reg_i_3_n_0 : STD_LOGIC;
  signal pll_mosi_reg_i_4_n_0 : STD_LOGIC;
  signal pll_sen_read : STD_LOGIC;
  signal read_data_reg : STD_LOGIC;
  signal read_data_reg025_out : STD_LOGIC;
  signal \read_data_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[10]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[13]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[14]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[15]_i_2_n_0\ : STD_LOGIC;
  signal \read_data_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[16]_i_2_n_0\ : STD_LOGIC;
  signal \read_data_reg[17]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[17]_i_2_n_0\ : STD_LOGIC;
  signal \read_data_reg[18]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[18]_i_2_n_0\ : STD_LOGIC;
  signal \read_data_reg[19]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[19]_i_2_n_0\ : STD_LOGIC;
  signal \read_data_reg[1]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[20]_i_2_n_0\ : STD_LOGIC;
  signal \read_data_reg[21]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[22]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[22]_i_2_n_0\ : STD_LOGIC;
  signal \read_data_reg[23]_i_2_n_0\ : STD_LOGIC;
  signal \read_data_reg[23]_i_3_n_0\ : STD_LOGIC;
  signal \read_data_reg[2]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[5]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[6]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \read_data_reg[9]_i_1_n_0\ : STD_LOGIC;
  signal start_prev : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \bits_send[1]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \bits_send[2]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \bits_send[3]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \bits_send[4]_i_3\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of data_ready_reg_i_1 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of pll_mosi_reg_i_2 : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of pll_mosi_reg_i_4 : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of pll_mosi_reg_i_5 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \read_data_reg[15]_i_2\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \read_data_reg[16]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \read_data_reg[16]_i_2\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \read_data_reg[17]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \read_data_reg[17]_i_2\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \read_data_reg[18]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \read_data_reg[18]_i_2\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \read_data_reg[19]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \read_data_reg[19]_i_2\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \read_data_reg[1]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \read_data_reg[20]_i_2\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \read_data_reg[23]_i_2\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \read_data_reg[23]_i_3\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \read_data_reg[2]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \read_data_reg[3]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \read_data_reg[7]_i_1\ : label is "soft_lutpair11";
begin
  D(1 downto 0) <= \^d\(1 downto 0);
  Q(23 downto 0) <= \^q\(23 downto 0);
  ip2mb_reg0(0) <= \^ip2mb_reg0\(0);
  pll_mosi_read <= \^pll_mosi_read\;
\bits_send[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \bits_send_reg_n_0_[0]\,
      O => \bits_send[0]_i_1_n_0\
    );
\bits_send[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \bits_send_reg_n_0_[1]\,
      I1 => \bits_send_reg_n_0_[0]\,
      O => \^d\(0)
    );
\bits_send[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \bits_send_reg_n_0_[2]\,
      I1 => \bits_send_reg_n_0_[0]\,
      I2 => \bits_send_reg_n_0_[1]\,
      O => \^d\(1)
    );
\bits_send[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \bits_send_reg_n_0_[3]\,
      I1 => \bits_send_reg_n_0_[2]\,
      I2 => \bits_send_reg_n_0_[0]\,
      I3 => \bits_send_reg_n_0_[1]\,
      O => \bits_send[3]_i_1_n_0\
    );
\bits_send[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => START_receive,
      I1 => start_prev,
      O => read_data_reg025_out
    );
\bits_send[4]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => is_second_cycle_going_reg_n_0,
      I1 => is_first_cycle_going,
      O => \bits_send[4]_i_2_n_0\
    );
\bits_send[4]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \bits_send_reg_n_0_[4]\,
      I1 => \bits_send_reg_n_0_[2]\,
      I2 => \bits_send_reg_n_0_[0]\,
      I3 => \bits_send_reg_n_0_[1]\,
      I4 => \bits_send_reg_n_0_[3]\,
      O => \bits_send[4]_i_3_n_0\
    );
\bits_send_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => \bits_send[4]_i_2_n_0\,
      D => \bits_send[0]_i_1_n_0\,
      Q => \bits_send_reg_n_0_[0]\,
      R => read_data_reg025_out
    );
\bits_send_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => \bits_send[4]_i_2_n_0\,
      D => \^d\(0),
      Q => \bits_send_reg_n_0_[1]\,
      R => read_data_reg025_out
    );
\bits_send_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => \bits_send[4]_i_2_n_0\,
      D => \^d\(1),
      Q => \bits_send_reg_n_0_[2]\,
      R => read_data_reg025_out
    );
\bits_send_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => \bits_send[4]_i_2_n_0\,
      D => \bits_send[3]_i_1_n_0\,
      Q => \bits_send_reg_n_0_[3]\,
      R => read_data_reg025_out
    );
\bits_send_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => \bits_send[4]_i_2_n_0\,
      D => \bits_send[4]_i_3_n_0\,
      Q => \bits_send_reg_n_0_[4]\,
      R => read_data_reg025_out
    );
data_ready_reg_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AB00ABAB"
    )
        port map (
      I0 => \^ip2mb_reg0\(0),
      I1 => is_first_cycle_going,
      I2 => is_second_cycle_going_reg_n_0,
      I3 => start_prev,
      I4 => START_receive,
      O => data_ready_reg_i_1_n_0
    );
data_ready_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => '1',
      D => data_ready_reg_i_1_n_0,
      Q => \^ip2mb_reg0\(0),
      R => '0'
    );
is_first_cycle_going_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF00000000"
    )
        port map (
      I0 => \bits_send_reg_n_0_[3]\,
      I1 => \bits_send_reg_n_0_[4]\,
      I2 => \bits_send_reg_n_0_[1]\,
      I3 => \bits_send_reg_n_0_[0]\,
      I4 => \bits_send_reg_n_0_[2]\,
      I5 => is_first_cycle_going,
      O => is_first_cycle_going_i_1_n_0
    );
is_first_cycle_going_reg: unisim.vcomponents.FDSE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => '1',
      D => is_first_cycle_going_i_1_n_0,
      Q => is_first_cycle_going,
      S => read_data_reg025_out
    );
is_second_cycle_going_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAB8AAAAAAAAAAAA"
    )
        port map (
      I0 => is_second_cycle_going_reg_n_0,
      I1 => read_data_reg025_out,
      I2 => is_first_cycle_going,
      I3 => \read_data_reg[16]_i_2_n_0\,
      I4 => \bits_send_reg_n_0_[4]\,
      I5 => \bits_send_reg_n_0_[3]\,
      O => is_second_cycle_going_i_1_n_0
    );
is_second_cycle_going_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => '1',
      D => is_second_cycle_going_i_1_n_0,
      Q => is_second_cycle_going_reg_n_0,
      R => '0'
    );
pll_cs_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ECECFFECA8A800A8"
    )
        port map (
      I0 => pll_mosi_reg_i_4_n_0,
      I1 => is_first_cycle_going,
      I2 => is_second_cycle_going_reg_n_0,
      I3 => START_receive,
      I4 => start_prev,
      I5 => pll_sen_read,
      O => pll_cs_reg_i_1_n_0
    );
pll_cs_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => '1',
      D => pll_cs_reg_i_1_n_0,
      Q => pll_sen_read,
      R => '0'
    );
pll_mosi_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000022200020"
    )
        port map (
      I0 => pll_mosi_reg_i_2_n_0,
      I1 => read_data_reg025_out,
      I2 => pll_mosi_reg_i_3_n_0,
      I3 => pll_mosi_reg_i_4_n_0,
      I4 => \^pll_mosi_read\,
      I5 => data_ready_reg7_out,
      O => pll_mosi_reg_i_1_n_0
    );
pll_mosi_reg_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEEE0000"
    )
        port map (
      I0 => \bits_send_reg_n_0_[3]\,
      I1 => \bits_send_reg_n_0_[2]\,
      I2 => \bits_send_reg_n_0_[0]\,
      I3 => \bits_send_reg_n_0_[1]\,
      I4 => \bits_send_reg_n_0_[4]\,
      O => pll_mosi_reg_i_2_n_0
    );
pll_mosi_reg_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008B8B000000"
    )
        port map (
      I0 => pll_mosi_reg_reg_0,
      I1 => \bits_send_reg_n_0_[0]\,
      I2 => pll_mosi_reg_reg_1,
      I3 => \bits_send_reg_n_0_[4]\,
      I4 => \read_data_reg[16]_i_2_n_0\,
      I5 => \bits_send_reg_n_0_[3]\,
      O => pll_mosi_reg_i_3_n_0
    );
pll_mosi_reg_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \bits_send_reg_n_0_[3]\,
      I1 => \bits_send_reg_n_0_[4]\,
      I2 => \bits_send_reg_n_0_[1]\,
      I3 => \bits_send_reg_n_0_[0]\,
      I4 => \bits_send_reg_n_0_[2]\,
      O => pll_mosi_reg_i_4_n_0
    );
pll_mosi_reg_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => is_first_cycle_going,
      I1 => is_second_cycle_going_reg_n_0,
      O => data_ready_reg7_out
    );
pll_mosi_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => '1',
      D => pll_mosi_reg_i_1_n_0,
      Q => \^pll_mosi_read\,
      R => '0'
    );
pll_sck_INST_0: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AAA8"
    )
        port map (
      I0 => clk_out3,
      I1 => is_clk_running,
      I2 => is_first_cycle_going,
      I3 => is_second_cycle_going_reg_n_0,
      O => pll_sck
    );
pll_sen_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pll_sen_read,
      I1 => pll_sen_write,
      O => pll_sen
    );
\read_data_reg[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AABAAAAA"
    )
        port map (
      I0 => \^q\(0),
      I1 => \bits_send_reg_n_0_[3]\,
      I2 => pll_ld_sdo,
      I3 => \read_data_reg[16]_i_2_n_0\,
      I4 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[0]_i_1_n_0\
    );
\read_data_reg[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AEAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \^q\(10),
      I1 => \bits_send_reg_n_0_[0]\,
      I2 => \bits_send_reg_n_0_[1]\,
      I3 => pll_ld_sdo,
      I4 => \bits_send_reg_n_0_[2]\,
      I5 => \read_data_reg[15]_i_2_n_0\,
      O => \read_data_reg[10]_i_1_n_0\
    );
\read_data_reg[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AABAAAAAAAAAAAAA"
    )
        port map (
      I0 => \^q\(11),
      I1 => \bits_send_reg_n_0_[0]\,
      I2 => pll_ld_sdo,
      I3 => \bits_send_reg_n_0_[1]\,
      I4 => \bits_send_reg_n_0_[2]\,
      I5 => \read_data_reg[15]_i_2_n_0\,
      O => \read_data_reg[11]_i_1_n_0\
    );
\read_data_reg[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AEAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \^q\(12),
      I1 => pll_ld_sdo,
      I2 => \bits_send_reg_n_0_[2]\,
      I3 => \bits_send_reg_n_0_[0]\,
      I4 => \bits_send_reg_n_0_[1]\,
      I5 => \read_data_reg[15]_i_2_n_0\,
      O => \read_data_reg[12]_i_1_n_0\
    );
\read_data_reg[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAEAAAAAAAAAA"
    )
        port map (
      I0 => \^q\(13),
      I1 => \bits_send_reg_n_0_[1]\,
      I2 => \bits_send_reg_n_0_[0]\,
      I3 => pll_ld_sdo,
      I4 => \bits_send_reg_n_0_[2]\,
      I5 => \read_data_reg[15]_i_2_n_0\,
      O => \read_data_reg[13]_i_1_n_0\
    );
\read_data_reg[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAEAAAAAAAAAA"
    )
        port map (
      I0 => \^q\(14),
      I1 => \bits_send_reg_n_0_[0]\,
      I2 => \bits_send_reg_n_0_[1]\,
      I3 => pll_ld_sdo,
      I4 => \bits_send_reg_n_0_[2]\,
      I5 => \read_data_reg[15]_i_2_n_0\,
      O => \read_data_reg[14]_i_1_n_0\
    );
\read_data_reg[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAABAAAAAAAAAA"
    )
        port map (
      I0 => \^q\(15),
      I1 => \bits_send_reg_n_0_[2]\,
      I2 => \bits_send_reg_n_0_[0]\,
      I3 => pll_ld_sdo,
      I4 => \bits_send_reg_n_0_[1]\,
      I5 => \read_data_reg[15]_i_2_n_0\,
      O => \read_data_reg[15]_i_1_n_0\
    );
\read_data_reg[15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \bits_send_reg_n_0_[3]\,
      I1 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[15]_i_2_n_0\
    );
\read_data_reg[16]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAAABA"
    )
        port map (
      I0 => \^q\(16),
      I1 => \bits_send_reg_n_0_[3]\,
      I2 => pll_ld_sdo,
      I3 => \read_data_reg[16]_i_2_n_0\,
      I4 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[16]_i_1_n_0\
    );
\read_data_reg[16]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => \bits_send_reg_n_0_[2]\,
      I1 => \bits_send_reg_n_0_[0]\,
      I2 => \bits_send_reg_n_0_[1]\,
      O => \read_data_reg[16]_i_2_n_0\
    );
\read_data_reg[17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AB"
    )
        port map (
      I0 => \^q\(17),
      I1 => \read_data_reg[17]_i_2_n_0\,
      I2 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[17]_i_1_n_0\
    );
\read_data_reg[17]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFBFFFFF"
    )
        port map (
      I0 => \bits_send_reg_n_0_[3]\,
      I1 => \bits_send_reg_n_0_[2]\,
      I2 => pll_ld_sdo,
      I3 => \bits_send_reg_n_0_[0]\,
      I4 => \bits_send_reg_n_0_[1]\,
      O => \read_data_reg[17]_i_2_n_0\
    );
\read_data_reg[18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AB"
    )
        port map (
      I0 => \^q\(18),
      I1 => \read_data_reg[18]_i_2_n_0\,
      I2 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[18]_i_1_n_0\
    );
\read_data_reg[18]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFBFFFFF"
    )
        port map (
      I0 => \bits_send_reg_n_0_[3]\,
      I1 => \bits_send_reg_n_0_[2]\,
      I2 => pll_ld_sdo,
      I3 => \bits_send_reg_n_0_[1]\,
      I4 => \bits_send_reg_n_0_[0]\,
      O => \read_data_reg[18]_i_2_n_0\
    );
\read_data_reg[19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AB"
    )
        port map (
      I0 => \^q\(19),
      I1 => \read_data_reg[19]_i_2_n_0\,
      I2 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[19]_i_1_n_0\
    );
\read_data_reg[19]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFBFF"
    )
        port map (
      I0 => \bits_send_reg_n_0_[3]\,
      I1 => \bits_send_reg_n_0_[2]\,
      I2 => \bits_send_reg_n_0_[1]\,
      I3 => pll_ld_sdo,
      I4 => \bits_send_reg_n_0_[0]\,
      O => \read_data_reg[19]_i_2_n_0\
    );
\read_data_reg[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \^q\(1),
      I1 => \read_data_reg[17]_i_2_n_0\,
      I2 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[1]_i_1_n_0\
    );
\read_data_reg[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAABAAAAA"
    )
        port map (
      I0 => \^q\(20),
      I1 => \bits_send_reg_n_0_[3]\,
      I2 => \read_data_reg[20]_i_2_n_0\,
      I3 => \bits_send_reg_n_0_[2]\,
      I4 => pll_ld_sdo,
      I5 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[20]_i_1_n_0\
    );
\read_data_reg[20]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \bits_send_reg_n_0_[1]\,
      I1 => \bits_send_reg_n_0_[0]\,
      O => \read_data_reg[20]_i_2_n_0\
    );
\read_data_reg[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAABAAAA"
    )
        port map (
      I0 => \^q\(21),
      I1 => \bits_send_reg_n_0_[3]\,
      I2 => \read_data_reg[22]_i_2_n_0\,
      I3 => \bits_send_reg_n_0_[0]\,
      I4 => \bits_send_reg_n_0_[1]\,
      I5 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[21]_i_1_n_0\
    );
\read_data_reg[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAABAAAA"
    )
        port map (
      I0 => \^q\(22),
      I1 => \bits_send_reg_n_0_[3]\,
      I2 => \read_data_reg[22]_i_2_n_0\,
      I3 => \bits_send_reg_n_0_[1]\,
      I4 => \bits_send_reg_n_0_[0]\,
      I5 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[22]_i_1_n_0\
    );
\read_data_reg[22]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \bits_send_reg_n_0_[2]\,
      I1 => pll_ld_sdo,
      O => \read_data_reg[22]_i_2_n_0\
    );
\read_data_reg[23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"002A"
    )
        port map (
      I0 => is_second_cycle_going_reg_n_0,
      I1 => \bits_send_reg_n_0_[4]\,
      I2 => \bits_send_reg_n_0_[3]\,
      I3 => is_first_cycle_going,
      O => read_data_reg
    );
\read_data_reg[23]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AB"
    )
        port map (
      I0 => \^q\(23),
      I1 => \read_data_reg[23]_i_3_n_0\,
      I2 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[23]_i_2_n_0\
    );
\read_data_reg[23]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFEF"
    )
        port map (
      I0 => \bits_send_reg_n_0_[3]\,
      I1 => \bits_send_reg_n_0_[1]\,
      I2 => pll_ld_sdo,
      I3 => \bits_send_reg_n_0_[0]\,
      I4 => \bits_send_reg_n_0_[2]\,
      O => \read_data_reg[23]_i_3_n_0\
    );
\read_data_reg[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \^q\(2),
      I1 => \read_data_reg[18]_i_2_n_0\,
      I2 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[2]_i_1_n_0\
    );
\read_data_reg[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \^q\(3),
      I1 => \read_data_reg[19]_i_2_n_0\,
      I2 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[3]_i_1_n_0\
    );
\read_data_reg[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AABAAAAAAAAAAAAA"
    )
        port map (
      I0 => \^q\(4),
      I1 => \bits_send_reg_n_0_[3]\,
      I2 => \read_data_reg[20]_i_2_n_0\,
      I3 => \bits_send_reg_n_0_[2]\,
      I4 => pll_ld_sdo,
      I5 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[4]_i_1_n_0\
    );
\read_data_reg[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAABAAAAAAAAAAAA"
    )
        port map (
      I0 => \^q\(5),
      I1 => \bits_send_reg_n_0_[3]\,
      I2 => \read_data_reg[22]_i_2_n_0\,
      I3 => \bits_send_reg_n_0_[0]\,
      I4 => \bits_send_reg_n_0_[1]\,
      I5 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[5]_i_1_n_0\
    );
\read_data_reg[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAABAAAAAAAAAAAA"
    )
        port map (
      I0 => \^q\(6),
      I1 => \bits_send_reg_n_0_[3]\,
      I2 => \read_data_reg[22]_i_2_n_0\,
      I3 => \bits_send_reg_n_0_[1]\,
      I4 => \bits_send_reg_n_0_[0]\,
      I5 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[6]_i_1_n_0\
    );
\read_data_reg[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \^q\(7),
      I1 => \read_data_reg[23]_i_3_n_0\,
      I2 => \bits_send_reg_n_0_[4]\,
      O => \read_data_reg[7]_i_1_n_0\
    );
\read_data_reg[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EAAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \^q\(8),
      I1 => \bits_send_reg_n_0_[1]\,
      I2 => \bits_send_reg_n_0_[0]\,
      I3 => \bits_send_reg_n_0_[2]\,
      I4 => pll_ld_sdo,
      I5 => \read_data_reg[15]_i_2_n_0\,
      O => \read_data_reg[8]_i_1_n_0\
    );
\read_data_reg[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AEAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \^q\(9),
      I1 => \bits_send_reg_n_0_[1]\,
      I2 => \bits_send_reg_n_0_[0]\,
      I3 => pll_ld_sdo,
      I4 => \bits_send_reg_n_0_[2]\,
      I5 => \read_data_reg[15]_i_2_n_0\,
      O => \read_data_reg[9]_i_1_n_0\
    );
\read_data_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => read_data_reg,
      D => \read_data_reg[0]_i_1_n_0\,
      Q => \^q\(0),
      R => read_data_reg025_out
    );
\read_data_reg_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => read_data_reg,
      D => \read_data_reg[10]_i_1_n_0\,
      Q => \^q\(10),
      R => read_data_reg025_out
    );
\read_data_reg_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => read_data_reg,
      D => \read_data_reg[11]_i_1_n_0\,
      Q => \^q\(11),
      R => read_data_reg025_out
    );
\read_data_reg_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => read_data_reg,
      D => \read_data_reg[12]_i_1_n_0\,
      Q => \^q\(12),
      R => read_data_reg025_out
    );
\read_data_reg_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => read_data_reg,
      D => \read_data_reg[13]_i_1_n_0\,
      Q => \^q\(13),
      R => read_data_reg025_out
    );
\read_data_reg_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => read_data_reg,
      D => \read_data_reg[14]_i_1_n_0\,
      Q => \^q\(14),
      R => read_data_reg025_out
    );
\read_data_reg_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => read_data_reg,
      D => \read_data_reg[15]_i_1_n_0\,
      Q => \^q\(15),
      R => read_data_reg025_out
    );
\read_data_reg_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => read_data_reg,
      D => \read_data_reg[16]_i_1_n_0\,
      Q => \^q\(16),
      R => read_data_reg025_out
    );
\read_data_reg_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => read_data_reg,
      D => \read_data_reg[17]_i_1_n_0\,
      Q => \^q\(17),
      R => read_data_reg025_out
    );
\read_data_reg_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => read_data_reg,
      D => \read_data_reg[18]_i_1_n_0\,
      Q => \^q\(18),
      R => read_data_reg025_out
    );
\read_data_reg_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => read_data_reg,
      D => \read_data_reg[19]_i_1_n_0\,
      Q => \^q\(19),
      R => read_data_reg025_out
    );
\read_data_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => read_data_reg,
      D => \read_data_reg[1]_i_1_n_0\,
      Q => \^q\(1),
      R => read_data_reg025_out
    );
\read_data_reg_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => read_data_reg,
      D => \read_data_reg[20]_i_1_n_0\,
      Q => \^q\(20),
      R => read_data_reg025_out
    );
\read_data_reg_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => read_data_reg,
      D => \read_data_reg[21]_i_1_n_0\,
      Q => \^q\(21),
      R => read_data_reg025_out
    );
\read_data_reg_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => read_data_reg,
      D => \read_data_reg[22]_i_1_n_0\,
      Q => \^q\(22),
      R => read_data_reg025_out
    );
\read_data_reg_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => read_data_reg,
      D => \read_data_reg[23]_i_2_n_0\,
      Q => \^q\(23),
      R => read_data_reg025_out
    );
\read_data_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => read_data_reg,
      D => \read_data_reg[2]_i_1_n_0\,
      Q => \^q\(2),
      R => read_data_reg025_out
    );
\read_data_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => read_data_reg,
      D => \read_data_reg[3]_i_1_n_0\,
      Q => \^q\(3),
      R => read_data_reg025_out
    );
\read_data_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => read_data_reg,
      D => \read_data_reg[4]_i_1_n_0\,
      Q => \^q\(4),
      R => read_data_reg025_out
    );
\read_data_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => read_data_reg,
      D => \read_data_reg[5]_i_1_n_0\,
      Q => \^q\(5),
      R => read_data_reg025_out
    );
\read_data_reg_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => read_data_reg,
      D => \read_data_reg[6]_i_1_n_0\,
      Q => \^q\(6),
      R => read_data_reg025_out
    );
\read_data_reg_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => read_data_reg,
      D => \read_data_reg[7]_i_1_n_0\,
      Q => \^q\(7),
      R => read_data_reg025_out
    );
\read_data_reg_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => read_data_reg,
      D => \read_data_reg[8]_i_1_n_0\,
      Q => \^q\(8),
      R => read_data_reg025_out
    );
\read_data_reg_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => read_data_reg,
      D => \read_data_reg[9]_i_1_n_0\,
      Q => \^q\(9),
      R => read_data_reg025_out
    );
start_prev_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => '1',
      D => START_receive,
      Q => start_prev,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll_send is
  port (
    \bits_send_reg[1]_0\ : out STD_LOGIC;
    \bits_send_reg[0]_0\ : out STD_LOGIC;
    is_clk_running : out STD_LOGIC;
    pll_sen_write : out STD_LOGIC;
    pll_mosi : out STD_LOGIC;
    START_send : in STD_LOGIC;
    clk_out3 : in STD_LOGIC;
    \pll_mosi_reg_i_2__0_0\ : in STD_LOGIC;
    \pll_mosi_reg_i_2__0_1\ : in STD_LOGIC;
    \pll_mosi_reg_i_2__0_2\ : in STD_LOGIC;
    \pll_mosi_reg_i_2__0_3\ : in STD_LOGIC;
    \pll_mosi_reg_i_2__0_4\ : in STD_LOGIC;
    \pll_mosi_reg_i_2__0_5\ : in STD_LOGIC;
    pll_mosi_reg_i_8_0 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    pll_mosi_read : in STD_LOGIC;
    pll_mosi_reg_reg_0 : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll_send;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll_send is
  signal bits_send0 : STD_LOGIC;
  signal \bits_send[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \bits_send[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \bits_send[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \bits_send[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \bits_send[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \bits_send[4]_i_2__0_n_0\ : STD_LOGIC;
  signal bits_send_reg : STD_LOGIC_VECTOR ( 4 downto 2 );
  signal \^bits_send_reg[0]_0\ : STD_LOGIC;
  signal \^bits_send_reg[1]_0\ : STD_LOGIC;
  signal \^is_clk_running\ : STD_LOGIC;
  signal is_clk_running_i_1_n_0 : STD_LOGIC;
  signal \pll_cs_reg_i_1__0_n_0\ : STD_LOGIC;
  signal pll_mosi_reg_i_15_n_0 : STD_LOGIC;
  signal pll_mosi_reg_i_16_n_0 : STD_LOGIC;
  signal \pll_mosi_reg_i_1__0_n_0\ : STD_LOGIC;
  signal \pll_mosi_reg_i_2__0_n_0\ : STD_LOGIC;
  signal \pll_mosi_reg_i_3__0_n_0\ : STD_LOGIC;
  signal \pll_mosi_reg_i_4__0_n_0\ : STD_LOGIC;
  signal \pll_mosi_reg_i_5__0_n_0\ : STD_LOGIC;
  signal \pll_mosi_reg_i_6__0_n_0\ : STD_LOGIC;
  signal \pll_mosi_reg_i_7__0_n_0\ : STD_LOGIC;
  signal pll_mosi_reg_i_8_n_0 : STD_LOGIC;
  signal pll_mosi_write : STD_LOGIC;
  signal \^pll_sen_write\ : STD_LOGIC;
  signal start_prev : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \bits_send[0]_i_1__0\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \bits_send[1]_i_1__0\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \bits_send[4]_i_2__0\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \bits_send[4]_i_3__0\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \pll_cs_reg_i_1__0\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of pll_mosi_reg_i_16 : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \pll_mosi_reg_i_3__0\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \pll_mosi_reg_i_6__0\ : label is "soft_lutpair15";
begin
  \bits_send_reg[0]_0\ <= \^bits_send_reg[0]_0\;
  \bits_send_reg[1]_0\ <= \^bits_send_reg[1]_0\;
  is_clk_running <= \^is_clk_running\;
  pll_sen_write <= \^pll_sen_write\;
\bits_send[0]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6066"
    )
        port map (
      I0 => \^bits_send_reg[0]_0\,
      I1 => \^is_clk_running\,
      I2 => start_prev,
      I3 => START_send,
      O => \bits_send[0]_i_1__0_n_0\
    );
\bits_send[1]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6A006A6A"
    )
        port map (
      I0 => \^bits_send_reg[1]_0\,
      I1 => \^is_clk_running\,
      I2 => \^bits_send_reg[0]_0\,
      I3 => start_prev,
      I4 => START_send,
      O => \bits_send[1]_i_1__0_n_0\
    );
\bits_send[2]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAA00006AAA6AAA"
    )
        port map (
      I0 => bits_send_reg(2),
      I1 => \^bits_send_reg[0]_0\,
      I2 => \^bits_send_reg[1]_0\,
      I3 => \^is_clk_running\,
      I4 => start_prev,
      I5 => START_send,
      O => \bits_send[2]_i_1__0_n_0\
    );
\bits_send[3]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1230303030303030"
    )
        port map (
      I0 => \^is_clk_running\,
      I1 => bits_send0,
      I2 => bits_send_reg(3),
      I3 => \^bits_send_reg[1]_0\,
      I4 => \^bits_send_reg[0]_0\,
      I5 => bits_send_reg(2),
      O => \bits_send[3]_i_1__0_n_0\
    );
\bits_send[4]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FF7F0080"
    )
        port map (
      I0 => \^is_clk_running\,
      I1 => bits_send_reg(3),
      I2 => bits_send_reg(2),
      I3 => \bits_send[4]_i_2__0_n_0\,
      I4 => bits_send_reg(4),
      I5 => bits_send0,
      O => \bits_send[4]_i_1__0_n_0\
    );
\bits_send[4]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \^bits_send_reg[1]_0\,
      I1 => \^bits_send_reg[0]_0\,
      O => \bits_send[4]_i_2__0_n_0\
    );
\bits_send[4]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => START_send,
      I1 => start_prev,
      O => bits_send0
    );
\bits_send_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => '1',
      D => \bits_send[0]_i_1__0_n_0\,
      Q => \^bits_send_reg[0]_0\,
      R => '0'
    );
\bits_send_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => '1',
      D => \bits_send[1]_i_1__0_n_0\,
      Q => \^bits_send_reg[1]_0\,
      R => '0'
    );
\bits_send_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => '1',
      D => \bits_send[2]_i_1__0_n_0\,
      Q => bits_send_reg(2),
      R => '0'
    );
\bits_send_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => '1',
      D => \bits_send[3]_i_1__0_n_0\,
      Q => bits_send_reg(3),
      R => '0'
    );
\bits_send_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => '1',
      D => \bits_send[4]_i_1__0_n_0\,
      Q => bits_send_reg(4),
      R => '0'
    );
is_clk_running_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFAA2AAAAA"
    )
        port map (
      I0 => \^is_clk_running\,
      I1 => bits_send_reg(3),
      I2 => bits_send_reg(2),
      I3 => \bits_send[4]_i_2__0_n_0\,
      I4 => bits_send_reg(4),
      I5 => bits_send0,
      O => is_clk_running_i_1_n_0
    );
is_clk_running_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => '1',
      D => is_clk_running_i_1_n_0,
      Q => \^is_clk_running\,
      R => '0'
    );
\pll_cs_reg_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B0AAB0B0"
    )
        port map (
      I0 => \^pll_sen_write\,
      I1 => \pll_mosi_reg_i_3__0_n_0\,
      I2 => \^is_clk_running\,
      I3 => start_prev,
      I4 => START_send,
      O => \pll_cs_reg_i_1__0_n_0\
    );
pll_cs_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => '1',
      D => \pll_cs_reg_i_1__0_n_0\,
      Q => \^pll_sen_write\,
      R => '0'
    );
pll_mosi_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pll_mosi_write,
      I1 => pll_mosi_read,
      O => pll_mosi
    );
pll_mosi_reg_i_15: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00800000FF000000"
    )
        port map (
      I0 => \^bits_send_reg[0]_0\,
      I1 => \^bits_send_reg[1]_0\,
      I2 => pll_mosi_reg_i_8_0(4),
      I3 => bits_send_reg(3),
      I4 => bits_send_reg(4),
      I5 => bits_send_reg(2),
      O => pll_mosi_reg_i_15_n_0
    );
pll_mosi_reg_i_16: unisim.vcomponents.LUT5
    generic map(
      INIT => X"C808C8C8"
    )
        port map (
      I0 => pll_mosi_reg_i_8_0(1),
      I1 => \^bits_send_reg[1]_0\,
      I2 => \^bits_send_reg[0]_0\,
      I3 => pll_mosi_reg_i_8_0(0),
      I4 => bits_send_reg(3),
      O => pll_mosi_reg_i_16_n_0
    );
\pll_mosi_reg_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ACA0AFA0ACA0A0A0"
    )
        port map (
      I0 => pll_mosi_reg_reg_0(0),
      I1 => \pll_mosi_reg_i_2__0_n_0\,
      I2 => bits_send0,
      I3 => \^is_clk_running\,
      I4 => \pll_mosi_reg_i_3__0_n_0\,
      I5 => pll_mosi_write,
      O => \pll_mosi_reg_i_1__0_n_0\
    );
\pll_mosi_reg_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF0DDF0DD0"
    )
        port map (
      I0 => \pll_mosi_reg_i_4__0_n_0\,
      I1 => \pll_mosi_reg_i_5__0_n_0\,
      I2 => bits_send_reg(4),
      I3 => \pll_mosi_reg_i_6__0_n_0\,
      I4 => \pll_mosi_reg_i_7__0_n_0\,
      I5 => pll_mosi_reg_i_8_n_0,
      O => \pll_mosi_reg_i_2__0_n_0\
    );
\pll_mosi_reg_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      I0 => bits_send_reg(3),
      I1 => bits_send_reg(2),
      I2 => \^bits_send_reg[1]_0\,
      I3 => \^bits_send_reg[0]_0\,
      I4 => bits_send_reg(4),
      O => \pll_mosi_reg_i_3__0_n_0\
    );
\pll_mosi_reg_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A7777555F7777FFF"
    )
        port map (
      I0 => bits_send_reg(3),
      I1 => \pll_mosi_reg_i_2__0_2\,
      I2 => \^bits_send_reg[1]_0\,
      I3 => \^bits_send_reg[0]_0\,
      I4 => bits_send_reg(2),
      I5 => \pll_mosi_reg_i_2__0_3\,
      O => \pll_mosi_reg_i_4__0_n_0\
    );
\pll_mosi_reg_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C00000000AACACAC"
    )
        port map (
      I0 => \pll_mosi_reg_i_2__0_4\,
      I1 => \pll_mosi_reg_i_2__0_5\,
      I2 => bits_send_reg(2),
      I3 => \^bits_send_reg[0]_0\,
      I4 => \^bits_send_reg[1]_0\,
      I5 => bits_send_reg(3),
      O => \pll_mosi_reg_i_5__0_n_0\
    );
\pll_mosi_reg_i_6__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EAAA"
    )
        port map (
      I0 => bits_send_reg(3),
      I1 => \^bits_send_reg[1]_0\,
      I2 => \^bits_send_reg[0]_0\,
      I3 => bits_send_reg(2),
      O => \pll_mosi_reg_i_6__0_n_0\
    );
\pll_mosi_reg_i_7__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BEEE8222"
    )
        port map (
      I0 => \pll_mosi_reg_i_2__0_0\,
      I1 => bits_send_reg(2),
      I2 => \^bits_send_reg[0]_0\,
      I3 => \^bits_send_reg[1]_0\,
      I4 => \pll_mosi_reg_i_2__0_1\,
      O => \pll_mosi_reg_i_7__0_n_0\
    );
pll_mosi_reg_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88A888AA88A88888"
    )
        port map (
      I0 => pll_mosi_reg_i_15_n_0,
      I1 => pll_mosi_reg_i_16_n_0,
      I2 => pll_mosi_reg_i_8_0(2),
      I3 => \^bits_send_reg[1]_0\,
      I4 => \^bits_send_reg[0]_0\,
      I5 => pll_mosi_reg_i_8_0(3),
      O => pll_mosi_reg_i_8_n_0
    );
pll_mosi_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => '1',
      D => \pll_mosi_reg_i_1__0_n_0\,
      Q => pll_mosi_write,
      R => '0'
    );
start_prev_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_out3,
      CE => '1',
      D => START_send,
      Q => start_prev,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0 is
  port (
    clk_out1 : out STD_LOGIC;
    clk_out2 : out STD_LOGIC;
    clk_out3 : out STD_LOGIC;
    clk_in1 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0 is
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0_clk_wiz
     port map (
      clk_in1 => clk_in1,
      clk_out1 => clk_out1,
      clk_out2 => clk_out2,
      clk_out3 => clk_out3
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top_PLL_control is
  port (
    start_adc_count : out STD_LOGIC;
    \bits_send_reg[1]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ip2mb_reg0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    pll_trig : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 23 downto 0 );
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    pll_sck : out STD_LOGIC;
    pll_sen : out STD_LOGIC;
    pll_mosi : out STD_LOGIC;
    ATTEN : out STD_LOGIC_VECTOR ( 5 downto 0 );
    s00_axi_aclk : in STD_LOGIC;
    slv_reg0 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    pll_ld_sdo : in STD_LOGIC;
    pll_mosi_reg_reg : in STD_LOGIC;
    pll_mosi_reg_reg_0 : in STD_LOGIC;
    \pll_mosi_reg_i_2__0\ : in STD_LOGIC;
    \pll_mosi_reg_i_2__0_0\ : in STD_LOGIC;
    \pll_mosi_reg_i_2__0_1\ : in STD_LOGIC;
    \pll_mosi_reg_i_2__0_2\ : in STD_LOGIC;
    \pll_mosi_reg_i_2__0_3\ : in STD_LOGIC;
    \pll_mosi_reg_i_2__0_4\ : in STD_LOGIC;
    pll_mosi_reg_i_8 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    pll_mosi_reg_reg_1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    \atten_reg_reg[5]_0\ : in STD_LOGIC_VECTOR ( 5 downto 0 );
    shift_front : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top_PLL_control;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top_PLL_control is
  signal START_receive : STD_LOGIC;
  signal START_send : STD_LOGIC;
  signal clk_8MHz : STD_LOGIC;
  signal clk_pll_spi : STD_LOGIC;
  signal is_clk_running : STD_LOGIC;
  signal p_2_in : STD_LOGIC_VECTOR ( 15 downto 1 );
  signal pll_mosi_read : STD_LOGIC;
  signal pll_sen_write : STD_LOGIC;
  signal \^pll_trig\ : STD_LOGIC;
  signal pll_trig_reg_i_1_n_0 : STD_LOGIC;
  signal start_adc_count_r0 : STD_LOGIC;
  signal start_adc_count_r1 : STD_LOGIC;
  signal \start_adc_count_r1_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \start_adc_count_r1_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \start_adc_count_r1_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \start_adc_count_r1_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \start_adc_count_r1_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \start_adc_count_r1_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \start_adc_count_r1_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \start_adc_count_r1_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \start_adc_count_r1_carry__0_n_1\ : STD_LOGIC;
  signal \start_adc_count_r1_carry__0_n_2\ : STD_LOGIC;
  signal \start_adc_count_r1_carry__0_n_3\ : STD_LOGIC;
  signal start_adc_count_r1_carry_i_1_n_0 : STD_LOGIC;
  signal start_adc_count_r1_carry_i_2_n_0 : STD_LOGIC;
  signal start_adc_count_r1_carry_i_3_n_0 : STD_LOGIC;
  signal start_adc_count_r1_carry_i_4_n_0 : STD_LOGIC;
  signal start_adc_count_r1_carry_i_5_n_0 : STD_LOGIC;
  signal start_adc_count_r1_carry_i_6_n_0 : STD_LOGIC;
  signal start_adc_count_r1_carry_i_7_n_0 : STD_LOGIC;
  signal start_adc_count_r1_carry_i_8_n_0 : STD_LOGIC;
  signal start_adc_count_r1_carry_n_0 : STD_LOGIC;
  signal start_adc_count_r1_carry_n_1 : STD_LOGIC;
  signal start_adc_count_r1_carry_n_2 : STD_LOGIC;
  signal start_adc_count_r1_carry_n_3 : STD_LOGIC;
  signal start_adc_count_r_i_2_n_0 : STD_LOGIC;
  signal start_adc_count_r_i_3_n_0 : STD_LOGIC;
  signal start_adc_count_r_i_4_n_0 : STD_LOGIC;
  signal start_adc_count_r_i_5_n_0 : STD_LOGIC;
  signal trig_tguard_counter : STD_LOGIC;
  signal \trig_tguard_counter[0]_i_1_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[0]_i_2_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[0]_i_3_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[0]_i_4_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[15]_i_10_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[15]_i_11_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[15]_i_12_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[15]_i_13_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[15]_i_14_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[15]_i_15_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[15]_i_16_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[15]_i_17_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[15]_i_18_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[15]_i_19_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[15]_i_1_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[15]_i_4_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[15]_i_5_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[15]_i_6_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[15]_i_7_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[15]_i_8_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter[15]_i_9_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[15]_i_3_n_2\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[15]_i_3_n_3\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \trig_tguard_counter_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[0]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[10]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[11]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[12]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[13]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[14]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[15]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[1]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[2]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[3]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[4]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[5]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[6]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[7]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[8]\ : STD_LOGIC;
  signal \trig_tguard_counter_reg_n_0_[9]\ : STD_LOGIC;
  signal trig_tsweep_counter07_out : STD_LOGIC;
  signal \trig_tsweep_counter[0]_i_1_n_0\ : STD_LOGIC;
  signal \trig_tsweep_counter[0]_i_4_n_0\ : STD_LOGIC;
  signal \trig_tsweep_counter[0]_i_5_n_0\ : STD_LOGIC;
  signal \trig_tsweep_counter[0]_i_6_n_0\ : STD_LOGIC;
  signal trig_tsweep_counter_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \trig_tsweep_counter_reg[0]_i_3_n_0\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[0]_i_3_n_1\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[0]_i_3_n_2\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[0]_i_3_n_3\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[0]_i_3_n_4\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[0]_i_3_n_5\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[0]_i_3_n_6\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[0]_i_3_n_7\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \trig_tsweep_counter_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal NLW_clk_wizard_main_clk_out1_UNCONNECTED : STD_LOGIC;
  signal NLW_start_adc_count_r1_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_start_adc_count_r1_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_trig_tguard_counter_reg[15]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_trig_tguard_counter_reg[15]_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_trig_tsweep_counter_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of start_adc_count_r_i_5 : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \trig_tguard_counter[0]_i_4\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \trig_tguard_counter[15]_i_15\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \trig_tguard_counter[15]_i_17\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \trig_tguard_counter[15]_i_19\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \trig_tguard_counter[15]_i_6\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \trig_tguard_counter[15]_i_9\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \trig_tsweep_counter[0]_i_6\ : label is "soft_lutpair18";
begin
  pll_trig <= \^pll_trig\;
START_receive_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => slv_reg0(1),
      Q => START_receive,
      R => '0'
    );
START_send_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => slv_reg0(0),
      Q => START_send,
      R => '0'
    );
\atten_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \atten_reg_reg[5]_0\(0),
      Q => ATTEN(0),
      R => '0'
    );
\atten_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \atten_reg_reg[5]_0\(1),
      Q => ATTEN(1),
      R => '0'
    );
\atten_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \atten_reg_reg[5]_0\(2),
      Q => ATTEN(2),
      R => '0'
    );
\atten_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \atten_reg_reg[5]_0\(3),
      Q => ATTEN(3),
      R => '0'
    );
\atten_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \atten_reg_reg[5]_0\(4),
      Q => ATTEN(4),
      R => '0'
    );
\atten_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \atten_reg_reg[5]_0\(5),
      Q => ATTEN(5),
      R => '0'
    );
clk_wizard_main: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_clk_wiz_0
     port map (
      clk_in1 => s00_axi_aclk,
      clk_out1 => NLW_clk_wizard_main_clk_out1_UNCONNECTED,
      clk_out2 => clk_8MHz,
      clk_out3 => clk_pll_spi
    );
pll_receive: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll_receive
     port map (
      D(1 downto 0) => D(1 downto 0),
      Q(23 downto 0) => Q(23 downto 0),
      START_receive => START_receive,
      clk_out3 => clk_pll_spi,
      ip2mb_reg0(0) => ip2mb_reg0(0),
      is_clk_running => is_clk_running,
      pll_ld_sdo => pll_ld_sdo,
      pll_mosi_read => pll_mosi_read,
      pll_mosi_reg_reg_0 => pll_mosi_reg_reg,
      pll_mosi_reg_reg_1 => pll_mosi_reg_reg_0,
      pll_sck => pll_sck,
      pll_sen => pll_sen,
      pll_sen_write => pll_sen_write
    );
pll_send_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pll_send
     port map (
      START_send => START_send,
      \bits_send_reg[0]_0\ => \bits_send_reg[1]\(0),
      \bits_send_reg[1]_0\ => \bits_send_reg[1]\(1),
      clk_out3 => clk_pll_spi,
      is_clk_running => is_clk_running,
      pll_mosi => pll_mosi,
      pll_mosi_read => pll_mosi_read,
      \pll_mosi_reg_i_2__0_0\ => \pll_mosi_reg_i_2__0\,
      \pll_mosi_reg_i_2__0_1\ => \pll_mosi_reg_i_2__0_0\,
      \pll_mosi_reg_i_2__0_2\ => \pll_mosi_reg_i_2__0_1\,
      \pll_mosi_reg_i_2__0_3\ => \pll_mosi_reg_i_2__0_2\,
      \pll_mosi_reg_i_2__0_4\ => \pll_mosi_reg_i_2__0_3\,
      \pll_mosi_reg_i_2__0_5\ => \pll_mosi_reg_i_2__0_4\,
      pll_mosi_reg_i_8_0(4 downto 0) => pll_mosi_reg_i_8(4 downto 0),
      pll_mosi_reg_reg_0(0) => pll_mosi_reg_reg_1(0),
      pll_sen_write => pll_sen_write
    );
pll_trig_reg_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00CE"
    )
        port map (
      I0 => \^pll_trig\,
      I1 => \trig_tguard_counter[15]_i_1_n_0\,
      I2 => trig_tguard_counter,
      I3 => trig_tsweep_counter07_out,
      O => pll_trig_reg_i_1_n_0
    );
pll_trig_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => '1',
      D => pll_trig_reg_i_1_n_0,
      Q => \^pll_trig\,
      R => '0'
    );
start_adc_count_r1_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => start_adc_count_r1_carry_n_0,
      CO(2) => start_adc_count_r1_carry_n_1,
      CO(1) => start_adc_count_r1_carry_n_2,
      CO(0) => start_adc_count_r1_carry_n_3,
      CYINIT => '0',
      DI(3) => start_adc_count_r1_carry_i_1_n_0,
      DI(2) => start_adc_count_r1_carry_i_2_n_0,
      DI(1) => start_adc_count_r1_carry_i_3_n_0,
      DI(0) => start_adc_count_r1_carry_i_4_n_0,
      O(3 downto 0) => NLW_start_adc_count_r1_carry_O_UNCONNECTED(3 downto 0),
      S(3) => start_adc_count_r1_carry_i_5_n_0,
      S(2) => start_adc_count_r1_carry_i_6_n_0,
      S(1) => start_adc_count_r1_carry_i_7_n_0,
      S(0) => start_adc_count_r1_carry_i_8_n_0
    );
\start_adc_count_r1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => start_adc_count_r1_carry_n_0,
      CO(3) => start_adc_count_r1,
      CO(2) => \start_adc_count_r1_carry__0_n_1\,
      CO(1) => \start_adc_count_r1_carry__0_n_2\,
      CO(0) => \start_adc_count_r1_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \start_adc_count_r1_carry__0_i_1_n_0\,
      DI(2) => \start_adc_count_r1_carry__0_i_2_n_0\,
      DI(1) => \start_adc_count_r1_carry__0_i_3_n_0\,
      DI(0) => \start_adc_count_r1_carry__0_i_4_n_0\,
      O(3 downto 0) => \NLW_start_adc_count_r1_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \start_adc_count_r1_carry__0_i_5_n_0\,
      S(2) => \start_adc_count_r1_carry__0_i_6_n_0\,
      S(1) => \start_adc_count_r1_carry__0_i_7_n_0\,
      S(0) => \start_adc_count_r1_carry__0_i_8_n_0\
    );
\start_adc_count_r1_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => trig_tsweep_counter_reg(15),
      I1 => shift_front(15),
      I2 => trig_tsweep_counter_reg(14),
      I3 => shift_front(14),
      O => \start_adc_count_r1_carry__0_i_1_n_0\
    );
\start_adc_count_r1_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => trig_tsweep_counter_reg(13),
      I1 => shift_front(13),
      I2 => trig_tsweep_counter_reg(12),
      I3 => shift_front(12),
      O => \start_adc_count_r1_carry__0_i_2_n_0\
    );
\start_adc_count_r1_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => trig_tsweep_counter_reg(11),
      I1 => shift_front(11),
      I2 => trig_tsweep_counter_reg(10),
      I3 => shift_front(10),
      O => \start_adc_count_r1_carry__0_i_3_n_0\
    );
\start_adc_count_r1_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => trig_tsweep_counter_reg(9),
      I1 => shift_front(9),
      I2 => trig_tsweep_counter_reg(8),
      I3 => shift_front(8),
      O => \start_adc_count_r1_carry__0_i_4_n_0\
    );
\start_adc_count_r1_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => shift_front(15),
      I1 => trig_tsweep_counter_reg(15),
      I2 => shift_front(14),
      I3 => trig_tsweep_counter_reg(14),
      O => \start_adc_count_r1_carry__0_i_5_n_0\
    );
\start_adc_count_r1_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => shift_front(13),
      I1 => trig_tsweep_counter_reg(13),
      I2 => shift_front(12),
      I3 => trig_tsweep_counter_reg(12),
      O => \start_adc_count_r1_carry__0_i_6_n_0\
    );
\start_adc_count_r1_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => shift_front(11),
      I1 => trig_tsweep_counter_reg(11),
      I2 => shift_front(10),
      I3 => trig_tsweep_counter_reg(10),
      O => \start_adc_count_r1_carry__0_i_7_n_0\
    );
\start_adc_count_r1_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => shift_front(9),
      I1 => trig_tsweep_counter_reg(9),
      I2 => shift_front(8),
      I3 => trig_tsweep_counter_reg(8),
      O => \start_adc_count_r1_carry__0_i_8_n_0\
    );
start_adc_count_r1_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => trig_tsweep_counter_reg(7),
      I1 => shift_front(7),
      I2 => trig_tsweep_counter_reg(6),
      I3 => shift_front(6),
      O => start_adc_count_r1_carry_i_1_n_0
    );
start_adc_count_r1_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => trig_tsweep_counter_reg(5),
      I1 => shift_front(5),
      I2 => trig_tsweep_counter_reg(4),
      I3 => shift_front(4),
      O => start_adc_count_r1_carry_i_2_n_0
    );
start_adc_count_r1_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => trig_tsweep_counter_reg(3),
      I1 => shift_front(3),
      I2 => trig_tsweep_counter_reg(2),
      I3 => shift_front(2),
      O => start_adc_count_r1_carry_i_3_n_0
    );
start_adc_count_r1_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => trig_tsweep_counter_reg(1),
      I1 => shift_front(1),
      I2 => trig_tsweep_counter_reg(0),
      I3 => shift_front(0),
      O => start_adc_count_r1_carry_i_4_n_0
    );
start_adc_count_r1_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => shift_front(7),
      I1 => trig_tsweep_counter_reg(7),
      I2 => shift_front(6),
      I3 => trig_tsweep_counter_reg(6),
      O => start_adc_count_r1_carry_i_5_n_0
    );
start_adc_count_r1_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => shift_front(5),
      I1 => trig_tsweep_counter_reg(5),
      I2 => shift_front(4),
      I3 => trig_tsweep_counter_reg(4),
      O => start_adc_count_r1_carry_i_6_n_0
    );
start_adc_count_r1_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => shift_front(3),
      I1 => trig_tsweep_counter_reg(3),
      I2 => shift_front(2),
      I3 => trig_tsweep_counter_reg(2),
      O => start_adc_count_r1_carry_i_7_n_0
    );
start_adc_count_r1_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => shift_front(1),
      I1 => trig_tsweep_counter_reg(1),
      I2 => shift_front(0),
      I3 => trig_tsweep_counter_reg(0),
      O => start_adc_count_r1_carry_i_8_n_0
    );
start_adc_count_r_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => start_adc_count_r1,
      I1 => start_adc_count_r_i_2_n_0,
      O => start_adc_count_r0
    );
start_adc_count_r_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => start_adc_count_r_i_3_n_0,
      I1 => start_adc_count_r_i_4_n_0,
      I2 => \trig_tguard_counter_reg_n_0_[7]\,
      I3 => start_adc_count_r_i_5_n_0,
      I4 => \trig_tguard_counter[15]_i_11_n_0\,
      O => start_adc_count_r_i_2_n_0
    );
start_adc_count_r_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \trig_tguard_counter_reg_n_0_[12]\,
      I1 => \trig_tguard_counter_reg_n_0_[13]\,
      I2 => \trig_tguard_counter_reg_n_0_[14]\,
      I3 => \trig_tguard_counter_reg_n_0_[15]\,
      O => start_adc_count_r_i_3_n_0
    );
start_adc_count_r_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \trig_tguard_counter_reg_n_0_[10]\,
      I1 => \trig_tguard_counter_reg_n_0_[11]\,
      I2 => \trig_tguard_counter_reg_n_0_[8]\,
      I3 => \trig_tguard_counter_reg_n_0_[9]\,
      O => start_adc_count_r_i_4_n_0
    );
start_adc_count_r_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \trig_tguard_counter_reg_n_0_[1]\,
      I1 => \trig_tguard_counter_reg_n_0_[0]\,
      I2 => \trig_tguard_counter_reg_n_0_[5]\,
      I3 => \trig_tguard_counter_reg_n_0_[6]\,
      O => start_adc_count_r_i_5_n_0
    );
start_adc_count_r_reg: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_8MHz,
      CE => '1',
      D => start_adc_count_r0,
      Q => start_adc_count,
      R => '0'
    );
\trig_tguard_counter[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF12"
    )
        port map (
      I0 => \trig_tguard_counter_reg_n_0_[0]\,
      I1 => \trig_tguard_counter[0]_i_2_n_0\,
      I2 => trig_tguard_counter,
      I3 => \trig_tguard_counter[0]_i_3_n_0\,
      O => \trig_tguard_counter[0]_i_1_n_0\
    );
\trig_tguard_counter[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \trig_tguard_counter[15]_i_7_n_0\,
      I1 => \trig_tguard_counter[15]_i_6_n_0\,
      I2 => \trig_tguard_counter[15]_i_5_n_0\,
      I3 => \trig_tguard_counter[15]_i_4_n_0\,
      O => \trig_tguard_counter[0]_i_2_n_0\
    );
\trig_tguard_counter[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => start_adc_count_r_i_2_n_0,
      I1 => \trig_tguard_counter[0]_i_4_n_0\,
      I2 => trig_tsweep_counter_reg(2),
      I3 => trig_tsweep_counter_reg(4),
      I4 => \trig_tguard_counter[15]_i_14_n_0\,
      I5 => \trig_tguard_counter[15]_i_16_n_0\,
      O => \trig_tguard_counter[0]_i_3_n_0\
    );
\trig_tguard_counter[0]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFBFFFFF"
    )
        port map (
      I0 => \trig_tguard_counter[15]_i_13_n_0\,
      I1 => trig_tsweep_counter_reg(3),
      I2 => trig_tsweep_counter_reg(5),
      I3 => trig_tsweep_counter_reg(12),
      I4 => trig_tsweep_counter_reg(13),
      O => \trig_tguard_counter[0]_i_4_n_0\
    );
\trig_tguard_counter[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000100010001FFFF"
    )
        port map (
      I0 => \trig_tguard_counter[15]_i_4_n_0\,
      I1 => \trig_tguard_counter[15]_i_5_n_0\,
      I2 => \trig_tguard_counter[15]_i_6_n_0\,
      I3 => \trig_tguard_counter[15]_i_7_n_0\,
      I4 => \trig_tguard_counter[15]_i_8_n_0\,
      I5 => start_adc_count_r_i_2_n_0,
      O => \trig_tguard_counter[15]_i_1_n_0\
    );
\trig_tguard_counter[15]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \trig_tguard_counter[15]_i_18_n_0\,
      I1 => \trig_tguard_counter[15]_i_14_n_0\,
      I2 => trig_tsweep_counter_reg(1),
      I3 => trig_tsweep_counter_reg(0),
      I4 => \trig_tguard_counter[15]_i_19_n_0\,
      I5 => \trig_tguard_counter[15]_i_13_n_0\,
      O => \trig_tguard_counter[15]_i_10_n_0\
    );
\trig_tguard_counter[15]_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \trig_tguard_counter_reg_n_0_[2]\,
      I1 => \trig_tguard_counter_reg_n_0_[3]\,
      I2 => \trig_tguard_counter_reg_n_0_[4]\,
      O => \trig_tguard_counter[15]_i_11_n_0\
    );
\trig_tguard_counter[15]_i_12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \trig_tguard_counter_reg_n_0_[6]\,
      I1 => \trig_tguard_counter_reg_n_0_[5]\,
      O => \trig_tguard_counter[15]_i_12_n_0\
    );
\trig_tguard_counter[15]_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => trig_tsweep_counter_reg(11),
      I1 => trig_tsweep_counter_reg(10),
      I2 => trig_tsweep_counter_reg(15),
      I3 => trig_tsweep_counter_reg(14),
      O => \trig_tguard_counter[15]_i_13_n_0\
    );
\trig_tguard_counter[15]_i_14\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => trig_tsweep_counter_reg(6),
      I1 => trig_tsweep_counter_reg(7),
      O => \trig_tguard_counter[15]_i_14_n_0\
    );
\trig_tguard_counter[15]_i_15\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => trig_tsweep_counter_reg(3),
      I1 => trig_tsweep_counter_reg(4),
      O => \trig_tguard_counter[15]_i_15_n_0\
    );
\trig_tguard_counter[15]_i_16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFFF"
    )
        port map (
      I0 => trig_tsweep_counter_reg(1),
      I1 => trig_tsweep_counter_reg(0),
      I2 => trig_tsweep_counter_reg(9),
      I3 => trig_tsweep_counter_reg(8),
      O => \trig_tguard_counter[15]_i_16_n_0\
    );
\trig_tguard_counter[15]_i_17\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => trig_tsweep_counter_reg(13),
      I1 => trig_tsweep_counter_reg(12),
      I2 => trig_tsweep_counter_reg(5),
      I3 => trig_tsweep_counter_reg(3),
      O => \trig_tguard_counter[15]_i_17_n_0\
    );
\trig_tguard_counter[15]_i_18\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => trig_tsweep_counter_reg(4),
      I1 => trig_tsweep_counter_reg(3),
      I2 => trig_tsweep_counter_reg(9),
      I3 => trig_tsweep_counter_reg(8),
      O => \trig_tguard_counter[15]_i_18_n_0\
    );
\trig_tguard_counter[15]_i_19\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => trig_tsweep_counter_reg(13),
      I1 => trig_tsweep_counter_reg(12),
      I2 => trig_tsweep_counter_reg(5),
      I3 => trig_tsweep_counter_reg(2),
      O => \trig_tguard_counter[15]_i_19_n_0\
    );
\trig_tguard_counter[15]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0003030303030302"
    )
        port map (
      I0 => \trig_tguard_counter[15]_i_9_n_0\,
      I1 => \trig_tguard_counter[15]_i_5_n_0\,
      I2 => \trig_tguard_counter[15]_i_10_n_0\,
      I3 => \trig_tguard_counter_reg_n_0_[5]\,
      I4 => \trig_tguard_counter_reg_n_0_[6]\,
      I5 => \trig_tguard_counter[15]_i_11_n_0\,
      O => trig_tguard_counter
    );
\trig_tguard_counter[15]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFFFF"
    )
        port map (
      I0 => \trig_tguard_counter_reg_n_0_[0]\,
      I1 => \trig_tguard_counter_reg_n_0_[1]\,
      I2 => \trig_tguard_counter_reg_n_0_[4]\,
      I3 => \trig_tguard_counter_reg_n_0_[3]\,
      I4 => \trig_tguard_counter_reg_n_0_[2]\,
      I5 => \trig_tguard_counter[15]_i_12_n_0\,
      O => \trig_tguard_counter[15]_i_4_n_0\
    );
\trig_tguard_counter[15]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \trig_tguard_counter_reg_n_0_[7]\,
      I1 => start_adc_count_r_i_4_n_0,
      I2 => \trig_tguard_counter_reg_n_0_[12]\,
      I3 => \trig_tguard_counter_reg_n_0_[13]\,
      I4 => \trig_tguard_counter_reg_n_0_[14]\,
      I5 => \trig_tguard_counter_reg_n_0_[15]\,
      O => \trig_tguard_counter[15]_i_5_n_0\
    );
\trig_tguard_counter[15]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \trig_tguard_counter[15]_i_13_n_0\,
      I1 => trig_tsweep_counter_reg(2),
      I2 => trig_tsweep_counter_reg(5),
      I3 => trig_tsweep_counter_reg(12),
      I4 => trig_tsweep_counter_reg(13),
      O => \trig_tguard_counter[15]_i_6_n_0\
    );
\trig_tguard_counter[15]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFFFFFF"
    )
        port map (
      I0 => trig_tsweep_counter_reg(0),
      I1 => trig_tsweep_counter_reg(1),
      I2 => \trig_tguard_counter[15]_i_14_n_0\,
      I3 => trig_tsweep_counter_reg(8),
      I4 => trig_tsweep_counter_reg(9),
      I5 => \trig_tguard_counter[15]_i_15_n_0\,
      O => \trig_tguard_counter[15]_i_7_n_0\
    );
\trig_tguard_counter[15]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \trig_tguard_counter[15]_i_16_n_0\,
      I1 => \trig_tguard_counter[15]_i_14_n_0\,
      I2 => trig_tsweep_counter_reg(4),
      I3 => trig_tsweep_counter_reg(2),
      I4 => \trig_tguard_counter[15]_i_17_n_0\,
      I5 => \trig_tguard_counter[15]_i_13_n_0\,
      O => \trig_tguard_counter[15]_i_8_n_0\
    );
\trig_tguard_counter[15]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \trig_tguard_counter_reg_n_0_[0]\,
      I1 => \trig_tguard_counter_reg_n_0_[1]\,
      O => \trig_tguard_counter[15]_i_9_n_0\
    );
\trig_tguard_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => '1',
      D => \trig_tguard_counter[0]_i_1_n_0\,
      Q => \trig_tguard_counter_reg_n_0_[0]\,
      R => '0'
    );
\trig_tguard_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tguard_counter,
      D => p_2_in(10),
      Q => \trig_tguard_counter_reg_n_0_[10]\,
      R => \trig_tguard_counter[15]_i_1_n_0\
    );
\trig_tguard_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tguard_counter,
      D => p_2_in(11),
      Q => \trig_tguard_counter_reg_n_0_[11]\,
      R => \trig_tguard_counter[15]_i_1_n_0\
    );
\trig_tguard_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tguard_counter,
      D => p_2_in(12),
      Q => \trig_tguard_counter_reg_n_0_[12]\,
      R => \trig_tguard_counter[15]_i_1_n_0\
    );
\trig_tguard_counter_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \trig_tguard_counter_reg[8]_i_1_n_0\,
      CO(3) => \trig_tguard_counter_reg[12]_i_1_n_0\,
      CO(2) => \trig_tguard_counter_reg[12]_i_1_n_1\,
      CO(1) => \trig_tguard_counter_reg[12]_i_1_n_2\,
      CO(0) => \trig_tguard_counter_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => p_2_in(12 downto 9),
      S(3) => \trig_tguard_counter_reg_n_0_[12]\,
      S(2) => \trig_tguard_counter_reg_n_0_[11]\,
      S(1) => \trig_tguard_counter_reg_n_0_[10]\,
      S(0) => \trig_tguard_counter_reg_n_0_[9]\
    );
\trig_tguard_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tguard_counter,
      D => p_2_in(13),
      Q => \trig_tguard_counter_reg_n_0_[13]\,
      R => \trig_tguard_counter[15]_i_1_n_0\
    );
\trig_tguard_counter_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tguard_counter,
      D => p_2_in(14),
      Q => \trig_tguard_counter_reg_n_0_[14]\,
      R => \trig_tguard_counter[15]_i_1_n_0\
    );
\trig_tguard_counter_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tguard_counter,
      D => p_2_in(15),
      Q => \trig_tguard_counter_reg_n_0_[15]\,
      R => \trig_tguard_counter[15]_i_1_n_0\
    );
\trig_tguard_counter_reg[15]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \trig_tguard_counter_reg[12]_i_1_n_0\,
      CO(3 downto 2) => \NLW_trig_tguard_counter_reg[15]_i_3_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \trig_tguard_counter_reg[15]_i_3_n_2\,
      CO(0) => \trig_tguard_counter_reg[15]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_trig_tguard_counter_reg[15]_i_3_O_UNCONNECTED\(3),
      O(2 downto 0) => p_2_in(15 downto 13),
      S(3) => '0',
      S(2) => \trig_tguard_counter_reg_n_0_[15]\,
      S(1) => \trig_tguard_counter_reg_n_0_[14]\,
      S(0) => \trig_tguard_counter_reg_n_0_[13]\
    );
\trig_tguard_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tguard_counter,
      D => p_2_in(1),
      Q => \trig_tguard_counter_reg_n_0_[1]\,
      R => \trig_tguard_counter[15]_i_1_n_0\
    );
\trig_tguard_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tguard_counter,
      D => p_2_in(2),
      Q => \trig_tguard_counter_reg_n_0_[2]\,
      R => \trig_tguard_counter[15]_i_1_n_0\
    );
\trig_tguard_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tguard_counter,
      D => p_2_in(3),
      Q => \trig_tguard_counter_reg_n_0_[3]\,
      R => \trig_tguard_counter[15]_i_1_n_0\
    );
\trig_tguard_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tguard_counter,
      D => p_2_in(4),
      Q => \trig_tguard_counter_reg_n_0_[4]\,
      R => \trig_tguard_counter[15]_i_1_n_0\
    );
\trig_tguard_counter_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \trig_tguard_counter_reg[4]_i_1_n_0\,
      CO(2) => \trig_tguard_counter_reg[4]_i_1_n_1\,
      CO(1) => \trig_tguard_counter_reg[4]_i_1_n_2\,
      CO(0) => \trig_tguard_counter_reg[4]_i_1_n_3\,
      CYINIT => \trig_tguard_counter_reg_n_0_[0]\,
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => p_2_in(4 downto 1),
      S(3) => \trig_tguard_counter_reg_n_0_[4]\,
      S(2) => \trig_tguard_counter_reg_n_0_[3]\,
      S(1) => \trig_tguard_counter_reg_n_0_[2]\,
      S(0) => \trig_tguard_counter_reg_n_0_[1]\
    );
\trig_tguard_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tguard_counter,
      D => p_2_in(5),
      Q => \trig_tguard_counter_reg_n_0_[5]\,
      R => \trig_tguard_counter[15]_i_1_n_0\
    );
\trig_tguard_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tguard_counter,
      D => p_2_in(6),
      Q => \trig_tguard_counter_reg_n_0_[6]\,
      R => \trig_tguard_counter[15]_i_1_n_0\
    );
\trig_tguard_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tguard_counter,
      D => p_2_in(7),
      Q => \trig_tguard_counter_reg_n_0_[7]\,
      R => \trig_tguard_counter[15]_i_1_n_0\
    );
\trig_tguard_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tguard_counter,
      D => p_2_in(8),
      Q => \trig_tguard_counter_reg_n_0_[8]\,
      R => \trig_tguard_counter[15]_i_1_n_0\
    );
\trig_tguard_counter_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \trig_tguard_counter_reg[4]_i_1_n_0\,
      CO(3) => \trig_tguard_counter_reg[8]_i_1_n_0\,
      CO(2) => \trig_tguard_counter_reg[8]_i_1_n_1\,
      CO(1) => \trig_tguard_counter_reg[8]_i_1_n_2\,
      CO(0) => \trig_tguard_counter_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => p_2_in(8 downto 5),
      S(3) => \trig_tguard_counter_reg_n_0_[8]\,
      S(2) => \trig_tguard_counter_reg_n_0_[7]\,
      S(1) => \trig_tguard_counter_reg_n_0_[6]\,
      S(0) => \trig_tguard_counter_reg_n_0_[5]\
    );
\trig_tguard_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tguard_counter,
      D => p_2_in(9),
      Q => \trig_tguard_counter_reg_n_0_[9]\,
      R => \trig_tguard_counter[15]_i_1_n_0\
    );
\trig_tsweep_counter[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => trig_tguard_counter,
      I1 => \trig_tguard_counter[15]_i_1_n_0\,
      O => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000007"
    )
        port map (
      I0 => \trig_tsweep_counter[0]_i_4_n_0\,
      I1 => trig_tsweep_counter_reg(13),
      I2 => trig_tsweep_counter_reg(14),
      I3 => trig_tsweep_counter_reg(15),
      I4 => start_adc_count_r_i_2_n_0,
      O => trig_tsweep_counter07_out
    );
\trig_tsweep_counter[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEFFFEFEFEFEFEFE"
    )
        port map (
      I0 => trig_tsweep_counter_reg(10),
      I1 => trig_tsweep_counter_reg(11),
      I2 => trig_tsweep_counter_reg(12),
      I3 => \trig_tsweep_counter[0]_i_6_n_0\,
      I4 => trig_tsweep_counter_reg(8),
      I5 => trig_tsweep_counter_reg(9),
      O => \trig_tsweep_counter[0]_i_4_n_0\
    );
\trig_tsweep_counter[0]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => trig_tsweep_counter_reg(0),
      O => \trig_tsweep_counter[0]_i_5_n_0\
    );
\trig_tsweep_counter[0]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000057"
    )
        port map (
      I0 => trig_tsweep_counter_reg(5),
      I1 => trig_tsweep_counter_reg(3),
      I2 => trig_tsweep_counter_reg(4),
      I3 => trig_tsweep_counter_reg(7),
      I4 => trig_tsweep_counter_reg(6),
      O => \trig_tsweep_counter[0]_i_6_n_0\
    );
\trig_tsweep_counter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tsweep_counter07_out,
      D => \trig_tsweep_counter_reg[0]_i_3_n_7\,
      Q => trig_tsweep_counter_reg(0),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[0]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \trig_tsweep_counter_reg[0]_i_3_n_0\,
      CO(2) => \trig_tsweep_counter_reg[0]_i_3_n_1\,
      CO(1) => \trig_tsweep_counter_reg[0]_i_3_n_2\,
      CO(0) => \trig_tsweep_counter_reg[0]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \trig_tsweep_counter_reg[0]_i_3_n_4\,
      O(2) => \trig_tsweep_counter_reg[0]_i_3_n_5\,
      O(1) => \trig_tsweep_counter_reg[0]_i_3_n_6\,
      O(0) => \trig_tsweep_counter_reg[0]_i_3_n_7\,
      S(3 downto 1) => trig_tsweep_counter_reg(3 downto 1),
      S(0) => \trig_tsweep_counter[0]_i_5_n_0\
    );
\trig_tsweep_counter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tsweep_counter07_out,
      D => \trig_tsweep_counter_reg[8]_i_1_n_5\,
      Q => trig_tsweep_counter_reg(10),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tsweep_counter07_out,
      D => \trig_tsweep_counter_reg[8]_i_1_n_4\,
      Q => trig_tsweep_counter_reg(11),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tsweep_counter07_out,
      D => \trig_tsweep_counter_reg[12]_i_1_n_7\,
      Q => trig_tsweep_counter_reg(12),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \trig_tsweep_counter_reg[8]_i_1_n_0\,
      CO(3) => \NLW_trig_tsweep_counter_reg[12]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \trig_tsweep_counter_reg[12]_i_1_n_1\,
      CO(1) => \trig_tsweep_counter_reg[12]_i_1_n_2\,
      CO(0) => \trig_tsweep_counter_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \trig_tsweep_counter_reg[12]_i_1_n_4\,
      O(2) => \trig_tsweep_counter_reg[12]_i_1_n_5\,
      O(1) => \trig_tsweep_counter_reg[12]_i_1_n_6\,
      O(0) => \trig_tsweep_counter_reg[12]_i_1_n_7\,
      S(3 downto 0) => trig_tsweep_counter_reg(15 downto 12)
    );
\trig_tsweep_counter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tsweep_counter07_out,
      D => \trig_tsweep_counter_reg[12]_i_1_n_6\,
      Q => trig_tsweep_counter_reg(13),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tsweep_counter07_out,
      D => \trig_tsweep_counter_reg[12]_i_1_n_5\,
      Q => trig_tsweep_counter_reg(14),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tsweep_counter07_out,
      D => \trig_tsweep_counter_reg[12]_i_1_n_4\,
      Q => trig_tsweep_counter_reg(15),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tsweep_counter07_out,
      D => \trig_tsweep_counter_reg[0]_i_3_n_6\,
      Q => trig_tsweep_counter_reg(1),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tsweep_counter07_out,
      D => \trig_tsweep_counter_reg[0]_i_3_n_5\,
      Q => trig_tsweep_counter_reg(2),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tsweep_counter07_out,
      D => \trig_tsweep_counter_reg[0]_i_3_n_4\,
      Q => trig_tsweep_counter_reg(3),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tsweep_counter07_out,
      D => \trig_tsweep_counter_reg[4]_i_1_n_7\,
      Q => trig_tsweep_counter_reg(4),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \trig_tsweep_counter_reg[0]_i_3_n_0\,
      CO(3) => \trig_tsweep_counter_reg[4]_i_1_n_0\,
      CO(2) => \trig_tsweep_counter_reg[4]_i_1_n_1\,
      CO(1) => \trig_tsweep_counter_reg[4]_i_1_n_2\,
      CO(0) => \trig_tsweep_counter_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \trig_tsweep_counter_reg[4]_i_1_n_4\,
      O(2) => \trig_tsweep_counter_reg[4]_i_1_n_5\,
      O(1) => \trig_tsweep_counter_reg[4]_i_1_n_6\,
      O(0) => \trig_tsweep_counter_reg[4]_i_1_n_7\,
      S(3 downto 0) => trig_tsweep_counter_reg(7 downto 4)
    );
\trig_tsweep_counter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tsweep_counter07_out,
      D => \trig_tsweep_counter_reg[4]_i_1_n_6\,
      Q => trig_tsweep_counter_reg(5),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tsweep_counter07_out,
      D => \trig_tsweep_counter_reg[4]_i_1_n_5\,
      Q => trig_tsweep_counter_reg(6),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tsweep_counter07_out,
      D => \trig_tsweep_counter_reg[4]_i_1_n_4\,
      Q => trig_tsweep_counter_reg(7),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tsweep_counter07_out,
      D => \trig_tsweep_counter_reg[8]_i_1_n_7\,
      Q => trig_tsweep_counter_reg(8),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
\trig_tsweep_counter_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \trig_tsweep_counter_reg[4]_i_1_n_0\,
      CO(3) => \trig_tsweep_counter_reg[8]_i_1_n_0\,
      CO(2) => \trig_tsweep_counter_reg[8]_i_1_n_1\,
      CO(1) => \trig_tsweep_counter_reg[8]_i_1_n_2\,
      CO(0) => \trig_tsweep_counter_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \trig_tsweep_counter_reg[8]_i_1_n_4\,
      O(2) => \trig_tsweep_counter_reg[8]_i_1_n_5\,
      O(1) => \trig_tsweep_counter_reg[8]_i_1_n_6\,
      O(0) => \trig_tsweep_counter_reg[8]_i_1_n_7\,
      S(3 downto 0) => trig_tsweep_counter_reg(11 downto 8)
    );
\trig_tsweep_counter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_8MHz,
      CE => trig_tsweep_counter07_out,
      D => \trig_tsweep_counter_reg[8]_i_1_n_6\,
      Q => trig_tsweep_counter_reg(9),
      R => \trig_tsweep_counter[0]_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HMC769_v1_0 is
  port (
    ATTEN : out STD_LOGIC_VECTOR ( 5 downto 0 );
    start_adc_count : out STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    pll_sck : out STD_LOGIC;
    pll_sen : out STD_LOGIC;
    s00_axi_rvalid : out STD_LOGIC;
    pll_mosi : out STD_LOGIC;
    pll_trig : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pll_ld_sdo : in STD_LOGIC;
    shift_front : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HMC769_v1_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HMC769_v1_0 is
  signal HMC769_v1_0_S00_AXI_inst_n_10 : STD_LOGIC;
  signal HMC769_v1_0_S00_AXI_inst_n_11 : STD_LOGIC;
  signal HMC769_v1_0_S00_AXI_inst_n_12 : STD_LOGIC;
  signal HMC769_v1_0_S00_AXI_inst_n_13 : STD_LOGIC;
  signal HMC769_v1_0_S00_AXI_inst_n_5 : STD_LOGIC;
  signal HMC769_v1_0_S00_AXI_inst_n_6 : STD_LOGIC;
  signal HMC769_v1_0_S00_AXI_inst_n_7 : STD_LOGIC;
  signal HMC769_v1_0_S00_AXI_inst_n_9 : STD_LOGIC;
  signal ip2mb_reg0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal ip2mb_reg1 : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \pll_send_i/bits_send_reg\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal slv_reg0 : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal slv_reg1 : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal slv_reg3 : STD_LOGIC_VECTOR ( 23 to 23 );
  signal slv_reg4 : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal top_PLL_control_i_n_29 : STD_LOGIC;
  signal top_PLL_control_i_n_30 : STD_LOGIC;
begin
HMC769_v1_0_S00_AXI_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HMC769_v1_0_S00_AXI
     port map (
      D(1) => top_PLL_control_i_n_29,
      D(0) => top_PLL_control_i_n_30,
      Q(0) => slv_reg3(23),
      \axi_rdata_reg[23]_0\(23 downto 0) => ip2mb_reg1(23 downto 0),
      bits_send_reg(1 downto 0) => \pll_send_i/bits_send_reg\(1 downto 0),
      ip2mb_reg0(0) => ip2mb_reg0(0),
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(3 downto 0) => s00_axi_araddr(3 downto 0),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arready => s00_axi_arready,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(3 downto 0) => s00_axi_awaddr(3 downto 0),
      s00_axi_awready => s00_axi_awready,
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wready => s00_axi_wready,
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid,
      slv_reg0(1 downto 0) => slv_reg0(1 downto 0),
      \slv_reg1_reg[4]_0\(4 downto 0) => slv_reg1(4 downto 0),
      \slv_reg2_reg[0]_0\ => HMC769_v1_0_S00_AXI_inst_n_6,
      \slv_reg2_reg[1]_0\ => HMC769_v1_0_S00_AXI_inst_n_5,
      \slv_reg3_reg[13]_0\ => HMC769_v1_0_S00_AXI_inst_n_10,
      \slv_reg3_reg[17]_0\ => HMC769_v1_0_S00_AXI_inst_n_9,
      \slv_reg3_reg[1]_0\ => HMC769_v1_0_S00_AXI_inst_n_13,
      \slv_reg3_reg[21]_0\ => HMC769_v1_0_S00_AXI_inst_n_7,
      \slv_reg3_reg[5]_0\ => HMC769_v1_0_S00_AXI_inst_n_12,
      \slv_reg3_reg[9]_0\ => HMC769_v1_0_S00_AXI_inst_n_11,
      \slv_reg4_reg[5]_0\(5 downto 0) => slv_reg4(5 downto 0)
    );
top_PLL_control_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top_PLL_control
     port map (
      ATTEN(5 downto 0) => ATTEN(5 downto 0),
      D(1) => top_PLL_control_i_n_29,
      D(0) => top_PLL_control_i_n_30,
      Q(23 downto 0) => ip2mb_reg1(23 downto 0),
      \atten_reg_reg[5]_0\(5 downto 0) => slv_reg4(5 downto 0),
      \bits_send_reg[1]\(1 downto 0) => \pll_send_i/bits_send_reg\(1 downto 0),
      ip2mb_reg0(0) => ip2mb_reg0(0),
      pll_ld_sdo => pll_ld_sdo,
      pll_mosi => pll_mosi,
      \pll_mosi_reg_i_2__0\ => HMC769_v1_0_S00_AXI_inst_n_7,
      \pll_mosi_reg_i_2__0_0\ => HMC769_v1_0_S00_AXI_inst_n_9,
      \pll_mosi_reg_i_2__0_1\ => HMC769_v1_0_S00_AXI_inst_n_11,
      \pll_mosi_reg_i_2__0_2\ => HMC769_v1_0_S00_AXI_inst_n_10,
      \pll_mosi_reg_i_2__0_3\ => HMC769_v1_0_S00_AXI_inst_n_13,
      \pll_mosi_reg_i_2__0_4\ => HMC769_v1_0_S00_AXI_inst_n_12,
      pll_mosi_reg_i_8(4 downto 0) => slv_reg1(4 downto 0),
      pll_mosi_reg_reg => HMC769_v1_0_S00_AXI_inst_n_5,
      pll_mosi_reg_reg_0 => HMC769_v1_0_S00_AXI_inst_n_6,
      pll_mosi_reg_reg_1(0) => slv_reg3(23),
      pll_sck => pll_sck,
      pll_sen => pll_sen,
      pll_trig => pll_trig,
      s00_axi_aclk => s00_axi_aclk,
      shift_front(15 downto 0) => shift_front(15 downto 0),
      slv_reg0(1 downto 0) => slv_reg0(1 downto 0),
      start_adc_count => start_adc_count
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s00_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s00_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    pll_sen : out STD_LOGIC;
    pll_sck : out STD_LOGIC;
    pll_mosi : out STD_LOGIC;
    pll_ld_sdo : in STD_LOGIC;
    pll_cen : out STD_LOGIC;
    pll_trig : out STD_LOGIC;
    ATTEN : out STD_LOGIC_VECTOR ( 5 downto 0 );
    start_adc_count : out STD_LOGIC;
    shift_front : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_HMC769_0_0,HMC769_v1_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "HMC769_v1_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of s00_axi_aclk : signal is "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of s00_axi_aclk : signal is "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 S00_AXI_RST RST";
  attribute X_INTERFACE_PARAMETER of s00_axi_aresetn : signal is "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axi_arready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY";
  attribute X_INTERFACE_INFO of s00_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID";
  attribute X_INTERFACE_INFO of s00_axi_awready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY";
  attribute X_INTERFACE_INFO of s00_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID";
  attribute X_INTERFACE_INFO of s00_axi_bready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BREADY";
  attribute X_INTERFACE_INFO of s00_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BVALID";
  attribute X_INTERFACE_INFO of s00_axi_rready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RREADY";
  attribute X_INTERFACE_PARAMETER of s00_axi_rready : signal is "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 16, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 6, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RVALID";
  attribute X_INTERFACE_INFO of s00_axi_wready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WREADY";
  attribute X_INTERFACE_INFO of s00_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WVALID";
  attribute X_INTERFACE_INFO of s00_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR";
  attribute X_INTERFACE_INFO of s00_axi_arprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT";
  attribute X_INTERFACE_INFO of s00_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR";
  attribute X_INTERFACE_INFO of s00_axi_awprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT";
  attribute X_INTERFACE_INFO of s00_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BRESP";
  attribute X_INTERFACE_INFO of s00_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RDATA";
  attribute X_INTERFACE_INFO of s00_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RRESP";
  attribute X_INTERFACE_INFO of s00_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WDATA";
  attribute X_INTERFACE_INFO of s00_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB";
begin
  pll_cen <= \<const0>\;
  s00_axi_bresp(1) <= \<const0>\;
  s00_axi_bresp(0) <= \<const0>\;
  s00_axi_rresp(1) <= \<const0>\;
  s00_axi_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HMC769_v1_0
     port map (
      ATTEN(5 downto 0) => ATTEN(5 downto 0),
      pll_ld_sdo => pll_ld_sdo,
      pll_mosi => pll_mosi,
      pll_sck => pll_sck,
      pll_sen => pll_sen,
      pll_trig => pll_trig,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(3 downto 0) => s00_axi_araddr(5 downto 2),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arready => s00_axi_arready,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(3 downto 0) => s00_axi_awaddr(5 downto 2),
      s00_axi_awready => s00_axi_awready,
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wready => s00_axi_wready,
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid,
      shift_front(15 downto 0) => shift_front(15 downto 0),
      start_adc_count => start_adc_count
    );
end STRUCTURE;
