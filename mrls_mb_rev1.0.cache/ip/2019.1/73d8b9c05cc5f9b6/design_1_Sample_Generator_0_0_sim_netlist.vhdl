-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Fri Mar 19 16:09:46 2021
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_Sample_Generator_0_0_sim_netlist.vhdl
-- Design      : design_1_Sample_Generator_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Sample_Generator_v3_0 is
  port (
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC;
    data_clk : in STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    data_in_IF1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    data_in_IF2 : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Sample_Generator_v3_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Sample_Generator_v3_0 is
  signal cnt100_reg : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal cnt_10 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \cnt_100_carry__0_n_0\ : STD_LOGIC;
  signal \cnt_100_carry__0_n_1\ : STD_LOGIC;
  signal \cnt_100_carry__0_n_2\ : STD_LOGIC;
  signal \cnt_100_carry__0_n_3\ : STD_LOGIC;
  signal \cnt_100_carry__1_n_0\ : STD_LOGIC;
  signal \cnt_100_carry__1_n_1\ : STD_LOGIC;
  signal \cnt_100_carry__1_n_2\ : STD_LOGIC;
  signal \cnt_100_carry__1_n_3\ : STD_LOGIC;
  signal \cnt_100_carry__2_n_2\ : STD_LOGIC;
  signal \cnt_100_carry__2_n_3\ : STD_LOGIC;
  signal cnt_100_carry_n_0 : STD_LOGIC;
  signal cnt_100_carry_n_1 : STD_LOGIC;
  signal cnt_100_carry_n_2 : STD_LOGIC;
  signal cnt_100_carry_n_3 : STD_LOGIC;
  signal \cnt_10[15]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_10[15]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_10[15]_i_4_n_0\ : STD_LOGIC;
  signal \cnt_10_reg_n_0_[0]\ : STD_LOGIC;
  signal \cnt_10_reg_n_0_[10]\ : STD_LOGIC;
  signal \cnt_10_reg_n_0_[11]\ : STD_LOGIC;
  signal \cnt_10_reg_n_0_[12]\ : STD_LOGIC;
  signal \cnt_10_reg_n_0_[13]\ : STD_LOGIC;
  signal \cnt_10_reg_n_0_[14]\ : STD_LOGIC;
  signal \cnt_10_reg_n_0_[15]\ : STD_LOGIC;
  signal \cnt_10_reg_n_0_[1]\ : STD_LOGIC;
  signal \cnt_10_reg_n_0_[2]\ : STD_LOGIC;
  signal \cnt_10_reg_n_0_[3]\ : STD_LOGIC;
  signal \cnt_10_reg_n_0_[4]\ : STD_LOGIC;
  signal \cnt_10_reg_n_0_[5]\ : STD_LOGIC;
  signal \cnt_10_reg_n_0_[6]\ : STD_LOGIC;
  signal \cnt_10_reg_n_0_[7]\ : STD_LOGIC;
  signal \cnt_10_reg_n_0_[8]\ : STD_LOGIC;
  signal \cnt_10_reg_n_0_[9]\ : STD_LOGIC;
  signal data0 : STD_LOGIC_VECTOR ( 15 downto 1 );
  signal m00_axis_tlast_r0 : STD_LOGIC;
  signal m00_axis_tlast_r_i_2_n_0 : STD_LOGIC;
  signal m00_axis_tlast_r_i_3_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_r0 : STD_LOGIC;
  signal m00_axis_tvalid_r_i_2_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_r_i_3_n_0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal \p_0_in__0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal reset_inner : STD_LOGIC;
  signal \NLW_cnt_100_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_cnt_100_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \cnt100[0]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \cnt100[1]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \cnt100[2]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \cnt100[3]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of m00_axis_tlast_r_i_2 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of m00_axis_tvalid_r_i_2 : label is "soft_lutpair0";
begin
\cnt100[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt100_reg(0),
      O => \p_0_in__0\(0)
    );
\cnt100[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cnt100_reg(0),
      I1 => cnt100_reg(1),
      O => \p_0_in__0\(1)
    );
\cnt100[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => cnt100_reg(1),
      I1 => cnt100_reg(0),
      I2 => cnt100_reg(2),
      O => \p_0_in__0\(2)
    );
\cnt100[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => cnt100_reg(2),
      I1 => cnt100_reg(0),
      I2 => cnt100_reg(1),
      I3 => cnt100_reg(3),
      O => \p_0_in__0\(3)
    );
\cnt100_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \p_0_in__0\(0),
      Q => cnt100_reg(0),
      R => data_clk
    );
\cnt100_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \p_0_in__0\(1),
      Q => cnt100_reg(1),
      R => data_clk
    );
\cnt100_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \p_0_in__0\(2),
      Q => cnt100_reg(2),
      R => data_clk
    );
\cnt100_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \p_0_in__0\(3),
      Q => cnt100_reg(3),
      R => data_clk
    );
cnt_100_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => cnt_100_carry_n_0,
      CO(2) => cnt_100_carry_n_1,
      CO(1) => cnt_100_carry_n_2,
      CO(0) => cnt_100_carry_n_3,
      CYINIT => \cnt_10_reg_n_0_[0]\,
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(4 downto 1),
      S(3) => \cnt_10_reg_n_0_[4]\,
      S(2) => \cnt_10_reg_n_0_[3]\,
      S(1) => \cnt_10_reg_n_0_[2]\,
      S(0) => \cnt_10_reg_n_0_[1]\
    );
\cnt_100_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => cnt_100_carry_n_0,
      CO(3) => \cnt_100_carry__0_n_0\,
      CO(2) => \cnt_100_carry__0_n_1\,
      CO(1) => \cnt_100_carry__0_n_2\,
      CO(0) => \cnt_100_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(8 downto 5),
      S(3) => \cnt_10_reg_n_0_[8]\,
      S(2) => \cnt_10_reg_n_0_[7]\,
      S(1) => \cnt_10_reg_n_0_[6]\,
      S(0) => \cnt_10_reg_n_0_[5]\
    );
\cnt_100_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_100_carry__0_n_0\,
      CO(3) => \cnt_100_carry__1_n_0\,
      CO(2) => \cnt_100_carry__1_n_1\,
      CO(1) => \cnt_100_carry__1_n_2\,
      CO(0) => \cnt_100_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(12 downto 9),
      S(3) => \cnt_10_reg_n_0_[12]\,
      S(2) => \cnt_10_reg_n_0_[11]\,
      S(1) => \cnt_10_reg_n_0_[10]\,
      S(0) => \cnt_10_reg_n_0_[9]\
    );
\cnt_100_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_100_carry__1_n_0\,
      CO(3 downto 2) => \NLW_cnt_100_carry__2_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \cnt_100_carry__2_n_2\,
      CO(0) => \cnt_100_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_cnt_100_carry__2_O_UNCONNECTED\(3),
      O(2 downto 0) => data0(15 downto 13),
      S(3) => '0',
      S(2) => \cnt_10_reg_n_0_[15]\,
      S(1) => \cnt_10_reg_n_0_[14]\,
      S(0) => \cnt_10_reg_n_0_[13]\
    );
\cnt_10[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => m00_axis_tlast_r_i_3_n_0,
      I1 => \cnt_10_reg_n_0_[0]\,
      O => cnt_10(0)
    );
\cnt_10[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFDFFFF00000000"
    )
        port map (
      I0 => \cnt_10_reg_n_0_[13]\,
      I1 => \cnt_10[15]_i_2_n_0\,
      I2 => \cnt_10[15]_i_3_n_0\,
      I3 => \cnt_10[15]_i_4_n_0\,
      I4 => \cnt_10_reg_n_0_[0]\,
      I5 => data0(10),
      O => cnt_10(10)
    );
\cnt_10[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFDFFFF00000000"
    )
        port map (
      I0 => \cnt_10_reg_n_0_[13]\,
      I1 => \cnt_10[15]_i_2_n_0\,
      I2 => \cnt_10[15]_i_3_n_0\,
      I3 => \cnt_10[15]_i_4_n_0\,
      I4 => \cnt_10_reg_n_0_[0]\,
      I5 => data0(11),
      O => cnt_10(11)
    );
\cnt_10[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFDFFFF00000000"
    )
        port map (
      I0 => \cnt_10_reg_n_0_[13]\,
      I1 => \cnt_10[15]_i_2_n_0\,
      I2 => \cnt_10[15]_i_3_n_0\,
      I3 => \cnt_10[15]_i_4_n_0\,
      I4 => \cnt_10_reg_n_0_[0]\,
      I5 => data0(12),
      O => cnt_10(12)
    );
\cnt_10[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00020000"
    )
        port map (
      I0 => \cnt_10_reg_n_0_[13]\,
      I1 => \cnt_10[15]_i_2_n_0\,
      I2 => \cnt_10[15]_i_3_n_0\,
      I3 => \cnt_10[15]_i_4_n_0\,
      I4 => \cnt_10_reg_n_0_[0]\,
      I5 => data0(13),
      O => cnt_10(13)
    );
\cnt_10[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFDFFFF00000000"
    )
        port map (
      I0 => \cnt_10_reg_n_0_[13]\,
      I1 => \cnt_10[15]_i_2_n_0\,
      I2 => \cnt_10[15]_i_3_n_0\,
      I3 => \cnt_10[15]_i_4_n_0\,
      I4 => \cnt_10_reg_n_0_[0]\,
      I5 => data0(14),
      O => cnt_10(14)
    );
\cnt_10[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFDFFFF00000000"
    )
        port map (
      I0 => \cnt_10_reg_n_0_[13]\,
      I1 => \cnt_10[15]_i_2_n_0\,
      I2 => \cnt_10[15]_i_3_n_0\,
      I3 => \cnt_10[15]_i_4_n_0\,
      I4 => \cnt_10_reg_n_0_[0]\,
      I5 => data0(15),
      O => cnt_10(15)
    );
\cnt_10[15]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \cnt_10_reg_n_0_[8]\,
      I1 => \cnt_10_reg_n_0_[7]\,
      I2 => \cnt_10_reg_n_0_[10]\,
      I3 => \cnt_10_reg_n_0_[9]\,
      O => \cnt_10[15]_i_2_n_0\
    );
\cnt_10[15]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \cnt_10_reg_n_0_[4]\,
      I1 => \cnt_10_reg_n_0_[3]\,
      I2 => \cnt_10_reg_n_0_[6]\,
      I3 => \cnt_10_reg_n_0_[5]\,
      O => \cnt_10[15]_i_3_n_0\
    );
\cnt_10[15]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \cnt_10_reg_n_0_[14]\,
      I1 => \cnt_10_reg_n_0_[15]\,
      I2 => \cnt_10_reg_n_0_[11]\,
      I3 => \cnt_10_reg_n_0_[12]\,
      I4 => \cnt_10_reg_n_0_[2]\,
      I5 => \cnt_10_reg_n_0_[1]\,
      O => \cnt_10[15]_i_4_n_0\
    );
\cnt_10[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFDFFFF00000000"
    )
        port map (
      I0 => \cnt_10_reg_n_0_[13]\,
      I1 => \cnt_10[15]_i_2_n_0\,
      I2 => \cnt_10[15]_i_3_n_0\,
      I3 => \cnt_10[15]_i_4_n_0\,
      I4 => \cnt_10_reg_n_0_[0]\,
      I5 => data0(1),
      O => cnt_10(1)
    );
\cnt_10[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFDFFFF00000000"
    )
        port map (
      I0 => \cnt_10_reg_n_0_[13]\,
      I1 => \cnt_10[15]_i_2_n_0\,
      I2 => \cnt_10[15]_i_3_n_0\,
      I3 => \cnt_10[15]_i_4_n_0\,
      I4 => \cnt_10_reg_n_0_[0]\,
      I5 => data0(2),
      O => cnt_10(2)
    );
\cnt_10[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFDFFFF00000000"
    )
        port map (
      I0 => \cnt_10_reg_n_0_[13]\,
      I1 => \cnt_10[15]_i_2_n_0\,
      I2 => \cnt_10[15]_i_3_n_0\,
      I3 => \cnt_10[15]_i_4_n_0\,
      I4 => \cnt_10_reg_n_0_[0]\,
      I5 => data0(3),
      O => cnt_10(3)
    );
\cnt_10[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFDFFFF00000000"
    )
        port map (
      I0 => \cnt_10_reg_n_0_[13]\,
      I1 => \cnt_10[15]_i_2_n_0\,
      I2 => \cnt_10[15]_i_3_n_0\,
      I3 => \cnt_10[15]_i_4_n_0\,
      I4 => \cnt_10_reg_n_0_[0]\,
      I5 => data0(4),
      O => cnt_10(4)
    );
\cnt_10[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFDFFFF00000000"
    )
        port map (
      I0 => \cnt_10_reg_n_0_[13]\,
      I1 => \cnt_10[15]_i_2_n_0\,
      I2 => \cnt_10[15]_i_3_n_0\,
      I3 => \cnt_10[15]_i_4_n_0\,
      I4 => \cnt_10_reg_n_0_[0]\,
      I5 => data0(5),
      O => cnt_10(5)
    );
\cnt_10[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFDFFFF00000000"
    )
        port map (
      I0 => \cnt_10_reg_n_0_[13]\,
      I1 => \cnt_10[15]_i_2_n_0\,
      I2 => \cnt_10[15]_i_3_n_0\,
      I3 => \cnt_10[15]_i_4_n_0\,
      I4 => \cnt_10_reg_n_0_[0]\,
      I5 => data0(6),
      O => cnt_10(6)
    );
\cnt_10[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFDFFFF00000000"
    )
        port map (
      I0 => \cnt_10_reg_n_0_[13]\,
      I1 => \cnt_10[15]_i_2_n_0\,
      I2 => \cnt_10[15]_i_3_n_0\,
      I3 => \cnt_10[15]_i_4_n_0\,
      I4 => \cnt_10_reg_n_0_[0]\,
      I5 => data0(7),
      O => cnt_10(7)
    );
\cnt_10[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFDFFFF00000000"
    )
        port map (
      I0 => \cnt_10_reg_n_0_[13]\,
      I1 => \cnt_10[15]_i_2_n_0\,
      I2 => \cnt_10[15]_i_3_n_0\,
      I3 => \cnt_10[15]_i_4_n_0\,
      I4 => \cnt_10_reg_n_0_[0]\,
      I5 => data0(8),
      O => cnt_10(8)
    );
\cnt_10[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFDFFFF00000000"
    )
        port map (
      I0 => \cnt_10_reg_n_0_[13]\,
      I1 => \cnt_10[15]_i_2_n_0\,
      I2 => \cnt_10[15]_i_3_n_0\,
      I3 => \cnt_10[15]_i_4_n_0\,
      I4 => \cnt_10_reg_n_0_[0]\,
      I5 => data0(9),
      O => cnt_10(9)
    );
\cnt_10_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_clk,
      CE => '1',
      D => cnt_10(0),
      Q => \cnt_10_reg_n_0_[0]\,
      R => p_0_in
    );
\cnt_10_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_clk,
      CE => '1',
      D => cnt_10(10),
      Q => \cnt_10_reg_n_0_[10]\,
      R => p_0_in
    );
\cnt_10_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_clk,
      CE => '1',
      D => cnt_10(11),
      Q => \cnt_10_reg_n_0_[11]\,
      R => p_0_in
    );
\cnt_10_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_clk,
      CE => '1',
      D => cnt_10(12),
      Q => \cnt_10_reg_n_0_[12]\,
      R => p_0_in
    );
\cnt_10_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_clk,
      CE => '1',
      D => cnt_10(13),
      Q => \cnt_10_reg_n_0_[13]\,
      R => p_0_in
    );
\cnt_10_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_clk,
      CE => '1',
      D => cnt_10(14),
      Q => \cnt_10_reg_n_0_[14]\,
      R => p_0_in
    );
\cnt_10_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_clk,
      CE => '1',
      D => cnt_10(15),
      Q => \cnt_10_reg_n_0_[15]\,
      R => p_0_in
    );
\cnt_10_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_clk,
      CE => '1',
      D => cnt_10(1),
      Q => \cnt_10_reg_n_0_[1]\,
      R => p_0_in
    );
\cnt_10_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_clk,
      CE => '1',
      D => cnt_10(2),
      Q => \cnt_10_reg_n_0_[2]\,
      R => p_0_in
    );
\cnt_10_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_clk,
      CE => '1',
      D => cnt_10(3),
      Q => \cnt_10_reg_n_0_[3]\,
      R => p_0_in
    );
\cnt_10_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_clk,
      CE => '1',
      D => cnt_10(4),
      Q => \cnt_10_reg_n_0_[4]\,
      R => p_0_in
    );
\cnt_10_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_clk,
      CE => '1',
      D => cnt_10(5),
      Q => \cnt_10_reg_n_0_[5]\,
      R => p_0_in
    );
\cnt_10_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_clk,
      CE => '1',
      D => cnt_10(6),
      Q => \cnt_10_reg_n_0_[6]\,
      R => p_0_in
    );
\cnt_10_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_clk,
      CE => '1',
      D => cnt_10(7),
      Q => \cnt_10_reg_n_0_[7]\,
      R => p_0_in
    );
\cnt_10_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_clk,
      CE => '1',
      D => cnt_10(8),
      Q => \cnt_10_reg_n_0_[8]\,
      R => p_0_in
    );
\cnt_10_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_clk,
      CE => '1',
      D => cnt_10(9),
      Q => \cnt_10_reg_n_0_[9]\,
      R => p_0_in
    );
\data_sum_1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF1(0),
      Q => m00_axis_tdata(0),
      R => p_0_in
    );
\data_sum_1_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF1(10),
      Q => m00_axis_tdata(10),
      R => p_0_in
    );
\data_sum_1_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF1(11),
      Q => m00_axis_tdata(11),
      R => p_0_in
    );
\data_sum_1_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF1(12),
      Q => m00_axis_tdata(12),
      R => p_0_in
    );
\data_sum_1_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF1(13),
      Q => m00_axis_tdata(13),
      R => p_0_in
    );
\data_sum_1_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF1(14),
      Q => m00_axis_tdata(14),
      R => p_0_in
    );
\data_sum_1_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF1(15),
      Q => m00_axis_tdata(15),
      R => p_0_in
    );
\data_sum_1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF1(1),
      Q => m00_axis_tdata(1),
      R => p_0_in
    );
\data_sum_1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF1(2),
      Q => m00_axis_tdata(2),
      R => p_0_in
    );
\data_sum_1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF1(3),
      Q => m00_axis_tdata(3),
      R => p_0_in
    );
\data_sum_1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF1(4),
      Q => m00_axis_tdata(4),
      R => p_0_in
    );
\data_sum_1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF1(5),
      Q => m00_axis_tdata(5),
      R => p_0_in
    );
\data_sum_1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF1(6),
      Q => m00_axis_tdata(6),
      R => p_0_in
    );
\data_sum_1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF1(7),
      Q => m00_axis_tdata(7),
      R => p_0_in
    );
\data_sum_1_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF1(8),
      Q => m00_axis_tdata(8),
      R => p_0_in
    );
\data_sum_1_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF1(9),
      Q => m00_axis_tdata(9),
      R => p_0_in
    );
\data_sum_2[16]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => reset_inner,
      O => p_0_in
    );
\data_sum_2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF2(0),
      Q => m00_axis_tdata(16),
      R => p_0_in
    );
\data_sum_2_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF2(10),
      Q => m00_axis_tdata(26),
      R => p_0_in
    );
\data_sum_2_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF2(11),
      Q => m00_axis_tdata(27),
      R => p_0_in
    );
\data_sum_2_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF2(12),
      Q => m00_axis_tdata(28),
      R => p_0_in
    );
\data_sum_2_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF2(13),
      Q => m00_axis_tdata(29),
      R => p_0_in
    );
\data_sum_2_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF2(14),
      Q => m00_axis_tdata(30),
      R => p_0_in
    );
\data_sum_2_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF2(15),
      Q => m00_axis_tdata(31),
      R => p_0_in
    );
\data_sum_2_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF2(1),
      Q => m00_axis_tdata(17),
      R => p_0_in
    );
\data_sum_2_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF2(2),
      Q => m00_axis_tdata(18),
      R => p_0_in
    );
\data_sum_2_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF2(3),
      Q => m00_axis_tdata(19),
      R => p_0_in
    );
\data_sum_2_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF2(4),
      Q => m00_axis_tdata(20),
      R => p_0_in
    );
\data_sum_2_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF2(5),
      Q => m00_axis_tdata(21),
      R => p_0_in
    );
\data_sum_2_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF2(6),
      Q => m00_axis_tdata(22),
      R => p_0_in
    );
\data_sum_2_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF2(7),
      Q => m00_axis_tdata(23),
      R => p_0_in
    );
\data_sum_2_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF2(8),
      Q => m00_axis_tdata(24),
      R => p_0_in
    );
\data_sum_2_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => data_in_IF2(9),
      Q => m00_axis_tdata(25),
      R => p_0_in
    );
m00_axis_tlast_r_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000010000"
    )
        port map (
      I0 => m00_axis_tlast_r_i_2_n_0,
      I1 => cnt100_reg(3),
      I2 => cnt100_reg(2),
      I3 => \cnt_10_reg_n_0_[0]\,
      I4 => reset_inner,
      I5 => m00_axis_tlast_r_i_3_n_0,
      O => m00_axis_tlast_r0
    );
m00_axis_tlast_r_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => cnt100_reg(0),
      I1 => cnt100_reg(1),
      O => m00_axis_tlast_r_i_2_n_0
    );
m00_axis_tlast_r_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFF"
    )
        port map (
      I0 => \cnt_10[15]_i_4_n_0\,
      I1 => \cnt_10[15]_i_3_n_0\,
      I2 => \cnt_10[15]_i_2_n_0\,
      I3 => \cnt_10_reg_n_0_[13]\,
      O => m00_axis_tlast_r_i_3_n_0
    );
m00_axis_tlast_r_reg: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => m00_axis_tlast_r0,
      Q => m00_axis_tlast,
      R => '0'
    );
m00_axis_tvalid_r_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000200020002AAAA"
    )
        port map (
      I0 => m00_axis_tvalid_r_i_2_n_0,
      I1 => \cnt_10_reg_n_0_[15]\,
      I2 => \cnt_10_reg_n_0_[14]\,
      I3 => \cnt_10_reg_n_0_[13]\,
      I4 => m00_axis_tvalid_r_i_3_n_0,
      I5 => \cnt_10_reg_n_0_[0]\,
      O => m00_axis_tvalid_r0
    );
m00_axis_tvalid_r_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04000000"
    )
        port map (
      I0 => cnt100_reg(2),
      I1 => reset_inner,
      I2 => cnt100_reg(3),
      I3 => cnt100_reg(1),
      I4 => cnt100_reg(0),
      O => m00_axis_tvalid_r_i_2_n_0
    );
m00_axis_tvalid_r_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \cnt_10[15]_i_2_n_0\,
      I1 => \cnt_10_reg_n_0_[4]\,
      I2 => \cnt_10_reg_n_0_[3]\,
      I3 => \cnt_10_reg_n_0_[6]\,
      I4 => \cnt_10_reg_n_0_[5]\,
      I5 => \cnt_10[15]_i_4_n_0\,
      O => m00_axis_tvalid_r_i_3_n_0
    );
m00_axis_tvalid_r_reg: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => m00_axis_tvalid_r0,
      Q => m00_axis_tvalid,
      R => '0'
    );
reset_inner_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => m00_axis_aresetn,
      Q => reset_inner,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    data_clk : in STD_LOGIC;
    clk_5MHz : in STD_LOGIC;
    data_in_IF1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    data_in_IF2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 47 downto 0 );
    m00_axis_tstrb : out STD_LOGIC_VECTOR ( 5 downto 0 );
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tready : in STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    m00_axis_config_tvalid : out STD_LOGIC;
    m00_axis_config_tdata : out STD_LOGIC_VECTOR ( 47 downto 0 );
    m00_axis_config_tready : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_Sample_Generator_0_0,Sample_Generator_v3_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "Sample_Generator_v3_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  signal \<const1>\ : STD_LOGIC;
  signal \^m00_axis_tdata\ : STD_LOGIC_VECTOR ( 39 downto 0 );
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of m00_axis_aclk : signal is "xilinx.com:signal:clock:1.0 m00_axis_aclk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of m00_axis_aclk : signal is "XIL_INTERFACENAME m00_axis_aclk, ASSOCIATED_BUSIF M00_AXIS, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_aresetn : signal is "xilinx.com:signal:reset:1.0 m00_axis_aresetn RST";
  attribute X_INTERFACE_PARAMETER of m00_axis_aresetn : signal is "XIL_INTERFACENAME m00_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_config_tready : signal is "xilinx.com:interface:axis:1.0 M_AXIS_CONFIG TREADY";
  attribute X_INTERFACE_PARAMETER of m00_axis_config_tready : signal is "XIL_INTERFACENAME M_AXIS_CONFIG, TDATA_NUM_BYTES 6, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_config_tvalid : signal is "xilinx.com:interface:axis:1.0 M_AXIS_CONFIG TVALID";
  attribute X_INTERFACE_INFO of m00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TLAST";
  attribute X_INTERFACE_INFO of m00_axis_tready : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TREADY";
  attribute X_INTERFACE_PARAMETER of m00_axis_tready : signal is "XIL_INTERFACENAME M00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 6, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TVALID";
  attribute X_INTERFACE_INFO of m00_axis_config_tdata : signal is "xilinx.com:interface:axis:1.0 M_AXIS_CONFIG TDATA";
  attribute X_INTERFACE_INFO of m00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TDATA";
  attribute X_INTERFACE_INFO of m00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TSTRB";
begin
  m00_axis_config_tdata(47) <= \<const0>\;
  m00_axis_config_tdata(46) <= \<const0>\;
  m00_axis_config_tdata(45) <= \<const0>\;
  m00_axis_config_tdata(44) <= \<const0>\;
  m00_axis_config_tdata(43) <= \<const0>\;
  m00_axis_config_tdata(42) <= \<const0>\;
  m00_axis_config_tdata(41) <= \<const0>\;
  m00_axis_config_tdata(40) <= \<const0>\;
  m00_axis_config_tdata(39) <= \<const0>\;
  m00_axis_config_tdata(38) <= \<const0>\;
  m00_axis_config_tdata(37) <= \<const0>\;
  m00_axis_config_tdata(36) <= \<const0>\;
  m00_axis_config_tdata(35) <= \<const0>\;
  m00_axis_config_tdata(34) <= \<const0>\;
  m00_axis_config_tdata(33) <= \<const0>\;
  m00_axis_config_tdata(32) <= \<const0>\;
  m00_axis_config_tdata(31) <= \<const0>\;
  m00_axis_config_tdata(30) <= \<const0>\;
  m00_axis_config_tdata(29) <= \<const0>\;
  m00_axis_config_tdata(28) <= \<const0>\;
  m00_axis_config_tdata(27) <= \<const0>\;
  m00_axis_config_tdata(26) <= \<const0>\;
  m00_axis_config_tdata(25) <= \<const0>\;
  m00_axis_config_tdata(24) <= \<const0>\;
  m00_axis_config_tdata(23) <= \<const0>\;
  m00_axis_config_tdata(22) <= \<const0>\;
  m00_axis_config_tdata(21) <= \<const0>\;
  m00_axis_config_tdata(20) <= \<const0>\;
  m00_axis_config_tdata(19) <= \<const0>\;
  m00_axis_config_tdata(18) <= \<const0>\;
  m00_axis_config_tdata(17) <= \<const0>\;
  m00_axis_config_tdata(16) <= \<const0>\;
  m00_axis_config_tdata(15) <= \<const0>\;
  m00_axis_config_tdata(14) <= \<const0>\;
  m00_axis_config_tdata(13) <= \<const0>\;
  m00_axis_config_tdata(12) <= \<const0>\;
  m00_axis_config_tdata(11) <= \<const0>\;
  m00_axis_config_tdata(10) <= \<const0>\;
  m00_axis_config_tdata(9) <= \<const0>\;
  m00_axis_config_tdata(8) <= \<const0>\;
  m00_axis_config_tdata(7) <= \<const0>\;
  m00_axis_config_tdata(6) <= \<const0>\;
  m00_axis_config_tdata(5) <= \<const0>\;
  m00_axis_config_tdata(4) <= \<const0>\;
  m00_axis_config_tdata(3) <= \<const0>\;
  m00_axis_config_tdata(2) <= \<const0>\;
  m00_axis_config_tdata(1) <= \<const0>\;
  m00_axis_config_tdata(0) <= \<const0>\;
  m00_axis_config_tvalid <= \<const0>\;
  m00_axis_tdata(47) <= \<const0>\;
  m00_axis_tdata(46) <= \<const0>\;
  m00_axis_tdata(45) <= \<const0>\;
  m00_axis_tdata(44) <= \<const0>\;
  m00_axis_tdata(43) <= \<const0>\;
  m00_axis_tdata(42) <= \<const0>\;
  m00_axis_tdata(41) <= \<const0>\;
  m00_axis_tdata(40) <= \^m00_axis_tdata\(39);
  m00_axis_tdata(39 downto 24) <= \^m00_axis_tdata\(39 downto 24);
  m00_axis_tdata(23) <= \<const0>\;
  m00_axis_tdata(22) <= \<const0>\;
  m00_axis_tdata(21) <= \<const0>\;
  m00_axis_tdata(20) <= \<const0>\;
  m00_axis_tdata(19) <= \<const0>\;
  m00_axis_tdata(18) <= \<const0>\;
  m00_axis_tdata(17) <= \<const0>\;
  m00_axis_tdata(16) <= \^m00_axis_tdata\(15);
  m00_axis_tdata(15 downto 0) <= \^m00_axis_tdata\(15 downto 0);
  m00_axis_tstrb(5) <= \<const0>\;
  m00_axis_tstrb(4) <= \<const0>\;
  m00_axis_tstrb(3) <= \<const1>\;
  m00_axis_tstrb(2) <= \<const1>\;
  m00_axis_tstrb(1) <= \<const1>\;
  m00_axis_tstrb(0) <= \<const1>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Sample_Generator_v3_0
     port map (
      data_clk => data_clk,
      data_in_IF1(15 downto 0) => data_in_IF1(15 downto 0),
      data_in_IF2(15 downto 0) => data_in_IF2(15 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      m00_axis_aresetn => m00_axis_aresetn,
      m00_axis_tdata(31 downto 16) => \^m00_axis_tdata\(39 downto 24),
      m00_axis_tdata(15 downto 0) => \^m00_axis_tdata\(15 downto 0),
      m00_axis_tlast => m00_axis_tlast,
      m00_axis_tvalid => m00_axis_tvalid
    );
end STRUCTURE;
